export const environment = {
  production: true,

  websiteRoot: JSON.parse((<HTMLInputElement>document.getElementById('__ClientContext')).value).websiteRoot,
  imageUrl: 'areas/ng/iMISAngular_Orders/assets/image',
  baseUrl: JSON.parse((<HTMLInputElement>document.getElementById('__ClientContext')).value).baseUrl,
  token: (<HTMLInputElement>document.getElementById('__RequestVerificationToken')).value,
  ApiBaseURL:  (<HTMLInputElement>document.getElementById('apiUrl')).value,
  CurrentUserName: (<HTMLInputElement>document.getElementById('ctl01_AccountArea_PartyName')).innerText
};
