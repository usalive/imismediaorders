export const environment = {
  production: false,

  websiteRoot: '../',
  imageUrl: 'assets/image',
  baseUrl: '/',
  token: '',
 
  ApiBaseURL:  (<HTMLInputElement>document.getElementById('apiUrl')).value,
  CurrentUserName: 'localuser'
};
