import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import {Routes,RouterModule} from '@angular/router';

// External Module
import { DataTablesModule } from '../../node_modules/angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { CookieService } from 'ngx-cookie-service';

// Component
import { AppComponent } from './app.component';
import { AdjustmentComponent } from './pages/components/adjustment/adjustment.component';
import { DashboardAdvertisersComponent } from './pages/components/dashboard-advertisers/dashboard-advertisers.component';
import { DashboardOrdersComponent } from './pages/components/dashboard-orders/dashboard-orders.component';
import { MediaScheduleNewComponent } from './pages/components/mediaScheduleNew/mediaScheduleNew.component';
import { MissingMaterialsComponent } from './pages/components/MissingMaterials/MissingMaterials.component';
import { OrdersComponent } from './pages/components/orders/orders.component';
import { OrderTabComponent } from './pages/components/orderTab/orderTab.component';
import { PostOrdersComponent } from './pages/components/PostOrders/PostOrders.component';
import { ProductionComponent } from './pages/components/production/production.component';
import { GlobalClass } from './pages/GlobalClass';
import { NumberDirective } from '../app/pages/directives/numbers-only.directive';
import { DecimalNumberDirective } from '../app/pages/directives/number-decimal.directive';
import { SafePipe } from '../app/pages/directives/safe.pipe';

// services
import { AdjustmentService } from './pages/services/adjustment.service';
import { DashboardTabService } from './pages/services/dashboard-tab.service';
import { DashboardAdvertiserService } from './pages/services/dashboardAdvertisers.service';
import { DashboardOrderService } from './pages/services/dashboardOrders.service';
import { MediaScheduleService } from './pages/services/mediaSchedule.service';
import { MissingMaterialsService } from './pages/services/MissingMaterials.service';
import { OrdersService } from './pages/services/orders.service';
import { OrderTabService } from './pages/services/orderTab.service';
import { PostOrdersService } from './pages/services/PostOrders.service';
import { ProductionService } from './pages/services/production.service';

const appRoutes:Routes=[
  {path:'orderDashboard',component:DashboardOrdersComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardAdvertisersComponent,
    DashboardOrdersComponent,
    AdjustmentComponent,
    MediaScheduleNewComponent,
    MissingMaterialsComponent,
    OrdersComponent,
    OrderTabComponent,
    PostOrdersComponent,
    ProductionComponent,
    NumberDirective,
    SafePipe,
    DecimalNumberDirective,
    
  ],
  imports: [
    BrowserModule, NgxPaginationModule,
    CommonModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    DataTablesModule,
    NgSelectModule,
    PDFExportModule,
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot({
      preventDuplicates: true,
      timeOut: 5000,
      closeButton: true,
      positionClass: 'toast-top-right',
      progressBar: true,
      enableHtml: true
    }),
    //RouterModule.forRoot(appRoutes)
  ],
  providers: [
    GlobalClass,
    AdjustmentService,
    DashboardTabService,
    DashboardAdvertiserService,
    DashboardOrderService,
    MediaScheduleService,
    MissingMaterialsService,
    OrdersService,
    OrderTabService,
    PostOrdersService,
    ProductionService,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
