import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as alasql from 'alasql';
import { GlobalClass } from '../../GlobalClass';
import { MissingMaterialsService } from '../../services/MissingMaterials.service';
import { AppComponent } from '../../../app.component';
import { environment } from '../../../../environments/environment';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import 'select2';
declare const $: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'asi-MissingMaterials',
  templateUrl: './MissingMaterials.component.html',
  styleUrls: ['./MissingMaterials.component.css']
})
export class MissingMaterialsComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  missingMaterialDatatableTrigger: Subject<any> = new Subject();

  missingMaterialDatatable: DataTables.Settings = {};

  isMissingMaterial_DataLoad_FirstTime = true;

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  offset = 0;
  hasNext = true;
  imgurl = '';
  apiBaseURL = '';

  obj: any = {};

  loadTime = 'onLoadTime';

  partyAndOrganizationData = [];
  mediaOrderPartyData=[];
  advertisers: any = [];
  agencies = [];
  billingToContacts = [];
  
  missingMaterial: any = {};
  missingMaterials = [];
  mediaAssets = [];
  issueDates: any = [];
  adTypes = [];
  POadvertisers = [];

  isShowDefaultTable = false;
  isShowPagination = false;
  dataLoading = true;

  selectedMediaAsset = '';
  selectedIssueDate = '';
  selectedAdType = '';
  selectedAdvertiser = null;
  changeDropdown = 0;
  productionStatusId=0;
  isShowInvoiceDate = false;
  postData = {};

  mediaAssetSelect = {
    multiple: false,
    formatSearching: 'Searching the media asset...',
    formatNoMatches: 'No media asset found'
  };

  issueDateSelect = {
    multiple: false,
    formatSearching: 'Searching the issue date...',
    formatNoMatches: 'No issue date found'
  };
  contacts: any;
  isLoaded: boolean;
  bootbox: any;

  frmMissingMaterial: FormGroup;

  constructor(private missingMaterialsService: MissingMaterialsService,
    private toastr: ToastrService,
    private globalClass: GlobalClass,
    private appComponent: AppComponent,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    try {
      this.websiteRoot = environment.websiteRoot;
      this.frmMissingMaterial = this.formBuilder.group({
        'MediaAsset': [null],
        'IssueDate': [null]
      });
    } catch (error) {
      console.log(error);
    }
    this.missingMaterial = {};
    this.missingMaterial.MediaAssetId = '';
    this.missingMaterial.IssueDateId = '';
    this.missingMaterial.chkIssueDate = 'IssueDate';
    this.getProductionStatus();
  }

  ngAfterViewInit() {

    this.isMissingMaterial_DataLoad_FirstTime = false;
    this.missingMaterialDatatableTrigger.next();

    this.isLoaded = true;
  }

  ngOnDestroy() {
    this.isMissingMaterial_DataLoad_FirstTime = true;
    this.missingMaterialDatatableTrigger.unsubscribe();
  }

  /// <summary>
  /// Get the data on change of dropdown
  /// </summary>
  getDataOnChangeofDropdown(_loadTime = null) {
    try {
      const data = this.missingMaterial;
      data.Status = '';
      data.ProductionStatusId = this.productionStatusId;
      this.missingMaterialsService.getDataForPostOrdersDropDown(data, this.reqVerificationToken).subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const apiData = result.Data;
              console.log("getDataOnChangeOfDropdown:"+ JSON.stringify(result.Data));
              let mAssets = apiData[0].MediaAssets;
              const iDates = apiData[0].IssueDates;
              if (mAssets.length > 0) {
                if (_loadTime === 'onLoadTime') {
                  mAssets = alasql('SELECT * FROM ? AS add order by MediaAssetName asc', [mAssets]);
                  this.mediaAssets = mAssets;
                }
                if (this.selectedMediaAsset !== '' && this.selectedMediaAsset !== undefined) {
                  this.missingMaterial.MediaAssetId = this.selectedMediaAsset;
                } else {
                  this.frmMissingMaterial.controls['MediaAsset'].setValue(null);
                }
              } else {
                if (_loadTime === 'onLoadTime') {
                  this.mediaAssets = [];
                }
                this.missingMaterial.MediaAssetId = this.selectedMediaAsset;
              }

              // set Issue Date Dropdown
              if (this.changeDropdown === 0) {
                if (iDates.length > 0) {
                  this.issueDates = [];
                  let issueDate = iDates;
                  const orderByIssueDate = alasql('SELECT * FROM ? AS add order by CoverDate desc', [issueDate]);
                  this.issueDates = orderByIssueDate;

                  for (let i = 0; i < this.issueDates.length; i++) {
                    const tempCoverDate = this.issueDates[i]['CoverDate'];
                    const convertCoverDate = this.globalClass.getDateInFormat(tempCoverDate);
                    this.issueDates[i]['CoverDate'] = convertCoverDate;
                  }

                  issueDate = this.selectedIssueDate;

                  if (issueDate > 0) {
                    this.selectedIssueDate = '';
                  }

                  if (this.selectedIssueDate !== '' && this.selectedIssueDate !== undefined) {
                    this.missingMaterial.IssueDateId = this.selectedIssueDate;

                  } else {
                    this.frmMissingMaterial.controls['IssueDate'].setValue(null);
                  }
                } else {
                  this.issueDates = [];
                  this.missingMaterial.IssueDateId = this.selectedIssueDate;
                }
              }
            }
          }
        }
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /// <summary>
  /// Get the list of all party data from ASI
  /// </summary>
  getAllPartyDataThroughASI() {
    try {
      // if (this.globalClass.getMainPartyAndOrganizationData().length <= 0
      //   && this.globalClass.getMainAdvertisersData().length <= 0
      //   && this.globalClass.getMainAgenciesData().length <= 0
      //   && this.globalClass.getMainBillingToContactsData().length <= 0) {
      //   this.hasNext = true;
      //   this.offset = 0;
      //   this.advertisers = [];
      //   this.contacts = [];
      //   this.getPartyThroughASI();
      // } else {
        
      // }
      this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
        this.advertisers = this.globalClass.getMainAdvertisersData();
        this.agencies = this.globalClass.getMainAgenciesData();
        this.billingToContacts = this.globalClass.getMainBillingToContactsData();
        this.getAllAdvertisers();
        setTimeout(() => {
          this.displayOrderDatatableJquery();  
        }, 1200);
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getPartyThroughASI() {
    try {
      this.showLoader();
      this.missingMaterialsService.getAllAdvertiser1(this.websiteRoot, this.reqVerificationToken, this.offset).subscribe(result => {
        if (result != null && result !== undefined && result !== '') {
          const ItemData = result.Items.$values;
          if (ItemData.length > 0) {

            ItemData.forEach((itemdatavalue, key) => {
              this.partyAndOrganizationData.push(itemdatavalue);
              const type = itemdatavalue.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                this.advertisers.push(itemdatavalue);
                this.agencies.push(itemdatavalue);
              } else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                this.billingToContacts.push(itemdatavalue);
              }
            });

            this.offset = result.Offset;
            const nextOffset = this.offset + 500;

            if (result.Count === 500) {
              this.offset = nextOffset;
              this.getPartyThroughASI();
            } else {
              this.hideLoader();
              this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
              this.globalClass.setMainAdvertisersData(this.advertisers);
              this.globalClass.setMainAgenciesData(this.agencies);
              this.globalClass.setMainBillingToContactsData(this.billingToContacts);
              this.getAllAdvertisers();
              this.isLoaded = true;
              this.displayOrderDatatableJquery();
            }
          } else {
            this.hideLoader();
            this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
            this.globalClass.setMainAdvertisersData(this.advertisers);
            this.globalClass.setMainAgenciesData(this.agencies);
            this.globalClass.setMainBillingToContactsData(this.billingToContacts);
            this.getAllAdvertisers();
            this.isLoaded = true;
            this.displayOrderDatatableJquery();
          }
        } else {
          this.hideLoader();
          this.partyAndOrganizationData = [];
          this.advertisers = [];
          this.agencies = [];
          this.billingToContacts = [];
          this.POadvertisers = [];
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getProductionStatus() {
    this.showLoader();
    try {
      this.missingMaterialsService.getProductionStatus(this.reqVerificationToken).subscribe(result => {
        if (result.StatusCode === 1) {
          const productionStatusData=result.Data;
          this.productionStatusId  = productionStatusData.ProductionStatusId;
          console.log("getProductionStatus-Data:"+JSON.stringify(productionStatusData));
          console.log("getProductionStatus:"+this.productionStatusId);
          this.getDataOnChangeofDropdown(this.loadTime);
          //this.getAllPartyDataThroughASI();
          this.displayOrderDatatableJquery(); 
        } else {
          this.productionStatusId=0;
        }
      }, error => {
        this.hideLoader();
                  console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /// <summary>
  /// Get list of all advertisers
  /// </summary>
  getAllAdvertisers() {
    try {
      this.showLoader();
      this.missingMaterialsService.getMediaOrdersAdvertiser(this.reqVerificationToken).subscribe(result => {
        if (result.StatusCode === 1) {
          this.hideLoader();
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const data = result.Data;
                result.Data.forEach((data, key) => {
                  const advertiserId = data.AdvertiserId;
                  this.missingMaterialsService.getAdvertiserDetail_ByAdvertiserId(this.websiteRoot, this.reqVerificationToken, advertiserId).subscribe(result => {
                    let ItemData=[];  
                    if (result != null && result !== undefined && result !== '') {
                      ItemData.push(result.Items.$values);
                      for (const itemdatavalue of ItemData)
                      {
                        this.mediaOrderPartyData.push({Id:advertiserId,OrganizationName:itemdatavalue[0].Name});
                      }
                    }
                }, error => {
                  this.hideLoader();
                  console.log(error);
                });
              });
              this.setAdvertiserList(data);
            } else {
              this.POadvertisers = [];
            }
          } else {
            this.POadvertisers = [];
          }
        } else {
          this.hideLoader();
          this.POadvertisers = [];
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /// <summary>
  /// Get the list of all Issue Dates from Database
  /// </summary>
  getAllIssueDates() {
    try {
      this.missingMaterialsService.getAll().subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const issueDate = result.Data;
              this.issueDates = alasql('SELECT * FROM ? AS add order by CoverDate desc', [issueDate]);
              for (let i = 0; i < this.issueDates.length; i++) {
                const tempCoverDate = this.issueDates[i]['CoverDate'];
                const convertCoverDate = this.globalClass.getDateInFormat(tempCoverDate);
                this.issueDates[i]['CoverDate'] = convertCoverDate;
              }
            } else {
              this.issueDates = [];
            }
          } else {
            this.issueDates = [];
          }
        } else if (result.StatusCode === 3) {
          this.issueDates = [];
        } else {
          this.issueDates = [];
        }
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // server side pagination implementation
  displayOrderDatatableJquery() {
    try {
      this.obj = {};
      this.obj.productionstatusId = this.productionStatusId;
      this.obj.MediaAssetId = this.missingMaterial.MediaAssetId;
      this.obj.IssueDateId = this.missingMaterial.IssueDateId;
      this.intiOrderDatable(this.obj);
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  intiOrderDatable(data) {
    const tempPartyAndOrganizationData = this.mediaOrderPartyData;
    try {
      this.missingMaterialDatatable = {
        pagingType: 'full_numbers',
        pageLength: 10,
        serverSide: true,
        processing: true,
        lengthChange: false,    // hide show N record dropdown
        searching: false,
        destroy: true,
        ordering: false,
        orderMulti: false,
        scrollCollapse: false,
        responsive: true,
        search: false,
        ajax: (dataTablesParameters: any, callback) => {
          this.showLoader();
          this.missingMaterialsService.getAllOrdersProductionStatusdWise(data, dataTablesParameters,
            this.reqVerificationToken).subscribe(resp => {
              this.hideLoader();
              callback({
                recordsTotal: resp.RecordsTotal,
                recordsFiltered: resp.RecordsFiltered,
                data: resp.Data
              });
            }, error => {
              this.hideLoader();
              console.log(error);
            });
        },
        columns: [
          {
            name: 'Advertiser',
            orderable: false,
            render: function (data1, type, row) {
              // let advertiserName = '';
              // const advertiserId = row.AdvertiserId;
              // const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id LIKE ?', [tempPartyAndOrganizationData, advertiserId]);
              // if (objAdvertiser.length > 0) {
              //   advertiserName = objAdvertiser[0].Name;
              // } else {
              //   advertiserName = '';
              // }
              const advertiserId = row.AdvertiserId;
              let advertiserName = tempPartyAndOrganizationData.find(x=>x.Id===advertiserId).OrganizationName;  //alasql('SELECT * FROM ? AS add WHERE Id LIKE ?', [tempPartyAndOrganizationData, advertiserId]);
              return advertiserName;
            },
          },
          { data: 'BT_ID', name: 'Bill to ID', orderable: false },
          { data: 'ST_ID', name: 'Ship to ID', orderable: false },
          {
            name: 'Gross Cost',
            orderable: false,
            render: function (data2, type, row) {
              const customHTML = row.GrossCost.toFixed(2);
              return customHTML;
            },
          },
          {
            name: 'Net Cost',
            orderable: false,
            render: function (data3, type, row) {
              const customHTML = row.NetCost.toFixed(2);
              return customHTML;
            },
          },
          {
            name: 'Description',
            orderable: false,
            render: function (data4, type, row) {
              let AdTypeName = row.AdTypeName;
              let customData = '';
              if (AdTypeName === undefined || AdTypeName == null || AdTypeName === '') {
                AdTypeName = '';
              } else {
                customData = AdTypeName;
              }

              let adcolorName = row.AdcolorName;
              if (adcolorName === undefined || adcolorName == null || adcolorName === '') {
                adcolorName = '';
              } else {
                customData = '|' + adcolorName;
              }

              let adSizeName = row.AdSizeName;
              if (adSizeName === undefined || adSizeName == null || adSizeName === '') {
                adSizeName = '';
              } else {
                customData = '|' + adSizeName;
              }
              let frequencyName = row.FrequencyName;
              if (frequencyName === undefined || AdTypeName == null || AdTypeName === '') {
                frequencyName = '';
              } else {
                customData = '|' + frequencyName;
              }
              customData = AdTypeName + '|' + adcolorName + '|' + adSizeName + '|' + frequencyName;
              const customDataLength = customData.length - 1;
              const customDataindexOf = customData.indexOf('|');
              const customDataLastIndexOf = customData.lastIndexOf('|');
              if (customDataindexOf === 0) {
                customData.substring(0, 0);
              }
              if (customDataLastIndexOf === customDataLength) {
                customData.substring(customDataLength, customDataLength);
              }
              return customData;
            },
          },
        ]
      };

      if (!this.isMissingMaterial_DataLoad_FirstTime) {
        this.hideLoader();
        setTimeout(() => {
          try {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.missingMaterialDatatableTrigger.next();
            });
          } catch (error) {
            console.log(error);
          }
        }, 1000);
      }
    } catch (err) {
      this.hideLoader();
      console.log(err);
    }
  }


  /// <summary>
  /// Create list for missing material
  /// </summary>
  createListforMissingMaterial(objOrders) {
    try {
      if (objOrders != null && objOrders !== undefined && objOrders !== '' && objOrders.length > 0) {
        if (objOrders.length > 0) {
          if (this.globalClass.getMainPartyAndOrganizationData().length > 0) {
            this.globalClass.setMainPartyAndOrganizationData(this.globalClass.getMainPartyAndOrganizationData());
            this.missingMaterials = [];
            objOrders.forEach((data, key) => {
              const advertiserId = data.AdvertiserId;
              const ST_ID = data.ST_ID;
              const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?',
                [this.partyAndOrganizationData, advertiserId]);
              const objContact = alasql('SELECT * FROM ? AS add WHERE Id = ?',
                [this.partyAndOrganizationData, ST_ID]);
              if (objAdvertiser.length > 0) {
                data.AdvertiserName = objAdvertiser[0].Name;
              } else {
                data.AdvertiserName = '';
              }
              if (objContact.length > 0) {
                data.billToContactName = objContact[0].Name;
              } else {
                data.billToContactName = '';
              }
              this.missingMaterials.push(data);
            });

            this.missingMaterials = alasql('SELECT * FROM ? AS add ORDER BY AdvertiserName ASC', [this.missingMaterials]);
            this.isShowDefaultTable = false;
            this.dataLoading = false;
            this.isShowPagination = true;

          } else {
            this.missingMaterials = [];
            this.isShowDefaultTable = true;
            this.isShowPagination = false;
            this.dataLoading = false;
            this.getAllPartyDataThroughASI();
          }
        } else {
          this.missingMaterials = [];
          this.isShowDefaultTable = true;
          this.isShowPagination = false;
          this.dataLoading = false;
        }
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }



  /// <summary>
  /// Set all advertiser list
  /// </summary>
  setAdvertiserList(objAdvertisers) {
    try {
      if (objAdvertisers.length > 0) {
        if (this.globalClass.getMainAdvertisersData().length > 0) {
          this.advertisers = [];
          this.advertisers = this.globalClass.getMainAdvertisersData();
          this.POadvertisers = [];

          objAdvertisers.forEach((data, key) => {
            const advertiserId = data.AdvertiserId;
            const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.advertisers, advertiserId]);
            this.POadvertisers.push(objAdvertiser[0]);
          });

          if (this.POadvertisers.length > 0) {
            const advertiser = this.selectedAdvertiser;
            if (advertiser > 0) {
              this.selectedAdvertiser = '';
            }
            if (this.selectedAdvertiser !== ''
              && this.selectedAdvertiser !== undefined) {
              this.missingMaterial.AdvertiserId = this.selectedAdvertiser;

            } else {
              this.missingMaterial.AdvertiserId = '';

            }
          }
        } else {
          this.POadvertisers = [];
          this.getAllPartyDataThroughASI();
        }
      } else {
        this.POadvertisers = [];
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /// <summary>
  /// Dropdown change events
  /// </summary>
  onChangeMediaAsset() {
    try {
      const id = this.frmMissingMaterial.controls['MediaAsset'].value;

      if (id !== undefined && id !== '' && id !== 0 && id != null) {

        this.selectedMediaAsset = id;
        this.changeDropdown = 0;

        this.missingMaterial.IssueDateId = '';
        this.missingMaterial.AdTypeId = '';
        this.missingMaterial.AdvertiserId = '';

        this.getDataOnChangeofDropdown();
      } else if (id === '' || id === null) {
        this.getAllIssueDates();
      } else {
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  onChangeIssueDate() {
    try {
      const id = this.frmMissingMaterial.controls['IssueDate'].value;

      if (id !== undefined && id !== '' && id !== 0 && id != null) {

        this.selectedIssueDate = id;
        this.changeDropdown = 1;

        this.missingMaterial.AdTypeId = '';
        this.missingMaterial.AdvertiserId = '';

        this.getDataOnChangeofDropdown();
      } else {
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /// <summary>
  /// Search orders by media asset
  /// </summary>
  searchOrdersbyMediaAsset() {
    try {
      this.isShowDefaultTable = false;
      this.isShowPagination = false;
      this.obj = {};
      this.obj.productionstatusId = this.productionStatusId;
      this.obj.MediaAssetId = this.missingMaterial.MediaAssetId;
      this.obj.IssueDateId = this.missingMaterial.IssueDateId;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }
}
