import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrderTabService } from '../../services/orderTab.service';
import { ProductionService } from '../../services/production.service';
import { DashboardAdvertiserService } from '../../services/dashboardAdvertisers.service';
import { GlobalClass } from '../../GlobalClass';
import { environment } from '../../../../environments/environment';
import { AppComponent } from '../../../app.component';
import 'select2';
declare const $, jQuery: any;
declare var imgviewerfun: any;

@Component({
  selector: 'asi-production',
  templateUrl: './production.component.html',
  styleUrls: ['./production.component.css']
})
export class ProductionComponent implements OnInit {

  productionForm: FormGroup;

  showMsgs = false;
  isShowPickUpDiv = false;
  isShowNewDiv = false;
  isShowDefaultNetAdjustmentTable = false;
  isShowDefaultGrossAdjustmentTable = false;
  isShowDefaultTable = false;
  setProductionNoClick = true;
  disableOnTrackingNumber=false;
  flag = true;

  organizationWiseParty: any[] = [];
  billingToContacts: any[] = [];
  tmpbillingToContacts: any[] = [];
  grossAdAdjustments: any[] = [];
  netAdAdjustments: any[] = [];
  personData: any[] = [];
  agencies: any[] = [];
  advertisers: any[] = [];
  selectedAgencyIds: any = [];
  selectedAdvertiserIds: any = [];
  issueDateWiseAdjustment: any[] = [];
  partyAndOrganizationData: any = [];
  selectedCheckbox: any[] = [];
  selectedMediaOrderIds: any[] = [];
  productionStatus: any[] = [];
  positions: any[] = [];
  separations: any[] = [];
  mediaAssetsList: any[] = [];
  tempArray: any = [];
  mediaAsset: any = [];
  productionTab: any = [];
  newMediaAssets :any[];
  tabOrderId: any;
  selectedMediaOrderId: any;
  selectedAdvertiserId: any;
  selectedAgencyId: any;

  offset: number;
  hasNext = false;

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  apiBaseURL = '';

  originalFileImage = '';
  tempGrossAmount = '';
  grossCost = '';
  netCost = '';
  mediaAssetIssueDate = '';
  buttonText = 'Add';

  originalFileImageSrc = '';
  isPdfTrueForOriginal = false;
  tempPdfPathForOriginal = '';
  isImgTrueForOriginal = false;

  proofFileImageSrc = '';
  isPdfTrueForProof = false;
  tempPdfPathForProof = '';
  isImgTrueForProof = false;

  finalFileImageSrc = '';
  isPdfTrueForfinal = false;
  tempPdfPathForFinal = '';
  isImgTrueForfinal = false;

  production: any = {};
  selectedIssueMediaOrder: any = {};
  position: any = {};
  separation: any = {};
  materialContact: any = {};
  productionStatusNew: any = {};
  FullName: any = {};

  constructor(private globalClass: GlobalClass,
    private toastr: ToastrService,
    private productionService: ProductionService,
    private dashboardAdvertiserService: DashboardAdvertiserService,
    private formBuilder: FormBuilder,
    private appComponent: AppComponent,
    private orderTabService: OrderTabService) { }

  ngOnInit() {
    this.getToken();
    this.productionFormValidation();
    this.initProductionController();
    this.grossCost = this.globalClass.getGrossCost();
    this.netCost = this.globalClass.getNetCost();
    setTimeout(() => {
      this.initSelect2('PickupMediaOrderId');
      this.initSelect2('MaterialContactId');
      this.initSelect2('ddlStatus');
      this.initSelect2('ddlSeparation');
      this.initSelect2('ddlPosition');
    }, 1000);
  
  }

  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }
  }

  productionFormValidation() {
    const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.productionForm = this.formBuilder.group({
      rdoNewPickUp: [null],
      rdoCreateExpected: [null],
      OnHand: [null],
      OnDate: [null],
      Tracking: [null],
      Changes: [null],
      PickupMediaOrderId: [null],
      billto1: [null],
      MaterialContactId: [null],
      ddlStatus: [null],
      OrigFileName: [null],
      OrigFileNameUpload: [null],
      ProofFileName: [null],
      ProofFileNameUpload: [null],
      FinalFileName: [null],
      FinalFileNameUpload: [null],
      WebAdUrl: [null, Validators.pattern(reg)],
      txtHeadLine: [null],
      txtTearSheet: [null],
      txtPgNo: [null],
      ddlSeparation: [null],
      ddlPosition: [null],
      ProductionComment: [null]
    });
  }
  initProductionController() {
    try {

      this.partyAndOrganizationData = [];

      this.tabOrderId = this.globalClass.getOrderId();

      this.production = {};
      this.production.buyId = this.globalClass.getOrderId();
      this.issueDateWiseAdjustment = [];
      this.isShowDefaultTable = false;
      this.selectedIssueMediaOrder = {};
      this.selectedCheckbox = [];
      this.selectedMediaOrderIds = [];
      this.setProductionNoClick = true;

      // page array data
      this.productionTab = [];
      this.flag = true;

      // page modal
      this.productionStatus = [];
      this.positions = [];
      this.separations = [];
      this.mediaAssetsList = [];
      this.mediaAssetIssueDate = '';
      this.tempArray = [];
      this.mediaAsset = [];
      this.production.NewInd = '1';
    
      setTimeout(() => {
        if (this.globalClass.getbillingDetails().BillTo == 0) {
          this.production.AdvertiserAgencyInd = '1'
        } else {
          this.production.AdvertiserAgencyInd = '0'
        }  
      }, 1000);
      
      this.production.CreateExpectedInd = '0';
     

     
      this.buttonText = 'Add';
      
      try {
        this.getAllProductionStatus();
      } catch (error) {
        console.log('getAllProductionStatus Error : ' + error);
      }

      try {
        this.getAllPosition();
      } catch (error) {
        console.log('getAllPosition Error : ' + error);
      }

      try {
        this.getAllSeparation();
      } catch (error) {
        console.log('getAllSeparation Error : ' + error);
      }

      try {
        this.getAdvertiserThroughASI();
      } catch (error) {
        console.log('getAdvertiserThroughASI Error : ' + error);
      }
    } catch (error) {
      console.log(error);
    }
  }

  // Initialization of gross cost and net cost
  initGrossandNetCost() {
    const totalSum = alasql('SELECT sum(GrossCost),sum(NetCost) FROM ? AS add ', [this.issueDateWiseAdjustment]);
    const grossAmount = parseFloat(totalSum[0]['SUM(GrossCost)']);
    const netAmount = parseFloat(totalSum[0]['SUM(NetCost)']);
    this.globalClass.setGrossCost((grossAmount).toFixed(2));
    this.globalClass.setNetCost((netAmount).toFixed(2));
  }

  // Get number of issue media order
  getNoOfIssueMediaOrder() {
    try {
      const buyerId = this.production.buyId;
      if (buyerId != null && buyerId !== undefined && buyerId !== '') {
        this.productionService.getMediaOrderbyBuyId(this.reqVerificationToken, buyerId).subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.isShowDefaultTable = false;
                this.issueDateWiseAdjustment = result.Data;
                this.initGrossandNetCost();
              } else {
                this.isShowDefaultTable = true;
                this.issueDateWiseAdjustment = [];
              }
            } else {
              this.isShowDefaultTable = true;
              this.issueDateWiseAdjustment = [];
            }
          } else if (result.StatusCode === 3) {
            this.isShowDefaultTable = true;
            this.issueDateWiseAdjustment = [];
          } else {
            this.toastr.error(result.Message, 'Error!');
          }
        });
      } else {
        console.log('Buyer Id is null');
      }

    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  // Get the list of all production status from Database
  getAllProductionStatus() {
    try {
      this.productionService.getAllProductionStatus().subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              this.productionStatus = result.Data;
              this.productionStatusNew = result.Data;
              this.initGrossandNetCost();
            } else {
              this.productionStatus = [];
            }
          } else {
            this.productionStatus = [];
          }
        } else if (result.StatusCode === 3) {
          this.productionStatus = [];
        } else {
          this.productionStatus = [];
          this.toastr.error(result.Message, 'Error!');
        }
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Get the list of all position from Database
  getAllPosition() {
    try {
      this.productionService.getAllPosition().subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              this.positions = result.Data;
              this.initGrossandNetCost();
            } else {
              this.positions = [];
            }
          } else {
            this.positions = [];
          }
        } else if (result.StatusCode === 3) {
          this.positions = [];
        } else {
          this.positions = [];
          this.toastr.error(result.Message, 'Error!');
        }
      });
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  // Get the list of all separation from Database
  getAllSeparation() {
    try {
      this.productionService.getAllSeperation().subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              this.separations = result.Data;
              this.initGrossandNetCost();
            } else {
              this.separations = [];
            }
          } else {
            this.separations = [];
          }
        } else if (result.StatusCode === 3) {
          this.separations = [];
        } else {
          this.separations = [];
          this.toastr.error(result.Message, 'Error!');
        }
      });
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  // Get the list of order which is not in buyer
  getOrdersNotInBuyer(buyId, adid) {
    try {
      this.productionService.getOrdersNotInBuyer(buyId, adid).subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {

              result.Data.forEach((value, key) => {
                const issueDate = this.globalClass.getDateInFormat(value.IssueDate);
                const pageNo = value.PageNumber == null ? '' : value.PageNumber;
                if (pageNo != null && pageNo !== undefined && pageNo !== '') {
                  this.mediaAsset.MediaAssetIssueDate = value.MediaAssetName + ' | ' + issueDate + ' | ' + pageNo;
                } else {
                  this.mediaAsset.MediaAssetIssueDate = value.MediaAssetName + ' | ' + issueDate;
                }
                this.mediaAsset.MediaOrderId = value.MediaOrderId;
                this.tempArray.push(this.mediaAsset);
                this.mediaAsset = {};
              });

              this.mediaAssetsList = this.tempArray;
              console.log("MediaAssetIssueDate1",this.mediaAssetsList);
              this.initGrossandNetCost();
            } else {
              this.mediaAssetsList = [];
            }
          } else {
            this.mediaAssetsList = [];
          }
        } else if (result.StatusCode === 3) {
          this.mediaAssetsList = [];
        } else {
          this.mediaAssetsList = [];
          this.toastr.error(result.Message, 'Error!');
        }
      });
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  // Get the list of all advertiser data from ASI
  getAdvertiserThroughASI() {
    try {
      if (this.globalClass.getMainPartyAndOrganizationData().length <= 0 && this.globalClass.getMainAdvertisersData().length <= 0
        && this.globalClass.getMainAgenciesData().length <= 0 && this.globalClass.getMainBillingToContactsData().length <= 0) {
        this.hasNext = true;
        this.offset = 0;
        this.getAdvertiserData();

      } else {
        this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
        this.advertisers = this.globalClass.getMainAdvertisersData();
        this.agencies = this.globalClass.getMainAgenciesData();
        this.personData = this.globalClass.getMainBillingToContactsData();
        this.production.AdvertiserAgencyInd = '1';
        this.getNoOfIssueMediaOrder();
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  getAdvertiserData() {
    try {
      this.showLoader();
      this.dashboardAdvertiserService.getAllAdvertiser1(this.websiteRoot, this.baseUrl,
        this.reqVerificationToken, this.offset).subscribe(result => {

          if (result != null && result !== undefined && result !== '') {

            const ItemData = result.Items.$values;

            if (ItemData.length > 0) {

              ItemData.forEach((itemdatavalue, key) => {

                this.partyAndOrganizationData.push(itemdatavalue);
                const type = itemdatavalue.$type;

                if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                  this.advertisers.push(itemdatavalue);
                  this.agencies.push(itemdatavalue);
                } else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                  this.personData.push(itemdatavalue);
                }
              });

              this.offset = result.Offset;
              const nextOffset = this.offset + 500;

              if (result.Count === 500) {
                this.offset = nextOffset;
                this.getAdvertiserData();
              } else {
                this.hideLoader();
                this.setGlobalData();
                this.production.AdvertiserAgencyInd = '1';
                this.getNoOfIssueMediaOrder();
              }
            } else {
              this.hideLoader();
              this.setGlobalData();
              this.production.AdvertiserAgencyInd = '1';
              this.getNoOfIssueMediaOrder();
            }
          } else {
            this.hideLoader();
            this.advertisers = [];
          }
        }, error => {
          this.hideLoader();
          console.log(error);
        });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Delete Issue Order Adjustment
  deleteMediaOrderMediaLinesbyMediaOrderId(objProduction) {
    try {
      if(objProduction.IsFrozen)
      {
        this.toastr.warning("This media order can't be deleted as it has been posted","Warning");
      }
      else
      {
        if (confirm('Are you sure you want to delete the selected object(s) and all of their children?')) {
          const mediaOrderId = objProduction.MediaOrderId;
          if (mediaOrderId != null && mediaOrderId !== undefined && mediaOrderId !== '') {
            this.productionService.deleteMediaOrderLinesbyMediaOrder(this.reqVerificationToken, mediaOrderId).subscribe(result => {
              if (result.StatusCode === 1) {
  
                if (result.Data != null) {
                  this.checkboxChange(objProduction);
                  const data = result.Data;
                  const grossCost = data.GrossCost;
                  const netCost = data.NetCost;
  
                  this.globalClass.setGrossCost((grossCost).toFixed(2));
                  this.globalClass.setNetCost((netCost).toFixed(2));
  
                  this.getNoOfIssueMediaOrder();
  
                  this.toastr.success('Deleted successfully.', 'Success!');
                }
              } else {
                this.toastr.error(result.Message, 'Error!');
              }
            });
          } else {
            console.log('media Order Id is null');
          }
        }
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Edit Issue Order Adjustment
  editMediaOrderMediaLinesbyMediaOrderId(MediaOrderId,isFrozen) {
    if(isFrozen)
    {
      this.toastr.warning("This media order can't be edited as it has been posted","Warning");
    }
    else{
      this.globalClass.setOrderId(this.globalClass.getOrderId());
      this.globalClass.setBillingDetailsMediaOrderId(MediaOrderId);
      this.orderTabService.send_OrderTab('MediaScheduleTab');
    }
  }

  setOnTNEnableOrDisabled(rdoName)
  {
    if(rdoName==='0')
    {
      this.productionForm.controls['OnDate'].disable();
      this.productionForm.controls['Tracking'].disable();
    }
    else
    {
      this.productionForm.controls['OnDate'].enable();
      this.productionForm.controls['Tracking'].enable();
    }
  }

  // Enable disable code for pickup and new
  setEnableOrDisable(rdoName) {
    this.production.NewInd = rdoName;
    console.log(this.production);
    // tslint:disable-next-line:radix
    if (rdoName === '1') {
      // enable
      this.dvNew(false);

      this.dvPickup(true);

      this.production.ChangesInd = null;
      this.productionForm.controls['PickupMediaOrderId'].setValue(null);

      this.production.CreateExpectedInd = '0';
    } else if (rdoName === '0') {
      this.dvNew(true);

      // enable
      this.dvPickup(false);

      this.production.CreateExpectedInd = '';
      this.production.OnHandInd = '';
      this.production.MaterialExpectedDate = '';
      this.production.TrackingNumber = '';
    }
  }

  dvNew(status) {
    if (status) {
      // disable
      this.productionForm.controls['rdoCreateExpected'].disable();
      this.productionForm.controls['OnHand'].disable();
      this.productionForm.controls['OnDate'].disable();
      this.productionForm.controls['Tracking'].disable();
    } else {
      // enable
      this.productionForm.controls['rdoCreateExpected'].enable();
      this.productionForm.controls['OnHand'].enable();
      this.productionForm.controls['OnDate'].enable();
      this.productionForm.controls['Tracking'].enable();
    }

  }

  dvPickup(status) {
    if (status) {
      this.productionForm.controls['Changes'].disable();
      this.productionForm.controls['PickupMediaOrderId'].disable();
    } else {
      this.productionForm.controls['Changes'].enable();
      this.productionForm.controls['PickupMediaOrderId'].enable();
    }
  }

  // Insert production data  to database
  save(production) {
    
    try {
      production.MediaOrderIds = this.selectedMediaOrderIds;
      production.PickupMediaOrderId = null;
      production.MaterialContactId = null;
      production.ProductionStatusId = null;
      production.SeparationId = null;
      production.PositionId = null;

      const cPickupMediaOrderId = this.productionForm.controls['PickupMediaOrderId'].value;
     
      const cMaterialContactId = this.productionForm.controls['MaterialContactId'].value;
      console.log(cMaterialContactId);
      const cProductionStatusId = this.productionForm.controls['ddlStatus'].value;
      const cSeparationId = this.productionForm.controls['ddlSeparation'].value;
      const cPositionId = this.productionForm.controls['ddlPosition'].value;

      if (cPickupMediaOrderId != null && cPickupMediaOrderId !== '' && cPickupMediaOrderId !== undefined) {
        // tslint:disable-next-line:radix
        production.PickupMediaOrderId = parseInt(cPickupMediaOrderId);
      }

      if (cMaterialContactId != null && cMaterialContactId !== '' && cMaterialContactId !== undefined) {
        production.MaterialContactId = cMaterialContactId;
      }

      if (cProductionStatusId != null && cProductionStatusId !== '' && cProductionStatusId !== undefined) {
        production.ProductionStatusId = cProductionStatusId.ProductionStatusId;
      }

      if (cSeparationId != null && cSeparationId !== '' && cSeparationId !== undefined) {
        production.SeparationId = cSeparationId.SeparationId;
      }

      if (cPositionId != null && cPositionId !== '' && cPositionId !== undefined) {
        production.PositionId = cPositionId.PositionId;
      }


      if (production.NewInd === '0') {
        production.NewInd = false;
      } else if (production.NewInd === '1') {
        production.NewInd = true;
      }

      if (production.AdvertiserAgencyInd === '0') {
        production.AdvertiserAgencyInd = false;
      } else if (production.AdvertiserAgencyInd === '1') {
        production.AdvertiserAgencyInd = true;
      }

      if (production.CreateExpectedInd === '0') {
        production.CreateExpectedInd = false;
      } else if (production.CreateExpectedInd === '1') {
        production.CreateExpectedInd = true;
      }

      if (production.MaterialExpectedDate === undefined
        || production.MaterialExpectedDate === ''
        || production.MaterialExpectedDate === null) {
        production.MaterialExpectedDate = '';
      } else {
        production.MaterialExpectedDate = this.globalClass.getDateInFormat(production.MaterialExpectedDate);
      }

      if (production.TrackingNumber === undefined
        || production.TrackingNumber === ''
        || production.TrackingNumber === null) {
        production.TrackingNumber = '';
      } else {

        if (!isNaN(production.TrackingNumber)) {
          // tslint:disable-next-line:radix
          production.TrackingNumber = parseInt(production.TrackingNumber);
        } else {
          production.TrackingNumber = '';
        }
      }

      if (production.ChangesInd === undefined || production.ChangesInd === '' || production.ChangesInd === null) {
        production.ChangesInd = null;
      }

      if (production.OnHandInd === undefined || production.OnHandInd === '' || production.OnHandInd === null) {
        production.OnHandInd = null;
      }

      if (production.OriginalFile === undefined
        || production.OriginalFile === ''
        || production.OriginalFile === null) {
        production.OriginalFile = null;
        production.OriginalFileExtension = null;
        production.OrigFileName = null;
      }

      if (production.ProofFile === undefined
        || production.ProofFile === ''
        || production.ProofFile === null) {
        production.ProofFile = null;
        production.ProofFileExtension = null;
        production.ProofFileName = null;
      }

      if (production.FinalFile === undefined
        || production.FinalFile === ''
        || production.FinalFile === null) {
        production.FinalFileName = null;
        production.FinalFile = null;
        production.FinalFileExtension = null;
      }

      if (production.WebAdUrl === undefined
        || production.WebAdUrl === ''
        || production.WebAdUrl === null) {
        production.WebAdUrl = '';
      }

      if (production.TearSheets === undefined
        || production.TearSheets === ''
        || production.TearSheets === null) {
        production.TearSheets = '';
      } else {
        if (!isNaN(production.TearSheets)) {
          // tslint:disable-next-line:radix
          production.TearSheets = parseInt(production.TearSheets);
        } else {
          production.TearSheets = '';
        }
      }

      if (production.HeadLine === undefined
        || production.HeadLine === ''
        || production.HeadLine === null) {
        production.HeadLine = '';
      }

      if (production.PageNumber === undefined
        || production.PageNumber === ''
        || production.PageNumber === null) {
        production.PageNumber = '';
      } else {
        if (!isNaN(production.PageNumber)) {
          // tslint:disable-next-line:radix
          production.PageNumber = parseInt(production.PageNumber);
        } else {
          production.PageNumber = '';
        }
      }

      if (production.ProductionComment === undefined
        || production.ProductionComment === ''
        || production.ProductionComment === null) {
        production.ProductionComment = '';
      }

      production.Completed = false;
      console.log("production",production);
      this.showLoader();
      this.productionService.saveMediaOrderProductionDetails(this.reqVerificationToken, production).subscribe(result => {
        if (result.StatusCode === 1) {
          this.hideLoader();
          this.productionForm.reset();
          
          this.getNoOfIssueMediaOrder();
          this.production = {};

          this.production.buyId = this.globalClass.getOrderId();

          this.production.NewInd = '1';
          this.production.CreateExpectedInd = '0';
          this.production.AdvertiserAgencyInd = '1';

          this.originalFileImage = '';

          this.originalFileImageSrc = '';
          this.proofFileImageSrc = '';
          this.finalFileImageSrc = '';

          this.tempPdfPathForOriginal = '';
          this.tempPdfPathForProof = '';
          this.tempPdfPathForFinal = '';

          this.selectedIssueMediaOrder = [];
          this.selectedCheckbox = [];
          this.selectedMediaOrderIds = [];
          this.selectedAdvertiserIds = [];
          this.selectedAgencyIds = [];

          this.initSelect2('PickupMediaOrderId');
          this.initSelect2('MaterialContactId');
          this.initSelect2('ddlStatus');
          this.initSelect2('ddlSeparation');
          this.initSelect2('ddlPosition');

          this.showMsgs = false;
          this.toastr.success('Order saved successfully.', 'Success!');
        } else {
          this.hideLoader();
          this.toastr.error(result.Message, 'Error!');
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Insert production data  to database
  finishNExit() {
    try {
      const gAmount = 0;
      const nAmount = 0;
      this.globalClass.removeBillingDetails();
      this.globalClass.removeOrderId();
      this.globalClass.setGrossCost((gAmount).toFixed(2));
      this.globalClass.setNetCost((nAmount).toFixed(2));
      this.globalClass.removeIsFrozen();
      this.globalClass.removeMainPartyAndOrganizationData();
      this.globalClass.removeMainAdvertisersData();
      this.globalClass.removeMainAgenciesData();
      this.globalClass.removeMainBillingToContactsData();
      this.appComponent.showMainPage = true;
      this.appComponent.showOrderFormPage = false;
      this.appComponent.setAdvertisersTabActive();

    } catch (error) {
      console.log(error);
    }
  }

  // Insert production data  to database
  finishNCreate() {
    try {
      const gAmount = 0;
      const nAmount = 0;
      //this.globalClass.removeBillingDetails();
      this.globalClass.removeOrderId();
      this.globalClass.setPreOrderFill(true);
      this.globalClass.removeIsFrozen();
      this.globalClass.setFromPreviousTab(true);
      this.globalClass.setFromProductionTab(true);
      //this.globalClass.removeMainPartyAndOrganizationData();
      //this.globalClass.removeMainAdvertisersData();
      //this.globalClass.removeMainAgenciesData();
      //this.globalClass.removeMainBillingToContactsData();
      this.globalClass.setGrossCost((gAmount).toFixed(2));
      this.globalClass.setNetCost((nAmount).toFixed(2));
      this.orderTabService.send_OrderTab('OrdersTab');
    } catch (error) {
      console.log(error);
    }
  }

  changeddlFrom() {
  }

  changeddlContactProduction() {
  }

  changeddlStatus() {
  }

  changeddlSeparation() {
  }

  changeddlPosition() {
  }

  /// Set bill to contact
  setBillToContact(changeBillToValue) {
    try {
      this.getBillToContact(changeBillToValue);
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  initImageViewerForOriginalFile() {
    try {
      this.showImageInViewerForOriginalFile('docs-pictures-originalFile', 'docs-buttons-originalFile');
    } catch (error) {
      console.log(error);
    }
  }

  initImageViewerForProofFile() {
    try {
      this.showImageInViewerForProofFile('docs-pictures-proofFile', 'docs-buttons-proofFile');
    } catch (error) {
      console.log(error);
    }
  }

  initImageViewerForFinalFile() {
    try {
      this.showImageInViewerForFinalFile('docs-pictures-finalFile', 'docs-buttons-finalFile');
    } catch (error) {
      console.log(error);
    }
  }

  uploadImage(event: any) {
    try {
      const logoElement = event;
      if (logoElement.target.files.length > 0) {
        const uploadType = logoElement.target.files[0].type;

        if (uploadType !== undefined && uploadType != null) {

          if (logoElement.target.files[0].type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(logoElement.target.files[0]);
            this.tempPdfPathForOriginal = tmppath;
            this.isPdfTrueForOriginal = true;
            this.isImgTrueForOriginal = false;
          } else if (logoElement.target.files[0].type.indexOf('image') !== -1) {
            this.isPdfTrueForOriginal = false;
            this.isImgTrueForOriginal = true;
          } else {
            const tmppath = URL.createObjectURL(logoElement.target.files[0]);
            this.tempPdfPathForOriginal = tmppath;
            this.isPdfTrueForOriginal = false;
            this.isImgTrueForOriginal = false;
          }

          if (logoElement.target.files && logoElement.target.files[0]) {
            const FR = new FileReader();
            // tslint:disable-next-line:no-shadowed-variable
            FR.onload = (event: any) => {
              if (this.isImgTrueForOriginal === false && this.isPdfTrueForOriginal === false) {
                this.originalFileImageSrc = '';
              } else {
                this.originalFileImageSrc = event.target.result;
              }

              const strImage = event.target.result.substring((event.target.result.indexOf(',') + 1));
              const temp = logoElement.target.files[0].name;

              this.production.OriginalFile = strImage;

              this.production.OriginalFileExtension = '';
              this.production.OriginalFileExtension = temp.substring(temp.lastIndexOf('.'));

              this.production.OrigFileName = '';
              this.production.OrigFileName = logoElement.target.files[0].name;
            };
            FR.readAsDataURL(logoElement.target.files[0]);
          }
        }
      } else {
        this.isPdfTrueForOriginal = false;
        this.isImgTrueForOriginal = false;
      }

    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  uploadImageforProof(event: any) {
    try {
      const logoElement = event;

      if (logoElement.target.files.length > 0) {
        const uploadType = logoElement.target.files[0].type;

        if (uploadType !== undefined && uploadType != null) {
          if (logoElement.target.files[0].type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(logoElement.target.files[0]);
            this.tempPdfPathForProof = tmppath;

            this.isPdfTrueForProof = true;
            this.isImgTrueForProof = false;
          } else if (logoElement.target.files[0].type.indexOf('image') !== -1) {
            this.isPdfTrueForProof = false;
            this.isImgTrueForProof = true;
          } else {
            this.isPdfTrueForProof = false;
            this.isImgTrueForProof = false;
            const tmppath = URL.createObjectURL(logoElement.target.files[0]);
            this.tempPdfPathForProof = tmppath;
          }

          if (logoElement.target.files && logoElement.target.files[0]) {
            const FR = new FileReader();
            // tslint:disable-next-line:no-shadowed-variable
            FR.onload = (event: any) => {

              if (this.isImgTrueForProof === false && this.isPdfTrueForProof === false) {
                this.proofFileImageSrc = '';
              } else {
                this.proofFileImageSrc = event.target.result;
              }
              const strImage = event.target.result.substring((event.target.result.indexOf(',') + 1));

              const temp = logoElement.target.files[0].name;

              this.production.ProofFile = strImage;
              this.production.ProofFileExtension = '';

              this.production.ProofFileExtension = temp.substring(temp.lastIndexOf('.'));
              this.production.ProofFileName = '';

              this.production.ProofFileName = logoElement.target.files[0].name;

            };
            FR.readAsDataURL(logoElement.target.files[0]);
          }
        }
      } else {
        this.isPdfTrueForProof = false;
        this.isImgTrueForProof = false;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  uploadImageforfinal(event: any) {
    try {
      const logoElement = event;

      if (logoElement.target.files.length > 0) {
        const uploadType = logoElement.target.files[0].type;

        if (uploadType !== undefined && uploadType != null) {
          if (logoElement.target.files[0].type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(logoElement.target.files[0]);
            this.tempPdfPathForFinal = tmppath;
            this.isPdfTrueForfinal = true;
            this.isImgTrueForfinal = false;
          } else if (logoElement.target.files[0].type.indexOf('image') !== -1) {
            this.isPdfTrueForfinal = false;
            this.isImgTrueForfinal = true;
          } else {
            const tmppath = URL.createObjectURL(logoElement.target.files[0]);
            this.tempPdfPathForFinal = tmppath;

            this.isPdfTrueForfinal = false;
            this.isImgTrueForfinal = false;
          }

          if (logoElement.target.files && logoElement.target.files[0]) {
            const FR = new FileReader();
            // tslint:disable-next-line:no-shadowed-variable
            FR.onload = (event: any) => {

              if (this.isImgTrueForfinal === false && this.isPdfTrueForfinal === false) {
                this.finalFileImageSrc = '';
              } else {
                this.finalFileImageSrc = event.target.result;
              }

              const strImage = event.target.result.substring((event.target.result.indexOf(',') + 1));


              const temp = logoElement.target.files[0].name;

              this.production.FinalFile = strImage;
              this.production.FinalFileExtension = '';

              this.production.FinalFileExtension = temp.substring(temp.lastIndexOf('.'));
              this.production.FinalFileName = '';

              this.production.FinalFileName = logoElement.target.files[0].name;
            };
            FR.readAsDataURL(logoElement.target.files[0]);
          }
        }
      } else {
        this.isPdfTrueForfinal = false;
        this.isImgTrueForfinal = false;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }


  showImageInViewerForOriginalFile(pictureClasssName, buttonClasssName) {
    try {
      if (this.isPdfTrueForOriginal === false && this.isImgTrueForOriginal === false) {
        if (this.originalFileImageSrc !== '' && this.originalFileImageSrc !== undefined && this.originalFileImageSrc != null) {
          window.open(this.tempPdfPathForOriginal, '_blank');
        }
      } else {
        if (this.isPdfTrueForOriginal === true) {
          window.open(this.tempPdfPathForOriginal, '_blank');
        } else if (this.isImgTrueForOriginal === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun(pictureClasssName, buttonClasssName);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  /*-------------------------------------------------------------------------*/

  showImageInViewerForProofFile(pictureClasssName, buttonClasssName) {
    try {
      if (this.isPdfTrueForProof === false && this.isImgTrueForProof === false) {
        if (this.proofFileImageSrc !== '' && this.proofFileImageSrc !== undefined && this.proofFileImageSrc != null) {
          window.open(this.tempPdfPathForProof, '_blank');
        }
      } else {
        if (this.isPdfTrueForProof === true) {
          window.open(this.tempPdfPathForProof, '_blank');
        } else if (this.isImgTrueForProof === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun(pictureClasssName, buttonClasssName);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  /*-------------------------------------------------------------------------*/

  showImageInViewerForFinalFile(pictureClasssName, buttonClasssName) {
    try {
      if (this.isPdfTrueForfinal === false && this.isImgTrueForfinal === false) {
        if (this.finalFileImageSrc !== '' && this.finalFileImageSrc !== undefined && this.finalFileImageSrc != null) {
          window.open(this.tempPdfPathForFinal, '_blank');
        }
      } else {
        if (this.isPdfTrueForfinal === true) {
          window.open(this.tempPdfPathForFinal, '_blank');
        } else if (this.isImgTrueForfinal === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun(pictureClasssName, buttonClasssName);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  /*-------------------------------------------------------------------------*/

  goToPreviousTab() {
    // tslint:disable-next-line:radix
    if (parseInt(this.production.buyId) > 0) {
      this.globalClass.setOrderId(this.production.buyId);
      this.orderTabService.send_OrderTab('AdjustmentTab');
    } else {
      this.orderTabService.send_OrderTab('AdjustmentTab');
    }
  }

  checkboxChange(objProduction) {
    try {
      const qId = objProduction.IssueDateId;
      const mId = objProduction.MediaAssetId;
      const mOrderId = objProduction.MediaOrderId;
      const advertiserId = objProduction.AdvertiserId;
      const agencyId = objProduction.AgencyId;

      const idx = this.selectedCheckbox.indexOf(qId);
      if (idx > -1) {
        this.selectedCheckbox.splice(idx, 1);

        this.selectedMediaOrderIds.forEach((data, key) => {
          // tslint:disable-next-line:radix
          if (parseInt(data) === parseInt(mOrderId)) {
            this.selectedMediaOrderIds.splice(key, 1);
            this.selectedAdvertiserIds.splice(key, 1);
            this.selectedAgencyIds.splice(key, 1);
          }
        });

        if (this.selectedCheckbox.length < 1) {
          this.tempGrossAmount = parseFloat('0').toFixed(2);
          // initPageControls();
          this.isShowDefaultGrossAdjustmentTable = true;
          this.isShowDefaultNetAdjustmentTable = true;
          this.production = {};

          this.production.buyId = this.globalClass.getOrderId();

          this.production.NewInd = '1';
          this.production.CreateExpectedInd = '0';
          this.production.AdvertiserAgencyInd = '1';

          this.originalFileImage = '';

          this.originalFileImageSrc = '';
          this.proofFileImageSrc = '';
          this.finalFileImageSrc = '';
          this.tempPdfPathForOriginal = '';
          this.tempPdfPathForProof = '';
          this.tempPdfPathForFinal = '';

          this.initSelect2('PickupMediaOrderId');
          this.initSelect2('MaterialContactId');
          this.initSelect2('ddlStatus');
          this.initSelect2('ddlSeparation');
          this.initSelect2('ddlPosition');

          this.buttonText = 'Add';
          this.setProductionNoClick = true;
        } else if (this.selectedCheckbox.length === 1) {
        
          this.setProductionNoClick = false;
          this.selectedMediaOrderId = this.selectedMediaOrderIds[0];
          this.selectedAdvertiserId = this.selectedAdvertiserIds[0];
          this.selectedAgencyId = this.selectedAgencyIds[0];
          //this.getOrdersNotInBuyer(this.production.buyId, this.selectedAdvertiserId);
         this.getOrdersNotInBuyer(this.tabOrderId, this.selectedAdvertiserId);
          const changeBillToValue = this.production.AdvertiserAgencyInd;
          console.log(changeBillToValue);
          this.getBillToContact(changeBillToValue);
          this.getMediaOrderProduction(this.selectedMediaOrderId);

          this.dvNew(false);
          this.dvPickup(true);

        } 
        else if (this.selectedCheckbox.length > 1) {
          this.production = {};

          this.production.buyId = this.globalClass.getOrderId();

          this.production.NewInd = '1';
          this.production.CreateExpectedInd = '0';
          this.production.AdvertiserAgencyInd = '1';

          this.initSelect2('PickupMediaOrderId');
          this.initSelect2('MaterialContactId');
          this.initSelect2('ddlStatus');
          this.initSelect2('ddlSeparation');
          this.initSelect2('ddlPosition');

          this.originalFileImage = '';
          this.originalFileImageSrc = '';
          this.proofFileImageSrc = '';
          this.finalFileImageSrc = '';
          this.tempPdfPathForOriginal = '';
          this.tempPdfPathForProof = '';
          this.tempPdfPathForFinal = '';
          this.buttonText = 'Add';
          this.setProductionNoClick = true;
        }
      } else {
        this.selectedCheckbox.push(qId);
        this.selectedMediaOrderIds.push(mOrderId);
        this.selectedAdvertiserIds.push(advertiserId);
        this.selectedAgencyIds.push(agencyId);
        if (this.selectedCheckbox.length > 1) {
          this.tempGrossAmount = parseFloat('0').toFixed(2);
          this.isShowDefaultGrossAdjustmentTable = true;
          this.isShowDefaultNetAdjustmentTable = true;
          this.grossAdAdjustments = [];
          this.netAdAdjustments = [];
        }

        if (this.selectedCheckbox.length === 1) {

    
          // this.production = {};
          // this.production.buyId = this.globalClass.getOrderId();
          this.setProductionNoClick = false;
          this.selectedMediaOrderId = this.selectedMediaOrderIds[0];
          this.selectedAdvertiserId = this.selectedAdvertiserIds[0];
          this.selectedAgencyId = this.selectedAgencyIds[0];
          console.log("call", this.tabOrderId, this.selectedAdvertiserId);
          this.getOrdersNotInBuyer(this.tabOrderId, this.selectedAdvertiserId);
        
        //  this.getOrdersNotInBuyer(this.production.buyId, this.selectedAdvertiserId);  
          const changeBillToValue = this.production.AdvertiserAgencyInd;
          this.getBillToContact(changeBillToValue);
          this.getMediaOrderProduction(this.selectedMediaOrderId);
          this.dvNew(false);
          this.dvPickup(true);
        }
      }

      if (this.selectedCheckbox.length > 1) {
        this.production = {};
        this.production.buyId = this.globalClass.getOrderId();

        this.production.NewInd = '1';
        this.production.CreateExpectedInd = '0';
        this.production.AdvertiserAgencyInd = '1';

        this.initSelect2('PickupMediaOrderId');
        this.initSelect2('MaterialContactId');
        this.initSelect2('ddlStatus');
        this.initSelect2('ddlSeparation');
        this.initSelect2('ddlPosition');

        this.originalFileImage = '';
        this.originalFileImageSrc = '';
        this.proofFileImageSrc = '';
        this.finalFileImageSrc = '';
        this.tempPdfPathForOriginal = '';
        this.tempPdfPathForProof = '';
        this.tempPdfPathForFinal = '';
        this.buttonText = 'Add';
        this.setProductionNoClick = true;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  setGlobalData() {
    this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
    this.globalClass.setMainAdvertisersData(this.advertisers);
    this.globalClass.setMainAgenciesData(this.agencies);
    this.globalClass.setMainBillingToContactsData(this.personData);
  }

  initSelect2(formcontrolName) {
    try {
      this.productionForm.controls[formcontrolName].setValue(null);
      let p = '';
      if (formcontrolName === 'PickupMediaOrderId') {
        p = 'Media asset | Issue date | Page number';
      } else if (formcontrolName === 'MaterialContactId') {
        p = 'Select contact';
      } else if (formcontrolName === 'ddlStatus') {
        p = 'Select status';
      } else if (formcontrolName === 'ddlSeparation') {
        p = 'Select separation';
      } else if (formcontrolName === 'ddlPosition') {
        p = 'Select position';
      }
      setTimeout(() => {
        $('#' + formcontrolName).attr('placeholder', p);
      }, 100);
    } catch (error) {
      console.log(error);
    }
  }

  // Get bill to contact
  getBillToContact(changeBillToValue) {
    console.log("billtcontact parm value: "+ changeBillToValue);
    try {
      let id = 0;
      const temp = changeBillToValue;

      if (temp === true) {
        changeBillToValue = 1;
      } else if (temp === false) {
        changeBillToValue = 0;
      }

      // tslint:disable-next-line:radix
      if (parseInt(changeBillToValue) === 0 || parseInt(changeBillToValue) === 1) {
        // tslint:disable-next-line:radix
        if (parseInt(changeBillToValue) === 1) {
          this.initSelect2('MaterialContactId');
          this.billingToContacts = [];
          id = this.selectedAdvertiserId;
          // tslint:disable-next-line:radix
        } else if (parseInt(changeBillToValue) === 0) {
          this.initSelect2('MaterialContactId');
          this.billingToContacts = [];
          id = this.selectedAgencyId;
        }
      }
      const partyData = this.partyAndOrganizationData;
      this.organizationWiseParty = [];
      // tslint:disable-next-line:radix
      if (id !== undefined && id > 0) {
        this.billingToContacts = [];
        this.tmpbillingToContacts=[];
        const agencyName = this.partyAndOrganizationData.find(x=>x.Id===id).OrganizationName;
        this.productionService.getAdvertiserAgency_BillToContact(this.websiteRoot,this.reqVerificationToken, 0,agencyName,'').subscribe(result => {
          if (result != null && result !== undefined && result !== '') {
            const ItemData = result.Items.$values;
            if (ItemData.length > 0) {
              for (const itemdatavalue of ItemData) {
                const type = itemdatavalue.$type;
                if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                  this.tmpbillingToContacts.push(itemdatavalue);
                }
              }
              
              if(this.tmpbillingToContacts.length>0)
              {
                this.billingToContacts = this.tmpbillingToContacts;
                this.globalClass.setMainBillingToContactsData(this.billingToContacts);
              }
            } 
          } else {
            this.hideLoader();
            this.billingToContacts = [];
          }
        }, error => {
          this.hideLoader();
          console.log(error);
        });          

        // partyData.forEach((person, key) => {
        //   if (person.PrimaryOrganization !== undefined) {
        //     if (person.PrimaryOrganization.OrganizationPartyId !== undefined) {
        //       const organizationI = person.PrimaryOrganization.OrganizationPartyId;
        //       if (id === organizationI) {
        //         this.organizationWiseParty.push(person);
        //       }
        //     }
        //   }
        // });
      } else {
        this.organizationWiseParty = [];
      }
      //this.billingToContacts = this.organizationWiseParty;
      console.log("PersonName", this.billingToContacts);
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  // Get media order production from database by media order id
  getMediaOrderProduction(mOrderId) {
    try {
      this.productionService.getById(mOrderId).subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            this.buttonText = 'Update';
            this.production = result.Data;

            if (this.production.NewPickupInd === false) {
              this.production.NewInd = '0';

              this.isShowNewDiv = false;
              this.isShowPickUpDiv = true;

              // disable
              this.dvNew(true);

              // enable
              this.dvPickup(false);

            } else if (this.production.NewPickupInd === true) {
              this.production.NewInd = '1';

              this.isShowNewDiv = true;
              this.isShowPickUpDiv = false;

              // enable
              this.dvNew(false);

              // disable
              this.dvPickup(true);
            }

            if (this.production.CreateExpectedInd === false) {
              this.production.CreateExpectedInd = '0';
            } else {
              this.production.CreateExpectedInd = '1';
            }

            if (this.production.AdvertiserAgencyInd === false) {
              this.production.AdvertiserAgencyInd = '0';
            } else {
              this.production.AdvertiserAgencyInd = '1';
            }

            let extensionOriginal = null;
            let extensionProof = null;
            let extensionFinal = null;

            this.production.buyId = this.globalClass.getOrderId();

            if (this.production.OriginalFile != null) {
              const tempOriginal = this.production.OriginalFile;
              const fileNameOriginal = tempOriginal.substring(tempOriginal.lastIndexOf('/')).split('/').pop();
              extensionOriginal = fileNameOriginal.substring(fileNameOriginal.lastIndexOf('.'));
              this.production.OrigFileName = fileNameOriginal;
            }

            if (this.production.ProofFile != null) {
              const tempProof = this.production.ProofFile;
              const fileNameProof = tempProof.substring(tempProof.lastIndexOf('/')).split('/').pop();
              extensionProof = fileNameProof.substring(fileNameProof.lastIndexOf('.'));
              this.production.ProofFileName = fileNameProof;
            }

            if (this.production.FinalFile != null) {
              const tempFinal = this.production.FinalFile;
              const fileNameFinal = tempFinal.substring(tempFinal.lastIndexOf('/')).split('/').pop();
              extensionFinal = fileNameFinal.substring(fileNameFinal.lastIndexOf('.'));
              this.production.FinalFileName = fileNameFinal;
            }

            if (extensionOriginal === null) {
              this.isPdfTrueForOriginal = false;
              this.isImgTrueForOriginal = false;
            } else {
              if (extensionOriginal === '.pdf') {
                this.isPdfTrueForOriginal = true;
                this.isImgTrueForOriginal = false;
                this.tempPdfPathForOriginal = this.production.OriginalFile;
              } else if (extensionOriginal === '.png'
                || extensionOriginal === '.jpg'
                || extensionOriginal === '.jpeg') {
                this.isPdfTrueForOriginal = false;
                this.isImgTrueForOriginal = true;
              } else {
                this.isPdfTrueForOriginal = true;
                this.isImgTrueForOriginal = false;
                this.tempPdfPathForOriginal = this.production.OriginalFile;
              }
            }

            if (extensionProof === null) {
              this.isPdfTrueForProof = false;
              this.isImgTrueForProof = false;
            } else {
              if (extensionProof === '.pdf') {
                this.isPdfTrueForProof = true;
                this.isImgTrueForProof = false;
                this.tempPdfPathForProof = this.production.ProofFile;
              } else if (extensionProof === '.png'
                || extensionProof === '.jpg'
                || extensionProof === '.jpeg') {
                this.isPdfTrueForProof = false;
                this.isImgTrueForProof = true;
              } else {
                this.isPdfTrueForProof = true;
                this.isImgTrueForProof = false;
                this.tempPdfPathForProof = this.production.ProofFile;
              }
            }

            if (extensionFinal === null) {
              this.isPdfTrueForfinal = false;
              this.isImgTrueForfinal = false;
            } else {
              if (extensionFinal === '.pdf') {
                this.isPdfTrueForfinal = true;
                this.isImgTrueForfinal = false;
                this.tempPdfPathForFinal = this.production.FinalFile;
              } else if (extensionFinal === '.png'
                || extensionFinal === '.jpg'
                || extensionFinal === '.jpeg') {
                this.isPdfTrueForfinal = false;
                this.isImgTrueForfinal = true;
              } else {
                this.isPdfTrueForfinal = true;
                this.isImgTrueForfinal = false;
                this.tempPdfPathForFinal = this.production.FinalFile;
              }
            }

            if (result.Data.MaterialContactId != null && result.Data.MaterialContactId !== ''
              && result.Data.MaterialContactId !== undefined) {
              this.production.MaterialContactId = (result.Data.MaterialContactId).toString();
              this.productionForm.controls['MaterialContactId'].setValue(this.production.MaterialContactId);
            }

            if (result.Data.MaterialExpectedDate !== undefined && result.Data.MaterialExpectedDate !== ''
              && result.Data.MaterialExpectedDate != null) {
              this.production.MaterialExpectedDate = this.globalClass.getDateInFormat(result.Data.MaterialExpectedDate);
            }

            this.originalFileImageSrc = this.production.OriginalFile;
            this.proofFileImageSrc = this.production.ProofFile;
            this.finalFileImageSrc = this.production.FinalFile;

            // PickupMediaOrderId DDL
            if (this.production.PickupMediaOrderId != null
              && this.production.PickupMediaOrderId !== ''
              && this.production.PickupMediaOrderId !== undefined) {
                
                  this.productionForm.controls['PickupMediaOrderId'].setValue(this.production.PickupMediaOrderId);
            }

            // PositionId DDL
            if (this.production.PositionId != null
              && this.production.PositionId !== ''
              && this.production.PositionId !== undefined) {
              const responseData = this.positions;
              const Positions = alasql('SELECT Name FROM ? AS add WHERE PositionId  = ?',
                // tslint:disable-next-line:radix
                [responseData, parseInt(this.production.PositionId)]);

              if (Positions !== undefined && Positions !== '' && Positions != null) {
              
                this.position = {};
                this.position.PositionId = this.production.PositionId;
                this.position.Name = Positions[0].Name;
              }
            }


            // SeparationId DLL
            if (this.production.SeparationId != null && this.production.SeparationId !== ''
              && this.production.SeparationId !== undefined) {
              const responseData = this.separations;
              const Separations = alasql('SELECT Name FROM ? AS add WHERE SeparationId  = ?',
                // tslint:disable-next-line:radix
                [responseData, parseInt(this.production.SeparationId)]);
              
              if (Separations !== undefined && Separations !== '' && Separations != null) {
                
                this.separation = {};
                this.separation.SeparationId = this.production.SeparationId;
                this.separation.Name = Separations[0].Name;
              }
            }

            try {
              // MaterialContactId DLL
              if (this.production.MaterialContactId != null && this.production.MaterialContactId !== ''
                && this.production.MaterialContactId !== undefined) {
                this.getBillToContact(this.production.AdvertiserAgencyInd);
                const responseData = this.billingToContacts;
                console.log("PersonName",responseData);
                
                const Contacts = alasql('SELECT PersonName FROM ? AS add WHERE Id  = ?',
                  [responseData, this.production.MaterialContactId.toString()]);
                if (Contacts != null && Contacts !== '' && Contacts !== undefined) {
                  this.materialContact = {};
                  this.materialContact.Id = this.production.MaterialContactId;
                  this.FullName = {};
                  this.FullName.FullName = Contacts[0].PersonName.FullName;
                  this.materialContact.PersonName =  this.FullName.FullName;
                  this.productionForm.controls['MaterialContactId'].setValue(this.production.MaterialContactId);
                }
              } else {
                this.initSelect2('MaterialContactId');
              }
            } catch (error) {
              console.log(error);
            }

            // ProductionStatusId DLL
            if (this.production.ProductionStatusId != null && this.production.ProductionStatusId !== ''
              && this.production.ProductionStatusId !== undefined) {
              const responseData = this.productionStatus;
              const cProductionStatus = alasql('SELECT Name FROM ? AS add WHERE ProductionStatusId  = ?',
                // tslint:disable-next-line:radix
                
                [responseData, parseInt(this.production.ProductionStatusId)]);
             
              if (cProductionStatus !== undefined && cProductionStatus !== '' && cProductionStatus != null) {
                this.productionStatusNew = {};
                this.productionStatusNew.ProductionStatusId = this.production.ProductionStatusId;
                this.productionStatusNew.Name = cProductionStatus[0].Name;
              }
            }
            this.initGrossandNetCost();
          } else {
            this.buttonText = 'Add';
          }
        }
      });
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }
}
