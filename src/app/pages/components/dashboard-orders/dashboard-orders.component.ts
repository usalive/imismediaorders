import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, Renderer } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { DataTableDirective } from 'angular-datatables';
import { GlobalClass } from '../../GlobalClass';
import { ToastrService } from 'ngx-toastr';
import * as alasql from 'alasql';
import { AppComponent } from '../../../app.component';

// Service
import { DashboardAdvertiserService } from '../../services/dashboardAdvertisers.service';
import { DashboardOrderService } from '../../services/dashboardOrders.service';
import { Subject } from 'rxjs';
import { OrderTabService } from '../../services/orderTab.service';
import { jsonpCallbackContext } from '@angular/common/http/src/module';

@Component({
  selector: 'asi-dashboard-orders',
  templateUrl: './dashboard-orders.component.html',
  styleUrls: ['./dashboard-orders.component.css']
})
export class DashboardOrdersComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  orderDatatableTrigger: Subject<any> = new Subject();
  orderDatatable: DataTables.Settings = {};
  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  apiBaseURL = '';

  columnNames: any = {};
  filter: any = {};
  postData: any = {};
  orderData = [];

  advertiserId: any = {};

  partyAndOrganizationData = [];
  advertisers = [];
  agencies = [];
  billingToContacts = [];
  mainAdvertiserId:any;

  customFilter = [];
  allMediaOrders = [];

  apiOrderData = [];

  selectedIssueMediaOrder = [];
  issueDateWiseOrder = [];

  selectedCheckbox = [];
  selectedIssueMediaOrderData = [];

  advertiserName = '';
  selectedAllDublicateOrders = false;
  newBuyId = null;
  advertisersIds:any = {};

  isLoaded = false;
  filterByAdvertiser = false;
  dataLoading = true;

  hasNext = true;
  offset = 0;

  isShowDefaultTable = false;

  dropDown: any = [];

  isOrderDataLoad_FirstTime = true;

  constructor
    (
    private toastr: ToastrService,
    private dashboardAdvertiserService: DashboardAdvertiserService,
    private dashboardOrderService: DashboardOrderService,
    private globalClass: GlobalClass,
    private renderer: Renderer,
    private orderTabService: OrderTabService,
    private appComponent: AppComponent
    ) { }

  ngOnInit() {

    this.advertiserName = '';
    this.globalClass.setBillingDetailsMediaOrderId('');
    // set enviroment data
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
    
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
      this.apiBaseURL = environment.ApiBaseURL;
    } catch (error) {
      console.log(error);
    }

    // set column names
    try {
      this.columnNames = {};
      this.columnNames.AdvertiserName = 'AdvertiserName';
      this.columnNames.AdvertiserId = 'AdvertiserId';
      this.columnNames.billToContact = 'billToContact';
      this.columnNames.ST_ID = 'ST_ID';
      this.columnNames.Status = 'OrderStatus';
      this.columnNames.FlightStartDate = 'FlightStartDate';
      this.columnNames.FlightEndDate = 'FlightEndDate';
      this.columnNames.GrossCost = 'GrossCost';
      this.columnNames.NetCost = 'NetCost';
      this.columnNames.BuyId = 'BuyId';
    } catch (error) {
      console.log(error);
    }

    try {
      this.filter = {};
      this.filter.AdverdtiserType = 'Contains';
      this.filter.BillingContactType = 'Contains';
      this.filter.Status = 'Proposal';
      this.filter.GrossAmountType = 'Equal';
      this.filter.NetAmountType = 'Equal';
      this.filter.BuyIdType = 'Equal';
    } catch (error) {
      console.log(error);
    }

    try {
      if(this.appComponent.t1) {
        this.globalClass.setAdvertiserID(this.appComponent.t1);
        this.appComponent.t1 = null;
      }
      this.advertiserId = this.globalClass.getAdvertiserID();
      this.mainAdvertiserId = this.globalClass.getAdvertiserID();
      console.log("advertisersIds:"+ this.advertiserId);
      if (this.advertiserId != null && this.advertiserId !== undefined && this.advertiserId !== '') {
        let aID = this.advertiserId;
        // tslint:disable-next-line:radix
        aID = parseInt(aID);
        // tslint:disable-next-line:radix
        if (parseInt(aID) > 0) {
          const searchFilter = { 'AdverdtiserId': aID };
          console.log("on page load:"+ JSON.stringify(searchFilter));
          if (this.globalClass.getMainPartyAndOrganizationData().length > 0 && this.globalClass.getMainAdvertisersData().length > 0
            && this.globalClass.getMainAgenciesData().length > 0 && this.globalClass.getMainBillingToContactsData().length > 0) {
            this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
            this.advertisers = this.globalClass.getMainAdvertisersData();
            this.agencies = this.globalClass.getMainAgenciesData();
            this.billingToContacts = this.globalClass.getMainBillingToContactsData();
            this.hideLoader()
            this.customFilter = [];
            this.searchByAdvertiserId(searchFilter, true);
          } else {
            this.filterByAdvertiser = true;
            this.getAllPartyDataThroughASI();
          }
        }
      } else {
        //this.filterByAdvertiser = false;
        //this.getAllPartyDataThroughASI();
        this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
        if (this.partyAndOrganizationData.length > 0) {
          const filterType = 'Equals';
          const fieldName = 'AdvertiserId';
          if (filterType === 'Equals') {
            const advertiserName = '';
            const filterWord = '';
            this.advertiserFilterNotbyEqual(this.globalClass.getDashboardAdvertiserIDs(), filterType, advertiserName, fieldName, null, null, null, null);
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  ngAfterViewInit(): void {
    this.isOrderDataLoad_FirstTime = false;
    this.orderDatatableTrigger.next();

    try {
      this.renderer.listenGlobal('document', 'click', (event) => {
        if (event.target.hasAttribute('view-editOrder-id')) {
          const editOrderData = event.target.getAttribute('view-editOrder-id');

          if (editOrderData !== undefined && editOrderData !== null && editOrderData !== '') {
            const SpiltData = editOrderData.toString().split(',');
            this.editOrder(SpiltData[0], SpiltData[1], SpiltData[2], SpiltData[3]);
          }
        } else if (event.target.hasAttribute('view-Duplicate-Order')) {
          const duplicate_Buy_Id = event.target.getAttribute('view-Duplicate-Order');
          this.duplicateIO(duplicate_Buy_Id);
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  ngOnDestroy() {
    this.isOrderDataLoad_FirstTime = true;
    this.orderDatatableTrigger.unsubscribe();
  }

  /// <summary>
  /// Get the list of all party data from ASI
  /// </summary>
  getAllPartyDataThroughASI() {
    try {
      if (this.globalClass.getMainPartyAndOrganizationData().length <= 0 && this.globalClass.getMainAdvertisersData().length <= 0
        && this.globalClass.getMainAgenciesData().length <= 0 && this.globalClass.getMainBillingToContactsData().length <= 0) {

        this.hasNext = true;
        this.offset = 0;
        this.resetData();

        this.getPartyThroughASI();

      } else {
        this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
        this.advertisers = this.globalClass.getMainAdvertisersData();
        this.agencies = this.globalClass.getMainAgenciesData();
        this.billingToContacts = this.globalClass.getMainBillingToContactsData();

        const data = [];
        this.hideLoader()
        this.displayOrderDatatableJquery(data);
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  getPartyThroughASI() {
    try {
      // tslint:disable-next-line:max-line-length
      this.dashboardAdvertiserService.getAllAdvertiser1(this.websiteRoot, this.baseUrl, this.reqVerificationToken, this.offset).subscribe(result => {

        if (result != null && result !== undefined && result !== '') {
          const itemData = result.Items.$values;

          if (itemData.length > 0) {
            for (const itemdatavalue of itemData) {
              this.partyAndOrganizationData.push(itemdatavalue);
              const type = itemdatavalue.$type;

              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                this.advertisers.push(itemdatavalue);
                this.agencies.push(itemdatavalue);
              } else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                this.billingToContacts.push(itemdatavalue);
              }
            }

            this.offset = result.Offset;
            const nextOffset = this.offset + 500;

            if (result.Count === 500) {
              this.offset = nextOffset;
              this.getPartyThroughASI();
            } else {
              this.hideLoader();
              this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
              this.globalClass.setMainAdvertisersData(this.advertisers);
              this.globalClass.setMainAgenciesData(this.agencies);
              this.globalClass.setMainBillingToContactsData(this.billingToContacts);

              if (this.filterByAdvertiser === true) {
                let searchFilter: any = {};
                if (this.advertiserId != null && this.advertiserId !== undefined && this.advertiserId !== '') {
                  let aID = this.advertiserId;
                  // tslint:disable-next-line:radix
                  aID = parseInt(aID);
                  // tslint:disable-next-line:radix
                  if (parseInt(aID) > 0) {
                    this.customFilter = [];
                    searchFilter = { 'AdverdtiserId': aID };
                  }
                }
                this.showLoader();
                this.searchByAdvertiserId(searchFilter, true);
                this.filterByAdvertiser = false;
              } else {
                const data = [];
                this.showLoader();
                this.displayOrderDatatableJquery(data);
              }
            }
          } else {
            this.hideLoader();
            this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
            this.globalClass.setMainAdvertisersData(this.advertisers);
            this.globalClass.setMainAgenciesData(this.agencies);
            this.globalClass.setMainBillingToContactsData(this.billingToContacts);

            if (this.filterByAdvertiser === true) {
              let searchFilter: any = {};
              if (this.advertiserId != null && this.advertiserId !== undefined && this.advertiserId !== '') {
                let aID = this.advertiserId;
                // tslint:disable-next-line:radix
                aID = parseInt(aID);
                // tslint:disable-next-line:radix
                if (parseInt(aID) > 0) {
                  this.customFilter = [];
                  searchFilter = { 'AdverdtiserId': aID };
                }
              }
              this.showLoader();
              this.searchByAdvertiserId(searchFilter, true);
              this.filterByAdvertiser = false;
            } else {
              const data = [];
              this.showLoader();
              this.displayOrderDatatableJquery(data);
            }
          }
        } else {
          this.hideLoader();
          this.resetData();
          this.globalClass.removeMainPartyAndOrganizationData();
          this.isShowDefaultTable = true;
          this.dataLoading = false;
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  resetData() {
    this.partyAndOrganizationData = [];
    this.advertisers = [];
    this.agencies = [];
    this.billingToContacts = [];
  }

  displayOrderDatatableJquery(filterOptions) {
    console.log("displayOrderDatatableJquery:"+ JSON.stringify(filterOptions));
    const tempPartyAndOrganizationData = this.partyAndOrganizationData;
    const tempimgURL = this.imgurl;
    const tempwebsiteURL = this.websiteRoot;
    try {
      const filterOptionsdata = [];
      if (filterOptions != null && filterOptions !== undefined && filterOptions !== '' && filterOptions.length > 0) {
        filterOptions.forEach((adata) => {
          if (adata.ColumnNo === 1) {
            filterOptionsdata['AdvertiserName'] = adata;
          } else if (adata.ColumnNo === 2) {
            filterOptionsdata['AdvertiserId'] = adata;
          } else if (adata.ColumnNo === 3) {
            filterOptionsdata['ST_ID'] = adata;
          } else if (adata.ColumnNo === 4) {
            filterOptionsdata['BuyId'] = adata;
          } else if (adata.ColumnNo === 10) {
            filterOptionsdata["MediaAsset"] = adata;
          } else if (adata.ColumnNo == 6) {
            filterOptionsdata['OrderStatus'] = adata;
          } else if (adata.ColumnNo === 7) {
            filterOptionsdata['FlightStartDate'] = adata;
          } else if (adata.ColumnNo === 8) {
            filterOptionsdata['FlightEndDate'] = adata;
          } else if (adata.ColumnNo === 9) {
            filterOptionsdata['GrossCost'] = adata;
          } else if (adata.ColumnNo === 10) {
            filterOptionsdata['NetCost'] = adata;
          } 
        });
      }
      this.orderDatatable = {
        pagingType: 'full_numbers',
        pageLength: 10,
        serverSide: true,
        processing: true,
        lengthChange: false,  
        searching: false,
        destroy: true,
        ordering: false,
        orderMulti: false,
        scrollCollapse: false,
        ajax: (dataTablesParameters: any, callback) => {
          this.showLoader();
          this.dashboardOrderService.getOrdersBuyIDWise(dataTablesParameters, filterOptionsdata).subscribe(resp => {
            //console.log("datatableResponse:"+ JSON.stringify(resp.Data));
            this.hideLoader();
            callback({
              recordsTotal: resp.RecordsTotal,
              recordsFiltered: resp.RecordsFiltered,
              data: resp.Data
            });
          }, error => {
            this.hideLoader();
            console.log(error);
          });
        },
        columns: [
          {
            name: '',
            orderable: false,
            // tslint:disable-next-line:no-shadowed-variable
            render: function (data: any, type: any, row: any) {
              const advertiserId = row.AdvertiserId;
              const agencyId = row.AgencyId;
              const stId = row.ST_ID;
              const buyId = row.BuyId;
              const customHTML = '<a view-editOrder-id="' + advertiserId + ',' + agencyId + ',' + stId + ',' + buyId + '"' +
                ' href="javascript:" title="Edit" style="width:75px;text-align:center;">' +
                '<img view-editOrder-id="' + advertiserId + ',' + agencyId + ',' + stId + ',' + buyId + '"' +
                ' src="' + tempwebsiteURL + '/' + tempimgURL + '/edit-icon.png" alt="" /></a>';
              return customHTML;
            }
          },

          {
            name: 'Advertiser',
            orderable: false,
            // tslint:disable-next-line:no-shadowed-variable
            render: function (data: any, type: any, row: any) {
              let advertiserName = '';
              const advertiserId = row.AdvertiserId;
              const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id LIKE ?', [tempPartyAndOrganizationData, advertiserId]);
              if (objAdvertiser.length > 0) {
                advertiserName = objAdvertiser[0].Name;
              } else {
                advertiserName = '';
              }
              return advertiserName;
            }
          },

          {
            name: 'Bill To Id',
            orderable: false,
            data: 'AdvertiserId'
          },

          // {
          //   name: 'Billing Contact',
          //   orderable: false,
          //   // tslint:disable-next-line:no-shadowed-variable
          //   render: function (data: any, type: any, row: any) {
          //     let billToContactName = '';
          //     const contactId = row.ST_ID;
          //     const objContact = alasql('SELECT * FROM ? AS add WHERE Id LIKE ?', [tempPartyAndOrganizationData, contactId]);
          //     if (objContact.length > 0) {
          //       billToContactName = objContact[0].Name;
          //     } else {
          //       billToContactName = '';
          //     }
          //     return billToContactName;
          //   },
          // },

          {
            name: 'Ship To Id',
            orderable: false,
            data: 'ST_ID'
          },

          {
            name: 'Buy Id',
            orderable: false,
            data: 'BuyId'
          },

          {
            name: 'Media Asset',
            orderable: false,
            render: function (data, type, row) {
              //console.log("Media Asset:"+ row.MediaAssets);
              var customHTML = "";
              if(row.MediaAssets !==null)
              {
                var temp = row.MediaAssets.split(', ');
                var mediaAsset = [];
                var mediaAssetCount = [];
                var c = 0;
                temp.forEach((value, key) => {
                  if (key == 0) {
                    mediaAsset[c] = value;
                    mediaAssetCount[c] = 1;
                    c++;
                  }
                  else if (mediaAsset.indexOf(value) >= 0) {
                    mediaAssetCount[mediaAsset.indexOf(value)] = mediaAssetCount[mediaAsset.indexOf(value)] + 1;
                  }
                  else {
                    mediaAsset[c] = value;
                    mediaAssetCount[c] = 1;
                    c++;
                  }
  
                });
                
                mediaAsset.forEach((v, k) => {
                  if (k == 0) {
                    customHTML = '<div style="color:#598edc;">' + v + ' (' + mediaAssetCount[k] + ')</div>';
                  }
                  else {
                    customHTML = customHTML + '<div style="color:#598edc;">' + v + ' (' + mediaAssetCount[k] + ')</div>';
                  }
                });
              }
              return customHTML;
            },
          },

          {
            name: 'Status',
            orderable: false,
            // tslint:disable-next-line:no-shadowed-variable
            render: function (data: any, type: any, row: any) {
              let pNoOfOrders = 0;
              let rNoOfOrders = 0;
              let cNoOfOrders = 0;
              if (row.OrderStatus.length > 0) {
                const status1 = row.OrderStatus[0].Status;
                if (status1 === 'Proposal') {
                  pNoOfOrders = row.OrderStatus[0].NoOfOrders;
                }
                if (status1 === 'Run') {
                  rNoOfOrders = row.OrderStatus[0].NoOfOrders;
                }
                if (status1 === 'Cancel') {
                  cNoOfOrders = row.OrderStatus[0].NoOfOrders;
                }

                const status2 = row.OrderStatus[1].Status;
                if (status2 === 'Proposal') {
                  pNoOfOrders = row.OrderStatus[1].NoOfOrders;
                }
                if (status2 === 'Run') {
                  rNoOfOrders = row.OrderStatus[1].NoOfOrders;
                }
                if (status2 === 'Cancel') {
                  cNoOfOrders = row.OrderStatus[1].NoOfOrders;
                }

                const status3 = row.OrderStatus[2].Status;
                if (status3 === 'Proposal') {
                  pNoOfOrders = row.OrderStatus[2].NoOfOrders;
                }
                if (status3 === 'Run') {
                  rNoOfOrders = row.OrderStatus[2].NoOfOrders;
                }
                if (status3 === 'Cancel') {
                  cNoOfOrders = row.OrderStatus[2].NoOfOrders;
                }
              }
              const customHTML = '<div style="color:#598edc;">Proposal (' + pNoOfOrders + ')</div>' +
                '<div style="color:#53ba85;">Run  (' + rNoOfOrders + ')</div>' +
                '<div style="color:#fe7271;">Cancel (' + cNoOfOrders + ')</div>';
              return customHTML;
            },
          },

          {
            name: 'Run Date From',
            orderable: false,
            // tslint:disable-next-line:no-shadowed-variable
            render: function (data: any, type: any, row: any) {
              let d, m, newdate;
              try {
                const dateObj = new Date(row.FlightStartDate);
                m = dateObj.getMonth() + 1;

                if (m.toString().length === 1) {
                  m = '0' + m;
                }
                d = dateObj.getDate();
                if (d.toString().length === 1) {
                  d = '0' + d;
                }
                const year = dateObj.getFullYear();
                newdate = m + '/' + d + '/' + year;
              } catch (error) {
                console.log(error);
              }
              return newdate;
            },
          },

          {
            name: 'Run Date To',
            orderable: false,
            // tslint:disable-next-line:no-shadowed-variable
            render: function (data: any, type: any, row: any) {
              let d, m, newdate;
              try {
                const dateObj = new Date(row.FlightEndDate);
                m = dateObj.getMonth() + 1;

                if (m.toString().length === 1) {
                  m = '0' + m;
                }
                d = dateObj.getDate();
                if (d.toString().length === 1) {
                  d = '0' + d;
                }
                const year = dateObj.getFullYear();
                newdate = m + '/' + d + '/' + year;
              } catch (error) {
                console.log(error);
              }
              return newdate;
            },
          },

          {
            name: 'Gross Cost',
            orderable: false,
            // tslint:disable-next-line:no-shadowed-variable
            render: function (data: any, type: any, row: any) {
              const customHTML = row.GrossCost.toFixed(2);
              return customHTML;
            },
          },

          {
            name: 'Net Cost',
            orderable: false,
            // tslint:disable-next-line:no-shadowed-variable
            render: function (data: any, type: any, row: any) {
              const customHTML = row.NetCost.toFixed(2);
              return customHTML;
            },
          },

          {
            name: 'Duplicate',
            orderable: false,
            // tslint:disable-next-line:no-shadowed-variable
            render: function (data: any, type: any, row: any) {
              const customHTML = '<a title="duplicate" data-toggle="modal" '
                + ' data-target="#duplicateIOModal" view-Duplicate-Order=' + row.BuyId
                + ' href="javascript:" style="width:75px;outline:none">' +
                '<img src="' + tempwebsiteURL + '/' + tempimgURL + '/copy-icon.png" alt="duplicate"' +
                ' view-Duplicate-Order="' + row.BuyId + '"/> </a>';
              return customHTML;
            },
          }]
      };

      if (!this.isOrderDataLoad_FirstTime) {
        this.hideLoader();
        setTimeout(() => {
          try {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.orderDatatableTrigger.next();
            });
          } catch (error) {
            console.log(error);
          }
        }, 1000);
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /// <summary>
  /// Search data by filters
  /// </summary>
  searchbyAdvertiser(objfilter) {
    console.log("searchbyAdvertiser:"+ JSON.stringify(objfilter));
    try {
      if (this.partyAndOrganizationData.length > 0) {
        this.allMediaOrders = [];
        const objAdvertiserIds = [];
        const advertiserName = objfilter.AdverdtiserName;
        const filterType = objfilter.AdverdtiserType;
        const fieldName = 'AdvertiserId';
        if (filterType === 'Equals') {
          // tslint:disable-next-line:no-shadowed-variable
          const filterWord = advertiserName;
          // tslint:disable-next-line:no-shadowed-variable
          const objAdvertisers = alasql('SELECT * FROM ? AS add WHERE Name = ?', [this.partyAndOrganizationData, filterWord]);
          objAdvertisers.forEach((data, key) => {
            // tslint:disable-next-line:radix
            const id = parseInt(data.Id);
            objAdvertiserIds.push(id);
          });

          this.showLoader()
          this.advertiserFilterNotbyEqual(objAdvertiserIds, filterType, advertiserName, fieldName,
            undefined, undefined, undefined, undefined);

        } else if (filterType === 'Contains') {
          // tslint:disable-next-line:no-shadowed-variable
          const filterWord = '%' + advertiserName + '%';
          // tslint:disable-next-line:no-shadowed-variable
          const objAdvertisers = alasql('SELECT * FROM ? AS add WHERE Name LIKE ?', [this.partyAndOrganizationData, filterWord]);
          objAdvertisers.forEach((data, key) => {
            // tslint:disable-next-line:radix
            const id = parseInt(data.Id);
            objAdvertiserIds.push(id);
          });
          console.log("searchbyAdvertiser:filterids:"+ objAdvertiserIds);
          this.showLoader()
          this.advertiserFilterNotbyEqual(objAdvertiserIds, filterType, advertiserName, fieldName,
            undefined, undefined, undefined, undefined);

        } else if (filterType === 'StartsWith') {
          const filterWord = advertiserName + '%';
          const objAdvertisers = alasql('SELECT * FROM ? AS add WHERE Name LIKE ?', [this.partyAndOrganizationData, filterWord]);
          objAdvertisers.forEach((data, key) => {
            // tslint:disable-next-line:radix
            const id = parseInt(data.Id);
            objAdvertiserIds.push(id);
          });

          this.showLoader()
          this.advertiserFilterNotbyEqual(objAdvertiserIds, filterType, advertiserName, fieldName,
            undefined, undefined, undefined, undefined);

        } else if (filterType === 'EndsWith') {
          const filterWord = '%' + advertiserName;
          const objAdvertisers = alasql('SELECT * FROM ? AS add WHERE Name LIKE ?', [this.partyAndOrganizationData, filterWord]);
          objAdvertisers.forEach((data, key) => {
            // tslint:disable-next-line:radix
            const id = parseInt(data.Id);
            objAdvertiserIds.push(id);
          });
          this.showLoader()
          this.advertiserFilterNotbyEqual(objAdvertiserIds, filterType, advertiserName, fieldName,
            undefined, undefined, undefined, undefined);
        }
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  searchByAdvertiserId(objfilter, isFromDashBoardAdvertiser) {
    try {
      if (this.partyAndOrganizationData.length > 0) {
        this.allMediaOrders = [];
        const objAdvertiserIds = [];
        const advertiserId = objfilter.AdverdtiserId;
        const filterType = 'Equals';
        const fieldName = 'AdvertiserId';
        if (filterType === 'Equals') {
          const advertiserName = advertiserId;
          const filterWord = advertiserId;
          const objAdvertisers = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.partyAndOrganizationData, filterWord]);
          objAdvertiserIds.push(advertiserId);
          if (isFromDashBoardAdvertiser !== true) {
            this.showLoader();
          }
          this.advertiserFilterNotbyEqual(objAdvertiserIds, filterType, advertiserName, fieldName, null, null, null, null);
        }
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  searchByBillingContact(objfilter) {
    try {
      if (this.partyAndOrganizationData.length > 0) {
        this.allMediaOrders = [];

        const objContactIds = [];
        const contactName = objfilter.BillingContactName;
        const filterType = objfilter.BillingContactType;
        const fieldName = 'ST_IDName';

        if (filterType === 'Equals') {
          // tslint:disable-next-line:no-shadowed-variable
          const filterWord = contactName;
          // tslint:disable-next-line:no-shadowed-variable
          const objAdvertisers = alasql('SELECT * FROM ? AS add WHERE Name LIKE ?', [this.partyAndOrganizationData, filterWord]);

          objAdvertisers.forEach((data, key) => {
            // tslint:disable-next-line:radix
            const id = parseInt(data.Id);
            objContactIds.push(id);
          });

          this.showLoader()
          this.advertiserFilterNotbyEqual(objContactIds, filterType, contactName, fieldName,
            undefined, undefined, undefined, undefined);

        } else if (filterType === 'Contains') {
          // tslint:disable-next-line:no-shadowed-variable
          const filterWord = '%' + contactName + '%';
          // tslint:disable-next-line:no-shadowed-variable
          const objAdvertisers = alasql('SELECT * FROM ? AS add WHERE Name LIKE ?', [this.partyAndOrganizationData, filterWord]);

          objAdvertisers.forEach((data, key) => {
            // tslint:disable-next-line:radix
            const id = parseInt(data.Id);
            objContactIds.push(id);
          });

          this.showLoader()
          this.advertiserFilterNotbyEqual(objContactIds, filterType, contactName, fieldName,
            undefined, undefined, undefined, undefined);

        } else if (filterType === 'StartsWith') {
          // tslint:disable-next-line:no-shadowed-variable
          const filterWord = contactName + '%';
          // tslint:disable-next-line:no-shadowed-variable
          const objAdvertisers = alasql('SELECT * FROM ? AS add WHERE Name LIKE ?', [this.partyAndOrganizationData, filterWord]);

          objAdvertisers.forEach((data, key) => {
            // tslint:disable-next-line:radix
            const id = parseInt(data.Id);
            objContactIds.push(id);
          });

          this.showLoader()
          this.advertiserFilterNotbyEqual(objContactIds, filterType, contactName, fieldName,
            undefined, undefined, undefined, undefined);
        } else if (filterType === 'EndsWith') {
          const filterWord = '%' + contactName;
          const objAdvertisers = alasql('SELECT * FROM ? AS add WHERE Name LIKE ?', [this.partyAndOrganizationData, filterWord]);
          objAdvertisers.forEach((data, key) => {
            // tslint:disable-next-line:radix
            const id = parseInt(data.Id);
            objContactIds.push(id);
          });

          this.showLoader()
          this.advertiserFilterNotbyEqual(objContactIds, filterType, contactName, fieldName,
            undefined, undefined, undefined, undefined);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  searchByBillingContactId(objfilter) {
    try {
      if (this.partyAndOrganizationData.length > 0) {
        this.allMediaOrders = [];

        const objContactIds = [];
        const contactId = objfilter.BillingContactId;
        const filterType = 'Equals';
        const fieldName = 'ST_ID';

        if (filterType === 'Equals') {
          const advertiserName = contactId;
          const filterWord = contactId;
          const objAdvertisers = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.partyAndOrganizationData, filterWord]);

          objContactIds.push(contactId);
          this.showLoader()
          this.advertiserFilterNotbyEqual(objContactIds, filterType, advertiserName, fieldName,
            undefined, undefined, undefined, undefined);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  searchByBuyId(objfilter) {
    try {
      if (this.partyAndOrganizationData.length > 0) {
        this.allMediaOrders = [];

        const objContactIds = [];
        const amountType = objfilter.BuyIdType;
        const amount = objfilter.BuyId;
        const fieldName = 'BuyId';

        this.showLoader()
        this.advertiserFilterNotbyEqual(objContactIds, '', '', fieldName, '', '', amountType, amount);
      }
    } catch (error) {
      console.log(error);
    }
  }

  searchByStatus(objfilter) {
    try {
      if (this.partyAndOrganizationData.length > 0) {
        this.allMediaOrders = [];
        const objContactIds = [];
        const status = objfilter.Status;
        const filterType = 'Contains';
        const fieldName = 'OrderStatus';
        if (filterType === 'Contains') {
          const filterWord = status;
          this.showLoader()
          this.advertiserFilterNotbyEqual(objContactIds, filterType, status, fieldName,
            undefined, undefined, undefined, undefined);
        }
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  searchByFlightStartDate(objfilter) {
    try {
      if (this.partyAndOrganizationData.length > 0) {
        this.allMediaOrders = [];
        const objContactIds = [];

        let fDate = '';
        let tDate = '';

        if (objfilter.fFromDate !== undefined && objfilter.fFromDate !== '' && objfilter.fFromDate !== null) {
          fDate = this.globalClass.getDateInFormat(objfilter.fFromDate);
        }

        if (objfilter.fToDate !== undefined && objfilter.fToDate !== '' && objfilter.fToDate !== null) {
          tDate = this.globalClass.getDateInFormat(objfilter.fToDate);
        }

        const fieldName = 'FlightStartDate';
        this.showLoader()
        this.advertiserFilterNotbyEqual(objContactIds, '', '', fieldName, fDate, tDate, undefined, undefined);
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  searchByFlightEndDate(objfilter) {
    try {
      if (this.partyAndOrganizationData.length > 0) {
        this.allMediaOrders = [];
        const objContactIds = [];

        let fDate = '';
        let tDate = '';

        if (objfilter.tFromDate !== undefined && objfilter.tFromDate !== null && objfilter.tFromDate !== '') {
          fDate = this.globalClass.getDateInFormat(objfilter.tFromDate);
        }

        if (objfilter.tToDate !== undefined && objfilter.tToDate !== null && objfilter.tToDate !== '') {
          tDate = this.globalClass.getDateInFormat(objfilter.tToDate);
        }

        const fieldName = 'FlightEndDate';
        this.showLoader()
        this.advertiserFilterNotbyEqual(objContactIds, '', '', fieldName, fDate, tDate, undefined, undefined);
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  searchByGrossAmount(objfilter) {
    try {
      if (this.partyAndOrganizationData.length > 0) {
        this.allMediaOrders = [];
        const objContactIds = [];
        const amountType = objfilter.GrossAmountType;
        const amount = objfilter.GrossAmount;
        const fieldName = 'GrossCost';
        this.showLoader()
        this.advertiserFilterNotbyEqual(objContactIds, '', '', fieldName, '', '', amountType, amount);
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  searchByNetAmount(objfilter) {
    try {
      if (this.partyAndOrganizationData.length > 0) {
        this.allMediaOrders = [];

        const objContactIds = [];
        const amountType = objfilter.NetAmountType;
        const amount = objfilter.NetAmount;
        const fieldName = 'NetCost';

        this.showLoader()
        this.advertiserFilterNotbyEqual(objContactIds, '', '', fieldName, '', '', amountType, amount);
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Get index number
  /// </summary>
  getIndexNo(fieldName, tableColumnName, columnNo) {
    try {
      let index = -1;
      this.customFilter.forEach((data, key) => {
        if (data.FieldName === fieldName) {
          if (tableColumnName === 'AdvertiserName') {
            if (data.ColumnNo === 1) {
              if (data.FilterOption.FilterIsArray === true) {
                index = key;
              }
            }
          } else if (tableColumnName === 'AdvertiserId') {
            if (data.FilterOption.FilterIsArray === true) {
              if (data.ColumnNo === 2) {
                index = key;
              }
            }
          } else if (tableColumnName === 'ST_IDName') {
            if (data.ColumnNo === 1) {
              if (data.FilterOption.FilterIsArray === true) {
                index = key;
              }
            }
          } else if (tableColumnName === 'ST_ID') {
            if (data.ColumnNo === 3) {
              if (data.FilterOption.FilterIsArray === true) {
                index = key;
              }
            }
          }
          else if (tableColumnName === 'BuyId') {
            if (data.ColumnNo === 4) {
              if (data.FilterOption.FilterIsArray === true) {
                index = key;
              }
            }
          }
          else if (tableColumnName === 'GrossCost') {
            if (data.ColumnNo === 9) {
              if (data.FilterOption.FilterIsArray === true) {
                index = key;
              }
            }
          }
          else if (tableColumnName === 'NetCost') {
            if (data.ColumnNo === 10) {
              if (data.FilterOption.FilterIsArray === true) {
                index = key;
              }
            }
          } else if (tableColumnName === undefined) {
            index = key;
          } else {
            index = key;
          }
        }
      });
      return index;
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  advertiserFilterNotbyEqual(objIds, filterType, name, fieldName, fDate, tDate, amountType, amount) {
    console.log("advertiserFilterNotbyEqual:"+ objIds);
    try {
      console.log("advertiserFilterNotbyEqual-fieldName:"+ fieldName);
      let fieldNameVar = fieldName;
      if (fieldNameVar === 'AdvertiserName') {
        if (filterType !== 'Equals') {
          fieldNameVar = 'AdvertiserName';
          if (this.customFilter.length > 0) {
            if (fieldNameVar === 'AdvertiserName') {
              const index = this.getIndexNo(fieldNameVar, 'AdvertiserName', undefined);
              if (index !== -1) {
                const obj = this.customFilter[index];
                const isTrue = obj.FilterOption.FilterIsArray;
               let objFilterOption: any = {};
               objFilterOption.FilterIsArray = true;
               objFilterOption.Ids = objIds;
               this.customFilter[index].FilterOption = objFilterOption;
              } else {
                let objFilterOption: any = {};
                let objSearch: any = {};
                objFilterOption.FilterIsArray = true;
                objFilterOption.Ids = objIds;
                objSearch.FieldName = fieldNameVar;
                objSearch.ColumnNo = 1;
                objSearch.FilterOption = objFilterOption;
                this.customFilter.push(objSearch);
              }
            }
          } else {
            let objFilterOption: any = {};
            let objSearch: any = {};
            objFilterOption.FilterIsArray = true;
            objFilterOption.Ids = objIds;
            objSearch.FieldName = fieldNameVar;
            objSearch.ColumnNo = 1;
            objSearch.FilterOption = objFilterOption;
            this.customFilter.push(objSearch);
          }
          this.getAllOrdersbycustomSearch();
        }
      } else if (fieldNameVar === 'AdvertiserId') {
        if (this.customFilter.length > 0) {
          if (fieldNameVar === 'AdvertiserId') {
            const index = this.getIndexNo(fieldNameVar, 'AdvertiserId', undefined);
            if (index !== -1) {
              const obj = this.customFilter[index];
              const isTrue = obj.FilterOption.FilterIsArray;

              if (isTrue === true) {
                // tslint:disable-next-line:prefer-const
                let objFilterOption: any = {};
                objFilterOption.FilterIsArray = true;
                objFilterOption.Ids = objIds;
                this.customFilter[index].FilterOption = objFilterOption;
              } else {
                // tslint:disable-next-line:prefer-const
                let objFilterOption: any = {};
                objFilterOption.FilterIsArray = true;
                objFilterOption.Ids = objIds;
                this.customFilter[index].FilterOption = objFilterOption;
              }

            } else {
              // tslint:disable-next-line:prefer-const
              let objFilterOption: any = {};
              // tslint:disable-next-line:prefer-const
              let objSearch: any = {};

              objFilterOption.FilterIsArray = true;
              objFilterOption.Ids = objIds;

              objSearch.FieldName = fieldNameVar;
              objSearch.ColumnNo = 2;
              objSearch.FilterOption = objFilterOption;

              this.customFilter.push(objSearch);
            }
          }
        } else {
          // tslint:disable-next-line:prefer-const
          let objFilterOption: any = {};
          // tslint:disable-next-line:prefer-const
          let objSearch: any = {};

          objFilterOption.FilterIsArray = true;
          objFilterOption.Ids = objIds;

          objSearch.FieldName = fieldNameVar;
          objSearch.ColumnNo = 2;
          objSearch.FilterOption = objFilterOption;

          this.customFilter.push(objSearch);
        }

        this.getAllOrdersbycustomSearch();
      } else if (fieldNameVar === 'ST_IDName') {
        if (filterType !== 'Equals') {
          fieldNameVar = 'ST_ID';
          if (this.customFilter.length > 0) {
            const index = this.getIndexNo(fieldNameVar, 'ST_IDName', undefined);
            if (index !== -1) {
              const fName = this.customFilter[index].FieldName;
              if (fName === 'ST_ID') {
                const obj = this.customFilter[index];
                const isTrue = obj.FilterOption.FilterIsArray;
                if (isTrue === true) {
                  // tslint:disable-next-line:prefer-const
                  let objFilterOption: any = {};
                  objFilterOption.FilterIsArray = true;
                  objFilterOption.Ids = objIds;
                  this.customFilter[index].FilterOption = objFilterOption;
                } else {
                  // tslint:disable-next-line:prefer-const
                  let objFilterOption: any = {};
                  objFilterOption.FilterIsArray = true;
                  objFilterOption.Ids = objIds;
                  this.customFilter[index].FilterOption = objFilterOption;
                }
              }
            } else {
              // tslint:disable-next-line:prefer-const
              let objFilterOption: any = {};
              // tslint:disable-next-line:prefer-const
              let objSearch: any = {};

              objFilterOption.FilterIsArray = true;
              objFilterOption.Ids = objIds;

              objSearch.FieldName = fieldNameVar;
              objSearch.ColumnNo = 3;
              objSearch.FilterOption = objFilterOption;

              this.customFilter.push(objSearch);
            }
          } else {
            // tslint:disable-next-line:prefer-const
            let objFilterOption: any = {};
            // tslint:disable-next-line:prefer-const
            let objSearch: any = {};

            objFilterOption.FilterIsArray = true;
            objFilterOption.Ids = objIds;

            objSearch.FieldName = fieldNameVar;
            objSearch.ColumnNo = 3;
            objSearch.FilterOption = objFilterOption;

            this.customFilter.push(objSearch);
          }
          //   this.showLoader()
          this.getAllOrdersbycustomSearch();

        } else {
          fieldNameVar = 'ST_ID';
          if (this.customFilter.length > 0) {
            if (fieldNameVar === 'ST_ID') {
              const index = this.getIndexNo(fieldNameVar, 'ST_IDName', undefined);
              if (index !== -1) {
                const obj = this.customFilter[index];
                const isTrue = obj.FilterOption.FilterIsArray;
                if (isTrue === true) {
                  // tslint:disable-next-line:prefer-const
                  let objFilterOption: any = {};
                  objFilterOption.FilterIsArray = true;
                  objFilterOption.Ids = objIds;
                  this.customFilter[index].FilterOption = objFilterOption;
                } else {
                  // tslint:disable-next-line:prefer-const
                  let objFilterOption: any = {};
                  objFilterOption.FilterIsArray = true;
                  objFilterOption.Ids = objIds;
                  this.customFilter[index].FilterOption = objFilterOption;
                }
              } else {
                // tslint:disable-next-line:prefer-const
                let objFilterOption: any = {};
                // tslint:disable-next-line:prefer-const
                let objSearch: any = {};

                objFilterOption.FilterIsArray = true;
                objFilterOption.Ids = objIds;

                objSearch.FieldName = fieldNameVar;
                objSearch.ColumnNo = 3;
                objSearch.FilterOption = objFilterOption;

                this.customFilter.push(objSearch);
              }
            }
          } else {
            // tslint:disable-next-line:prefer-const
            let objFilterOption: any = {};
            // tslint:disable-next-line:prefer-const
            let objSearch: any = {};

            objFilterOption.FilterIsArray = true;
            objFilterOption.Ids = objIds;

            objSearch.FieldName = fieldNameVar;
            objSearch.ColumnNo = 3;
            objSearch.FilterOption = objFilterOption;

            this.customFilter.push(objSearch);
          }
          this.getAllOrdersbycustomSearch();
        }
      } else if (fieldNameVar === 'ST_ID') {
        if (this.customFilter.length > 0) {
          if (fieldNameVar === 'ST_ID') {
            const index = this.getIndexNo(fieldNameVar, 'ST_ID', undefined);
            if (index !== -1) {
              const obj = this.customFilter[index];
              const isTrue = obj.FilterOption.FilterIsArray;
              if (isTrue === true) {
                // tslint:disable-next-line:prefer-const
                let objFilterOption: any = {};
                objFilterOption.FilterIsArray = true;
                objFilterOption.Ids = objIds;
                this.customFilter[index].FilterOption = objFilterOption;
              } else {
                // tslint:disable-next-line:prefer-const
                let objFilterOption: any = {};
                objFilterOption.FilterIsArray = true;
                objFilterOption.Ids = objIds;
                this.customFilter[index].FilterOption = objFilterOption;
              }
            } else {
              // tslint:disable-next-line:prefer-const
              let objFilterOption: any = {};
              // tslint:disable-next-line:prefer-const
              let objSearch: any = {};

              objFilterOption.FilterIsArray = true;
              objFilterOption.Ids = objIds;

              objSearch.FieldName = fieldNameVar;
              objSearch.ColumnNo = 3;
              objSearch.FilterOption = objFilterOption;

              this.customFilter.push(objSearch);
            }
          }
        } else {
          // tslint:disable-next-line:prefer-const
          let objFilterOption: any = {};
          // tslint:disable-next-line:prefer-const
          let objSearch: any = {};

          objFilterOption.FilterIsArray = true;
          objFilterOption.Ids = objIds;

          objSearch.FieldName = fieldNameVar;
          objSearch.ColumnNo = 3;
          objSearch.FilterOption = objFilterOption;

          this.customFilter.push(objSearch);
        }
        this.getAllOrdersbycustomSearch();
      } else if (fieldNameVar === 'OrderStatus') {
        if (this.customFilter.length > 0) {
          const index = this.getIndexNo(fieldNameVar, undefined, undefined);
          if (index !== -1) {
            const fName = this.customFilter[index].FieldName;
            const obj = this.customFilter[index];
            const isTrue = obj.FilterOption.FilterIsNumber;
            if (isTrue === true) {
              this.customFilter[index].FilterOption.Type = filterType;
              this.customFilter[index].FilterOption.value = name;
            } else {
              this.customFilter[index].FilterOption.Type = filterType;
              this.customFilter[index].FilterOption.value = name;
            }
          } else {
            // tslint:disable-next-line:prefer-const
            let objFilterOption: any = {};
            // tslint:disable-next-line:prefer-const
            let objSearch: any = {};

            objFilterOption.Type = filterType;
            objFilterOption.value = name;

            objSearch.FieldName = fieldNameVar;
            objSearch.ColumnNo = 6;
            objSearch.FilterOption = objFilterOption;

            this.customFilter.push(objSearch);
          }
        } else {
          // tslint:disable-next-line:prefer-const
          let objFilterOption: any = {};
          // tslint:disable-next-line:prefer-const
          let objSearch: any = {};

          objFilterOption.Type = filterType;
          objFilterOption.value = name;

          objSearch.FieldName = fieldNameVar;
          objSearch.ColumnNo = 6;
          objSearch.FilterOption = objFilterOption;

          this.customFilter.push(objSearch);
        }
        this.getAllOrdersbycustomSearch();
      } else if (fieldNameVar === 'FlightStartDate') {
        if (this.customFilter.length > 0) {
          const index = this.getIndexNo(fieldNameVar, undefined, undefined);
          if (index !== -1) {
            const fName = this.customFilter[index].FieldName;
            const obj = this.customFilter[index];
            const isTrue = obj.FilterOption.FilterIsDate;
            if (isTrue === true) {
              this.customFilter[index].FilterOption.ToDate = tDate;
              this.customFilter[index].FilterOption.FromDate = fDate;
            } else {
              this.customFilter[index].FilterOption.ToDate = tDate;
              this.customFilter[index].FilterOption.FromDate = fDate;
            }
          } else {
            // tslint:disable-next-line:prefer-const
            let objFilterOption: any = {};
            // tslint:disable-next-line:prefer-const
            let objSearch: any = {};
            objFilterOption.FilterIsDate = true;
            objFilterOption.ToDate = tDate;
            objFilterOption.FromDate = fDate;

            objSearch.FieldName = fieldNameVar;
            objSearch.ColumnNo = 7;
            objSearch.FilterOption = objFilterOption;

            this.customFilter.push(objSearch);
          }
        } else {
          // tslint:disable-next-line:prefer-const
          let objFilterOption: any = {};
          // tslint:disable-next-line:prefer-const
          let objSearch: any = {};

          objFilterOption.FilterIsDate = true;
          objFilterOption.ToDate = tDate;
          objFilterOption.FromDate = fDate;
          objSearch.FieldName = fieldNameVar;
          objSearch.ColumnNo = 7;
          objSearch.FilterOption = objFilterOption;
          this.customFilter.push(objSearch);
        }
        this.getAllOrdersbycustomSearch();
      } else if (fieldNameVar === 'FlightEndDate') {
        if (this.customFilter.length > 0) {
          const index = this.getIndexNo(fieldNameVar, undefined, undefined);
          if (index !== -1) {
            const fName = this.customFilter[index].FieldName;
            const obj = this.customFilter[index];
            const isTrue = obj.FilterOption.FilterIsDate;
            if (isTrue === true) {
              this.customFilter[index].FilterOption.ToDate = tDate;
              this.customFilter[index].FilterOption.FromDate = fDate;
            } else {
              this.customFilter[index].FilterOption.ToDate = tDate;
              this.customFilter[index].FilterOption.FromDate = fDate;
            }
          } else {
            // tslint:disable-next-line:prefer-const
            let objFilterOption: any = {};
            // tslint:disable-next-line:prefer-const
            let objSearch: any = {};

            objFilterOption.FilterIsDate = true;
            objFilterOption.ToDate = tDate;
            objFilterOption.FromDate = fDate;
            objSearch.FieldName = fieldNameVar;
            objSearch.ColumnNo = 8;
            objSearch.FilterOption = objFilterOption;
            this.customFilter.push(objSearch);
          }
        } else {
          // tslint:disable-next-line:prefer-const
          let objFilterOption: any = {};
          // tslint:disable-next-line:prefer-const
          let objSearch: any = {};

          objFilterOption.FilterIsDate = true;
          objFilterOption.ToDate = tDate;
          objFilterOption.FromDate = fDate;
          objSearch.FieldName = fieldNameVar;
          objSearch.ColumnNo = 8;
          objSearch.FilterOption = objFilterOption;
          this.customFilter.push(objSearch);
        }
        this.showLoader()
        this.getAllOrdersbycustomSearch();
      } else if (fieldNameVar === 'GrossCost' || fieldNameVar === 'NetCost' || fieldNameVar === 'BuyId') {
        if (amount > 0) {
          amount = amount;
        } else {
          amount = 0;
        }
        if (this.customFilter.length > 0) {
          const index = this.getIndexNo(fieldNameVar, fieldNameVar, undefined);
          if (index !== -1) {
            const fName = this.customFilter[index].FieldName;
            const obj = this.customFilter[index];
            const isTrue = obj.FilterOption.FilterIsNumber;
            if (isTrue === true) {
              this.customFilter[index].FilterOption.Type = amountType;
              this.customFilter[index].FilterOption.value = amount;
            } else {
              this.customFilter[index].FilterOption.Type = amountType;
              this.customFilter[index].FilterOption.value = amount;
            }
          } else {
            // tslint:disable-next-line:prefer-const
            let objFilterOption: any = {};
            // tslint:disable-next-line:prefer-const
            let objSearch: any = {};

            objFilterOption.FilterIsNumber = true;
            objFilterOption.Type = amountType;
            objFilterOption.value = amount;
            objSearch.FieldName = fieldNameVar;
            if (fieldNameVar === 'BuyId') {
              objSearch.ColumnNo = 4;
            } else if (fieldNameVar === 'GrossCost') {
              objSearch.ColumnNo = 9;
            } else if (fieldNameVar === 'NetCost') {
              objSearch.ColumnNo = 10;
            }
            objSearch.FilterOption = objFilterOption;
            this.customFilter.push(objSearch);
          }
        } else {
          // tslint:disable-next-line:prefer-const
          let objFilterOption: any = {};
          // tslint:disable-next-line:prefer-const
          let objSearch: any = {};

          objFilterOption.FilterIsNumber = true;
          objFilterOption.Type = amountType;
          objFilterOption.value = amount;
          objSearch.FieldName = fieldNameVar;
          if (fieldNameVar === 'BuyId') {
            objSearch.ColumnNo = 4;
          } else if (fieldNameVar === 'GrossCost') {
            objSearch.ColumnNo = 9;
          } else if (fieldNameVar === 'NetCost') {
            objSearch.ColumnNo = 10;
          }
          objSearch.FilterOption = objFilterOption;
          this.customFilter.push(objSearch);
        }
        this.getAllOrdersbycustomSearch();
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Get list of all orders by custom search
  /// </summary>
  getAllOrdersbycustomSearch() {
    try {
      if (this.customFilter.length > 0) {
        const data = this.customFilter;
        console.log("getAllOrdersbycustomSearch:"+ JSON.stringify(data));
        this.displayOrderDatatableJquery(data);
      } else {
        this.advertiserId = this.globalClass.getAdvertiserID();
        let aID = this.advertiserId;
        const searchFilter = { 'AdverdtiserId': aID };
        this.customFilter = [];
        this.searchByAdvertiserId(searchFilter, true);
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Delete index number wise form filter array
  /// </summary>
  deleteIndexNoWiseformFilterArray(fieldName, tableColumnName) {
    console.log("deleteIndexNoWiseformFilterArray:"+ JSON.stringify(this.customFilter));
    try {
      let index = -1;
      this.customFilter.forEach((data, key) => {
        if (data.FieldName === fieldName) {
          if (tableColumnName === 'AdvertiserName') {
            if (data.ColumnNo === 1) {
              if (data.FilterOption.FilterIsArray === true) {
                index = key;
                this.customFilter.splice(key, 1);
              }
            }
          } else if (tableColumnName === 'AdvertiserId') {
            if (data.ColumnNo === 2) {
              if (data.FilterOption.FilterIsArray === true) {
                index = key;
                this.customFilter.splice(key, 1);
              }
            }
          } else if (tableColumnName === 'ST_IDName') {
            if (data.ColumnNo === 3) {
              if (data.FilterOption.FilterIsArray === true) {
                index = key;
                this.customFilter.splice(key, 1);
              }
            }
          } else if (tableColumnName === 'ST_ID') {
            if (data.ColumnNo === 3) {
              if (data.FilterOption.FilterIsArray === true) {
                index = key;
                this.customFilter.splice(key, 1);
              }
            }
          } else if (tableColumnName === undefined) {
            index = key;
            this.customFilter.splice(key, 1);
          } else {
            index = key;
            this.customFilter.splice(key, 1);
          }
        }
      });
      return index;
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Clear the search field
  /// </summary>
  clearSearchFields(columnName) {
    try {
      if (columnName === this.columnNames.AdvertiserName) {
        console.log("clearsearchfield-1:Global:MainAdvertiserId:"+ this.advertiserId);
        this.filter.AdverdtiserName = '';
        this.filter.AdverdtiserType = 'Contains';
        this.deleteIndexNoWiseformFilterArray('AdvertiserId', 'AdvertiserName');
        this.allMediaOrders = [];
        this.showLoader();
        this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
        if (this.partyAndOrganizationData.length > 0) {
          const filterType = 'Equals';
          const fieldName = 'AdvertiserId';
          if (filterType === 'Equals') {
            const advertiserName = '';
            const filterWord = '';
            this.advertiserFilterNotbyEqual(this.globalClass.getDashboardAdvertiserIDs(), filterType, advertiserName, fieldName, null, null, null, null);
          }
        }
      } else if (columnName === this.columnNames.AdvertiserId) {
        console.log("clearsearchfield-2:Global:advertisersIds:"+ this.globalClass.getAdvertiserID());
        this.filter.AdverdtiserId = '';
        this.deleteIndexNoWiseformFilterArray('AdvertiserId', 'AdvertiserId');
        this.allMediaOrders = [];
        this.showLoader()
        //this.getAllOrdersbycustomSearch();
        
        this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
        if (this.partyAndOrganizationData.length > 0) {
          const filterType = 'Equals';
          const fieldName = 'AdvertiserId';
          if (filterType === 'Equals') {
            const advertiserName = '';
            const filterWord = '';
            this.advertiserFilterNotbyEqual(this.globalClass.getDashboardAdvertiserIDs(), filterType, advertiserName, fieldName, null, null, null, null);
          }
        }
      } else if (columnName === this.columnNames.billToContact) {
        this.filter.BillingContactName = '';
        this.filter.BillingContactType = 'Contains';
        this.deleteIndexNoWiseformFilterArray('AdvertiserId', 'AdvertiserId');
        this.allMediaOrders = [];
        this.showLoader()
        this.getAllOrdersbycustomSearch();
      } else if (columnName === this.columnNames.ST_ID) {
        this.filter.BillingContactId = '';
        this.deleteIndexNoWiseformFilterArray('ST_ID', 'ST_ID');
        this.allMediaOrders = [];
        this.showLoader()
        this.getAllOrdersbycustomSearch();
      } else if (columnName === this.columnNames.Status) {
        this.filter.Status = 'Proposal';
        this.deleteIndexNoWiseformFilterArray('OrderStatus', undefined);
        this.allMediaOrders = [];
        this.showLoader()
        this.getAllOrdersbycustomSearch();
      } else if (columnName === this.columnNames.FlightStartDate) {
        this.filter.fFromDate = '';
        this.filter.fToDate = '';
        this.deleteIndexNoWiseformFilterArray('FlightStartDate', undefined);
        this.allMediaOrders = [];
        this.showLoader()
        this.getAllOrdersbycustomSearch();
      } else if (columnName === this.columnNames.FlightEndDate) {
        this.filter.tFromDate = '';
        this.filter.tToDate = '';
        this.deleteIndexNoWiseformFilterArray('FlightEndDate', undefined);
        this.allMediaOrders = [];
        this.showLoader()
        this.getAllOrdersbycustomSearch();
      } else if (columnName === this.columnNames.GrossCost) {
        this.filter.GrossAmountType = 'Equal';
        this.filter.GrossAmount = '';
        this.deleteIndexNoWiseformFilterArray('GrossCost', undefined);
        this.allMediaOrders = [];
        this.showLoader()
        this.getAllOrdersbycustomSearch();
      } else if (columnName === this.columnNames.NetCost) {
        this.filter.NetAmountType = 'Equal';
        this.filter.NetAmount = '';
        this.deleteIndexNoWiseformFilterArray('NetCost', undefined);
        this.allMediaOrders = [];
        this.showLoader()
        this.getAllOrdersbycustomSearch();
      } else if (columnName === this.columnNames.BuyId) {
        this.filter.BuyId = '';
        this.filter.BuyIdType = 'Equal';
        this.deleteIndexNoWiseformFilterArray('BuyId', undefined);
        this.allMediaOrders = [];
        this.showLoader()
        this.getAllOrdersbycustomSearch();
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Set the value for clear the search field
  /// </summary>
  setClearAllSearchFields(popover, columnName) {
    try {
      if (popover.isOpen()) {
        popover.close();
      }
    } catch (error) {
      console.log(error);
    }

    try {
      if (columnName === this.columnNames.AdvertiserName) {
        this.filter.AdverdtiserName = '';
        this.filter.AdverdtiserType = 'Contains';
      } else if (columnName === this.columnNames.AdvertiserId) {
        this.filter.AdverdtiserId = '';
      } else if (columnName === this.columnNames.billToContact) {
        this.filter.BillingContactName = '';
        this.filter.BillingContactType = 'Contains';
      } else if (columnName === this.columnNames.ST_ID) {
        this.filter.BillingContactId = '';
      } else if (columnName === this.columnNames.Status) {
        this.filter.Status = 'Proposal';
      } else if (columnName === this.columnNames.FlightStartDate) {
        this.filter.fFromDate = '';
        this.filter.fToDate = '';
      } else if (columnName === this.columnNames.FlightEndDate) {
        this.filter.tFromDate = '';
        this.filter.tToDate = '';
      } else if (columnName === this.columnNames.GrossCost) {
        this.filter.GrossAmountType = 'Equal';
        this.filter.GrossAmount = '';
      } else if (columnName === this.columnNames.NetCost) {
        this.filter.NetAmountType = 'Equal';
        this.filter.NetAmount = '';
      } else if (columnName === this.columnNames.BuyId) {
        this.filter.BuyId = '';
        this.filter.BuyIdType = 'Equal';
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Edit insertion order
  /// </summary>
  editOrder(advertiserId, agencyId, stId, buyId) {

    this.globalClass.setDashboardOrganizationId(advertiserId);
    this.globalClass.setDashboardAgencyId(agencyId);
    this.globalClass.setDashboardContactId(stId);
    this.globalClass.setOrderId(buyId);

    this.globalClass.setFromDashboard(true);
    this.globalClass.setPreOrderFill(false);

    this.appComponent.showMainPage = false;
    this.appComponent.showOrderFormPage = true;

    this.orderTabService.send_OrderTab('OrdersTab');
  }

  /// <summary>
  /// Copy insertion order details for duplicate record
  /// </summary>
  duplicateIO(BuyerId) {
    try {
      this.selectedIssueMediaOrderData = [];
      this.selectedCheckbox = [];

      this.selectedIssueMediaOrder = [];
      this.selectedAllDublicateOrders = false;
      this.newBuyId = null;

      if (BuyerId != null && BuyerId !== undefined && BuyerId !== '') {
        this.dashboardOrderService.getOrdersbuyIdwiseforcopy(BuyerId, this.reqVerificationToken).subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.isShowDefaultTable = false;
                this.issueDateWiseOrder = result.Data;

                for (let i = 0; i < this.issueDateWiseOrder.length; i++) {
                  for (let j = 0; j < this.issueDateWiseOrder[i]['IssueDates'].length; j++) {
                    const tempCoverDate = this.issueDateWiseOrder[i]['IssueDates'][j]['CoverDate'];
                    const convertCoverDate = this.globalClass.getDateInFormat(tempCoverDate);
                    this.issueDateWiseOrder[i]['IssueDates'][j]['CoverDate'] = convertCoverDate;
                  }
                }

                if (this.advertisers.length > 0) {
                  const responseData = this.advertisers;
                  const advertisers = alasql('SELECT Name FROM ? AS add WHERE Id  = ?',
                    [responseData, this.issueDateWiseOrder[0].AdvertiserId.toString()]);
                  this.advertiserName = advertisers[0].Name;

                } else if (this.globalClass.getMainPartyAndOrganizationData().length > 0) {
                  const responseData = this.globalClass.getMainPartyAndOrganizationData();
                  const advertisers = alasql('SELECT Name FROM ? AS add WHERE Id  = ?',
                    [responseData, this.issueDateWiseOrder[0].AdvertiserId.toString()]);
                  this.advertiserName = advertisers[0].Name;
                }
              } else {
                this.isShowDefaultTable = true;
                this.issueDateWiseOrder = [];
              }
            } else {
              this.isShowDefaultTable = true;
              this.issueDateWiseOrder = [];
            }
          } else if (result.StatusCode === 3) {
            this.isShowDefaultTable = true;
            this.issueDateWiseOrder = [];
          } else {
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          console.log(error);
        });
      } else {
        console.log('Buyer Id is null');
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// checkbox change event
  /// </summary>
  chechboxChange(objOrder, flag) {
    try {
      const issueDateId = objOrder.IssueDateId;
      const mOrderId = objOrder.MediaOrderId;
      const buyId = objOrder.BuyId;

      const idx = this.selectedCheckbox.indexOf(mOrderId);

      if (idx > -1) {
        this.selectedIssueMediaOrderData.forEach((data, key) => {
          if (data.MediaOrderId === mOrderId) {
            if (flag === 'checkBox') {
              this.selectedCheckbox.splice(idx, 1);
              this.selectedIssueMediaOrderData.splice(key, 1);
            } else if (flag === 'issueDate') {
              this.selectedIssueMediaOrderData[key].IssueDateId = issueDateId;
            }
          }
        });
      } else {
        if (flag === 'checkBox') {
          this.postData = {};
          this.postData.IssueDateId = objOrder.IssueDateId;
          this.postData.MediaOrderId = objOrder.MediaOrderId;
          this.postData.BuyId = objOrder.BuyId;
          this.selectedIssueMediaOrderData.push(this.postData);
          this.selectedCheckbox.push(mOrderId);
        }
      }
      this.manageCheckboxOnandOff(objOrder.MediaOrderId);
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Manage checkbox on and off
  /// </summary>
  manageCheckboxOnandOff(objMediaOrderId) {
    try {
      const isTrue = this.selectedIssueMediaOrder[objMediaOrderId];
      if (isTrue === true) {
        this.selectedIssueMediaOrder[objMediaOrderId] = true;
      } else {
        this.selectedIssueMediaOrder[objMediaOrderId] = false;
      }

      const t = $('#dublicateOrderTable > tbody > tr > td > input');

      if (this.selectedCheckbox.length === 0) {
        this.selectedAllDublicateOrders = false;
      } else if (t.length === this.selectedCheckbox.length) {
        this.selectedAllDublicateOrders = true;
      } else {
        this.selectedAllDublicateOrders = false;
      }

    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  toggleAll() {
    try {
      if (this.selectedAllDublicateOrders !== false) {
        this.selectedAllDublicateOrders = false;
        this.dropDown = $('#dublicateOrderTable > tbody > tr > td > input');

        for (let i = 0; i < this.dropDown.length; i++) {
          const inputElementId = this.dropDown[i].id;
          this.selectedIssueMediaOrder[inputElementId] = false;
        }
        this.selectedIssueMediaOrderData = [];
        this.selectedCheckbox = [];
      } else {
        this.selectedAllDublicateOrders = true;
        this.dropDown = $('#dublicateOrderTable > tbody > tr > td > input');

        for (let i = 0; i < this.dropDown.length; i++) {
          const inputElementId = this.dropDown[i].id;
          const mediaOrder = alasql('SELECT * FROM ? AS add WHERE MediaOrderId Like ?', [this.issueDateWiseOrder, inputElementId]);

          this.postData = {};
          this.postData.IssueDateId = mediaOrder[0].IssueDateId;
          this.postData.MediaOrderId = mediaOrder[0].MediaOrderId;
          this.postData.BuyId = mediaOrder[0].BuyId;

          this.selectedIssueMediaOrderData.push(this.postData);
          this.selectedCheckbox.push(mediaOrder[0].MediaOrderId);
          this.selectedIssueMediaOrder[inputElementId] = true;
        }
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Add duplicate record
  /// </summary>
  addDuplicate() {
    try {
      if (this.selectedCheckbox.length > 0) {
        this.dashboardOrderService.postOrderforcopy(this.selectedIssueMediaOrderData).subscribe(result => {
          if (result.StatusCode === 1) {
            this.selectedIssueMediaOrderData = [];
            this.selectedCheckbox = [];
            this.selectedIssueMediaOrder = [];
            this.selectedAllDublicateOrders = false;
            const searchFilter = { 'AdverdtiserId': this.advertiserId };
            const data = [];
            this.searchByAdvertiserId(searchFilter, true);
            this.newBuyId = result.Data[0].BuyId;
            if (result.Message != null && result.Message != "") {
              var msg = result.Message.split('||');
              setTimeout(() => {
                this.toastr.info(msg[0] + '<br /><br />' + msg[1]);
              }, 0);
          } else {
              this.toastr.success('Copy to the buy ID ' + result.Data[0].BuyId, 'Success!');
          }
          } else {
            this.newBuyId = null;
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          console.log(error);
        });
      } else {
        this.toastr.error('Please select atleast one checkbox...', 'Error!');
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }

}
