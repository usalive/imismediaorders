import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { AppComponent } from '../../../app.component';
import { environment } from '../../../../environments/environment';
import { OrdersService } from '../../services/orders.service';
import { OrderTabService } from '../../services/orderTab.service';
import * as alasql from 'alasql';
import { GlobalClass } from '../../GlobalClass';
declare var $, jQuery: any;
import 'select2';
import { DomSanitizer } from '@angular/platform-browser';
import { isNullOrUndefined } from 'util';
import { Routes, RouterModule } from '@angular/router';
//import { DashboardOrder } from '../dashboard-orders/dashboard-orders.component';

// const routes:Routes=[
//   {path:'orderDashboard',component:DashboardOrder}
// ];

function ValidateAgencyRequired(c: AbstractControl): any {
  if (!c.parent || !c) {
    return;
  }
  const billTo = c.parent.get('BillTo');
  const agency = c.parent.get('Agency');
  if (billTo.value === '1' || billTo.value === 1) {
    if (agency.value === null || agency.value === undefined || agency.value === '') {
      return { invalid: true };
    } else {
      return;
    }
  } else {
    return;
  }
}

@Component({
  selector: 'asi-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orderForm: FormGroup;
  ordersFormReps: FormGroup;
  advertiserModalForm: FormGroup;
  agencyModalForm: FormGroup;
  contactModalForm: FormGroup;

  isOrdersFormSubmitted = false;
  isOrdersFormRepsSubmitted = false;
  isAdvertiserModalFormSubmitted = false;
  isAgencyModalFormSubmitted = false;
  isContactModalFormSubmitted = false;
  doesAdvertiserAgencyMapListFound=false;

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';

  advertisers = [];
  agencies = [];
  billingToContacts = [];
  reps = [];
  territories = [];
  tmpBillingToContacts = [];

  tabOrderId = 0;
  personData = [];
  partyAndOrganizationData = [];
  organizationWiseParty = [];
  isRequired = false;

  orderGrossCost = '';
  orderNetCost = '';

  adOrderId = 0;

  isShowCity = false;
  isShowPostalCode = false;
  isShowState=false;
  isShowCountry = false;
  isShowAgencyRequiredMsg = false;

  isShowDefaultTable = true;
  setAddLink = true;
  setNoClick = true;
  selectedAdvertiser= '';
  selectedAgency = '';
  selectedContactValue = '';

  advertiser: any = {};
  advertiser_AdvertiserId = 0;
  advertiserModalMsgs = false;
  isStatehave = false;
  subEntityNameCaption = 'State';
  countries = [];
  states = [];
  advertiser_CountrySubEntity = '';

  agency: any = {};
  agency_AgencyId = 0;
  agencyModalMsgs = false;
  isStatehave_Agency = false;
  subEntityNameCaption_Agency = 'State';
  countries_Agency = {};
  states_Agency = {};
  agency_CountrySubEntity_Agency = '';

  contact: any = {};
  contact_ContactId = 0;
  contactModalMsgs = false;
  isStatehave_Contact = false;
  subEntityNameCaption_Contact = 'State';
  contactSameAaAbove = false;
  contact_CountrySubEntity_Contact = '';

  repsId = 0;
  repsName = '';
  territoryId = 0;
  territoryName = '';
  commission = 0;
  tempArray = [];
  repTerritoryArray: any = {};

  parentArray = [];
  mediaOrderId = '';

  ddlAdverdtiserDisabled = false;
  ddlAgencyDisabled = false;

  dynamicRowData: any = {};

  partyCity = '';
  partyPostalCode = '';
  partyState='';
  partyCountry = '';

  partyAddress = '';
  partyMobile = '';
  partyMemberType = '';
  partyWork = '';
  partyEmail = '';
  partyHome = '';

  globalSalesValue: any = {};

  isEditMode = false;
  addStreetHTML = '';
  dvGroupHTML = '';
  dvGroupHTMLArr = [];
  repsTerrBlockTextVal = '';

  BTCaddStreetHTML='';
  isBTCShowCity=false;
  isBTCShowCountry=false;
  BTCpartyCity='';
  BTCpartyState='';
  BTCpartyPostalCode='';
  BTCpartyCountry='';

  test = 'first';
  contactCount = 0;
  dataCount = 800;
  dvId = '';

  territoryID = '0';
  territoryNameNew = '';
  organisation: any = {};
  selectedPartyOrganisationName = '';
  organisationDetailsID = '';
  selectedPartyOrganisationAddress = '';

  isFrozen = false;

  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private orderTabService: OrderTabService,
    private globalClass: GlobalClass,
    private ordersService: OrdersService,
    private appComponent: AppComponent,
    private sanitizer: DomSanitizer,
  ) { ToastrModule.forRoot(); }

  ngOnInit() {
    this.orderGrossCost = this.globalClass.getGrossCost();
    this.orderNetCost = this.globalClass.getNetCost();
    this.getToken();
    try {
      let ordID = null;
      if (this.appComponent.orderId) {
        this.globalClass.setOrderId(this.appComponent.orderId);
        this.appComponent.orderId = null;
      }
      ordID = this.globalClass.getOrderId();
      if (ordID !== undefined && ordID !== '' && ordID != null) {
        this.adOrderId = ordID;
      }
    } catch (error) {
      console.log(error);
    }
    this.validation();
    this.orderForm.controls['BillTo'].setValue('0');
    console.log("preOrderFill: "+this.globalClass.getPreOrderFill());
    console.log("fromDashboard: "+this.globalClass.getFromDashboard());
    console.log("fromPreviousTab: "+this.globalClass.getFromPreviousTab());
    if(this.globalClass.getFromDashboard()===false)
    {
      $("#ancAdvertiserFilter").show();
    }
    else{
      $("#ancAdvertiserFilter").hide();
    }
    this.getAdvertiserThroughASI();
    this.getCountryThroughASI();
    this.getAllReps();
  }

  get isValidAdvertiser() {
    return this.orderForm.get('Advertiser').invalid && (this.isOrdersFormSubmitted || this.orderForm.get('Advertiser').dirty || this.orderForm.get('Advertiser').touched);
  } 
  get isValidBillingContacts() {
    return this.orderForm.get('BillingContacts').invalid && (this.isOrdersFormSubmitted || this.orderForm.get('BillingContacts').dirty || this.orderForm.get('BillingContacts').touched);
  } 
  get isValidSelectedReps() {
    return this.ordersFormReps.get('SelectedReps').invalid && (this.isOrdersFormRepsSubmitted || this.ordersFormReps.get('SelectedReps').dirty || this.ordersFormReps.get('SelectedReps').touched);
  } 
  get isValidSelectedTerritories() {
    return this.ordersFormReps.get('SelectedTerritories').invalid && (this.isOrdersFormRepsSubmitted || this.ordersFormReps.get('SelectedTerritories').dirty || this.ordersFormReps.get('SelectedTerritories').touched);
  } 
  get isValidOrganization() {
    return this.advertiserModalForm.get('Organization').invalid && (this.isAdvertiserModalFormSubmitted || this.advertiserModalForm.get('Organization').dirty || this.advertiserModalForm.get('Organization').touched);
  } 
  get isValidEmail() {
    return this.advertiserModalForm.get('Email').invalid && (this.isAdvertiserModalFormSubmitted || this.advertiserModalForm.get('Email').dirty || this.advertiserModalForm.get('Email').touched);
  } 
  get isValidMemberType() {
    return this.advertiserModalForm.get('MemberType').invalid && (this.isAdvertiserModalFormSubmitted || this.advertiserModalForm.get('MemberType').dirty || this.advertiserModalForm.get('MemberType').touched);
  } 
  get isValidAOrganization() {
    return this.agencyModalForm.get('Organization').invalid && (this.isAgencyModalFormSubmitted || this.agencyModalForm.get('MemberType').dirty || this.agencyModalForm.get('MemberType').touched);
  } 
  get isValidAEmail() {
    return this.agencyModalForm.get('Email').invalid && (this.isAgencyModalFormSubmitted || this.agencyModalForm.get('Email').dirty || this.agencyModalForm.get('Email').touched);
  } 
  get isValidAMemberType() {
    return this.agencyModalForm.get('MemberType').invalid && (this.isAgencyModalFormSubmitted || this.agencyModalForm.get('MemberType').dirty || this.agencyModalForm.get('MemberType').touched);
  } 
  get isValidFirstName() {
    return this.contactModalForm.get('FirstName').invalid && (this.isContactModalFormSubmitted || this.contactModalForm.get('FirstName').dirty || this.contactModalForm.get('FirstName').touched);
  }
  get isValidLastName() {
    return this.contactModalForm.get('LastName').invalid && (this.isContactModalFormSubmitted || this.contactModalForm.get('LastName').dirty || this.contactModalForm.get('LastName').touched);
  }
  get isValidConEmail() {
    return this.contactModalForm.get('Email').invalid && (this.isContactModalFormSubmitted || this.contactModalForm.get('Email').dirty || this.contactModalForm.get('Email').touched);
  }
  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http://', 'https://');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }
  }

  validation() {
    this.ordersForm();
    this.orderFormReps();
    this.advertisersModalForm();
    this.agencyModalForms();
    this.contactsModalForm();
  }

  // ------ OrdersForm Validation ----------
  ordersForm() {
    this.orderForm = this.formBuilder.group({
      'CampaignName': [null],
      'Advertiser': [null, Validators.required],
      'Agency': [null, ValidateAgencyRequired],
      'BillTo': [null],
      'BillingContacts': [null, Validators.required],
    });
  }

  // ------ OrdersFormReps Validation ----------
  orderFormReps() {
    if (this.globalClass.getOrderId() > 0) {
      this.ordersFormReps = this.formBuilder.group({
        'SelectedReps': [null],
        'SelectedTerritories': [null],
      });
    } else {
      this.ordersFormReps = this.formBuilder.group({
        // 'SelectedReps': [null, Validators.required],
        // 'SelectedTerritories': [null, Validators.required],
        'SelectedReps': [null],
        'SelectedTerritories': [null],
      });
    }
  }

  // ------ advertiserModalForm Validation ----------
  advertisersModalForm() {
    this.advertiserModalForm = this.formBuilder.group({
      'Organization': [null, Validators.required],
      'Email': [null,
        Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9]{2,4}$')])],
      'Address': [null],
      'StreetLine2': [null],
      'City': [null],
      'State': [null],
      'PostalCode': [null],
      'MemberType': [null, Validators.required],
      'Country': [null],
      'Home': [null],
      'Mobile': [null],
      'Work': [null],
      'CountryCode': [null],
    });
  }

  // ------ agencyModalForm Validation ----------
  agencyModalForms() {
    this.agencyModalForm = this.formBuilder.group({
      'Organization': [null, Validators.required],
      'Email': [null,
        Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9]{2,4}$')])],
      'Address': [null],
      'StreetLine2': [null],
      'City': [null],
      'PostalCode': [null],
      'CountryCode': [null],
      'MemberType': [null, Validators.required],
      'State': [null],
      'Home': [null],
      'Mobile': [null],
      'Work': [null],
    });
  }

  // ------ contactModalForm. Validation ----------
  contactsModalForm() {
    this.contactModalForm = this.formBuilder.group({
      'Organisation': [null],
      'contactSameAaAbove': [null],
      'selectedNamePrefixValue': [null],
      'FirstName': [null, Validators.required],
      'MiddleName': [null],
      'LastName': [null],
      'Mobile': [null],
      'Email': [null,
        Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9]{2,4}$')])],
      'MemberType': [null],
      'Address': [null],
      'City': [null],
      'State': [null],
      'PinCode': [null],
      'Country': [null],
      'Home': [null],
      'Work': [null],
      'selectedMemberTypeValue': [null],
      'selectedCountryValue': [null],
    });
  }

  getAdvertiserThroughASI() {
    try {
      // if (this.globalClass.getMainPartyAndOrganizationData().length <= 0
      //   && this.globalClass.getMainAdvertisersData().length <= 0
      //   && this.globalClass.getMainAgenciesData().length <= 0
      //   && this.globalClass.getMainBillingToContactsData().length <= 0) {
      //   const hasNext = true;
      //   const offset = 0;
      //   const advertisers = [];
      //   const contacts = [];
      //   this.getAdvertiserData(hasNext, offset, advertisers, contacts, true);
      // } else {
        
      // }
      if (this.globalClass.getFromDashboard() === true) {
        if (this.globalClass.getPreOrderFill() === true) {
          this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
          this.advertisers = this.globalClass.getMainAdvertisersData();
          this.agencies = this.globalClass.getMainAgenciesData();
          this.billingToContacts = this.globalClass.getMainBillingToContactsData();
          this.personData = this.globalClass.getMainBillingToContactsData();
        } else {
          const organizationId = this.globalClass.getDashboardOrganizationId();
          const agencyId = this.globalClass.getDashboardAgencyId();
          if (parseInt(organizationId) > 0) {
            let organizationData = [];
            let contactData = [];
            if (organizationId !== undefined && organizationId != null && organizationId !== '' && organizationId !== '0') {
              organizationData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.globalClass.getMainAdvertisersData(), organizationId]);
            }
            // if (agencyId !== undefined && agencyId != null && agencyId !== '' && agencyId !== '0') {
            //   contactData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.globalClass.getMainAgenciesData(), agencyId]);
            // }
            if (organizationData.length > 0) {
              this.advertisers.push(organizationData[0]);
            }
          }
        }
      }

      if (this.globalClass.getFromDashboard() === false
        || this.globalClass.getFromDashboard() === true
        || this.globalClass.getFromDashboard() === undefined) {
        if (this.globalClass.getIsFrozen()) {
          this.isFrozen = true;
        }
        this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
        this.advertisers = this.globalClass.getMainAdvertisersData();
        this.agencies = this.globalClass.getMainAgenciesData();
        this.personData = this.globalClass.getMainBillingToContactsData();
        this.billingToContacts = this.globalClass.getMainBillingToContactsData();
      }

      if (this.globalClass.getFromDashboard() === true && this.globalClass.getFromPreviousTab()===false) {
        this.fillPartyComingDashBoard();
      } else {
        console.log("getAdvertiserThroughASI:last else part");
        if(this.globalClass.getFromPreviousTab()===false)
        {
          this.globalClass.removeMainPartyAndOrganizationData();
          this.globalClass.removeMainAdvertisersData();
          this.globalClass.removeMainAgenciesData();
          this.globalClass.removeMainBillingToContactsData();
          this.billingToContacts = [];
          this.advertisers = [];
          this.agencies = [];
        }
        this.generateBuyIdForOrder();
      }
    } catch (error) {
      console.log(error);
    }
  }

  getAdvertiserData(hasNext: boolean, offset, advertisers: any[], contacts: any[], issetloader: boolean) {
    try {
      if (issetloader === true) {
        this.showLoader();
      }
      this.ordersService.getAllAdvertiser1(this.websiteRoot, offset).subscribe(result => {
        if (result != null && result !== undefined && result !== '') {
          const ItemData = result.Items.$values;
          if (ItemData.length > 0) {

            ItemData.forEach(itemdatavalue => {
              this.partyAndOrganizationData.push(itemdatavalue);
              const type = itemdatavalue.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                advertisers.push(itemdatavalue);
              } else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                contacts.push(itemdatavalue);
              }
            });

            const totalCount = result.TotalCount;
            const count = result.Count;
            hasNext = result.HasNext;
            offset = result.Offset;
            const limit = result.Limit;
            // tslint:disable-next-line:radix
            const nextOffset = parseInt(offset) + 500;
            if (count === 500) {
              offset = nextOffset;
              this.getAdvertiserData(hasNext, offset, advertisers, contacts, true);
            } else {
              this.hideLoader();

              this.advertisers = advertisers;
              this.agencies = advertisers;
              this.personData = contacts;
              this.billingToContacts = contacts;
              this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
              this.globalClass.setMainAdvertisersData(this.advertisers);
              this.globalClass.setMainAgenciesData(this.agencies);
              this.globalClass.setMainBillingToContactsData(this.billingToContacts);

              if (this.globalClass.getFromDashboard() === true) {
                this.fillPartyComingDashBoard();
              } else {
                this.generateBuyIdForOrder();
              }
            }
          } else {
            this.hideLoader();
            this.advertisers = advertisers;
            this.agencies = advertisers;
            this.personData = contacts;
            this.billingToContacts = contacts;

            this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
            this.globalClass.setMainAdvertisersData(this.advertisers);
            this.globalClass.setMainAgenciesData(this.agencies);
            this.globalClass.setMainBillingToContactsData(this.billingToContacts);

            if (this.globalClass.getFromDashboard() === true) {
              this.fillPartyComingDashBoard();
            } else {
              this.generateBuyIdForOrder();
            }
          }
        } else {
          this.hideLoader();
          this.advertisers = [];
          this.agencies = [];
          this.personData = [];
          this.partyAndOrganizationData = [];
          this.billingToContacts = [];
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getCountryThroughASI() {
    try {
      const hasNext = true;
      const offset = 0;
      const country = [];
      this.getCountryData(hasNext, offset);
    } catch (error) {
      this.hideLoader();
    }
  }

  getCountryData(hasNext: boolean, offset) {
    this.ordersService.getCountry(this.websiteRoot, offset).subscribe(result => {
      if (result != null && result !== undefined && result !== '') {
        const itemData = result.Items.$values;
        this.countries = itemData;
        const totalCount = result.TotalCount;
        hasNext = result.HasNext;
        offset = result.Offset;
        const limit = result.Limit;
        const nextOffset = result.NextOffset;
        if (hasNext === true) {
          offset = nextOffset;
          this.getCountryData(hasNext, offset);
        }
      } else {
        this.countries = [];
      }
    }, error => {
      console.log(error);
    });
  }

  getAllReps() {
    try {
      this.ordersService.getRepsForOrderCreation().subscribe(result => {
        if (result.StatusCode === 1) {
          this.reps = result.Data;
        } else {
          this.toastr.error(result.Message, 'Error');
        }
      }, error => {
        console.log(error);
      });
    } catch (error) {
      console.log(error);
    }
  }

  fillPartyComingDashBoard() {
    try {
      const organizationId = this.globalClass.getDashboardOrganizationId();
      const contactId = this.globalClass.getDashboardContactId();
      const agencyId = this.globalClass.getDashboardAgencyId();
      const buy = this.globalClass.getOrderId();
      this.getAdvertiserMappingList(organizationId);
      if(this.globalClass.getPreOrderFill()===true && this.globalClass.getFromDashboard()===true)
      {
        this.getAdvertiser_RepTerritory_MappingList(organizationId);  
      }
      
      if (parseInt(organizationId) > 0 && parseInt(contactId) > 0) {
        if (this.globalClass.getPreOrderFill() === true) {
          this.billingToContacts = [];
          this.tmpBillingToContacts=[];
          this.showLoader();
          const partyData=this.advertisers;
          const advertiserName = partyData.find(x=>x.Id===organizationId).OrganizationName;
          this.ordersService.getAdvertiserAgency_BillToContact(this.websiteRoot,0,advertiserName,'').subscribe(result => {
            if (result != null && result !== undefined && result !== '') {
              const ItemData = result.Items.$values;
              for (const itemdatavalue of ItemData) {
                const type = itemdatavalue.$type;
                if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                  this.tmpBillingToContacts.push(itemdatavalue);
                }
              }
              if(this.tmpBillingToContacts.length>0)
              {
                this.billingToContacts = this.tmpBillingToContacts;
                this.globalClass.setMainBillingToContactsData(this.billingToContacts);
              }
            } else {
              this.hideLoader();
              this.billingToContacts = [];
            }
          }, error => {
            this.hideLoader();
            console.log(error);
          });
          this.ordersService.generateBuyId().subscribe(result => {
            if (result.StatusCode === 1) {
              const orderNumber = parseInt(result.Data.buyId);
              this.adOrderId = orderNumber;
              this.orderForm.controls['Advertiser'].disable();
              this.orderForm.controls['BillTo'].setValue('0');
              this.orderForm.controls['Advertiser'].setValue(organizationId);
              const advertiser= this.orderForm.controls['Advertiser'].value;
              const billTo = this.orderForm.controls['BillTo'].value;

              if (advertiser!== undefined && advertiser!== '' && advertiser!= null) {
                if (billTo === 0) {
                  this.isEditMode = false;
                  this.changeAdvertiser('other');
                }
              }

              if (agencyId !== '' && agencyId !== '0' && agencyId !== undefined && agencyId != null) {
                this.orderForm.controls['Agency'].setValue(agencyId);
                if (billTo === 1) {
                  this.isEditMode = false;
                  this.changeAgency('other');
                }
              } else {
                this.orderForm.controls['Agency'].setValue('');
              }
              setTimeout(() => {
                //console.log("Prefill:true : "+ this.billingToContacts);
                this.orderForm.controls['BillingContacts'].setValue(contactId);
                //this.globalClass.setPreOrderFill(false);
                const organizationData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.billingToContacts, contactId]);
                if (organizationData.length > 0) {
                  const text = organizationData[0].Name;
                  const id = organizationData[0].Id;
                  this.setAddressFielsManually(id, text);
                }
              }, 1500);
            }
          }, error => {
            console.log(error);
          });
        } else {
        if(this.globalClass.getPreOrderFill()===false && this.globalClass.getFromDashboard()===true)
        {
          this.orderForm.controls['Advertiser'].disable();
        }
        console.log("media order id:"+ this.globalClass.getbillingDetailsMediaOrderid());
          const buyId = buy;
          this.ordersService.getOrdersByBuyId(buyId).subscribe(result => {
            if (result.StatusCode === 1) {
              if(this.agencies.length===0 && result.Data[0].AgencyId !=='0')
              {
                const agencyId = result.Data[0].AgencyId;
                  this.ordersService.getOrganisationById(this.websiteRoot,agencyId).subscribe(result => {
                    if (result != null && result !== undefined && result !== '') {
                      const ItemData = result;
                      this.tmpBillingToContacts.push(ItemData);
                      if(this.tmpBillingToContacts.length>0)
                        {
                          this.agencies = this.tmpBillingToContacts;
                          this.globalClass.setMainAgenciesData(this.agencies);
                        }
                    } else {
                      this.hideLoader();
                      this.agencies = [];
                    }
                    console.log("fillBillingDetails:Condition2:agencies"+ JSON.stringify(this.agencies));
                  }, error => {
                    this.hideLoader();
                    console.log(error);
                  });
              }
              console.log("fillPartyComingDatabase: agencies:"+ JSON.stringify(this.agencies.length) +":agencyId:"+ result.Data[0].AgencyId);
              const billingDetails: any = {};
              if(this.globalClass.getbillingDetailsMediaOrderid() ==='' || this.globalClass.getbillingDetailsMediaOrderid() === '0')
              {
                console.log("if section:"+ result.Data.length);
                if (result.Data[0].IsFrozen == true) {
                  this.isFrozen = true;
                  this.globalClass.setIsFrozen(this.isFrozen);
                  this.orderForm.controls['Agency'].disable();
                  this.orderForm.controls['BillTo'].disable();
                  this.orderForm.controls['BillingContacts'].disable();
                  $("#ancAgencyFilter").hide();
                }
                else {
                  this.isFrozen = false;
                  this.globalClass.setIsFrozen(this.isFrozen);
                }
              }else{
                console.log("else section:"+ result.Data[0].IsFrozen);
                for (const itemdatavalue of result.Data) {
                  if(itemdatavalue.MediaOrderId===this.globalClass.getbillingDetailsMediaOrderid())
                  {
                    console.log("media order detail:"+ itemdatavalue.IsFrozen);
                    if (itemdatavalue.IsFrozen == true) {
                      this.isFrozen = true;
                      this.globalClass.setIsFrozen(this.isFrozen);
                      this.orderForm.controls['Agency'].disable();
                      this.orderForm.controls['BillTo'].disable();
                      this.orderForm.controls['BillingContacts'].disable();
                      $("#ancAgencyFilter").hide();
                    }
                    else {
                      this.isFrozen = false;
                      this.globalClass.setIsFrozen(this.isFrozen);
                    }
                  }
                }
              }
              if (result.Data[0].CampaignName !== undefined && result.Data[0].CampaignName != null && result.Data[0].CampaignName !== '') {
                billingDetails.CampaignName = result.Data[0].CampaignName;
              }
              billingDetails.AdOrderId = result.Data[0].BuyId;
              billingDetails.AdvertiserId = result.Data[0].AdvertiserId;
              billingDetails.AgencyId = result.Data[0].AgencyId;
              billingDetails.BillTo = result.Data[0].BillToInd;
              billingDetails.AdCommissions = result.Data[0].MediaOrderReps;
              billingDetails.BillToContactId = result.Data[0].ST_ID;
              const totalSum = alasql('SELECT sum(GrossCost),sum(NetCost) FROM ? AS add ', [result.Data]);
              const grossAmount = totalSum[0]['SUM(GrossCost)'];
              const netAmount = totalSum[0]['SUM(NetCost)'];

              this.globalClass.setGrossCost((grossAmount).toFixed(2));
              this.globalClass.setNetCost((netAmount).toFixed(2));
              this.orderGrossCost = parseFloat(grossAmount).toFixed(2);
              this.orderNetCost = parseFloat(netAmount).toFixed(2);

              const billingDetailsData = billingDetails;
              console.log("fillPartyComingDashboard:billingDetailsData:"+ JSON.stringify(billingDetails));
              if (billingDetailsData.AdOrderId > 0) {
                this.isEditMode = true;
                this.mediaOrderId = this.globalClass.getbillingDetails().MediaOrderId;
                // tslint:disable-next-line:max-line-length
                if (billingDetailsData.CampaignName != null && billingDetailsData.CampaignName !== undefined && billingDetailsData.CampaignName !== '') {
                  this.orderForm.controls['CampaignName'].setValue(billingDetailsData.CampaignName);
                } else {
                  this.orderForm.controls['CampaignName'].setValue('');
                }

                this.orderForm.controls['BillTo'].setValue((billingDetailsData.BillTo === false ? '0' : '1'));
                this.adOrderId = billingDetailsData.AdOrderId;

                this.orderForm.controls['Advertiser'].setValue(billingDetailsData.AdvertiserId);

                const advertiser= this.orderForm.controls['Advertiser'].value;

                const billTo = this.orderForm.controls['BillTo'].value;
                if (advertiser!== undefined && advertiser!== '' && advertiser!= null) {
                  if (billTo === 0) {
                    this.isEditMode = false;
                    this.initBillToContactsForAdvertiser();
                  }
                }

                if (billingDetailsData.AgencyId !== '' && billingDetailsData.AgencyId !== '0' && billingDetailsData.AgencyId !== undefined
                  && billingDetailsData.AgencyId != null && billingDetailsData.AgencyId !== 0) {
                  this.orderForm.controls['Agency'].setValue(billingDetailsData.AgencyId);
                  if (billTo === 1) {
                    this.isEditMode = false;
                    this.initBillToContactsForAgency();
                  }
                } else {
                  this.orderForm.controls['Agency'].setValue('');
                }

                this.setBillToContact();
                setTimeout(() => {
                  this.orderForm.controls['BillingContacts'].setValue(billingDetailsData.BillToContactId);
                  const organizationData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.billingToContacts, billingDetailsData.BillToContactId]);
                  if (organizationData.length > 0) {
                    const text = organizationData[0].Name;
                    const id = organizationData[0].Id;
                    this.setAddressFielsManually(id, text);
                  }
                  if (billingDetailsData.AdCommissions.length > 0) {
                    this.createRepsandTerritoryWiseCommissionHTMLWithData(billingDetailsData.AdCommissions);
                  }
                }, 1500);
              }
            }
          }, error => {
            console.log(error);
          });
        }
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  setAddressFielsManually(Id, text) {
    try {
      console.log(Id +":"+ text);
      this.partyCity = '';
      this.partyState = '';
      this.partyPostalCode = '';
      this.partyCountry = '';
      this.partyAddress = '';
      this.partyMobile = '';
      this.partyMemberType = '';
      this.partyWork = '';
      this.partyEmail = '';
      this.partyHome = '';
      this.isShowCity = false;
      this.isShowPostalCode = false;
      this.isShowState=false;
      this.isShowCountry = false;

      const id = Id;
      const value = text;

      // tslint:disable-next-line:radix
      if (parseInt(id) > 0) {
        if (id !== '' && id !== undefined && id != null) {
          if (value !== '' && value !== undefined && value != null) {
            const partyData = this.billingToContacts;

            partyData.forEach((partyValue, key) => {
              const partyFullName = partyValue.PersonName.FullName;
              const partyId = partyValue.Id;

              // tslint:disable-next-line:radix
              if (parseInt(partyId) === parseInt(id)) {
                let additionalAttr = partyValue.AdditionalAttributes;
                if (additionalAttr !== undefined) {
                  additionalAttr = partyValue.AdditionalAttributes.$values;
                  if (additionalAttr !== undefined) {

                    additionalAttr.forEach((addValue, key2) => {
                      const type = addValue.Name;
                      if (type === 'CustomerTypeDescription') {
                        this.partyMemberType = addValue.Value;
                      }
                    });
                  }
                }

                let phones = partyValue.Phones;
                if (phones !== undefined) {
                  phones = partyValue.Phones.$values;
                  if (phones !== undefined) {

                    phones.forEach((phoneValue, key3) => {
                      const type = phoneValue.PhoneType;
                      if (type === 'Mobile') {

                         if (phoneValue.Number === 'undefined' || phoneValue.Number === undefined) {
                           this.partyMobile = '';
                         } else {
                           this.partyMobile = this.globalClass.isEmpty(phoneValue.Number);
                         }
                      } else if (type === '_Work Phone') {
                        if (phoneValue.Number === 'undefined' || phoneValue.Number === undefined) {
                          this.partyWork = '';
                        } else {
                          this.partyWork = this.globalClass.isEmpty(phoneValue.Number);
                        }
                      } else if (type === 'Address') {
                        if (phoneValue.Number === 'undefined' || phoneValue.Number === undefined) {
                          this.partyHome = '';
                        } else {
                          this.partyHome = this.globalClass.isEmpty(phoneValue.Number);
                        }
                      }
                    });
                  } else {
                    this.partyMobile = '';
                    this.partyWork = '';
                    this.partyHome = '';
                  }
                }

                let emails = partyValue.Emails;
                if (emails !== undefined) {
                  emails = partyValue.Emails.$values;
                  if (emails !== undefined) {
                    emails.forEach((emailValue, key4) => {
                      const type = emailValue.EmailType;
                      if (type === '_Primary') {
                        if (emailValue.Address === '' && emailValue.Address === undefined && emailValue.Address == null) {
                          this.partyEmail = '';
                        } else {
                          this.partyEmail = emailValue.Address;
                        }
                      }
                    });
                  }
                }

                let partyAddress = partyValue.Addresses;
                if (partyAddress !== undefined) {
                  partyAddress = partyValue.Addresses.$values[0];
                  if (partyAddress !== undefined) {
                    partyAddress = partyValue.Addresses.$values[0].Address;
                    if (partyAddress !== undefined) {
                      const addressLines = partyValue.Addresses.$values[0].Address.AddressLines;
                      if (addressLines !== undefined && addressLines != null && addressLines !== '') {

                        const streetArray = partyValue.Addresses.$values[0].Address.AddressLines.$values;
                        let street = '';
                        this.addStreetHTML = '';
                        streetArray.forEach((itemdatavalue, key5) => {

                          if(this.globalClass.isEmpty(itemdatavalue) != ''){
                            if (key5 === 0) {
                              street = '<span>' + itemdatavalue + '</span><br />';
                            } else {
                              street = street + '<span style="margin-left:65px">' + itemdatavalue + '</span><br />';
                            }
                          }                          
                        });
                       
                        this.addStreetHTML = street;

                        if (partyAddress !== undefined) {
                          let city = partyValue.Addresses.$values[0].Address.CityName;
                          city = this.globalClass.isEmpty(city);
                          this.partyCity = city +  ' ';
                          

                        
                          if (city !== '') {
                            // this.partyCity = partyValue.Addresses.$values[0].Address.CityName;
                            this.isShowCity = true;
                          } else {
                            this.isShowCity = false;
                          }
                          let state = partyValue.Addresses.$values[0].Address.CountrySubEntityCode;
                          state = this.globalClass.isEmpty(state);
                          this.partyState = state +  ' ';
                          if(state!==''){
                            this.isShowState = true;
                          }else{
                            this.isShowState = false;
                            this.partyState  = ' ';
                          }

                          let postalCode = partyValue.Addresses.$values[0].Address.PostalCode;
                          postalCode = this.globalClass.isEmpty(postalCode);
                          this.partyPostalCode = postalCode +  '';

                          if (postalCode !== '') {
                            // this.partyPostalCode = partyValue.Addresses.$values[0].Address.PostalCode;
                            this.isShowPostalCode = true;
                           
                          } else {
                            this.isShowPostalCode = false;
                           
                          }

                          let country = partyValue.Addresses.$values[0].Address.CountryName;
                          country = this.globalClass.isEmpty(country);
                          this.partyCountry = country;

                          if (country !== '') {
                            // this.partyCountry = partyValue.Addresses.$values[0].Address.CountryName;
                            this.isShowCountry = true;
                          } else {
                            this.isShowCountry = false;
                          }

                        } else {
                          this.isShowCity = false;
                          this.isShowPostalCode = false;
                          this.isShowState = false;
                          this.isShowCountry = false;
                        }
                      } else {
                        this.isShowCity = false;
                        this.isShowPostalCode = false;
                        this.isShowState = false;
                        this.isShowCountry = false;
                      }
                    } else {
                      this.isShowCity = false;
                      this.isShowPostalCode = false;
                      this.isShowState = false;
                      this.isShowCountry = false;
                    }
                  } else {
                    this.isShowCity = false;
                    this.isShowPostalCode = false;
                    this.isShowState = false;
                    this.isShowCountry = false;
                  }
                } else {
                  this.isShowCity = false;
                  this.isShowPostalCode = false;
                  this.isShowState = false;
                  this.isShowCountry = false;
                }
              }
            });
          } else {
          }
        } else {
        }
      } else {
      }
    } catch (error) {
      console.log(error);
    }
  }

  initBillToContactsForAdvertiser() {
    // const partyData = this.globalClass.getMainPartyAndOrganizationData();
    // this.organizationWiseParty = [];
    const advertiserId = this.globalClass.getDashboardOrganizationId();
    const advertiserName = this.partyAndOrganizationData.find(x=>x.Id===advertiserId).OrganizationName;
    this.billingToContacts = [];
    this.tmpBillingToContacts=[];
    //Calling Party API for getting only bill to contacts for active advertisers
    this.ordersService.getAdvertiserAgency_BillToContact(this.websiteRoot,0,advertiserName,'').subscribe(result => {
      if (result != null && result !== undefined && result !== '') {
        const ItemData = result.Items.$values;
        if (ItemData.length > 0) {
          for (const itemdatavalue of ItemData) {
            const type = itemdatavalue.$type;
            if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
              this.tmpBillingToContacts.push(itemdatavalue);
            }
          }
          if(this.tmpBillingToContacts.length>0)
          {
              this.billingToContacts = this.tmpBillingToContacts;
              this.globalClass.setMainBillingToContactsData(this.billingToContacts);
          }
        }
      } else {
        this.hideLoader();
        this.billingToContacts = [];
      }
    }, error => {
      this.hideLoader();
      console.log(error);
    });
    // partyData.forEach(person => {
    //   if (person.PrimaryOrganization !== undefined) {
    //     if (person.PrimaryOrganization.OrganizationPartyId !== undefined) {
    //       const organizationI = person.PrimaryOrganization.OrganizationPartyId;
    //       if (advertiserId === organizationI) {
    //         this.organizationWiseParty.push(person);
    //         this.test = 'secound';
    //       }
    //     }
    //   }
    // });

    // if (this.organizationWiseParty.length > 0) {
    //   const contactId = this.globalClass.getDashboardContactId();
    //   // tslint:disable-next-line:radix
    //   if (parseInt(contactId) > 0) {
    //     let contactData = [];
    //     if (contactId !== undefined && contactId != null && contactId !== '' && contactId !== '0') {
    //       contactData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.organizationWiseParty, contactId]);
    //     }
    //     if (contactData.length > 0) {
    //       const isExits = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.organizationWiseParty, contactId]);
    //       if (isExits.length > 0) {
    //       } else {
    //         this.billingToContacts.push(contactData[0]);
    //       }
    //     }
    //   }

    //   for (let i = 0; i < this.dataCount; i++) {
       
    //     if (this.organizationWiseParty.length > 0) {
    //       const obj = this.organizationWiseParty[this.contactCount];
    //       if (obj !== undefined && obj != null && obj !== '' && obj !== 0) {
    //         this.billingToContacts.push(obj);
    //       }
    //       this.contactCount++;
    //     }
    //   }
    //   this.tmpBillingToContacts = this.organizationWiseParty;
    // } else {
    //   this.billingToContacts = [];
    // }
  }

  initBillToContactsForAgency() {
    console.log("initbilltocontactforagency:"+ this.agencies +"-agencyId:"+ this.globalClass.getDashboardAgencyId());
    const partyData = this.globalClass.getMainPartyAndOrganizationData();
    this.organizationWiseParty = [];
    const agencyId = this.globalClass.getDashboardAgencyId();
    const agencyData = this.agencies;
    const advertiserName = agencyData.find(x=>x.Id===agencyId).OrganizationName;
    this.billingToContacts = [];
    this.tmpBillingToContacts=[];
    //Calling Party API for getting only bill to contacts for active advertisers
    this.ordersService.getAdvertiserAgency_BillToContact(this.websiteRoot,0,advertiserName,'').subscribe(result => {
      if (result != null && result !== undefined && result !== '') {
        const ItemData = result.Items.$values;
        if (ItemData.length > 0) {
          for (const itemdatavalue of ItemData) {
              const type = itemdatavalue.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                this.tmpBillingToContacts.push(itemdatavalue);
              }
            }
            if(this.tmpBillingToContacts.length>0)
            {
              this.billingToContacts = this.tmpBillingToContacts;
              this.globalClass.setMainBillingToContactsData(this.billingToContacts);
            }
        }
      } else {
        this.hideLoader();
        this.billingToContacts = [];
      }
    }, error => {
      this.hideLoader();
      console.log(error);
    });
    // partyData.forEach(person => {
    //   if (person.PrimaryOrganization !== undefined) {
    //     if (person.PrimaryOrganization.OrganizationPartyId !== undefined) {
    //       const organizationI = person.PrimaryOrganization.OrganizationPartyId;
    //       if (advertiserId === organizationI) {
    //         this.organizationWiseParty.push(person);
    //         this.test = 'secound';
    //       }
    //     }
    //   }
    // });

    // if (this.organizationWiseParty.length > 0) {
    //   const contactId = this.globalClass.getDashboardContactId();
    //   // tslint:disable-next-line:radix
    //   if (parseInt(contactId) > 0) {
    //     let contactData = [];
    //     if (contactId !== undefined && contactId != null && contactId !== '' && contactId !== '0') {
    //       contactData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.organizationWiseParty, contactId]);
    //     }
    //     if (contactData.length > 0) {
    //       const isExits = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.organizationWiseParty, contactId]);
    //       if (isExits.length > 0) {
    //       } else {
    //         this.billingToContacts.push(contactData[0]);
    //       }
    //       this.billingToContacts.push(contactData[0]);
    //     }
    //   }
    //   for (let i = 0; i < this.dataCount; i++) {
    //     if (this.organizationWiseParty.length > 0) {
    //       const obj = this.organizationWiseParty[this.contactCount];
    //       if (obj !== undefined && obj != null && obj !== '' && obj !== 0) {
    //         this.billingToContacts.push(obj);
    //       }
    //       this.contactCount++;
    //     }
    //   }
    //   this.tmpBillingToContacts = this.organizationWiseParty;
    // } else {
    //   this.billingToContacts = [];
    // }
  }

  generateBuyIdForOrder() {
    try {
      const billingDetailsData = this.globalClass.getbillingDetails();
      if (billingDetailsData.AdOrderId !== undefined && billingDetailsData.AdOrderId !== null && billingDetailsData.AdOrderId > 0) {
        console.log("generatebuyidfororder:null part"+ billingDetailsData.AdOrderId);
        this.fillBillingDetails(null);
      } else if (this.adOrderId > 0) {
        console.log("generatebuyidfororder:"+ this.adOrderId);
        this.fillBillingDetails(this.adOrderId);
      } else {
        this.ordersService.generateBuyId().subscribe(result => {
          if (result.StatusCode === 1) {
            // tslint:disable-next-line:radix
            const orderNumber = parseInt(result.Data.buyId);
            this.adOrderId = orderNumber;
            this.isEditMode = false;
            this.ddlAdverdtiserDisabled = false;
            this.ddlAgencyDisabled = false;

            this.initSelect2('ddlAdverdtiser', 'Select advertiser');
            this.initSelect2('ddlAgency', 'Select agency');
            this.initSelect2('ddlContact', 'Select contact');

            this.partyAddress = '';
            this.partyCity = '';
            this.partyPostalCode = '';
            this.partyCountry = '';
            this.partyMobile = '';
            this.partyMemberType = '';
            this.partyWork = '';
            this.partyEmail = '';
            this.partyHome = '';
            this.addStreetHTML = '';
            this.isShowCity = false;
            this.isShowPostalCode = false;
            this.isShowCountry = false;
          }
        }, error => {
          console.log(error);
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  // Inotialization of select2 dropdown
  initSelect2(id, text) {
    const selectedState = $('#' + id).find('span.select2-chosen');
    selectedState.text(text);
  }

  fillBillingDetails(buyId) {
    console.log("fillBillingDetails:comingfromproductiontab:"+ this.globalClass.getFromProductionTab());
    try {
      if (buyId !== undefined && buyId !== null) {
        console.log("fillBillingDetails:Condition1:"+ buyId);
        this.ordersService.getOrdersByBuyId(buyId).subscribe(result => {
          const obj: any = {};
          if (result.Data[0].CampaignName !== undefined && result.Data[0].CampaignName != null && result.Data[0].CampaignName !== '') {
            obj.CampaignName = result.Data[0].CampaignName;
          }
          obj.AdOrderId = result.Data[0].BuyId;
          obj.AdvertiserId = result.Data[0].AdvertiserId;
          obj.AgencyId = result.Data[0].AgencyId;
          obj.BillTo = result.Data[0].BillToInd;
          obj.AdCommissions = result.Data[0].MediaOrderReps;
          obj.BillToContactId = result.Data[0].ST_ID;
          this.globalClass.setbillingDetails(obj);
          const totalSum = alasql('SELECT sum(GrossCost),sum(NetCost) FROM ? AS add ', [result.Data]);
          const grossAmount = totalSum[0]['SUM(GrossCost)'];
          const netAmount = totalSum[0]['SUM(NetCost)'];
          this.globalClass.setGrossCost((grossAmount).toFixed(2));
          this.globalClass.setNetCost((netAmount).toFixed(2));
          this.orderGrossCost = parseFloat(grossAmount).toFixed(2);
          this.orderNetCost = parseFloat(netAmount).toFixed(2);
          const billingDetailsData = this.globalClass.getbillingDetails();
          if (billingDetailsData.AdOrderId > 0) {
            this.isEditMode = true;
            this.mediaOrderId = this.globalClass.getbillingDetails().MediaOrderId;
            if (billingDetailsData.CampaignName != null
              && billingDetailsData.CampaignName !== undefined
              && billingDetailsData.CampaignName !== '') {
              this.orderForm.controls['CampaignName'].setValue(billingDetailsData.CampaignName);
            } else {
              this.orderForm.controls['CampaignName'].setValue('');
            }

            this.orderForm.controls['BillTo'].setValue((billingDetailsData.BillTo === false ? '0' : '1'));
            this.adOrderId = billingDetailsData.AdOrderId;
            this.orderForm.controls['Advertiser'].setValue(billingDetailsData.AdvertiserId);
            if(this.globalClass.getPreOrderFill()===true)
            {
              this.orderForm.controls['Advertiser'].disable();
            }
            
            const advertiser= this.orderForm.controls['Advertiser'].value;

            if (advertiser!== undefined && advertiser!== '' && advertiser!= null) {
              this.isEditMode = false;
              this.changeAdvertiser('other');
            }

            if (billingDetailsData.AgencyId !== '' && billingDetailsData.AgencyId !== '0' && billingDetailsData.AgencyId !== undefined
              && billingDetailsData.AgencyId != null && billingDetailsData.AgencyId !== 0) {
              this.orderForm.controls['Agency'].setValue(billingDetailsData.AgencyId);
              this.isEditMode = false;
              this.changeAgency('other');
            } else {
              this.orderForm.controls['Agency'].setValue(null);
            }

            this.orderForm.controls['BillingContacts'].setValue(this.globalClass.getbillingDetails().BillToContactId);
            const bc = this.orderForm.controls['BillingContacts'].value;
            console.log("fillbillingdetail:billtocontact:"+ this.billingToContacts);
            const organizationData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.billingToContacts, this.globalClass.getbillingDetails().BillToContactId]);
            if (organizationData.length > 0) {
              const text = organizationData[0].Name;
              const id = organizationData[0].Id;
              this.setAddressFielsManually(id, text);
            }
            if (billingDetailsData.AdCommissions.length > 0) {
              this.createRepsandTerritoryWiseCommissionHTMLWithData(billingDetailsData.AdCommissions);
            }
          }
        }, error => {
          console.log(error);
        });

      } else {
        console.log("fillBillingDetails:Condition2:"+ buyId);
        const billingDetailsData = this.globalClass.getbillingDetails();
        console.log("fillBillingDetails:Condition2:billingdata"+ JSON.stringify(billingDetailsData));
        if (billingDetailsData.AdOrderId > 0) {
          this.isEditMode = true;
          this.mediaOrderId = this.globalClass.getbillingDetails().MediaOrderId;
          if (billingDetailsData.CampaignName != null
            && billingDetailsData.CampaignName !== undefined
            && billingDetailsData.CampaignName !== '') {
            this.orderForm.controls['CampaignName'].setValue(billingDetailsData.CampaignName);
          } else {
            this.orderForm.controls['CampaignName'].setValue('');
          }
          this.orderForm.controls['BillTo'].setValue(billingDetailsData.BillTo);
          this.adOrderId = billingDetailsData.AdOrderId;
          this.orderForm.controls['Advertiser'].setValue(billingDetailsData.AdvertiserId);
          if(this.globalClass.getPreOrderFill()===true)
          {
            this.orderForm.controls['Advertiser'].disable();
          }
          if(this.globalClass.getPreOrderFill()===false && this.globalClass.getFromDashboard()===true)
          {
            this.orderForm.controls['Advertiser'].disable();
          }
          const Advertiser= this.orderForm.controls['Advertiser'].value;
          if (Advertiser!== undefined && Advertiser!== '' && Advertiser!= null) {
            this.isEditMode = false;
            this.changeAdvertiser('other');
            this.getAdvertiserMappingList(billingDetailsData.AdvertiserId);
          }
          if (billingDetailsData.AgencyId !== ''
          && billingDetailsData.AgencyId != null
          && billingDetailsData.AgencyId !== undefined
          && billingDetailsData.AgencyId !== '0'
          && billingDetailsData.AgencyId !== 0) {
            console.log("fillBillingDetails:Condition2:agencies"+ JSON.stringify(this.agencies));
            console.log("fillBillingDetails:MappingDone:"+ this.doesAdvertiserAgencyMapListFound);
            if(this.doesAdvertiserAgencyMapListFound===false)
            {
              this.agencies=[];
              this.ordersService.getOrganisationById(this.websiteRoot,billingDetailsData.AgencyId).subscribe(result => {
                if (result != null && result !== undefined && result !== '') {
                  const ItemData = result;
                  this.tmpBillingToContacts.push(ItemData);
                  if(this.tmpBillingToContacts.length>0)
                    {
                      this.agencies = this.tmpBillingToContacts;
                      this.globalClass.setMainAgenciesData(this.agencies);
                    }
                } else {
                  this.hideLoader();
                  this.agencies = [];
                }
                console.log("fillBillingDetails:Condition2:agencies"+ JSON.stringify(this.agencies));
              }, error => {
                this.hideLoader();
                console.log(error);
              });
            }
            setTimeout(() => {
              this.orderForm.controls['Agency'].setValue(billingDetailsData.AgencyId);
              this.isEditMode = false;
              this.changeAgency('other');
            }, 1000);
        } else {
          this.orderForm.controls['Agency'].setValue(null);
        }
          
          setTimeout(() => {
            this.orderForm.controls['BillingContacts'].setValue(this.globalClass.getbillingDetails().BillToContactId);
            const billcont = this.orderForm.controls['BillingContacts'].value;
            //console.log("fillbillingdetails:else part:"+ this.billingToContacts);
            const organizationData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.globalClass.getMainBillingToContactsData(), billcont]);
            //console.log("fillbillingdetail:displayaddress:"+organizationData[0].Name);
            if (organizationData.length > 0) {
              const text = organizationData[0].Name;
              const id = organizationData[0].Id;
              this.setAddressFielsManually(id, text);
            }
          }, 1500);

          if (billingDetailsData.AdCommissions.length > 0) {
            this.createRepsandTerritoryWiseCommissionHTMLWithData(billingDetailsData.AdCommissions);
          }
        }
      }

      if(this.globalClass.getFromProductionTab()===true)
      {
        this.ordersService.generateBuyId().subscribe(result => {
          if (result.StatusCode === 1) {
            // tslint:disable-next-line:radix
            const orderNumber = parseInt(result.Data.buyId);
            this.adOrderId = orderNumber;
            this.isEditMode = false;
            this.ddlAdverdtiserDisabled = false;
            this.ddlAgencyDisabled = false;
          }
        }, error => {
          console.log(error);
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  createRepsandTerritoryWiseCommissionHTMLWithData(repsAndTerritoryWiseComm) {
    try {
      if (repsAndTerritoryWiseComm != null) {
        const adCommissions = repsAndTerritoryWiseComm;
        if (adCommissions.length > 0) {
          adCommissions.forEach(adCommission => {
            const repId = adCommission.RepId;
            const TerritoryId = adCommission.TerritoryId;
            const Split = adCommission.Split;
            const dynamicModelName = 'input' + TerritoryId + repId;
            this.globalSalesValue[dynamicModelName] = Split;
          });

          // start create dynamic HTML
          this.parentArray = [];
          const allRepsAndTerritory = adCommissions;
          if (allRepsAndTerritory.length > 0) {

            allRepsAndTerritory.forEach(adCommission => {
              this.repTerritoryArray.repsName = adCommission.RepsName;
              this.repTerritoryArray.territoryName = adCommission.TerritoryName;
              this.repTerritoryArray.Commission = adCommission.Commission;
              this.repTerritoryArray.repsId = adCommission.RepId;
              this.repTerritoryArray.territoryId = adCommission.TerritoryId;
              this.repTerritoryArray.Split = adCommission.Split;
              this.dvId = 'dv' + adCommission.TerritoryId + adCommission.RepId;
              const id = this.dvId;

              const dynamicModelName = 'input' + adCommission.TerritoryId + adCommission.RepId;
              const text = this.globalSalesValue[dynamicModelName];
              const the_string = 'dynamicRowData.input' + adCommission.TerritoryId + '' + adCommission.RepId + '';

              const obj: any = {};
              obj.id = id;
              obj.reps_territoryName = adCommission.RepsName + ' | ' + adCommission.TerritoryName;
              obj.Commission = adCommission.Commission.toFixed(2);
              obj.textboxid = dynamicModelName;
              obj.repsTerrBlockTextVal = text;
              obj.textboxval = text;
              this.repsTerrBlockTextVal = text;
              obj.territoryId = adCommission.TerritoryId;
              obj.repsId = adCommission.RepId;
              this.dvGroupHTMLArr.push(obj);

              this.parentArray.push(this.repTerritoryArray);
              this.repTerritoryArray = [];
            });
          }
          // end create dynamic HTML
        } else {
        }
      } else {
      }
    } catch (error) {
      console.log(error);
    }
  }

  initAdvertiserPopup() {
    try {
      const getSelectedAdvertiser = this.orderForm.controls['Advertiser'].value;
      this.advertiser = {};
      this.advertiser.AdvertiserId = 0;
      this.advertiser.CountrySubEntity = '';
      this.isStatehave = false;
      this.subEntityNameCaption = 'State';
      this.advertiserModalForm.reset();
      this.advertiserModalMsgs = false;
      this.setCountryDefault_Advertiser();
    } catch (error) {
      console.log(error);
    }
  }

  initAgencyPopup() {
    try {
      this.agency = {};
      this.agency.AgencyId = 0;
      this.agency.CountrySubEntity_Agency = '';
      this.isStatehave_Agency = false;
      this.agencyModalForm.reset();
      this.subEntityNameCaption_Agency = 'State';
      this.agencyModalMsgs = false;
      this.setCountryDefault_Agency();
    } catch (error) {
      console.log(error);
    }
  }

  initContactPopup() {
    try {
      this.contact = {};
      this.contact.ContactId = 0;
      this.contactModalForm.controls['selectedNamePrefixValue'].setValue('');
      this.contactModalForm.controls['State'].setValue('');
      this.contactSameAaAbove = false;
      this.setCountryDefault_Contact();
      this.BTCaddStreetHTML='';
      this.isBTCShowCity=false;
      this.isBTCShowCountry=false;
      this.BTCpartyCity='';
      this.BTCpartyState='';
      this.BTCpartyPostalCode='';
      this.BTCpartyCountry='';
      const flag = this.orderForm.controls['BillTo'].value;
      // tslint:disable-next-line:radix
      if (parseInt(flag) === 0) {
        const getSelectedAdvertiser = this.orderForm.controls['Advertiser'].value;
        const id = getSelectedAdvertiser;
        this.ordersService.getOrganisationById(this.websiteRoot, id).subscribe(result => {
          if (result != null && result !== undefined && result !== '') {
            const ItemData = result;
            this.organisation = ItemData;
            this.selectedPartyOrganisationName = ItemData.OrganizationName;
            const organisationDetails = this.organisation;
            this.organisationDetailsID = this.organisation.Id;
            if (organisationDetails != null && organisationDetails !== undefined && organisationDetails !== '') {
              const partyValue = organisationDetails;
              if (partyValue !== undefined) {
                let partyAddress = partyValue.Addresses;
                if (partyAddress !== undefined) {
                  partyAddress = partyValue.Addresses.$values[0];
                  if (partyAddress !== undefined) {
                    partyAddress = partyValue.Addresses.$values[0].Address;
                    if (partyAddress !== undefined) {
                      const addressLines = partyValue.Addresses.$values[0].Address.AddressLines;
                      if (addressLines !== undefined && addressLines != null && addressLines !== '') {
                        const streetArray = partyValue.Addresses.$values[0].Address.AddressLines.$values;
                        let street = '';
                        this.BTCaddStreetHTML = '';
                        streetArray.forEach((itemdatavalue, key5) => {
                          if(this.globalClass.isEmpty(itemdatavalue) != ''){
                            if (key5 === 0) {
                              street = '<span>' + itemdatavalue + '</span><br />';
                            } else {
                              street = street + '<span style="margin-left:65px">' + itemdatavalue + '</span><br />';
                            }
                          }                          
                        });
                        this.BTCaddStreetHTML = street;
                        if (partyAddress !== undefined) {
                          let city = partyValue.Addresses.$values[0].Address.CityName;
                          city = this.globalClass.isEmpty(city);
                          this.BTCpartyCity = city +  ' ';
                          if (city !== '') {
                            this.isBTCShowCity = true;
                          } else {
                            this.isBTCShowCity = false;
                          }
                          let state = partyValue.Addresses.$values[0].Address.CountrySubEntityCode;
                          state = this.globalClass.isEmpty(state);
                          this.BTCpartyState = state +  ' ';
                          let postalCode = partyValue.Addresses.$values[0].Address.PostalCode;
                          postalCode = this.globalClass.isEmpty(postalCode);
                          this.BTCpartyPostalCode = postalCode +  '';
                          let country = partyValue.Addresses.$values[0].Address.CountryName;
                          country = this.globalClass.isEmpty(country);
                          this.BTCpartyCountry = country;
                          if (country !== '') {
                            this.isBTCShowCountry = true;
                          } else {
                            this.isBTCShowCountry = false;
                          }
                        } else {
                          this.isBTCShowCity = false;
                          this.isBTCShowCountry = false;
                        }
                      } else {
                        this.isBTCShowCity = false;
                        this.isBTCShowCountry = false;
                      }
                    } else {
                      this.isBTCShowCity = false;
                      this.isBTCShowCountry = false;
                    }
                  } else {
                    this.isBTCShowCity = false;
                    this.isBTCShowCountry = false;
                  }
                } else {
                  this.isBTCShowCity = false;
                  this.isBTCShowCountry = false;
                }
              }
            }
          } else {
            this.organisation = [];
          }
        }, error => {
          console.log(error);
        });

        // tslint:disable-next-line:radix
      } else if (parseInt(flag) === 1) {
        const getSelectedAgency = this.orderForm.controls['Agency'].value;
        const id = getSelectedAgency;
        this.ordersService.getOrganisationById(this.websiteRoot, id).subscribe(result => {
          if (result != null && result !== undefined && result !== '') {
            const ItemData = result;
            this.organisation = ItemData;
            this.selectedPartyOrganisationName = ItemData.OrganizationName;
            const organisationDetails = this.organisation;
            this.organisationDetailsID = this.organisation.Id;
            if (organisationDetails != null && organisationDetails !== undefined && organisationDetails !== '') {
              const partyValue = organisationDetails;
              if (partyValue !== undefined) {
                let partyAddress = partyValue.Addresses;
                if (partyAddress !== undefined) {
                  partyAddress = partyValue.Addresses.$values[0];
                  if (partyAddress !== undefined) {
                    partyAddress = partyValue.Addresses.$values[0].Address;
                    if (partyAddress !== undefined) {
                      const addressLines = partyValue.Addresses.$values[0].Address.AddressLines;
                      if (addressLines !== undefined && addressLines != null && addressLines !== '') {
                        const streetArray = partyValue.Addresses.$values[0].Address.AddressLines.$values;
                        let street = '';
                        this.BTCaddStreetHTML = '';
                        streetArray.forEach((itemdatavalue, key5) => {
                          if(this.globalClass.isEmpty(itemdatavalue) != ''){
                            if (key5 === 0) {
                              street = '<span>' + itemdatavalue + '</span>';
                            } else {
                              street = street + '<span style="margin-left:65px">' + itemdatavalue + '</span>';
                            }
                          }                          
                        });
                        this.BTCaddStreetHTML = street;
                        if (partyAddress !== undefined) {
                          let city = partyValue.Addresses.$values[0].Address.CityName;
                          city = this.globalClass.isEmpty(city);
                          this.BTCpartyCity = city +  ' ';
                          if (city !== '') {
                            this.isBTCShowCity = true;
                          } else {
                            this.isBTCShowCity = false;
                          }
                          let state = partyValue.Addresses.$values[0].Address.CountrySubEntityCode;
                          state = this.globalClass.isEmpty(state);
                          this.BTCpartyState = state +  ' ';
                          let postalCode = partyValue.Addresses.$values[0].Address.PostalCode;
                          postalCode = this.globalClass.isEmpty(postalCode);
                          this.BTCpartyPostalCode = postalCode +  '';
                          let country = partyValue.Addresses.$values[0].Address.CountryName;
                          country = this.globalClass.isEmpty(country);
                          this.BTCpartyCountry = country;
                          if (country !== '') {
                            this.isBTCShowCountry = true;
                          } else {
                            this.isBTCShowCountry = false;
                          }
                        } else {
                          this.isBTCShowCity = false;
                          this.isBTCShowCountry = false;
                        }
                      } else {
                        this.isBTCShowCity = false;
                        this.isBTCShowCountry = false;
                      }
                    } else {
                      this.isBTCShowCity = false;
                      this.isBTCShowCountry = false;
                    }
                  } else {
                    this.isBTCShowCity = false;
                    this.isBTCShowCountry = false;
                  }
                } else {
                  this.isBTCShowCity = false;
                  this.isBTCShowCountry = false;
                }
              }
            }
          } else {
            this.organisation = [];
          }
        }, error => {
          console.log(error);
        });
      }

      this.contactModalForm.reset();
      this.contactModalMsgs = false;
    } catch (error) {
      console.log(error);
    }
  }

  createContactButtonName() {
    try {
      document.querySelector('#btncontact').innerHTML = 'Add Contact';
    } catch (error) {
      console.log(error);
    }
  }

  removeRepsTerritories(removeId, tId, rId) {
    try {
      const parr = this.parentArray;
      parr.forEach((salecommission, key) => {
        if (salecommission.repsId === rId && salecommission.territoryId === tId) {
          const dynamicModelName = 'input' + salecommission.territoryId + salecommission.repsId;
          parr.splice(key, 1);
          // const divID = 'dv' + salecommission.territoryId + salecommission.repsId;
          // $(divID).remove();
          //delete this.globalSalesValue[dynamicModelName];
        }
      });
      this.parentArray = parr;
      // this.dvGroupHTMLArr.forEach((element, key) => {
      //   if (element.repsId === rId && element.territoryId === tId) {
      //     this.dvGroupHTMLArr.splice(key, 1);
      //   }
      // });

      // if (this.dvGroupHTMLArr.length === 0) {
      // } else {
      //   try {
      //     const val: number = (100) / this.dvGroupHTMLArr.length;
      //     const repsblockval = parseFloat(val.toString()).toFixed(2);
      //     this.repsTerrBlockTextVal = repsblockval;
      //   } catch (error) {
      //   }
      // }

      // this.parentArray = parr;

      this.updateDynamicModelforReps();
      const myEl = document.querySelector('#' + removeId);
      myEl.remove();
    } catch (error) {
      console.log(error);
    }
  }

  updateDynamicModelforReps() {
    try {
      const tempArray = this.parentArray;
      console.log(tempArray);
      const commissionData = tempArray.length === 0 ? 100 : (100 / (tempArray.length));
      const commissionDataTemp = parseFloat(commissionData.toString()).toFixed(2);
      this.parentArray=[];
      this.dvGroupHTMLArr=[];
      tempArray.forEach(item=>{
        this.repTerritoryArray=[];
          this.dvId = 'dv' + item.territoryId + item.repsId;
          const id = this.dvId;
          const dynamicModelName = 'input' + item.territoryId + item.repsId;
          const text = commissionDataTemp;
          const the_string = 'dynamicRowData.input' + item.territoryId + '' + item.repsId + '';
          const obj: any = {};
          obj.id = id;
          obj.reps_territoryName = item.repsName + ' | ' + item.territoryName;
          obj.Commission = item.Commission;
          obj.textboxid = dynamicModelName;
          obj.textboxval = text;
          obj.repsTerrBlockTextVal = text;
          this.repsTerrBlockTextVal = text;
          obj.territoryId = item.territoryId;
          obj.repsId = item.repsId;
          this.dvGroupHTMLArr.push(obj);

          this.repTerritoryArray.repsName = item.repsName;
          this.repTerritoryArray.territoryName = item.territoryName;
          this.repTerritoryArray.Commission = item.Commission;
          this.repTerritoryArray.repsId = item.repsId;
          this.repTerritoryArray.territoryId = item.territoryId;
          this.repTerritoryArray.Split = commissionDataTemp;
          this.parentArray.push(this.repTerritoryArray);
      });
      if(this.parentArray.length===3)
      {
        this.parentArray[this.parentArray.length-1].Split="33.34";
        this.dvGroupHTMLArr[this.parentArray.length-1].repsTerrBlockTextVal="33.34";
        this.dvGroupHTMLArr[this.parentArray.length-1].textboxval="33.34";
      }

    } catch (error) {
      console.log(error);
    }
  }

  onCustomerTypeChange_Advertiser() {
    try {
    } catch (error) {
      console.log(error);
    }
  }

  onCustomerTypeChange_Aency() {
    try {
    } catch (error) {
      console.log(error);
    }
  }

  onCustomerTypeChange_Contact() {
    try {
    } catch (error) {
      console.log(error);
    }
  }

  onStateChange_Contacts() {
    try {
    } catch (error) {
      console.log(error);
    }
  }

  onStateChange_Agency() {
    try {
    } catch (error) {
      console.log(error);
    }
  }

  onStateChange_Advertiser() {
    try {
    } catch (error) {
      console.log(error);
    }
  }

  changeNamePrefix() {
    try {
    } catch (error) {
      console.log(error);
    }
  }

  changeAdvertiser(advtype: any) {
    try {
      const advertiserSelected = this.orderForm.controls['BillTo'].value;
      const getSelectedData = this.orderForm.controls['Advertiser'].value;

      if (getSelectedData != null && getSelectedData !== undefined) {
        let advertiserId = getSelectedData;
        this.getAdvertiserMappingList(advertiserId);
        if(this.globalClass.getFromPreviousTab()===false)
        {
          this.getAdvertiser_RepTerritory_MappingList(advertiserId);
        }
        if (advertiserId === undefined || advertiserId == null || advertiserId === '') {
          const Advertiser= getSelectedData;
          if (Advertiser!== undefined && Advertiser!== '' && Advertiser!= null) {
            advertiserId = Advertiser;
          } else {
          }
        } else {
        }
        if (advertiserSelected === false || advertiserSelected === '0' || advertiserSelected === 0) {
          //console.log("changeadvertiser:"+ this.partyAndOrganizationData +"-advertisers:"+ this.advertiser);
          const partyData = this.advertisers;
          this.organizationWiseParty = [];
          if (this.isEditMode === false) {
            this.billingToContacts=[];
            this.tmpBillingToContacts=[];
            const advertiserName = partyData.find(x=>x.Id===advertiserId).OrganizationName;
              //Calling Party API for getting only bill to contacts for active advertisers
              this.ordersService.getAdvertiserAgency_BillToContact(this.websiteRoot,0,advertiserName,'').subscribe(result => {
                if (result != null && result !== undefined && result !== '') {
                  const ItemData = result.Items.$values;
                  if (ItemData.length > 0) {
                    for (const itemdatavalue of ItemData) {
                      const type = itemdatavalue.$type;
                      if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                        this.tmpBillingToContacts.push(itemdatavalue);
                      }
                    }
                    if(this.tmpBillingToContacts.length>0)
                    {
                      this.billingToContacts = this.tmpBillingToContacts;
                      this.globalClass.setMainBillingToContactsData(this.billingToContacts);
                    }
                  } 
                } else {
                  this.hideLoader();
                  this.billingToContacts = [];
                }
              }, error => {
                this.hideLoader();
                console.log(error);
              });       


            // partyData.forEach((person, key) => {
            //   if (person.PrimaryOrganization !== undefined) {
            //     if (person.PrimaryOrganization.OrganizationPartyId !== undefined) {
            //       const organizationI = person.PrimaryOrganization.OrganizationPartyId;
            //       if (advertiserId === organizationI) {
            //         this.organizationWiseParty.push(person);
            //         if (this.organizationWiseParty.length > 0) {
            //           this.billingToContacts = this.organizationWiseParty;
            //         } else {
            //           this.billingToContacts = [];
            //         }
            //       } else {
            //       }
            //     }
            //   }
            // });
            // if (this.organizationWiseParty !== undefined) {
            //   if (this.organizationWiseParty.length === 0) {
            //     this.billingToContacts = [];
            //   }
            // }
            this.partyAddress = '';
            this.partyCity = '';
            this.partyState = '';
            this.partyPostalCode = '';
            this.partyCountry = '';
            this.partyMobile = '';
            this.partyMemberType = '';
            this.partyWork = '';
            this.partyEmail = '';
            this.partyHome = '';
            this.addStreetHTML = '';
            this.isShowCity = false;
            this.isShowPostalCode = false;
            this.isShowState = false;
            this.isShowCountry = false;
          }
        }
      } else {
        this.billingToContacts = [];
      }
    } catch (error) {
      console.log(error);
    }
    if (advtype === 'advertiser') {
      console.log("setcontactenableordisable");
      this.setContactEnableOrDisable('billto');
    }
  }

  getAdvertiserMappingList(advertiserId)
  {
    this.orderForm.controls['Agency'].setValue(null);
    if (advertiserId != null) {
      this.ordersService.getAdvertiserAgencyMapping_AgencyList(this.websiteRoot, advertiserId).subscribe(result => {
        this.hideLoader();
        if (result.StatusCode == '1') {
          if(result.Data ===null)
          {
            //this.agencies=this.globalClass.getMainAgenciesData();
            this.agencies=[];
            console.log("getAdvertiserMapping:loaded in else condition");
          }
          else
          {
            this.agencies=[];
            let advertiserAgencyMappingData = result.Data;
            this.agencies=advertiserAgencyMappingData;
            this.doesAdvertiserAgencyMapListFound=true;
            console.log("getAdvertiserMappingList:Agencies:"+ JSON.stringify(this.agencies));
          }
        }
        else
        {
          this.agencies=[];//this.globalClass.getMainAgenciesData();
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    }
  }

  getAdvertiser_RepTerritory_MappingList(advertiserId)
  {
    if (advertiserId != null) {
      this.ordersService.getAdvertiserRepTerritoryMapping_AgencyList(this.websiteRoot, advertiserId).subscribe(result => {
        this.hideLoader();
        if (result.StatusCode == '1') {
            this.globalSalesValue=[];
            this.repTerritoryArray = [];
            this.dvGroupHTMLArr=[];
            this.parentArray=[];
            let repTerritoryMappingList = result.Data;
            repTerritoryMappingList.forEach(item=>{
              this.dvId = 'dv' + item.TerritoryID + item.RepID;
            const id = this.dvId;
            const dynamicModelName = 'input' + item.TerritoryID + item.RepID;
            this.globalSalesValue[dynamicModelName] = item.Percentage;
            const text = this.globalSalesValue[dynamicModelName];
            const the_string = 'dynamicRowData.input' + item.TerritoryID + '' + item.RepID + '';

            const obj: any = {};
            this.repTerritoryArray=[];
            obj.id = id;
            obj.reps_territoryName = item.RepName + ' | ' + item.TerritoryName
            obj.Commission = item.Commission;
            obj.textboxid = dynamicModelName;
            obj.textboxval = text;
            obj.repsTerrBlockTextVal = text;
            this.repsTerrBlockTextVal=text;
            obj.territoryId = item.TerritoryID;
            obj.repsId = item.RepID;
            this.dvGroupHTMLArr.push(obj);

              this.repTerritoryArray.repsName = item.RepName;
              this.repTerritoryArray.territoryName = item.TerritoryName;
              this.repTerritoryArray.Commission = item.Commission;
              this.repTerritoryArray.repsId = item.RepID;
              this.repTerritoryArray.territoryId = item.TerritoryID;
              this.repTerritoryArray.Split = item.Percentage;
              this.parentArray.push(this.repTerritoryArray);

            });
            this.isOrdersFormRepsSubmitted = true;
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    }
  }

  changeAgency(agtype: any) {
    try {
      const agencySelected = this.orderForm.controls['BillTo'].value;
      const getSelectedData = this.orderForm.controls['Agency'].value;
      if (getSelectedData != null && getSelectedData !== undefined) {
        let agencyId = getSelectedData;
        if (agencyId === undefined || agencyId == null) {
          const Agency = getSelectedData;
          if (Agency !== undefined && Agency != null) {
            agencyId = Agency;
          }
        }
        if (parseInt(agencySelected) === 1 || agencySelected === '1') {
          const partyData = this.agencies;
          this.organizationWiseParty = [];
          if (this.isEditMode === false) {
            const agencyName = partyData.find(x=>x.Id===agencyId).OrganizationName;
            this.billingToContacts = [];
            this.tmpBillingToContacts=[];
            //Calling Party API for getting only bill to contacts for active advertisers
            this.ordersService.getAdvertiserAgency_BillToContact(this.websiteRoot,0,agencyName,'').subscribe(result => {
              if (result != null && result !== undefined && result !== '') {
                const ItemData = result.Items.$values;
                if (ItemData.length > 0) {
                  for (const itemdatavalue of ItemData) {
                    const type = itemdatavalue.$type;
                    if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                      this.tmpBillingToContacts.push(itemdatavalue);
                    }
                  }
                  if(this.tmpBillingToContacts.length>0)
                  {
                    this.billingToContacts = this.tmpBillingToContacts;
                    this.globalClass.setMainBillingToContactsData(this.billingToContacts);
                  }
                } 
              } else {
                this.hideLoader();
                this.billingToContacts = [];
              }
            }, error => {
              this.hideLoader();
              console.log(error);
            });       
            // partyData.forEach((person, key) => {
            //   if (person.PrimaryOrganization !== undefined) {
            //     if (person.PrimaryOrganization.OrganizationPartyId !== undefined) {
            //       const organizationI = person.PrimaryOrganization.OrganizationPartyId;
            //       if (advertiserId === organizationI) {
            //         this.organizationWiseParty.push(person);
            //         if (this.organizationWiseParty.length > 0) {
            //           this.billingToContacts = this.organizationWiseParty;
            //         } else {
            //           this.billingToContacts = [];
            //         }
            //       } else {
            //       }
            //     }
            //   }
            // });

            if (this.billingToContacts !== undefined) {
              if (this.billingToContacts.length === 0) {
                this.billingToContacts = [];
              }
            }

            this.partyAddress = '';
            this.partyCity = '';
            this.partyPostalCode = '';
            this.partyCountry = '';
            this.partyMobile = '';
            this.partyMemberType = '';
            this.partyWork = '';
            this.partyEmail = '';
            this.partyHome = '';
            this.partyState='';
            this.addStreetHTML = '';
            this.isShowCity = false;
            this.isShowPostalCode = false;
            this.isShowCountry = false;
            this.isShowState=false;
          }
        }
      } else {
        if (agencySelected === '1' || agencySelected === 1) {
          this.billingToContacts = [];
        }
      }
    } catch (error) {
      console.log(error);
    }
    if (agtype === 'agency') {
      this.setContactEnableOrDisable('billto');
    }
  }

  changeContact() {
    try {
      this.partyCity = '';
      this.partyPostalCode = '';
      this.partyCountry = '';
      this.partyState='';
      this.partyAddress = '';
      this.partyMobile = '';
      this.partyMemberType = '';
      this.partyWork = '';
      this.partyEmail = '';
      this.partyHome = '';
      this.isShowCity = false;
      this.isShowPostalCode = false;
      this.isShowCountry = false;
      this.isShowState=false;
      const getSelectedContact = this.orderForm.controls['BillingContacts'].value;

      if (getSelectedContact != null) {
        const id = getSelectedContact;
        const value = this.billingToContacts.find(s => s.Id).PersonName.FullName;

        // tslint:disable-next-line:radix
        if (parseInt(id) > 0) {
          if (id !== '' && id !== undefined && id != null) {
            if (value !== '' && value !== undefined && value != null) {
              const partyData = this.billingToContacts;

              partyData.forEach((partyValue, key) => {
                const partyFullName = partyValue.PersonName.FullName;
                const partyId = partyValue.Id;
                // tslint:disable-next-line:radix
                if (parseInt(partyId) === parseInt(id)) {

                  let additionalAttr = partyValue.AdditionalAttributes;
                  if (additionalAttr !== undefined) {
                    additionalAttr = partyValue.AdditionalAttributes.$values;
                    if (additionalAttr !== undefined) {
                      additionalAttr.forEach((addValue, key2) => {
                        const type = addValue.Name;
                        if (type === 'CustomerTypeDescription') {
                          this.partyMemberType = addValue.Value;
                        }
                      });
                    }
                  }

                  let Phones = partyValue.Phones;
                  if (Phones !== undefined) {
                    Phones = partyValue.Phones.$values;
                    if (Phones !== undefined) {
                      Phones.forEach((phoneValue, key2) => {
                        const type = phoneValue.PhoneType;
                        if (type === 'Mobile') {
                          this.partyMobile = this.globalClass.isEmpty(phoneValue.Number);
                          // if (phoneValue.Number === 'undefined') {
                          //   this.partyMobile = '';
                          // } else {
                          //   this.partyMobile = phoneValue.Number;
                          // }
                        } else if (type === '_Work Phone') {
                          this.partyWork = this.globalClass.isEmpty(phoneValue.Number);
                          // if (phoneValue.Number === 'undefined') {
                          //   this.partyWork = '';
                          // } else {
                          //   this.partyWork = phoneValue.Number;
                          // }
                        } else if (type === 'Address') {
                          this.partyHome = this.globalClass.isEmpty(phoneValue.Number);
                          // if (phoneValue.Number === 'undefined') {
                          //   this.partyHome = '';
                          // } else {
                          //   this.partyHome = phoneValue.Number;
                          // }
                        }
                      });
                    }
                  }

                  let Emails = partyValue.Emails;
                  if (Emails !== undefined) {
                    Emails = partyValue.Emails.$values;
                    if (Emails !== undefined) {
                      Emails.forEach((emailValue, key3) => {
                        const type = emailValue.EmailType;
                        if (type === '_Primary') {
                          this.partyEmail = this.globalClass.isEmpty(emailValue.Address);
                          // if (emailValue.Address === '' && emailValue.Address === undefined && emailValue.Address == null) {
                          //   this.partyEmail = '';
                          // } else {
                          //   this.partyEmail = emailValue.Address;
                          // }
                        }
                      });
                    }
                  }

                  let partyAddress = partyValue.Addresses;
                  if (partyAddress !== undefined) {
                    partyAddress = partyValue.Addresses.$values[0];
                    if (partyAddress !== undefined) {
                      partyAddress = partyValue.Addresses.$values[0].Address;
                      if (partyAddress !== undefined) {

                        const addressLines = partyValue.Addresses.$values[0].Address.AddressLines;
                        if (addressLines !== undefined && addressLines != null && addressLines !== '') {
                          const streetArray = partyValue.Addresses.$values[0].Address.AddressLines.$values;
                          let street = '';
                          this.addStreetHTML = '';

                          streetArray.forEach((itemdatavalue, key4) => {

                            if (this.globalClass.isEmpty(itemdatavalue) != '') {
                              if (key4 === 0) {
                                street = '<span>' + itemdatavalue + '</span><br />';
                              } else {
                                street = street + '<span style="margin-left:65px">' + itemdatavalue + '</span><br />';
                              }
                            }
                           
                          });
                         
                          this.addStreetHTML += street;

                          if (partyAddress !== undefined) {
                            let city = partyValue.Addresses.$values[0].Address.CityName;

                            city = this.globalClass.isEmpty(city);
                            this.partyCity = city +  ' ';

                            if (city !== '') {
                              // this.partyCity = partyValue.Addresses.$values[0].Address.CityName;
                              this.isShowCity = true;
                            } else {
                              // this.partyCity = '';
                              this.isShowCity = false;
                            }
                            // console.log(partyValue.Addresses.$values[0].Address.CountrySubEntityCode);
                            let state = partyValue.Addresses.$values[0].Address.CountrySubEntityCode;
                            
                            state = this.globalClass.isEmpty(state);
                            this.partyState = state +   ' ';
                            // console.log(state);
                            if(state!==''){
                              this.isShowState = true;
                            }else{
                              this.isShowState = false;
                              this.partyState =  '';
                            }

                            let postalCode = partyValue.Addresses.$values[0].Address.PostalCode;
                            

                            postalCode = this.globalClass.isEmpty(postalCode);
                            this.partyPostalCode = postalCode +  ''

                            if (postalCode !== '') {
                              // this.partyPostalCode = partyValue.Addresses.$values[0].Address.PostalCode;
                              this.isShowPostalCode = true;
                            } else {
                              // this.partyPostalCode = '';
                              this.isShowPostalCode = false;
                              
                            }

                            let country = partyValue.Addresses.$values[0].Address.CountryName;
                           
                            country = this.globalClass.isEmpty(country);
                            this.partyCountry = country;

                            if (country !== '') {
                              // this.partyCountry = partyValue.Addresses.$values[0].Address.CountryName;
                              this.isShowCountry = true;
                            } else {
                              // this.partyCountry = '';
                              this.isShowCountry = false;
                            }
                          } else {
                            this.isShowCity = false;
                            this.isShowPostalCode = false;
                            this.isShowCountry = false;
                            this.isShowState=false;
                          }
                        } else {
                          this.isShowCity = false;
                          this.isShowPostalCode = false;
                          this.isShowCountry = false;
                          this.isShowState=false;
                        }
                      } else {
                        this.isShowCity = false;
                        this.isShowPostalCode = false;
                        this.isShowCountry = false;
                        this.isShowState=false;
                      }
                    } else {
                      this.isShowCity = false;
                      this.isShowPostalCode = false;
                      this.isShowCountry = false;
                      this.isShowState=false;
                    }
                  } else {
                    this.isShowCity = false;
                    this.isShowPostalCode = false;
                    this.isShowCountry = false;
                    this.isShowState=false;
                  }
                }
              });
            }
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  setSameAsAbove() {
    try {
      const checkboxStatus = this.contactSameAaAbove;
      if (checkboxStatus === true) {
        const flag = this.orderForm.controls['BillTo'].value;
        // tslint:disable-next-line:radix
        if (parseInt(flag) === 0) {
          const organisationDetails = this.organisation;
          this.organisationDetailsID = organisationDetails.Id;

          if (organisationDetails != null && organisationDetails !== undefined && organisationDetails !== '') {
            let partyAddress = organisationDetails;
            const partyValue = organisationDetails;
            if (partyAddress !== undefined) {
              partyAddress = partyValue.Addresses;
              if (partyAddress !== undefined) {
                partyAddress = partyValue.Addresses.$values[0];
                if (partyAddress !== undefined) {
                  partyAddress = partyValue.Addresses.$values[0].Address;
                  if (partyAddress !== undefined) {
                    const addressLines = partyValue.Addresses.$values[0].Address.AddressLines;
                    if (addressLines !== undefined && addressLines != null && addressLines !== '') {
                      const streetArray = partyValue.Addresses.$values[0].Address.AddressLines.$values;
                      this.contact.Address = streetArray;
                    } else {
                      this.contact.Address = '';
                    }

                    const countryCode = partyValue.Addresses.$values[0].Address.CountryCode;
                    if (countryCode !== undefined && countryCode != null && countryCode !== '') {
                      const streetArray = countryCode;
                      this.contactModalForm.controls['selectedCountryValue'].setValue(streetArray);
                    } else {
                      this.contactModalForm.controls['selectedCountryValue'].setValue('');
                    }

                    const cityName = partyValue.Addresses.$values[0].Address.CityName;
                    if (cityName !== undefined && cityName != null && cityName !== '') {
                      const streetArray = cityName;
                      this.contact.City = streetArray;
                    } else {
                      this.contact.City = '';
                    }

                    const countrySubEntityCode = partyValue.Addresses.$values[0].Address.CountrySubEntityCode;
                    if (countrySubEntityCode !== undefined && countrySubEntityCode != null && countrySubEntityCode !== '') {
                      const streetArray = countrySubEntityCode;
                      this.contact.State = streetArray;
                    } else {
                      this.contact.State = '';
                    }

                    const postalCode = partyValue.Addresses.$values[0].Address.PostalCode;
                    if (postalCode !== undefined && postalCode != null && postalCode !== '') {
                      const streetArray = postalCode;
                      this.contact.PinCode = streetArray;
                    } else {
                      this.contact.PinCode = '';
                    }
                  }
                }
              }
            }
          }

          if (this.contact.State === undefined || this.contact.State == null || this.contact.State === '') {
            this.contact.State = 'nocodefound';
          }

          const organizations = this.organisation;
          if (organizations !== undefined && organizations != null && organizations !== '') {
            const organizationsPhone = this.organisation.Phones;
            if (organizationsPhone !== undefined && organizationsPhone != null && organizationsPhone !== '') {
              const organizationsValue = this.organisation.Phones.$values;
              if (organizationsValue !== undefined && organizationsValue != null && organizationsValue !== '') {
                const phones = this.organisation.Phones.$values;
                phones.forEach((phoneValue, key) => {
                  const type = phoneValue.PhoneType;
                  if (type === 'Mobile') {
                    this.contact.Mobile = phoneValue.Number;
                  } else if (type === '_Work Phone') {
                    this.contact.Work = phoneValue.Number;
                  } else if (type === 'Address') {
                    this.contact.Home = phoneValue.Number;
                  }
                });
              }
            }
          }
          // tslint:disable-next-line:radix
        } else if (parseInt(flag) === 1) {
          const organisationDetails = this.organisation;
          this.organisationDetailsID = organisationDetails.Id;

          if (organisationDetails != null && organisationDetails !== undefined && organisationDetails !== '') {
            let partyAddress = organisationDetails;
            const partyValue = organisationDetails;
            if (partyAddress !== undefined) {
              partyAddress = partyValue.Addresses;
              if (partyAddress !== undefined) {
                partyAddress = partyValue.Addresses.$values[0];
                if (partyAddress !== undefined) {
                  partyAddress = partyValue.Addresses.$values[0].Address;
                  if (partyAddress !== undefined) {

                    const addressLines = partyValue.Addresses.$values[0].Address.AddressLines;
                    if (addressLines !== undefined && addressLines != null && addressLines !== '') {
                      const streetArray = partyValue.Addresses.$values[0].Address.AddressLines.$values;
                      this.contact.Address = streetArray;
                    } else {
                      this.contact.Address = '';
                    }

                    const countryCode = partyValue.Addresses.$values[0].Address.CountryCode;
                    if (countryCode !== undefined && countryCode != null && countryCode !== '') {
                      const streetArray = countryCode;
                      this.contactModalForm.controls['selectedCountryValue'].setValue(streetArray);
                    } else {
                      this.contactModalForm.controls['selectedCountryValue'].setValue('');
                    }

                    const cityName = partyValue.Addresses.$values[0].Address.CityName;
                    if (cityName !== undefined && cityName != null && cityName !== '') {
                      const streetArray = cityName;
                      this.contact.City = streetArray;
                    } else {
                      this.contact.City = '';
                    }

                    const countrySubEntityCode = partyValue.Addresses.$values[0].Address.CountrySubEntityCode;
                    if (countrySubEntityCode !== undefined && countrySubEntityCode != null && countrySubEntityCode !== '') {
                      const streetArray = countrySubEntityCode;
                      this.contact.State = streetArray;
                    } else {
                      this.contact.State = '';
                    }

                    const postalCode = partyValue.Addresses.$values[0].Address.PostalCode;
                    if (postalCode !== undefined && postalCode != null && postalCode !== '') {
                      const streetArray = postalCode;
                      this.contact.PinCode = streetArray;
                    } else {
                      this.contact.PinCode = '';
                    }
                  }
                }
              }
            }
          }

          if (this.contact.State === undefined || this.contact.State == null || this.contact.State === '') {
            this.contact.State = 'nocodefound';
          }

          const organizations = this.organisation;
          if (organizations !== undefined && organizations != null && organizations !== '') {
            const organizationsPhone = this.organisation.Phones;
            if (organizationsPhone !== undefined && organizationsPhone != null && organizationsPhone !== '') {
              const organizationsValue = this.organisation.Phones.$values;
              if (organizationsValue !== undefined && organizationsValue != null && organizationsValue !== '') {
                const phones = this.organisation.Phones.$values;
                phones.forEach((phoneValue, key) => {
                  const type = phoneValue.PhoneType;
                  if (type === 'Mobile') {
                    this.contact.Mobile = phoneValue.Number;
                  } else if (type === '_Work Phone') {
                    this.contact.Work = phoneValue.Number;
                  } else if (type === 'Address') {
                    this.contact.Home = phoneValue.Number;
                  }
                });
              }
            }
          }
        }
      } else {
        this.contactSameAaAbove = false;
        this.contact.Address = '';
        this.contactModalForm.controls['selectedCountryValue'].setValue('');
        this.contact.City = '';
        this.contact.State = '';
        this.contact.PinCode = '';
        this.contact.Mobile = '';
        this.contact.Work = '';
        this.contact.Home = '';
        this.contactModalForm.reset();
        this.contactModalMsgs = false;
      }
    } catch (error) {
      console.log(error);
    }
  }

  onCountryChange() {
    try {
      const getSelectedCountry = this.advertiserModalForm.controls['CountryCode'].value;
      const id = getSelectedCountry;
      const value = this.countries.find(s => s.CountryCode).CountryName;

      if (id !== '' && id !== undefined && id != null) {
        if (value !== '' && value !== undefined && value != null) {
          const countries = this.countries;
          countries.forEach((countryvalue, key) => {
            if (countryvalue.CountryName === value && countryvalue.CountryCode === id) {
              this.subEntityNameCaption = countryvalue.SubEntityNameCaption;
              const state = countryvalue.CountrySubEntities;
              if (state !== '' && state !== undefined && state != null && state.$values.length > 0) {
                this.isStatehave = true;
                this.advertiser.State = '';
                this.states = state.$values;
                this.advertiser.CountrySubEntity = 'statelistY';
                this.advertiserModalForm.controls['State'].setValue('');
              } else {
                this.states = [];
                this.advertiser.State = '';
                this.isStatehave = false;
                this.advertiser.CountrySubEntity = 'statelistN';
                this.advertiserModalForm.controls['State'].setValue('');
              }
            }
          });
        }
      } else {
        this.states = [];
        this.subEntityNameCaption = 'State';
        this.isStatehave = false;
        this.advertiserModalForm.controls['State'].setValue('');
        this.advertiserModalForm.controls['CountryCode'].setValue('');
      }
    } catch (error) {
      console.log(error);
    }
  }

  onCountryChange_Agency() {
    try {
      const getSelectedCountry = this.agencyModalForm.controls['CountryCode'].value;
      const id = getSelectedCountry;
      const value = this.countries.find(s => s.CountryCode).CountryName;
      if (id !== '' && id !== undefined && id != null) {
        if (value !== '' && value !== undefined && value != null) {
          const countries = this.countries;
          countries.forEach((countryvalue, key) => {
            if (countryvalue.CountryName === value && countryvalue.CountryCode === id) {
              this.subEntityNameCaption_Agency = countryvalue.SubEntityNameCaption;
              const state = countryvalue.CountrySubEntities;
              if (state !== '' && state !== undefined && state != null && state.$values.length > 0) {
                this.isStatehave_Agency = true;
                this.agency.State = '';
                this.states = state.$values;
                this.agency.CountrySubEntity_Agency = 'statelistY';
                this.agencyModalForm.controls['State'].setValue('');
              } else {
                this.isStatehave_Agency = false;
                this.agency.State = '';
                this.states = [];
                this.agency.CountrySubEntity_Agency = 'statelistN';
                this.agencyModalForm.controls['State'].setValue('');
              }
            }
          });
        }
      } else {
        this.states = [];
        this.subEntityNameCaption_Agency = 'State';
        this.isStatehave_Agency = false;
        this.agencyModalForm.controls['State'].setValue('');
        this.agencyModalForm.controls['CountryCode'].setValue('');
      }
    } catch (error) {
      console.log(error);
    }
  }

  changeCountry_Contacts() {
    try {
      const getSelectedCountry = this.contactModalForm.controls['selectedCountryValue'].value;
      const id = getSelectedCountry;
      const value = this.countries.find(s => s.CountryCode).CountryName;
      if (id !== '' && id !== undefined && id != null) {
        if (value !== '' && value !== undefined && value != null) {
          const countries = this.countries;
          countries.forEach((countryvalue, key) => {
            if (countryvalue.CountryName === value && countryvalue.CountryCode === id) {
              this.subEntityNameCaption_Contact = countryvalue.SubEntityNameCaption;
              const state = countryvalue.CountrySubEntities;
              if (state !== '' && state !== undefined && state != null && state.$values.length > 0) {
                this.isStatehave_Contact = true;
                this.states = state.$values;
                this.contact.CountrySubEntity_Contact = 'statelistY';
                this.contactModalForm.controls['State'].setValue('');
              } else {
                this.isStatehave_Contact = false;
                this.contact.State = '';
                this.contact = [];
                this.contact.CountrySubEntity_Contact = 'statelistN';
                this.contactModalForm.controls['State'].setValue('');
              }
            }
          });
        }
      } else {
        this.states = [];
        this.subEntityNameCaption_Contact = 'State';
        this.isStatehave_Contact = false;
        this.contactModalForm.controls['State'].setValue('');
        this.contactModalForm.controls['selectedCountryValue'].setValue('');
      }
    } catch (error) {
      console.log(error);
    }
  }

  changeReps() {
    try {
      this.repsId = this.ordersFormReps.controls['SelectedReps'].value;
      if (this.repsId !== undefined || this.repsId != null) {
        this.ordersService.getAllTerritories(this.repsId).subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data.length > 0) {
              this.ordersFormReps.controls['SelectedTerritories'].setValue(null);
              this.territories = result.Data;
            } else {
              this.territories = [];
              this.ordersFormReps.controls['SelectedTerritories'].setValue(null);
            }
          } else {
            this.toastr.error(result.Message, 'Error');
          }
        }, error => {
          console.log(error);
        });
      } else {
        this.territories = [];
      }
    } catch (error) {
      console.log(error);
    }
  }

  changeTerritory() {
    try {
      this.territoryID = this.ordersFormReps.controls['SelectedTerritories'].value;
    } catch (error) {
      console.log(error);
    }
  }

  setContactEnableOrDisable(strtype: any) {
    console.log("setcontactenableordisable:"+ strtype);
    try {
      const changeAdverdtiserValue = this.orderForm.controls['Advertiser'].value;
      const changeAgencyValue = this.orderForm.controls['Agency'].value;
      const changeBillToValue = this.orderForm.controls['BillTo'].value;
      if (changeBillToValue === '1' || changeBillToValue === 1) {
        this.orderForm.controls['BillingContacts'].setValue(null);
      } else if (changeBillToValue === '0' || changeBillToValue === 0) {
        this.orderForm.controls['BillingContacts'].setValue(null);
      }
      if (changeAdverdtiserValue !== 0 && changeAdverdtiserValue !== undefined && changeAdverdtiserValue != null) {
        if (changeAgencyValue !== 0 && changeAgencyValue !== undefined && changeAgencyValue != null) {
          if (parseInt(changeBillToValue) === 0 || parseInt(changeBillToValue) === 1) {
            this.setNoClick = false;
          } else {
            this.setNoClick = false;
          }
        } else {
          this.setNoClick = false;
        }
      } else {
        this.setNoClick = false;
      }
      if (parseInt(changeBillToValue) === 0) {
        this.isRequired = false;
      } else if (parseInt(changeBillToValue) === 1) {
        this.isRequired = true;
      }
    } catch (error) {
      console.log(error);
    }
    if (strtype === 'billto') {
      this.setBillToContact();
    }
  }

  setBillToContact() {
    try {
      const changeBillToValue = this.orderForm.controls['BillTo'].value;

      if (parseInt(changeBillToValue) === 0 || parseInt(changeBillToValue) === 1) {
        if (parseInt(changeBillToValue) === 0) {
          const advertiserId = this.orderForm.controls['Advertiser'].value;
          if (advertiserId != null && advertiserId !== undefined) {
            this.billingToContacts = [];
            if (parseInt(advertiserId) > 0) {
              const advertiserName = this.advertisers.find(x=>x.Id===advertiserId).OrganizationName;
              this.billingToContacts = [];
              this.tmpBillingToContacts=[];
              //Calling Party API for getting only bill to contacts for active advertisers
              this.ordersService.getAdvertiserAgency_BillToContact(this.websiteRoot,0,advertiserName,'').subscribe(result => {
                if (result != null && result !== undefined && result !== '') {
                  const ItemData = result.Items.$values;
                  if (ItemData.length > 0) {
                    for (const itemdatavalue of ItemData) {
                      const type = itemdatavalue.$type;
                      if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                        this.tmpBillingToContacts.push(itemdatavalue);
                      }
                    }
                    if(this.tmpBillingToContacts.length>0)
                    {
                      this.billingToContacts = this.tmpBillingToContacts;
                      this.globalClass.setMainBillingToContactsData(this.billingToContacts);
                    }
                  }
                } else {
                  this.hideLoader();
                  this.billingToContacts = [];
                }
              }, error => {
                this.hideLoader();
                console.log(error);
              });
            } else {
              this.toastr.info('please select advertiser', 'Information!');
              this.organizationWiseParty = [];
            }
            this.addStreetHTML='';
            this.partyCity = '';
            this.partyPostalCode = '';
            this.partyCountry = '';
            this.partyAddress = '';
            this.partyMobile = '';
            this.partyMemberType = '';
            this.partyWork = '';
            this.partyEmail = '';
            this.partyHome = '';
            this.partyState = '';
            //this.billingToContacts = this.organizationWiseParty;
          } else {
            this.toastr.info('please select advertiser', 'Information!');
            this.organizationWiseParty = [];
            this.addStreetHTML='';
            this.partyCity = '';
            this.partyPostalCode = '';
            this.partyCountry = '';
            this.partyAddress = '';
            this.partyMobile = '';
            this.partyMemberType = '';
            this.partyWork = '';
            this.partyEmail = '';
            this.partyState = '';
            this.partyHome = '';
            //this.billingToContacts = this.organizationWiseParty;
          }
        } 
        else if (parseInt(changeBillToValue) === 1) {
          const agencyId = this.orderForm.controls['Agency'].value;
          console.log("setBillToContact:"+ agencyId);
          if (agencyId != null && agencyId !== undefined) {
            if (parseInt(agencyId) > 0) {
              const agencyData = this.agencies;
              //console.log("agencyData:settilltocontact:"+ JSON.stringify(agencyData));
              let organizationName='';
              this.billingToContacts = [];
              this.tmpBillingToContacts=[];
              try
              {
                organizationName = agencyData.find(x=>x.Id===agencyId).OrganizationName;
              }
              catch(error)
              {
                console.log("setBillToContact:Exception:entered");
                this.agencies=[];
                this.ordersService.getOrganisationById(this.websiteRoot,agencyId).subscribe(result => {
                  if (result != null && result !== undefined && result !== '') {
                    const ItemData = result;
                    this.tmpBillingToContacts.push(ItemData);
                    if(this.tmpBillingToContacts.length>0)
                      {
                        this.agencies = this.tmpBillingToContacts;
                        this.globalClass.setMainAgenciesData(this.agencies);
                        organizationName = ItemData.OrganizationName;
                        console.log("setBillToContact:Exception:found organizationid"+organizationName);
                      }
                  } else {
                    this.hideLoader();
                    this.agencies = [];
                  }
                }, error => {
                  this.hideLoader();
                  console.log(error);
                });  
              }   
              setTimeout(() => {
                //Calling Party API for getting only bill to contacts for active advertisers
                this.ordersService.getAdvertiserAgency_BillToContact(this.websiteRoot,0,organizationName,'').subscribe(result => {
                  if (result != null && result !== undefined && result !== '') {
                    const ItemData = result.Items.$values;
                    if (ItemData.length > 0) {
                      for (const itemdatavalue of ItemData) {
                        const type = itemdatavalue.$type;
                        if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                          this.tmpBillingToContacts.push(itemdatavalue);
                        }
                      }
                      if(this.tmpBillingToContacts.length>0)
                      {
                        this.billingToContacts = this.tmpBillingToContacts;
                        this.globalClass.setMainBillingToContactsData(this.billingToContacts);
                      }
                    } 
                  } else {
                    this.hideLoader();
                    this.billingToContacts = [];
                  }
                }, error => {
                  this.hideLoader();
                  console.log(error);
                }); 
              }, 1000);
            } else {
              this.toastr.info('please select agency', 'Information!');
              this.organizationWiseParty = [];
              this.billingToContacts=[];
            }
            this.partyState = '';
            this.partyCity = '';
            this.addStreetHTML='';
            this.partyPostalCode = '';
            this.partyCountry = '';
            this.partyAddress = '';
            this.partyMobile = '';
            this.partyMemberType = '';
            this.partyWork = '';
            this.partyEmail = '';
            this.partyHome = '';
            //this.billingToContacts = this.organizationWiseParty;
          } else {
            this.toastr.info('please select agency', 'Information!');
            this.organizationWiseParty = [];
            this.partyState = '';
            this.addStreetHTML='';
            this.partyCity = '';
            this.partyPostalCode = '';
            this.partyCountry = '';
            this.partyAddress = '';
            this.partyMobile = '';
            this.partyMemberType = '';
            this.partyWork = '';
            this.partyEmail = '';
            this.partyHome = '';
            //this.billingToContacts = this.organizationWiseParty;
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  addRepsTerritories() {
    try {
      this.isOrdersFormRepsSubmitted = true;
      this.territoryId = parseInt(this.ordersFormReps.controls['SelectedTerritories'].value);
      let commissionValue;
      try {
        this.territoryName = this.territories.find(s => s.TerritoryId === this.territoryId).TerritoryName;
        commissionValue = this.territories.find(s => s.TerritoryId === this.territoryId).Commission;
      } catch (error) {
        this.territoryName = '';
      }

      const repsId = this.ordersFormReps.controls['SelectedReps'].value;
      let repName = '';
      try {
        repName = this.reps.find(s => s.RepId === repsId).Name;
      } catch (error) {
        repName = '';
      }

      if (isNullOrUndefined(repsId)) {
        this.toastr.info('Please select reps', 'Information');
        return;
      }
      console.log("reps-territories:"+ this.territoryId);
      if (isNullOrUndefined(this.territoryId) || isNaN(this.territoryId)) {
        this.toastr.info('Please select territories', 'Information');
        return;
      }

      if (this.ordersFormReps.valid) {
        const allRepsAndTerritory = this.parentArray;
        let isDuplicate = false;
        if (allRepsAndTerritory.length > 0) {
          let isBreak = false;
          var territoryId :number=+this.territoryId;
          var repId:number =+repsId;
          allRepsAndTerritory.forEach((tempRTvalue, key) => {
            if (parseInt(tempRTvalue.territoryId)   === territoryId &&  parseInt(tempRTvalue.repsId) === repId) {
              if (isBreak === false) {
                this.toastr.info('This reps and territory already exits', 'Information!');
                isBreak = true;
                isDuplicate = true;
              }
            }
          });
        }

        if (isDuplicate === false) {
          this.isOrdersFormRepsSubmitted = false;
          const repsData = this.territories;
          console.log(this.territoryId +' '+ repsId +'-'+commissionValue);
          this.manageAdvertiserRepsTerritories(repsId,this.territoryId,commissionValue,repName,this.territoryName);
          // const commissionData = allRepsAndTerritory.length === 0 ? 100 : (100 / (allRepsAndTerritory.length + 1));
          // const commissionDataTemp = parseFloat(commissionData.toString()).toFixed(2);
          // repsData.forEach((repsValue, key) => {
          //   if (parseInt(repsValue.TerritoryId) === territoryId && parseInt(repsValue.RepId) === repId) {
          //     this.repTerritoryArray.repsName = repsValue.RepName;
          //     this.repTerritoryArray.territoryName = this.territoryName;
          //     this.repTerritoryArray.Commission = repsValue.Commission;
          //     this.repTerritoryArray.repsId = repId;
          //     this.repTerritoryArray.territoryId = territoryId;
          
          //   }
          // });
        } else {
          this.isOrdersFormRepsSubmitted = false;
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  manageAdvertiserRepsTerritories(repId,territoryId,assignedCommission,repsName,territoryName)
  {
    this.dvGroupHTMLArr=[];
    const obj: any = {};
    obj.repsName =  repsName;
    obj.territoryName = territoryName;
    obj.Commission = assignedCommission;
    obj.repsId = repId;
    obj.territoryId = territoryId;
    obj.Split = 0;
    this.parentArray.push(obj);
    let tempArray=[];
    tempArray = this.parentArray;
    const commissionData = tempArray.length === 0 ? 100 : (100 / (tempArray.length));
    const commissionDataTemp = parseFloat(commissionData.toString()).toFixed(2);
    this.parentArray=[];
    tempArray.forEach(item=>{
      this.repTerritoryArray=[];
        this.dvId = 'dv' + item.territoryId + item.repsId;
        const id = this.dvId;
        const dynamicModelName = 'input' + item.territoryId + item.repsId;
        const text = commissionDataTemp;
        const the_string = 'dynamicRowData.input' + item.territoryId + '' + item.repsId + '';
        const obj: any = {};
        obj.id = id;
        obj.reps_territoryName = item.repsName + ' | ' + item.territoryName;
        obj.Commission = item.Commission;
        obj.textboxid = dynamicModelName;
        obj.textboxval = text;
        obj.repsTerrBlockTextVal = text;
        this.repsTerrBlockTextVal = text;
        obj.territoryId = item.territoryId;
        obj.repsId = item.repsId;
        this.dvGroupHTMLArr.push(obj);

        this.repTerritoryArray.repsName = item.repsName;
        this.repTerritoryArray.territoryName = item.territoryName;
        this.repTerritoryArray.Commission = item.Commission;
        this.repTerritoryArray.repsId = item.repsId;
        this.repTerritoryArray.territoryId = item.territoryId;
        this.repTerritoryArray.Split = commissionDataTemp;
        this.parentArray.push(this.repTerritoryArray);
    });
    if(this.parentArray.length===3)
    {
      this.parentArray[this.parentArray.length-1].Split="33.34";
      this.dvGroupHTMLArr[this.parentArray.length-1].repsTerrBlockTextVal="33.34";
      this.dvGroupHTMLArr[this.parentArray.length-1].textboxval="33.34";
    }
    this.isOrdersFormRepsSubmitted = true;
    console.log(this.parentArray);
    this.globalClass.setRepTerritories(this.parentArray);
  }

  setCountryDefault_Advertiser() {

    try {
      const id = 'US';
      const value = 'United States';
      if (id !== undefined && id != null) {
        if (value !== undefined && value != null) {
          const countries = this.countries;
          countries.forEach(countryvalue => {
            if (countryvalue.CountryName === value && countryvalue.CountryCode === id) {
              this.subEntityNameCaption_Agency = countryvalue.SubEntityNameCaption;
              const state = countryvalue.CountrySubEntities;
              if (state !== '' && state !== undefined && state != null && state.$values.length > 0) {
                this.advertiser.CountryCode = 'US';
                this.advertiserModalForm.controls['CountryCode'].setValue(this.advertiser.CountryCode);
                this.isStatehave = true;
                this.advertiser.State = '';
                this.states = state.$values;
                this.advertiser.CountrySubEntity = '';
              } else {
                this.advertiser.CountryCode = 'US';
                this.advertiserModalForm.controls['CountryCode'].setValue(this.advertiser.CountryCode);
                this.isStatehave = false;
                this.advertiser.State = '';
                this.states = [];
                this.advertiser.CountrySubEntity = '';
              }
            }
          });
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  setCountryDefault_Agency() {
    try {
      const id = 'US';
      const value = 'United States';
      if (id !== undefined && id != null) {
        if (value !== undefined && value != null) {
          const countries = this.countries;
          countries.forEach(countryvalue => {
            if (countryvalue.CountryName === value && countryvalue.CountryCode === id) {
              this.subEntityNameCaption_Agency = countryvalue.SubEntityNameCaption;
              const state = countryvalue.CountrySubEntities;
              if (state !== '' && state !== undefined && state != null && state.$values.length > 0) {
                this.isStatehave_Agency = true;
                this.agency.State = '';
                this.agency.CountryCode = 'US';
                this.agencyModalForm.controls['CountryCode'].setValue(this.agency.CountryCode);
                this.states = state.$values;
                this.agency.CountrySubEntity_Agency = '';
              } else {
                this.isStatehave_Agency = false;
                this.agency.State = '';
                this.agency.CountryCode = 'US';
                this.agencyModalForm.controls['CountryCode'].setValue(this.agency.CountryCode);
                this.states = [];
                this.agency.CountrySubEntity_Agency = '';
              }
            }
          });
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  setCountryDefault_Contact() {
    try {
      const id = 'US';
      const value = 'United States';
      if (id !== undefined && id != null) {
        if (value !== undefined && value != null) {
          const countries = this.countries;
          countries.forEach(countryvalue => {
            if (countryvalue.CountryName === value && countryvalue.CountryCode === id) {
              this.subEntityNameCaption_Agency = countryvalue.SubEntityNameCaption;
              const state = countryvalue.CountrySubEntities;
              if (state !== '' && state !== undefined && state != null && state.$values.length > 0) {
                this.isStatehave_Contact = true;
                this.contact.CountryCode = 'US';
                this.contactModalForm.controls['selectedCountryValue'].setValue(this.contact.CountryCode);
                this.states = state.$values;
                this.contact.CountrySubEntity_Contact = '';
              } else {
                this.agency.CountryCode = 'US';
                this.contactModalForm.controls['selectedCountryValue'].setValue(this.contact.CountryCode);
                this.isStatehave_Contact = false;
                this.contact.CountrySubEntity_Contact = '';
              }
            }
          });
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  generateObjectForAgencySave() {
    try {

      let stateId = '';
      let stateValue = '';
      const getSelectedCountry = this.agencyModalForm.controls['CountryCode'].value;
      const getSelectedState = this.agencyModalForm.controls['State'].value;
      const countryId = getSelectedCountry;
      let countryValue = this.countries.find(s => s.CountryCode === countryId).CountryName;
      if (countryId === '') {
        countryValue = '';
      }

      if (this.agency.CountrySubEntity_Agency === 'statelistY') {
        stateId = getSelectedState;
        stateValue = this.states.find(s => s.Code === stateId).Name;
        if (stateId === undefined || stateId === '' || stateId == null) {
          if (stateValue === 'Select State') {
            stateValue = '';
          } else {
            stateValue = this.states.find(s => s.Code === stateId).Name;
          }
        }
      } else if (this.agency.CountrySubEntity_Agency === '') {
        stateId = getSelectedState;
        if (stateId === undefined || stateId === '' || stateId == null) {
          if (stateValue === 'Select State') {
            stateValue = '';
          } else {
            stateValue = '';
          }
        } else {
          stateValue = this.states.find(s => s.Code === stateId).Name;
        }
      } else if (this.agency.CountrySubEntity_Agency === 'statelistN') {
        stateId = '';
        stateValue = this.agency.State;
      }

      let organization = this.agencyModalForm.controls['Organization'].value;
      let email = this.agencyModalForm.controls['Email'].value;
      let address = this.agencyModalForm.controls['Address'].value;
      let streetLine2 = this.agencyModalForm.controls['StreetLine2'].value;
      let city = this.agencyModalForm.controls['City'].value;
      let postalCode = this.agencyModalForm.controls['PostalCode'].value;
      let home = this.agencyModalForm.controls['Home'].value;
      let mobile = this.agencyModalForm.controls['Mobile'].value;
      let work = this.agencyModalForm.controls['Work'].value;


      if (organization === undefined) {
        organization = '';
      }
      if (address === undefined) {
        address = '';
      }
      if(address===null)
      {
        address='';
      }
      if (city === undefined) {
        city = '';
      }
      if (postalCode === undefined) {
        postalCode = '';
      }
      if (mobile === undefined) {
        mobile = '';
      }
      if (email === undefined) {
        email = '';
      }
      if (work === undefined) {
        work = '';
      }
      if (home === undefined) {
        home = '';
      }
      if (streetLine2 === undefined) {
        streetLine2 = '';
      }
      const agencyObject = {
        '$type': 'Asi.Soa.Membership.DataContracts.OrganizationData, Asi.Contracts',
        'OrganizationName': '' + organization + '',
        'Addresses': {
          '$type': 'Asi.Soa.Membership.DataContracts.FullAddressDataCollection, Asi.Contracts',
          '$values': [
            {
              '$type': 'Asi.Soa.Membership.DataContracts.FullAddressData, Asi.Contracts',
              'Address': {
                '$type': 'Asi.Soa.Membership.DataContracts.AddressData, Asi.Contracts',
                'AddressLines': {
                  '$type': 'Asi.Soa.Membership.DataContracts.AddressLineDataCollection, Asi.Contracts',
                  '$values': [
                    '' + address + '',
                    ''+streetLine2+''
                  ]
                },
                'CityName': '' + city + '',
                'CountryCode': '' + countryId + '',
                'CountryName': '' + countryValue + '',
                'CountrySubEntityCode': '' + stateId + '',
                'CountrySubEntityName': '' + stateValue + '',
                // tslint:disable-next-line:max-line-length
                'FullAddress': '' + address + '\r' + city + ', ' + stateId + ', ' + postalCode + ', \r' + countryValue + '',
                'PostalCode': '' + postalCode + '',
              },
              // tslint:disable-next-line:max-line-length
              'AddresseeText': '' + organization + '\r\n' + address + '\r' + city + ', ' + stateId + '  ' + postalCode + '\r' + countryValue + '',
              'AddressPurpose': 'Address',
              'CommunicationPreferences': {
                '$type': 'Asi.Soa.Membership.DataContracts.CommunicationPreferenceDataCollection, Asi.Contracts',
                '$values': [
                  {
                    '$type': 'Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts',
                    'Reason': 'bill'
                  },
                  {
                    '$type': 'Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts',
                    'Reason': 'ship'
                  }
                ]
              },
              'Phone': '' + mobile + ''
            }
          ]
        },
        'Emails': {
          '$type': 'Asi.Soa.Membership.DataContracts.EmailDataCollection, Asi.Contracts',
          '$values': [
            {
              '$type': 'Asi.Soa.Membership.DataContracts.EmailData, Asi.Contracts',
              'Address': '' + email + '',
              'EmailType': '_Primary',
              'IsPrimary': true
            }
          ]
        },
        'Phones': {
          '$type': 'Asi.Soa.Membership.DataContracts.PhoneDataCollection, Asi.Contracts',
          '$values': [
            {
              '$type': 'Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts',
              'Number': '' + work + '',
              'PhoneType': '_Work Phone'
            },
            {
              '$type': 'Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts',
              'Number': '' + mobile + '',
              'PhoneType': 'Mobile'
            },
            {
              '$type': 'Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts',
              'Number': '' + home + '',
              'PhoneType': 'Address'
            }
          ]
        }
      };

      return agencyObject;
    } catch (error) {
      console.log(error);
    }
  }

  objectForSaveContact() {
    try {
      const firstName = this.contactModalForm.controls['FirstName'].value;
      const lastName = this.contactModalForm.controls['LastName'].value;
      let middleName = this.contactModalForm.controls['MiddleName'].value;
      const email = this.contactModalForm.controls['Email'].value;
      let mobile = this.contactModalForm.controls['Mobile'].value;

      if (middleName === undefined) {
        middleName = '';
      }

      if (mobile === undefined) {
        mobile = '';
      }

      const object = {
        '$type': 'Asi.Soa.Membership.DataContracts.PersonData, Asi.Contracts',
        'PersonName': {
          '$type': 'Asi.Soa.Membership.DataContracts.PersonNameData, Asi.Soa.Membership.Contracts',
          'FirstName': firstName,
          'MiddleName': middleName,
          'LastName': lastName
        },
        'PrimaryOrganization': {
          '$type': 'Asi.Soa.Membership.DataContracts.PrimaryOrganizationInformationData, Asi.Contracts',
          'OrganizationPartyId': '' + this.organisationDetailsID + ''
        },
        'Emails': {
          '$type': 'Asi.Soa.Membership.DataContracts.EmailDataCollection, Asi.Contracts',
          '$values': [
            {
              '$type': 'Asi.Soa.Membership.DataContracts.EmailData, Asi.Contracts',
              'Address': '' + email + '',
              'EmailType': '_Primary',
              'IsPrimary': true
            }
          ]
        },
        'Phones': {
          '$type': 'Asi.Soa.Membership.DataContracts.PhoneDataCollection, Asi.Contracts',
          '$values': [
            {
              '$type': 'Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts',
              'Number': mobile,
              'PhoneType': 'Mobile'
            }
          ]
        },
      };
      return object;
    } catch (error) {
      console.log(error);
    }
  }

  generateObjectForAdvertiserSave() {
    try {
      let stateId = '';
      let stateValue = '';
      const getSelectedCountry = this.advertiserModalForm.controls['CountryCode'].value;
      const getSelectedState = this.advertiserModalForm.controls['State'].value;
      const countryId = getSelectedCountry;
      let countryValue = this.countries.find(s => s.CountryCode === countryId).CountryName;
      if (countryId === '') {
        countryValue = '';
      }

      if (this.advertiser.CountrySubEntity === 'statelistY') {
        stateId = getSelectedState;
        stateValue = this.states.find(s => s.Code === stateId).Name;
        if (stateId === undefined || stateId === '' || stateId == null) {
          if (stateValue === 'Select state') {
            stateValue = '';
          } else {
            stateValue = this.states.find(s => s.Code === stateId).Name;
          }
        }
      } else if (this.advertiser.CountrySubEntity === '') {
        stateId = getSelectedState;
        if (stateId === undefined || stateId === '' || stateId == null) {
          stateValue = '';
        } else {
          stateValue = this.states.find(s => s.Code === stateId).Name;
        }
      } else if (this.advertiser.CountrySubEntity === 'statelistN') {
        stateId = '';
        stateValue = this.agency.State;
      }

      let organization = this.advertiserModalForm.controls['Organization'].value;
      let email = this.advertiserModalForm.controls['Email'].value;
      let address = this.advertiserModalForm.controls['Address'].value;
      let streeline2 = this.advertiserModalForm.controls['StreetLine2'].value;
      let city = this.advertiserModalForm.controls['City'].value;
      let postalCode = this.advertiserModalForm.controls['PostalCode'].value;
      let home = this.advertiserModalForm.controls['Home'].value;
      let mobile = this.advertiserModalForm.controls['Mobile'].value;
      let work = this.advertiserModalForm.controls['Work'].value;

      if (organization === undefined) {
        organization = '';
      }
      if (address === undefined) {
        address = '';
      }
      if(address===null)
      {
        address = '';
      }
      if (streeline2 === undefined) {
        streeline2 = '';
      }
      if (city === undefined) {
        city = '';
      }
      if (postalCode === undefined) {
        postalCode = '';
      }
      if (mobile === undefined) {
        mobile = '';
      }
      if (email === undefined) {
        email = '';
      }
      if (work === undefined) {
        work = '';
      }
      if (home === undefined) {
        home = '';
      }
      const advertiserObject = {
        '$type': 'Asi.Soa.Membership.DataContracts.OrganizationData, Asi.Contracts',
        'OrganizationName': '' + organization + '',
        'Addresses': {
          '$type': 'Asi.Soa.Membership.DataContracts.FullAddressDataCollection, Asi.Contracts',
          '$values': [
            {
              '$type': 'Asi.Soa.Membership.DataContracts.FullAddressData, Asi.Contracts',
              'Address': {
                '$type': 'Asi.Soa.Membership.DataContracts.AddressData, Asi.Contracts',
                'AddressLines': {
                  '$type': 'Asi.Soa.Membership.DataContracts.AddressLineDataCollection, Asi.Contracts',
                  '$values': [
                    '' + address + '',
                    ''+streeline2+''
                  ]
                },
                'CityName': '' + city + '',
                'CountryCode': '' + countryId + '',
                'CountryName': '' + countryValue + '',
                'CountrySubEntityCode': '' + stateId + '',
                'CountrySubEntityName': '' + stateValue + '',
                // tslint:disable-next-line:max-line-length
                'FullAddress': '' + address + '\r' + city + ', ' + stateId + ', ' + postalCode + ', \r' + countryValue + '',
                'PostalCode': '' + postalCode + '',
              },
              // tslint:disable-next-line:max-line-length
              'AddresseeText': '' + organization + '\r\n' + address + '\r' + city + ', ' + stateId + ' ' + postalCode + '\r' + countryValue + '',
              'AddressPurpose': 'Address',
              'CommunicationPreferences': {
                '$type': 'Asi.Soa.Membership.DataContracts.CommunicationPreferenceDataCollection, Asi.Contracts',
                '$values': [
                  {
                    '$type': 'Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts',
                    'Reason': 'bill'
                  },
                  {
                    '$type': 'Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts',
                    'Reason': 'ship'
                  }
                ]
              },
             'Phone': '' + mobile + ''
            }
          ]
        },
        'Emails': {
          '$type': 'Asi.Soa.Membership.DataContracts.EmailDataCollection, Asi.Contracts',
          '$values': [
            {
              '$type': 'Asi.Soa.Membership.DataContracts.EmailData, Asi.Contracts',
              'Address': '' + email + '',
              'EmailType': '_Primary',
              'IsPrimary': true
            }
          ]
        },
        'Phones': {
          '$type': 'Asi.Soa.Membership.DataContracts.PhoneDataCollection, Asi.Contracts',
          '$values': [
            {
              '$type': 'Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts',
              'Number': '' + work + '',
              'PhoneType': '_Work Phone'
            },
            {
              '$type': 'Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts',
              'Number': '' + mobile + '',
              'PhoneType': 'Mobile'
            },
            {
              '$type': 'Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts',
              'Number': '' + home + '',
              'PhoneType': 'Address'
            }
          ]
        }
      };
      return advertiserObject;
    } catch (error) {
      console.log(error);
    }
  }

  createAdvertiserButtonName() {
    try {
      document.querySelector('#btnadvertiser').innerHTML = 'Add Advertiser';
    } catch (error) {
    }
  }

  saveOrders() {
    try {
      this.isOrdersFormSubmitted = true;
      this.isOrdersFormRepsSubmitted = true;
      const salesCommission = this.parentArray;
      this.globalClass.setRepTerritories(this.parentArray);
      if(salesCommission.length>0)
      {
        const validationmsgforbillto = this.orderForm.controls['BillTo'].value;
        const validationmsgforagency = this.orderForm.controls['Agency'].value;
        if (validationmsgforbillto === '1' || validationmsgforbillto === 1) {
          if (validationmsgforagency === null || validationmsgforagency === undefined) {
            this.isShowAgencyRequiredMsg = true;
          } else {
            this.isShowAgencyRequiredMsg = false;
          }
        } else {
          this.isShowAgencyRequiredMsg = false;
        }
        const selectedAdvertiser= this.orderForm.controls['Advertiser'].value;

        let adverdtiserId = '';
        let adverdtiserValue = '';
        if (selectedAdvertiser!== undefined && selectedAdvertiser!= null && selectedAdvertiser!== '') {
          adverdtiserId = selectedAdvertiser;
          adverdtiserValue = this.advertisers.find(s => s.Id === selectedAdvertiser).OrganizationName;
        }
      const selectedAgency = this.orderForm.controls['Agency'].value;
      let agencyId = '';
      let agencyValue = '';
      if (selectedAgency !== undefined && selectedAgency != null && selectedAgency !== '') {
        if(this.agencies.length>0)
        {
          agencyId = selectedAgency;
          agencyValue = this.agencies.find(s => s.Id === selectedAgency).OrganizationName;
        }
      }
      const selectedContact = this.orderForm.controls['BillingContacts'].value;
      const contactId = selectedContact;
      const contactValue = this.billingToContacts.find(s => s.Id === selectedContact).PersonName.FullName;

      let objOrder: any = {};
      objOrder.AdOrderId = this.adOrderId;
      objOrder.AdvertiserId = adverdtiserId;
      objOrder.AdvertiserName = adverdtiserValue;
      if (agencyId !== '' && agencyId !== undefined && agencyId != null && agencyId !== '0') {
        objOrder.AgencyId = agencyId;
      } else {
        objOrder.AgencyId = 0;
      }
      objOrder.AgencyName = agencyValue;
      objOrder.BillTo = this.orderForm.controls['BillTo'].value;
      objOrder.BillToContactId = contactId;
      objOrder.BillToContactName = contactValue;
      objOrder.CampaignName = this.orderForm.controls['CampaignName'].value;
      objOrder.AdCommissions = [];
      if (salesCommission.length > 0) {
        salesCommission.forEach(salecommission => {
          const objSales: any = {};
          objSales.RepId = salecommission.repsId;
          objSales.RepsName = salecommission.repsName;
          objSales.TerritoryId = salecommission.territoryId;
          objSales.TerritoryName = salecommission.territoryName;
          objSales.Commission = salecommission.Commission;
          const dynamicModelName1 = 'input' + salecommission.territoryId + salecommission.repsId;
          let value = '';
          try {
            value = (<HTMLInputElement>document.getElementById(dynamicModelName1)).value;
          } catch (error) {
          }

          objSales.Split = value;
          if (value != null || value !== undefined || value !== 'undefined') {
            objSales.Split = value;
          } else {
            objSales.Split = 0;
          }
          objOrder.AdCommissions.push(objSales);
        });
      }
      if (this.ordersFormReps.valid) {
        this.checkValidation();
        if (this.orderForm.valid) {
          if (objOrder.AdCommissions.length > 0) {
            const commissions = objOrder.AdCommissions;
            let totalComm = 0;
            try {
              commissions.forEach(Commission => {
                const comm = parseFloat(Commission.Split);
                totalComm += comm;
              });
            } catch (error) {
            }

            if (totalComm > 100) {
              this.toastr.info('Percentage should not be greater than 100', 'Information!');
            } else if (totalComm < 100) {
              this.toastr.info('Percentage should not be less than 100', 'Information!');
            } else {
              if (objOrder.AdOrderId >= 0) {
                this.globalClass.setbillingDetails(objOrder);
                this.globalClass.setFromPreviousTab(false);
                this.globalClass.setFromProductionTab(false);
                const orderId = objOrder.AdOrderId;
                let mediaOrder = '';
                if (this.mediaOrderId !== undefined && this.mediaOrderId !== '' && this.mediaOrderId != null) {
                  mediaOrder = this.mediaOrderId;
                }
                this.isOrdersFormSubmitted = false;
                this.isOrdersFormRepsSubmitted = false;
                this.globalClass.setOrderId(orderId);
                console.log("saveOrder:"+ JSON.stringify(objOrder));
                objOrder = {};
                this.adOrderId = 0;
                this.removeDynamicAddedCommissionHTML();
                this.orderForm.reset();
                this.ordersFormReps.reset();
                this.globalClass.setBillingDetailsMediaOrderId(mediaOrder);
                this.orderTabService.send_OrderTab('MediaScheduleTab');
              }
            }
          } else {
            this.toastr.info('Please select alteast one reps and territory', 'Information!');
          }
        } else {
          console.log('ordersForm invalid');
        }
      } else {
        console.log('ordersFormReps Invalid');
      }
      }
      else{
        this.toastr.info('Please select alteast one reps and territory', 'Error!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  loadData() {
    const hasNext = true;
    const offset = 0;
    const advertisers = [];
    const contacts = [];
    const issetloader = false;
    this.getAdvertiserData(hasNext, offset, advertisers, contacts, issetloader);
  }

  saveAdvertiser() {
    try {
      if (this.advertiserModalForm.valid) {
        const advertiserObject = this.generateObjectForAdvertiserSave();
        if (advertiserObject != null && advertiserObject !== undefined) {
          if (this.advertiser.AdvertiserId === 0) {
            this.showLoader();
            this.ordersService.saveOrganization(this.websiteRoot, advertiserObject).subscribe(result => {
              if (result != null && result !== undefined && result !== '') {
                this.advertiserModalForm.reset();
                $('[data-dismiss=modal]').trigger({ type: 'click' });
                $('[data-dismiss=modal]').trigger({ type: 'click' });
                this.advertiserModalMsgs = false;
                this.hideLoader();
                this.advertiser = {};
                this.advertiser.AdvertiserId = 0;
                // tslint:disable-next-line:radix
                if (parseInt(result.Id) > 0) {
                  const aData = this.advertisers;
                  this.advertisers = [];
                  this.advertisers = aData;
                  this.advertisers.push(result);
                  this.advertisers = [...this.advertisers];
                  this.agencies = this.advertisers;

                  this.partyAndOrganizationData.push(result);
                  this.globalClass.setMainAdvertisersData(this.advertisers);
                  this.globalClass.setMainAgenciesData(this.advertisers);
                  this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
                }

                this.createAdvertiserButtonName();
                this.isStatehave = false;
                this.subEntityNameCaption = 'State';

                this.toastr.success('Save successfully.', 'Success!');
              } else {
                this.hideLoader();
                this.toastr.error(result.Message, 'Error');
              }
            }, error => {
              this.hideLoader();
              console.log(error);
            });
          }
        }
      } else {
        this.advertiserModalMsgs = true;
        this.toastr.error('Please enter mandatory fields in order to add advertier', 'Error!');
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  saveAgency() {
    try {
      if (this.agencyModalForm.valid) {
        const agencyObject = this.generateObjectForAgencySave();
        if (agencyObject != null && agencyObject !== undefined) {
          if (this.agency.AgencyId === 0) {
            this.showLoader();
            this.ordersService.saveOrganization(this.websiteRoot, agencyObject).subscribe(result => {
              if (result != null && result !== undefined && result !== '') {
                $('[data-dismiss=modal]').trigger({ type: 'click' });
                $('[data-dismiss=modal]').trigger({ type: 'click' });
                this.agencyModalForm.reset();
                this.hideLoader();
                this.agency = {};
                this.agency.AgencyId = 0;
                // tslint:disable-next-line:radix
                if (parseInt(result.Id) > 0) {
                  const aData = this.advertisers;
                  this.advertisers = [];
                  this.advertisers = aData;
                  this.advertisers.push(result);

                  this.agencies = this.advertisers;
                  this.agencies = [...this.agencies];
                  this.partyAndOrganizationData.push(result);
                  this.globalClass.setMainAdvertisersData(this.advertisers);
                  this.globalClass.setMainAgenciesData(this.advertisers);
                  this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
                }
                this.toastr.success('Save successfully.', 'Success!');

                this.agencyModalMsgs = false;
                this.isStatehave_Agency = false;
                this.subEntityNameCaption_Agency = 'State';
              } else {
                this.hideLoader();
                this.toastr.error(result.Message, 'Error');
              }
            }, error => {
              this.hideLoader();
              console.log(error);
            });
          }
        }
      } else {
        this.agencyModalMsgs = true;
        this.toastr.error('Please enter mandatory fields in order to add agency', 'Error!');
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  saveContact() {
    try {
      if (this.contactModalForm.valid) {
        const object = this.objectForSaveContact();
        if (object != null && object !== undefined) {
          if (this.contact.ContactId === 0) {
            this.showLoader();
            this.ordersService.saveContact(this.websiteRoot, object).subscribe(result => {
              if (result != null && result !== undefined && result !== '') {
                this.createContactButtonName();
                setTimeout(() => {
                  $('[data-dismiss=modal]').trigger({ type: 'click' });
                  $('[data-dismiss=modal]').trigger({ type: 'click' });
                }, 1000);
                this.contactModalForm.reset();

                this.hideLoader();
                this.contact = {};
                // tslint:disable-next-line:radix
                if (parseInt(result.Id) > 0) {
                  const cData = this.billingToContacts;
                  this.billingToContacts = [];
                  this.billingToContacts = cData;
                  this.billingToContacts.push(result);
                  this.billingToContacts = [...this.billingToContacts];
                  this.globalClass.setMainBillingToContactsData(this.billingToContacts);
                  this.partyAndOrganizationData.push(result);
                  this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
                }

                this.toastr.success('Save successfully.', 'Success!');
                this.contact.ContactId = 0;

                this.contactModalMsgs = false;
              } else {
                this.hideLoader();
                this.toastr.error(result.Message, 'Error');
              }
            }, error => {
              this.hideLoader();
              console.log(error);
            });
          }
        }
      } else {
        this.contactModalMsgs = true;
        this.toastr.error('Please enter mandatory fields in order to add bill to contact', 'Error!');
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  cancelAdvertiser() {
    try {
      this.advertiser = {};
      this.advertiser.AdvertiserId = 0;
      this.isStatehave = false;
      this.subEntityNameCaption = 'State';
      this.advertiserModalForm.reset();
      this.advertiserModalMsgs = false;
    } catch (error) {
    }
  }

  onCancelOrder()
  {
    try{
      const gAmount = 0;
      const nAmount = 0;
      this.globalClass.removeBillingDetails();
      this.globalClass.removeOrderId();
      this.globalClass.setGrossCost((gAmount).toFixed(2));
      this.globalClass.setNetCost((nAmount).toFixed(2));
      this.globalClass.removeIsFrozen();
      this.globalClass.removeMainPartyAndOrganizationData();
      this.globalClass.removeMainAdvertisersData();
      this.globalClass.removeMainAgenciesData();
      this.globalClass.removeMainBillingToContactsData();
      this.globalClass.setFromPreviousTab(false);
      
      this.appComponent.showMainPage = true;
      this.appComponent.showOrderFormPage = false;
      this.appComponent.setAdvertisersTabActive();
    }
    catch(error){}
  }

  cancelContact() {
    this.contactModalForm.reset();
    this.contactModalMsgs = false;
  }

  cancelAgency() {
    try {
      this.agency = {};
      this.agency.AgencyId = 0;
      this.isStatehave_Agency = false;
      this.subEntityNameCaption_Agency = 'State';
      this.agencyModalForm.reset();
      this.agencyModalMsgs = false;
    } catch (e) {
    }
  }

  removeDynamicAddedCommissionHTML() {
    this.dvGroupHTML = '';
  }

  checkValidation() {
    try {
      const getSelectedCountry = this.orderForm.controls['Advertiser'].value;
      const id = getSelectedCountry;
      if (id === undefined || id === '' || id == null) {
      }
      const getSelectedCountry2 = this.orderForm.controls['BillingContacts'].value;

      const id2 = getSelectedCountry2;
      if (id2 === undefined || id2 === '' || id2 == null) {
      }
    } catch (error) {
      console.log(error);
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }

  setClearAllSearchFields(popover) {
    try {
      if (popover.isOpen()) {
        popover.close();
      }
    } catch (error) {
      console.log(error);
    }
  }

  onChangeAdvertiserFilter(selectedValue)
  {
    if(selectedValue==="All")
    {
      $("#divAdvertiserFilter").hide();
    }
    else{
      $("#divAdvertiserFilter").show();
    }
  }

  onChangeAgencyFilter(selectedValue)
  {
    if(selectedValue==="All")
    {
      $("#divAgencyFilter").hide();
    }
    else{
      $("#divAgencyFilter").show();
    }
  }

  clearAdvertiserFilter()
  {
    $("#txtAdvertiserFilterValue").val("");
  }

  clearAgencyFilter()
  {
    $("#txtAgencyFilterValue").val("");
  }

  applyAdvertiserFilter(popover)
  {
    let advertiserFilterValue = $("#txtAdvertiserFilterValue").val();
    let advertiserFilterOption=$("#cboFilterType").val();
    if($.trim(advertiserFilterValue)==="" && advertiserFilterOption!=="All")
    {
      this.toastr.error("Please enter value", 'Error');
    }
    else{
        try 
        {
          this.showLoader();
          const hasNext = true;
          const offset = 0;
          const advertisers = [];
          const contacts = [];
          this.billingToContacts=[];
          this.agencies=[];
          this.advertiser=[];
          this.partyAddress = '';
          this.partyCity = '';
          this.partyState = '';
          this.partyPostalCode = '';
          this.partyCountry = '';
          this.partyMobile = '';
          this.partyMemberType = '';
          this.partyWork = '';
          this.partyEmail = '';
          this.partyHome = '';
          this.addStreetHTML = '';
          this.isShowCity = false;
          this.isShowPostalCode = false;
          this.isShowState = false;
          this.isShowCountry = false;
          this.orderForm.controls['BillingContacts'].setValue(null);
          this.orderForm.controls['Agency'].setValue(null);
          this.orderForm.controls['Advertiser'].setValue(null);
          this.getAdvertiserDataViaFilterSearch(hasNext, offset, advertisers, true,advertiserFilterOption, advertiserFilterValue);
          this.hideLoader();
          if (popover.isOpen()) {
            popover.close();
          }
        }
        catch(error)
        {
          console.log(error);
        }
    }
  }

  getAdvertiserDataViaFilterSearch(hasNext: boolean, offset, advertisers: any[], issetloader: boolean,filterOption,filterValue) {
    try {
      if (issetloader === true) {
        this.showLoader();
      }
      this.ordersService.getAdvertiserByFilterSearch(this.websiteRoot, offset,filterOption,filterValue).subscribe(result => {
        if (result != null && result !== undefined && result !== '') {
          const ItemData = result.Items.$values;
          if (ItemData.length > 0) {
            ItemData.forEach(itemdatavalue => {
              this.partyAndOrganizationData.push(itemdatavalue);
              const type = itemdatavalue.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                advertisers.push(itemdatavalue);
              }
            });
            const totalCount = result.TotalCount;
            const count = result.Count;
            hasNext = result.HasNext;
            offset = result.Offset;
            const limit = result.Limit;
            // tslint:disable-next-line:radix
            const nextOffset = parseInt(offset) + 500;
            if (count === 500) {
              offset = nextOffset;
              this.getAdvertiserDataViaFilterSearch(hasNext, offset, advertisers, true,filterOption,filterValue);
            } else {
              this.hideLoader();
              this.advertisers = advertisers;
              this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
              this.globalClass.setMainAdvertisersData(this.advertisers);
            }
          } else {
            this.hideLoader();
            this.advertisers = advertisers;
            this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
          }
        } else {
          this.hideLoader();
          this.advertisers = [];
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  applyAgencyFilter(popover)
  {
    let agencyFilterValue = $("#txtAgencyFilterValue").val();
    let agencyFilterOption=$("#cboAgencyFilterType").val();
    if($.trim(agencyFilterValue)==="" && agencyFilterOption!=="All")
    {
      this.toastr.error("Please enter value", 'Error');
    }
    else{
        try 
        {
          this.showLoader();
          const hasNext = true;
          const offset = 0;
          const agency = [];
          this.billingToContacts=[];
          this.agencies=[];
          this.partyAddress = '';
          this.partyCity = '';
          this.partyState = '';
          this.partyPostalCode = '';
          this.partyCountry = '';
          this.partyMobile = '';
          this.partyMemberType = '';
          this.partyWork = '';
          this.partyEmail = '';
          this.partyHome = '';
          this.addStreetHTML = '';
          this.isShowCity = false;
          this.isShowPostalCode = false;
          this.isShowState = false;
          this.isShowCountry = false;
          this.orderForm.controls['BillingContacts'].setValue(null);
          this.orderForm.controls['Agency'].setValue(null);
          this.getAgencyDataViaFilterSearch(hasNext, offset, agency, true,agencyFilterOption, agencyFilterValue);
          this.hideLoader();
          if (popover.isOpen()) {
            popover.close();
          }
        }
        catch(error)
        {
          console.log(error);
        }
    }
  }

  getAgencyDataViaFilterSearch(hasNext: boolean, offset, agency: any[], issetloader: boolean,filterOption,filterValue) {
    try {
      if (issetloader === true) {
        this.showLoader();
      }
      this.ordersService.getAdvertiserByFilterSearch(this.websiteRoot, offset,filterOption,filterValue).subscribe(result => {
        if (result != null && result !== undefined && result !== '') {
          const ItemData = result.Items.$values;
          if (ItemData.length > 0) {
            ItemData.forEach(itemdatavalue => {
              const type = itemdatavalue.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                agency.push(itemdatavalue);
              }
            });
            const totalCount = result.TotalCount;
            const count = result.Count;
            hasNext = result.HasNext;
            offset = result.Offset;
            const limit = result.Limit;
            const nextOffset = parseInt(offset) + 500;
            if (count === 500) {
              offset = nextOffset;
              this.getAgencyDataViaFilterSearch(hasNext, offset, agency, true,filterOption,filterValue);
            } else {
              this.hideLoader();
              this.agencies = agency;
              this.globalClass.setMainAgenciesData(agency);
            }
          } else {
            this.hideLoader();
            this.agencies = agency;
            this.globalClass.setMainAgenciesData(agency);
          }
        } else {
          this.hideLoader();
          this.agencies = [];
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }
}
