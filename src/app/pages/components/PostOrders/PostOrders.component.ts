import { Component, OnInit, AfterViewInit, OnDestroy, Renderer, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as alasql from 'alasql';
import { saveAs } from '@progress/kendo-file-saver';

import { PostOrdersService } from '../../services/PostOrders.service';
import { DataTableDirective } from 'angular-datatables';
import { environment } from '../../../../environments/environment';
import { AppComponent } from '../../../app.component';
import { GlobalClass } from '../../GlobalClass';

import 'select2';
import { Subject } from 'rxjs';
import { _getComponentHostLElementNode } from '@angular/core/src/render3/instructions';
declare const $, jQuery: any;



@Component({
  // tslint:disable-next-line:component-selector
  selector: 'asi-PostOrders',
  templateUrl: './PostOrders.component.html',
  styleUrls: ['./PostOrders.component.css']
})
export class PostOrdersComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  postOrderTableDatatableTrigger: Subject<any> = new Subject();
  postOrderTable: DataTables.Settings = {};
  isPostOrder_DataLoad_FirstTime = true;

  /*---------------------------------------*/
  bigCurrentPage = 1;
  totalRecords = 0;
  successRecords = 0;
  /*---------------------------------------*/
  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  offset = 0;
  hasNext = true;
  imgurl = '';
  apiBaseURL = '';
  /*---------------------------------------*/
  comboOrderIds = [];
  orderToPost = [];
  tempSelectedPostOrder = [];
  contacts = [];
  selectedPostOrder = [];
  partyAndOrganizationData = [];
  mediaOrderPartyData=[];
  advertisers = [];
  agencies = [];
  billingToContacts = [];
  postOrders = [];
  tempPostOrders=[];
  postOrder: any = {};
  objDelivery: any = {};
  /*---------------------------------------*/
  mediaAssets = [];     // Drop down
  issueDates = [];      // Drop down
  adTypes = [];         // Drop down
  POadvertisers = [];   // Drop down
  /*---------------------------------------*/
  isLazyLoading = false;
  isSelect2CustomSearch = false;
  setRedText = false;
  setGreenText = false;
  isStatusActive = true;
  isStatusHTMLActive = false;
  selectedAllPostOrders = false;
  isShowDefaultTable = false;
  isShowPagination = false;
  isShowInvoiceDate = false;
  /*---------------------------------------*/
  selectedMediaAsset = '';
  selectedIssueDate = '';
  selectedAdType = '';
  selectedAdvertiser = '';
  changeDropdown = 0;
  itemsPerPage = 10;
  postOrderDateSelected = 'currentDate';
  isLoaded: boolean;
  rcomboOrderIds: any[];
  noOfTimesCalled: number;
  loadTime = 'onLoadTime';
  /*---------------------------------------*/
  frmPostOrderCustomSearch: FormGroup;

  constructor(
    private postOrdersService: PostOrdersService,
    private toastr: ToastrService,
    private globalClass: GlobalClass,
    private formBuilder: FormBuilder,
    private appComponent: AppComponent,
    private renderer: Renderer) { }

  ngOnInit() {

    try {
      this.frmPostOrderCustomSearch = this.formBuilder.group({
        'MediaAsset': [null],
        'IssueDate': [null],
        'AdType': [null],
        'POadvertiser': [null]
      });
    } catch (error) {
      console.log(error);
    }

    this.postOrder = {};
    this.postOrder.chkIssueDate = 'IssueDate';

    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }

    this.getDataOnChangeofDropdown(this.loadTime);
    this.getAllPartyDataThroughASI();
  }

  ngAfterViewInit() {
    this.isPostOrder_DataLoad_FirstTime = false;
    this.postOrderTableDatatableTrigger.next();

    try {
      this.renderer.listenGlobal('document', 'click', (event) => {
        if (event.target.hasAttribute('chkMediaOrderID')) {
          const Selected_chkbox_ID = event.target.getAttribute('id');
          const temparr = Selected_chkbox_ID.toString().split('_');
          this.postOrderCheckboxChange(temparr[1]);
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  ngOnDestroy() {
    this.isPostOrder_DataLoad_FirstTime = true;
    this.postOrderTableDatatableTrigger.unsubscribe();
  }

  // Get the data on change of dropdown
  getDataOnChangeofDropdown(loadTime = null) {
    try {
      const data = this.postOrder;
      data.Status = 'Run';
      this.showLoader();

      this.postOrdersService.getDataForPostOrdersDropDown(data, this.reqVerificationToken).subscribe(result => {
        if (result.StatusCode === 1) {
          this.hideLoader();
          if (result.Data != null) {
            if (result.Data.length > 0) {

              const apiData = result.Data;
              let mAssets = apiData[0].MediaAssets;
              const iDates = apiData[0].IssueDates;
              const advertisers = apiData[0].Advertisers;
              let aTypes = apiData[0].AdTypes;

              // set Media Asset Dropdown
              if (mAssets.length > 0) {
                if (loadTime === 'onLoadTime') {
                  this.mediaAssets = [];
                  mAssets = alasql('SELECT * FROM ? AS add order by MediaAssetName asc', [mAssets]);
                  this.mediaAssets = mAssets;
                }
                if (this.selectedMediaAsset !== '' && this.selectedMediaAsset !== undefined) {
                  this.postOrder.MediaAssetId = this.selectedMediaAsset;
                } else {
                  this.frmPostOrderCustomSearch.controls['MediaAsset'].setValue(null);
                }
              } else {
                if (loadTime === 'onLoadTime') {
                  this.mediaAssets = [];
                }
                this.postOrder.MediaAssetId = this.selectedMediaAsset;
              }

              // set Issue Date Dropdown
              if (this.changeDropdown === 0) {
                if (iDates.length > 0) {
                  this.issueDates = [];

                  let issueDate = iDates;
                  const orderByIssueDate = alasql('SELECT * FROM ? AS add order by CoverDate desc', [issueDate]);
                  this.issueDates = orderByIssueDate;

                  for (let i = 0; i < this.issueDates.length; i++) {
                    const tempCoverDate = this.issueDates[i]['CoverDate'];
                    const convertCoverDate = this.globalClass.getDateInFormat(tempCoverDate);
                    this.issueDates[i]['CoverDate'] = convertCoverDate;
                  }

                  // tslint:disable-next-line:radix
                  issueDate = parseInt(this.selectedIssueDate);

                  if (issueDate > 0) {
                    this.selectedIssueDate = '';
                  }

                  if (this.selectedIssueDate !== '' && this.selectedIssueDate !== undefined) {
                    this.postOrder.IssueDateId = this.selectedIssueDate;
                  } else {
                    this.frmPostOrderCustomSearch.controls['IssueDate'].setValue(null);
                  }
                } else {
                  this.issueDates = [];
                  this.postOrder.IssueDateId = '';
                }
              }

              // set AdType Dropdown
              if (this.changeDropdown === 0 || this.changeDropdown === 1) {
                if (aTypes.length > 0) {

                  this.adTypes = [];

                  aTypes = alasql('SELECT * FROM ? AS add order by AdTypeName asc', [aTypes]);
                  this.adTypes = aTypes;

                  // tslint:disable-next-line:radix
                  const adtype = parseInt(this.selectedAdType);
                  if (adtype > 0) {
                    this.selectedAdType = '';
                  }

                  if (this.selectedAdType !== '' && this.selectedAdType !== undefined) {
                    this.postOrder.AdTypeId = this.selectedAdType;
                  } else {
                    this.frmPostOrderCustomSearch.controls['AdType'].setValue(null);
                  }
                } else {
                  this.adTypes = [];
                  this.postOrder.AdTypeId = '';
                }
              }

              // set Advertiser Dropdown
              if (this.changeDropdown === 0 || this.changeDropdown === 1 || this.changeDropdown === 2) {
                if (advertisers.length > 0) {
                  this.setAdvertiserList(advertisers);
                } else {
                  this.POadvertisers = [];
                  this.frmPostOrderCustomSearch.controls['POadvertiser'].setValue(null);
                }
              }

            } else {
              this.mediaAssets = [];
              this.issueDates = [];
              this.adTypes = [];
              this.POadvertisers = [];
            }
          }
        } else {
          this.hideLoader();
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });

    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Get the list of all party data from ASI
  getAllPartyDataThroughASI() {
    try {
      // if (this.globalClass.getMainPartyAndOrganizationData().length <= 0
      //   && this.globalClass.getMainAdvertisersData().length <= 0
      //   && this.globalClass.getMainAgenciesData().length <= 0
      //   && this.globalClass.getMainBillingToContactsData().length <= 0) {
      //   this.getPartyThroughASI();
      // } else {
        
      // }
      this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
        this.advertisers = this.globalClass.getMainAdvertisersData();
        this.agencies = this.globalClass.getMainAgenciesData();
        this.billingToContacts = this.globalClass.getMainBillingToContactsData();
        this.getAllAdvertisers();
        setTimeout(() => {
          this.isLoaded = false;
          this.displayOrderDatatableJquery(this.postOrder);  
        }, 1500);
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Get the list of all media assets from Database
  getAllMediaAssets() {
    try {
      this.postOrdersService.getAllMediaAsset().subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              this.mediaAssets = result.Data;
            } else {
              this.mediaAssets = [];
            }
          } else {
            this.mediaAssets = [];
          }
        } else if (result.StatusCode === 3) {
          this.mediaAssets = [];
        } else {
          this.mediaAssets = [];
        }
      }, error => {
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getAllAdType() {
    try {
      this.postOrdersService.getMediaOrdersAdType(this.reqVerificationToken).subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              this.adTypes = result.Data;
            } else {
              this.adTypes = [];
            }
          } else {
            this.adTypes = [];
          }
        } else if (result.StatusCode === 3) {
          this.adTypes = [];
        } else {
          this.adTypes = [];
        }
      }, error => {
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Get the list of all Issue Dates from Database
  getAllIssueDates() {
    try {
      this.postOrdersService.getAllIssuedate().subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const issueDate = result.Data;
              const orderByIssueDate = alasql('SELECT * FROM ? AS add order by CoverDate desc', [issueDate]);
              this.issueDates = orderByIssueDate;

              for (let i = 0; i < this.issueDates.length; i++) {
                const tempCoverDate = this.issueDates[i]['CoverDate'];
                const convertCoverDate = this.globalClass.getDateInFormat(tempCoverDate);
                this.issueDates[i]['CoverDate'] = convertCoverDate;
              }

            } else {
              this.issueDates = [];
            }
          } else {
            this.issueDates = [];
          }
        } else if (result.StatusCode === 3) {
          this.issueDates = [];
        } else {
          this.issueDates = [];
        }
      }, error => {
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  getPartyThroughASI() {
    try {
      this.showLoader();
      this.postOrdersService.getAllAdvertiser1(this.websiteRoot, this.reqVerificationToken, this.offset).subscribe(result => {
        if (result != null && result !== undefined && result !== '') {
          const ItemData = result.Items.$values;
          if (ItemData.length > 0) {
            ItemData.forEach((itemdatavalue, key) => {
              this.partyAndOrganizationData.push(itemdatavalue);
              const type = itemdatavalue.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                this.advertisers.push(itemdatavalue);
              } else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                this.contacts.push(itemdatavalue);
              }
            });
            this.advertisers = this.advertisers;
            this.agencies = this.advertisers;
            this.billingToContacts = this.contacts;
            const totalCount = result.TotalCount;
            const count = result.Count;
            this.hasNext = result.HasNext;
            this.offset = result.Offset;
            const limit = result.Limit;
            const nextOffset = this.offset + 500;
            if (count === 500) {
              this.offset = nextOffset;
              this.getPartyThroughASI();
            } else {
              this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
              this.globalClass.setMainAdvertisersData(this.advertisers);
              this.globalClass.setMainAgenciesData(this.agencies);
              this.globalClass.setMainBillingToContactsData(this.billingToContacts);
              this.getAllAdvertisers();
              this.hideLoader();
              this.isLoaded = true;
              this.displayOrderDatatableJquery(this.postOrder);
            }
          } else {
            this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
            this.globalClass.setMainAdvertisersData(this.advertisers);
            this.globalClass.setMainAgenciesData(this.agencies);
            this.globalClass.setMainBillingToContactsData(this.billingToContacts);
            this.getAllAdvertisers();
            this.isLoaded = true;
            this.hideLoader();
            this.displayOrderDatatableJquery(this.postOrder);
          }
        } else {
          this.partyAndOrganizationData = [];
          this.advertisers = [];
          this.agencies = [];
          this.billingToContacts = [];
          this.POadvertisers = [];
          this.hideLoader();
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Create list for post order
  createListforPostOrder(objOrders) {
    try {
      if (objOrders != null && objOrders !== undefined && objOrders !== '' && objOrders.length > 0) {
        if (objOrders.length > 0) {
          if (this.globalClass.getMainPartyAndOrganizationData().length > 0) {
            this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
            this.postOrders = [];

            objOrders.forEach((data, key) => {
              const advertiserId = data.AdvertiserId;
              const ST_ID = data.ST_ID;
              const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.partyAndOrganizationData, advertiserId]);
              const objContact = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.partyAndOrganizationData, ST_ID]);
              if (objAdvertiser.length > 0) {
                data.AdvertiserName = objAdvertiser[0].Name;
              } else {
                data.AdvertiserName = '';
              }
              if (objContact.length > 0) {
                data.billToContactName = objContact[0].Name;
              } else {
                data.billToContactName = '';
              }
              this.postOrders.push(data);
            });

            this.postOrders = alasql('SELECT * FROM ? AS add ORDER BY AdvertiserName ASC', [this.postOrders]);
            this.isShowDefaultTable = false;
            this.isShowPagination = true;

          } else {
            this.postOrders = [];
            this.isShowDefaultTable = true;
            this.isShowPagination = false;
            this.getAllPartyDataThroughASI();
          }
        } else {
          this.postOrders = [];
          this.isShowDefaultTable = true;
          this.isShowPagination = false;
        }
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Get the list of all Advertisers
  getAllAdvertisers() {
    try {
      this.showLoader();
      this.postOrdersService.getMediaOrdersAdvertiser(this.reqVerificationToken).subscribe(result => {
        if (result.StatusCode === 1) {
          this.hideLoader();
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const data = result.Data;
              result.Data.forEach((data, key) => {
                const advertiserId = data.AdvertiserId;
                  this.postOrdersService.getAdvertiserDetail_ByAdvertiserId(this.websiteRoot, this.reqVerificationToken, advertiserId).subscribe(result => {
                    let ItemData=[];  
                    if (result != null && result !== undefined && result !== '') {
                      ItemData.push(result.Items.$values);
                      for (const itemdatavalue of ItemData)
                      {
                        this.mediaOrderPartyData.push({Id:advertiserId,OrganizationName:itemdatavalue[0].Name});
                      }
                    }
                }, error => {
                  this.hideLoader();
                  console.log(error);
                });
              });
              this.setAdvertiserList(data);  
            } else {
              this.POadvertisers = [];
            }
          } else {
            this.POadvertisers = [];
          }
        } else {
          this.hideLoader();
          this.POadvertisers = [];
        }
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Set all advertiser list
  setAdvertiserList(objAdvertisers) {
    try {
      if (objAdvertisers.length > 0) {
        if (this.globalClass.getMainAdvertisersData().length > 0) {
          this.advertisers = this.globalClass.getMainAdvertisersData();
          this.POadvertisers = [];
          objAdvertisers.forEach((data, key) => {
            const advertiserId = data.AdvertiserId;
            const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.advertisers, advertiserId]);
            if (objAdvertiser.length > 0) {
              this.POadvertisers.push(objAdvertiser[0]);
            }
          });
          if (this.POadvertisers.length > 0) {
            this.POadvertisers = alasql('SELECT * FROM ? AS add order by OrganizationName asc', [this.POadvertisers]);
            const advertiser = parseInt(this.selectedAdvertiser);
            if (advertiser > 0) {
              this.selectedAdvertiser = '';
            }
            if (this.selectedAdvertiser !== '' && this.selectedAdvertiser !== undefined) {
              this.postOrder.AdvertiserId = this.selectedAdvertiser;
            } else {
              this.frmPostOrderCustomSearch.controls['POadvertiser'].setValue(null);
            }
          }
        } else {
          this.POadvertisers = [];
        }
      } else {
        this.POadvertisers = [];
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  /*----------------------------------------------------------------------*/

  // Dropdown change events
  onChangeMediaAsset() {
    try {
      const id = this.frmPostOrderCustomSearch.controls['MediaAsset'].value;

      if (id !== undefined && id !== '' && id !== 0 && id != null) {

        this.changeDropdown = 0;
        this.selectedMediaAsset = id;

        this.postOrder.AdTypeId = '';
        this.postOrder.AdvertiserId = '';
        this.postOrder.IssueDateId = '';
        this.getDataOnChangeofDropdown();

      } else if (id === '' || id === null) {
        this.postOrder.MediaAssetId = '';
        this.postOrder.IssueDateId = '';
        this.postOrder.AdTypeId = '';
        this.postOrder.AdvertiserId = '';

        this.getAllIssueDates();
        this.getAllAdType();
        this.getAllAdvertisers();
      }

    } catch (e) {
      this.hideLoader();
      console.log(e);
    }
  }

  onChangeIssueDate() {
    try {
      const id = this.frmPostOrderCustomSearch.controls['IssueDate'].value;
      if (id !== undefined && id !== '' && id !== 0 && id != null) {
        this.selectedIssueDate = id;
        this.changeDropdown = 1;
        this.postOrder.AdTypeId = '';
        this.postOrder.AdvertiserId = '';
        this.getDataOnChangeofDropdown();
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  onChangeAdType() {
    try {
      const id = this.frmPostOrderCustomSearch.controls['AdType'].value;
      if (id !== undefined && id !== '' && id !== 0 && id != null) {
        this.selectedAdType = id;
        this.changeDropdown = 2;

        this.postOrder.AdvertiserId = '';
        this.getDataOnChangeofDropdown();
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  onChangeAdvertiser() {
    try {
      const id = this.frmPostOrderCustomSearch.controls['POadvertiser'].value;
      if (id !== undefined && id !== '' && id !== 0 && id != null) {
        this.selectedAdvertiser = id;
        this.changeDropdown = 3;
        this.getDataOnChangeofDropdown();
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // server side pagination implementation
  displayOrderDatatableJquery(filterOptions) {
    try {
      if (this.isLoaded === false) {
        this.isLoaded = true;
        let data = [];
        data = filterOptions;
        this.intiOrderDatable(data);
      } else {
        let data = [];
        data = filterOptions;
        this.intiOrderDatable(data);
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  intiOrderDatable(data) {
    const tempPartyAndOrganizationData = this.mediaOrderPartyData;
    try {
      this.showLoader();
      this.postOrderTable = {
        destroy: true,
        pagingType: 'full_numbers',
        pageLength: 10,
        processing: true,
        serverSide: true,
        search: false,
        orderMulti: false,
        responsive: true,
        scrollCollapse: false,
        ordering: false,
        lengthChange: false,    // hide show N record dropdown
        searching: false,
        ajax: (dataTablesParameters: any, callback) => {
          this.selectedPostOrder = [];
          this.tempSelectedPostOrder = [];
          this.selectedAllPostOrders = false;
          this.postOrders = [];
          this.postOrdersService.getOrdersforpostorder(dataTablesParameters, data).subscribe(resp => {
            this.postOrders = resp.Data;
            //this.tempPostOrders=[];
            this.hideLoader();
            callback({
              recordsTotal: resp.RecordsTotal,
              recordsFiltered: resp.RecordsFiltered,
              data: resp.Data
            });
          }, error => {
            this.hideLoader();
            console.log(error);
          });
        },
        columns: [
          {
            name: '',
            orderable: false,
            render: function (_data, type, row) {
              const customHTML = ' <input type="checkbox"' +
                ' class="checkthis" id=chkmoid_' + row.MediaOrderId +
                ' chkMediaOrderID>';
              return customHTML;
            },
          },
          {
            name: 'Advertiser',
            orderable: false,
            render: function (_data, _type, row) {
              console.log("intiorderdatatable:"+ JSON.stringify(tempPartyAndOrganizationData));
              const advertiserId = row.AdvertiserId;
              let advertiserName = tempPartyAndOrganizationData.find(x=>x.Id===advertiserId).OrganizationName;  //alasql('SELECT * FROM ? AS add WHERE Id LIKE ?', [tempPartyAndOrganizationData, advertiserId]);
              return advertiserName;
            },
          },
          { data: 'BT_ID', name: 'Bill to ID', orderable: false },
          { data: 'ST_ID', name: 'Ship to ID', orderable: false },
          {
            name: 'Gross Cost',
            orderable: false,
            render: function (_data, type, row) {
              const customHTML = row.GrossCost.toFixed(2);
              return customHTML;
            },
          },
          {
            name: 'Net Cost',
            orderable: false,
            render: function (_data, type, row) {
              const customHTML = row.NetCost.toFixed(2);
              return customHTML;
            },
          },
          {
            name: 'Media Asset',
            orderable: false,
            render: function (_data, type, row) {
              const customHTML = row.MediaAssetName;
              return customHTML;
            },
          },
          {
            name: 'Description',
            orderable: false,
            render: function (_data, type, row) {
              let adTypeName = row.AdTypeName;
              let customData = '';
              if (adTypeName === undefined || adTypeName == null || adTypeName === '') {
                adTypeName = '';
              } else {
                customData = adTypeName;
              }

              let adcolorName = row.AdcolorName;
              if (adcolorName === undefined || adcolorName == null || adcolorName === '') {
                adcolorName = '';
              } else {
                customData = '|' + adcolorName;
              }

              let adSizeName = row.AdSizeName;
              if (adSizeName === undefined || adSizeName == null || adSizeName === '') {
                adSizeName = '';
              } else {
                customData = '|' + adSizeName;
              }
              let frequencyName = row.FrequencyName;
              if (frequencyName === undefined || adTypeName == null || adTypeName === '') {
                frequencyName = '';
              } else {
                customData = '|' + frequencyName;
              }
              customData = adTypeName + '|' + adcolorName + '|' + adSizeName + '|' + frequencyName;
              const customDataLength = customData.length - 1;
              const customDataindexOf = customData.indexOf('|');
              const customDataLastIndexOf = customData.lastIndexOf('|');
              if (customDataindexOf === 0) {
                customData.substring(0, 0);
              }
              if (customDataLastIndexOf === customDataLength) {
                customData.substring(customDataLength, customDataLength);
              }
              return customData;
            },
          },
        ]
      };

      if (!this.isPostOrder_DataLoad_FirstTime) {
        setTimeout(() => {
          try {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.postOrderTableDatatableTrigger.next();
            });
          } catch (error) {
            this.hideLoader();
            console.log(error);
          }
        }, 1000);
        this.hideLoader();
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Search orders by filter option
  searchOrdersbyCustomFilter(objPostOrder) {
    console.log(objPostOrder);
    const data = objPostOrder;
    this.isLoaded = true;
    this.displayOrderDatatableJquery(data);
  }


  // Set invoice data show or hide
  setInvoiceDateShowOrHide(IsHideShow) {
    try {
      if (IsHideShow === 'customDate') {
        this.isShowInvoiceDate = true;
        this.postOrderDateSelected = 'customDate';
      } else if (IsHideShow === 'currentDate') {
        this.postOrderDateSelected = 'currentDate';
        this.isShowInvoiceDate = false;
      } else if (IsHideShow === 'issueDate') {
        this.postOrderDateSelected = 'issueDate';
        this.isShowInvoiceDate = false;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Manage check all checkbox of Duplicate IO popup
  toggleAll() {
    try {
      if (this.selectedAllPostOrders !== false) {
        this.selectedAllPostOrders = false;

        const allPostOrderTableTd = $('#tblPostOrderData > tbody > tr > td > input');

        for (let i = 0; i < allPostOrderTableTd.length; i++) {
          const inputElementId = allPostOrderTableTd[i].id;
          const temparr = inputElementId.toString().split('_');

          // tslint:disable-next-line:radix
          const t = parseInt(temparr[1]);
          this.selectedPostOrder[t] = false;
          $('#chkmoid_' + t).prop('checked', false);
        }
      } else {
        this.selectedAllPostOrders = true;
        const allPostOrderTableTd = $('#tblPostOrderData > tbody > tr > td > input');
        for (let i = 0; i < allPostOrderTableTd.length; i++) {
          const inputElementId = allPostOrderTableTd[i].id;
          const temparr = inputElementId.toString().split('_');

          // tslint:disable-next-line:radix
          const t = parseInt(temparr[1]);
          this.selectedPostOrder[t] = true;
          $('#chkmoid_' + t).prop('checked', true);
        }
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Checkbox change event for post order
  postOrderCheckboxChange(objMediaOrderId) {
    try {
      // tslint:disable-next-line:radix
      const t = parseInt(objMediaOrderId);
      const isTrue = this.selectedPostOrder[t];

      if (isTrue === false || isTrue === undefined || isTrue === null) {
        this.selectedPostOrder[t] = true;
        $('#chkmoid_' + t).prop('checked', true);
      } else {
        this.selectedPostOrder[t] = false;
        this.selectedAllPostOrders = false;
        $('#chkmoid_' + t).prop('checked', false);
        return;
      }

      let cnt = 0;

      this.selectedPostOrder.forEach((data, key) => {
        cnt++;
        if (cnt === this.itemsPerPage) {
          if (this.selectedPostOrder.indexOf(false) >= 0) {
            this.selectedAllPostOrders = false;
          } else {
            this.selectedAllPostOrders = true;
          }
        } else {
          this.selectedAllPostOrders = false;
        }
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // display model with orders to post into asi system
  showOrdersToPostInASI() {
    const tempPartyAndOrganizationData = this.mediaOrderPartyData;
    if (this.selectedPostOrder.length > 0) {
      if (this.selectedPostOrder.indexOf(true) > 0) {
        this.totalRecords = 0;
        this.successRecords = 0;
        this.isStatusActive = true;
        this.isStatusHTMLActive = false;
        this.setRedText = false;
        this.setRedText = false;
        this.orderToPost = [];

        this.selectedPostOrder.forEach((data, key) => {
          if (data === true) {
            this.tempSelectedPostOrder[key] = data;
            const objSelectedOrder = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [this.postOrders, (key)]);
            if (objSelectedOrder.length > 0) {
              const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?',
                [this.partyAndOrganizationData, objSelectedOrder[0].AdvertiserId]);

              if (objAdvertiser.length > 0) {
                objSelectedOrder[0].AdvertiserName = objAdvertiser[0].Name;
              } else {
                const advertiserId = objSelectedOrder[0].AdvertiserId;
                let advertiserName = tempPartyAndOrganizationData.find(x=>x.Id===advertiserId).OrganizationName;
                objSelectedOrder[0].AdvertiserName = advertiserName;
              }
              this.orderToPost.push(objSelectedOrder[0]);
            }
          } else {
            try {
              if (this.tempSelectedPostOrder !== null) {
                if (this.tempSelectedPostOrder.length > 0) {
                  this.tempSelectedPostOrder[key].pop();
                }
              }
            } catch (error) {
              console.log(error);
            }
          }
        });
      } else {
        // warning message to select orders
        this.toastr.info('Please select one issue media order', 'Information!');
      }
    } else {
      // warning message to select orders
      this.toastr.info('Please select one issue media order', 'Information!');
    }
  }

  // Insert post orders data  to ASI
  savePostOrdersToASISystem() {
    try {
      this.comboOrderIds = [];
      this.totalRecords = this.orderToPost.length;

      this.tempSelectedPostOrder.forEach((data, key) => {
        if (data === true) {
          const mediaOrderId = key;
          this.objDelivery = {};
          let mediaOrderLines = [];
          let objSelectedOrder = [];
          objSelectedOrder = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [this.postOrders, key]);

          if (this.postOrderDateSelected === 'customDate') {
            this.postOrder.InvoiceDate = this.globalClass.getDateInFormat(this.postOrder.InvoiceDate);
          } else if (this.postOrderDateSelected === 'issueDate') {
            this.postOrder.InvoiceDate = this.globalClass.getDateInFormat(objSelectedOrder[0].CoverDate);
          } else {
            this.postOrder.InvoiceDate = this.globalClass.getDateInFormat(new Date());
          }

          if (objSelectedOrder.length > 0) {
            const billId = objSelectedOrder[0].BT_ID;
            const partyId = objSelectedOrder[0].ST_ID;
            this.postOrdersService.getPartyById(this.websiteRoot, this.reqVerificationToken, partyId).subscribe(result => {
              if (result != null && result !== undefined && result !== '' && result !== 'NotFound') {
                const objParty = result;
                const pId = result.PartyId;
                this.objDelivery.pId = pId;
                this.objDelivery.bId=billId;
                if (pId > 0) {
                  if (objParty != null && objParty !== undefined && objParty !== '') {
                    const partyAddress = objParty.Addresses;
                    if (partyAddress != null && partyAddress !== undefined && partyAddress !== '') {
                      const partyAddressValue = objParty.Addresses.$values;
                      if (partyAddressValue.length > 0) {
                        const partyAddressId = objParty.Addresses.$values[0].FullAddressId;
                        this.objDelivery.AddressId = partyAddressId;

                        const objAddress = objParty.Addresses.$values[0].Address;
                        if (objAddress != null && objAddress !== undefined && objAddress !== '') {

                          const objAddressLines = objParty.Addresses.$values[0].Address.AddressLines;
                          if (objAddressLines != null && objAddressLines !== undefined && objAddressLines !== '') {
                            const objAddressLinesValue = objParty.Addresses.$values[0].Address.AddressLines.$values;
                            if (objAddressLinesValue.length > 0) {
                              this.objDelivery.AddressLinesValue = objAddressLinesValue;
                            } else {
                              const address = [];
                              address.push('Default address');
                              this.objDelivery.AddressLinesValue = address;
                            }
                          } else {
                            const address = [];
                            address.push('Default address');
                            this.objDelivery.AddressLinesValue = address;
                          }

                          const cityName = objParty.Addresses.$values[0].Address.CityName;
                          const countryCode = objParty.Addresses.$values[0].Address.CountryCode;
                          const countryName = objParty.Addresses.$values[0].Address.CountryName;
                          const countrySubEntityCode = objParty.Addresses.$values[0].Address.CountrySubEntityCode;
                          const countrySubEntityName = objParty.Addresses.$values[0].Address.CountrySubEntityName;
                          const countyName = objParty.Addresses.$values[0].Address.CountyName;
                          const fullAddress = objParty.Addresses.$values[0].Address.FullAddress;
                          const postalCode = objParty.Addresses.$values[0].Address.PostalCode;
                          const verificationStatus = objParty.Addresses.$values[0].Address.VerificationStatus;

                          if (cityName != null && cityName !== undefined && cityName !== '') {
                            this.objDelivery.CityName = cityName;
                          } else {
                            this.objDelivery.CityName = '';
                          }

                          if (countryCode != null && countryCode !== undefined && countryCode !== '') {
                            this.objDelivery.CountryCode = countryCode;
                          } else {
                            this.objDelivery.CountryCode = '';
                          }

                          if (countryName != null && countryName !== undefined && countryName !== '') {
                            this.objDelivery.CountryName = countryName;
                          } else {
                            this.objDelivery.CountryName = '';
                          }

                          if (countrySubEntityCode !== null && countrySubEntityCode !== undefined
                            && countrySubEntityCode !== '') {
                            this.objDelivery.CountrySubEntityCode = countrySubEntityCode;
                          } else {
                            this.objDelivery.CountrySubEntityCode = '';
                          }

                          if (countrySubEntityName != null
                            && countrySubEntityName !== undefined
                            && countrySubEntityName !== '') {
                            this.objDelivery.CountrySubEntityName = countrySubEntityName;
                          } else {
                            this.objDelivery.CountrySubEntityName = '';
                          }

                          if (countyName != null && countyName !== undefined && countyName !== '') {
                            this.objDelivery.CountyName = countyName;
                          } else {
                            this.objDelivery.CountyName = '';
                          }

                          if (fullAddress != null && fullAddress !== undefined && fullAddress !== '') {
                            this.objDelivery.FullAddress = fullAddress;
                          } else {
                            this.objDelivery.FullAddress = '';
                          }
                          if (postalCode != null && postalCode !== undefined && postalCode !== '') {
                            this.objDelivery.PostalCode = postalCode;
                          } else {
                            this.objDelivery.PostalCode = '';
                          }
                          if (verificationStatus != null && verificationStatus !== undefined && verificationStatus !== '') {
                            if (verificationStatus === '0' || verificationStatus === 0) {
                              this.objDelivery.VerificationStatus = '';
                            } else {
                              this.objDelivery.VerificationStatus = verificationStatus;
                            }
                          } else {
                            this.objDelivery.VerificationStatus = '';
                          }
                        }

                        // AddressText etc
                        const addressText = objParty.Addresses.$values[0].AddresseeText;
                        const addressPurpose = objParty.Addresses.$values[0].AddressPurpose;
                        const email = objParty.Addresses.$values[0].Email;
                        const phone = objParty.Addresses.$values[0].Phone;
                        const displayName = objParty.Addresses.$values[0].DisplayName;
                        const displayOrganizationTitle = objParty.Addresses.$values[0].DisplayOrganizationTitle;
                        const displayOrganizationName = objParty.Addresses.$values[0].DisplayOrganizationName;

                        if (addressText != null && addressText !== undefined && addressText !== '') {
                          this.objDelivery.AddressText = addressText;
                        } else {
                          this.objDelivery.AddressText = '';
                        }

                        if (addressPurpose != null && addressPurpose !== undefined && addressPurpose !== '') {
                          this.objDelivery.AddressPurpose = addressPurpose;
                        } else {
                          this.objDelivery.AddressPurpose = '';
                        }

                        if (email != null && email !== undefined && email !== '') {
                          this.objDelivery.Email = email;
                        } else {
                          this.objDelivery.Email = '';
                        }

                        if (phone != null && phone !== undefined && phone !== '') {
                          this.objDelivery.Phone = phone;
                        } else {
                          this.objDelivery.Phone = '';
                        }

                        if (displayName != null && displayName !== undefined && displayName !== '') {
                          this.objDelivery.DisplayName = displayName;
                        } else {
                          this.objDelivery.DisplayName = '';
                        }

                        if (displayOrganizationTitle != null
                          && displayOrganizationTitle !== undefined
                          && displayOrganizationTitle !== '') {
                          this.objDelivery.DisplayOrganizationTitle = displayOrganizationTitle;
                        } else {
                          this.objDelivery.DisplayOrganizationTitle = '';
                        }

                        if (displayOrganizationName != null
                          && displayOrganizationName !== undefined
                          && displayOrganizationName !== '') {
                          this.objDelivery.DisplayOrganizationName = displayOrganizationName;
                        } else {
                          this.objDelivery.DisplayOrganizationName = 0;
                        }

                        // Salutation etc
                        const objSalutation = objParty.Addresses.$values[0].Salutation;
                        if (objSalutation != null && objSalutation !== undefined && objSalutation !== '') {
                          const SalutationText = objParty.Addresses.$values[0].Salutation.Text;
                          if (SalutationText != null && SalutationText !== undefined && SalutationText !== '') {
                            this.objDelivery.SalutationText = SalutationText;
                          } else {
                            this.objDelivery.SalutationText = '';
                          }

                          const objSalutationMethod = objParty.Addresses.$values[0].Salutation.SalutationMethod;
                          if (objSalutationMethod != null
                            && objSalutationMethod !== undefined
                            && objSalutationMethod !== '') {
                            const SalutationMethodId =
                              objParty.Addresses.$values[0].Salutation.SalutationMethod.PartySalutationMethodId;
                            if (SalutationMethodId != null
                              && SalutationMethodId !== undefined
                              && SalutationMethodId !== '') {
                              this.objDelivery.SalutationMethodId = SalutationMethodId;
                            } else {
                              this.objDelivery.SalutationMethodId = '';
                            }
                          }
                        }
                      }
                    }
                  }

                  if (mediaOrderId != null && mediaOrderId !== undefined) {
                    this.postOrdersService.getMediaOrderLinesbyOrderId(mediaOrderId, this.reqVerificationToken).subscribe(result1 => {
                      if (result1.StatusCode === 1) {
                        if (result1.Data != null) {
                          if (result1.Data.length > 0) {
                            mediaOrderLines = result1.Data;
                            this.saveComboOrder(this.objDelivery, mediaOrderLines, objSelectedOrder[0]);
                          }
                        }
                      } else if (result1.StatusCode === 3) {
                        this.saveComboOrder(this.objDelivery, mediaOrderLines, objSelectedOrder[0]);
                      } else {
                        this.toastr.error(result1.Msessage, 'Error!');
                      }
                    }, error => {
                      console.log(error);
                    });
                  } else {
                    console.log('media Order Id is null');
                  }
                }

              } else {
                this.toastr.error(result.Message, 'Error');
              }
            }, error => {
              console.log(error);
            });
          }
        }
      });

    } catch (error) {
      console.log(error);
    }
  }

  

  // save Combo Order To ASI System
  saveComboOrder(objDelivery, mediaOrderLines, objSelectedOrder) {
    try {
      if (objDelivery !== undefined && objDelivery != null && objDelivery !== '' &&
        objDelivery.pId !== undefined && objDelivery.pId != null && objDelivery.pId !== '') {

        let objMediaLines = [];
        let grossmediaOrderLines = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ?', [mediaOrderLines, true]);

        grossmediaOrderLines = alasql('SELECT * FROM ? AS add order by SequenceNumber  asc', [grossmediaOrderLines]);

        let netMediaLines = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ?', [mediaOrderLines, false]);
        netMediaLines = alasql('SELECT * FROM ? AS add order by SequenceNumber  asc', [netMediaLines]);

        objMediaLines = this.mediaOrderLinesObject(grossmediaOrderLines, netMediaLines, objSelectedOrder);

        if (objMediaLines.length > 0) {
        } else {
          objMediaLines = [];
        }

        const comboOrderId = objSelectedOrder.MediaOrderId;
        const objComboOrder = {
          '$type': 'Asi.Soa.Commerce.DataContracts.ComboOrderData, Asi.Contracts',
          'ComboOrderId': comboOrderId,
          'Currency': {
            '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
            'CurrencyCode': 'USD',
            'DecimalPositions': 2,
            'ExtensionData': {
              '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
            }
          },

          'Order': {
            '$type': 'Asi.Soa.Commerce.DataContracts.OrderData, Asi.Contracts',
            'BillToCustomerParty': {
              '$type': 'Asi.Soa.Commerce.DataContracts.CustomerPartyData, Asi.Contracts',
              'PartyId': objDelivery.bId
            },

            'Currency': {
              '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
              'CurrencyCode': 'USD',
              'DecimalPositions': 2,
              'ExtensionData': {
                '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
              }
            },

            'Delivery': {
              '$type': 'Asi.Soa.Commerce.DataContracts.DeliveryDataCollection, Asi.Contracts',
              '$values': [
                {
                  '$type': 'Asi.Soa.Commerce.DataContracts.DeliveryData, Asi.Contracts',
                  'Address': {
                    '$type': 'Asi.Soa.Membership.DataContracts.FullAddressData, Asi.Contracts',
                    'AdditionalLines': {
                      '$type': 'Asi.Soa.Membership.DataContracts.AddressLineDataCollection, Asi.Contracts',
                      '$values': []
                    },
                    'Address': {
                      '$type': 'Asi.Soa.Membership.DataContracts.AddressData, Asi.Contracts',
                      'AddressId': objDelivery.AddressId,
                      'AddressLines': {
                        '$type': 'Asi.Soa.Membership.DataContracts.AddressLineDataCollection, Asi.Contracts',
                        '$values':
                          objDelivery.AddressLinesValue
                      },

                      'CityName': objDelivery.CityName,
                      'CountryCode': objDelivery.CountryCode,
                      'CountryName': objDelivery.CountryName,
                      'CountrySubEntityCode': objDelivery.CountrySubEntityCode,
                      'CountrySubEntityName': objDelivery.CountrySubEntityName,
                      'CountyName': objDelivery.CountyName,
                      'FullAddress': objDelivery.FullAddress,
                      'PostalCode': objDelivery.PostalCode,
                      'VerificationStatus': objDelivery.VerificationStatus
                    },
                    'AddresseeText': objDelivery.AddressText,
                    'AddressPurpose': objDelivery.AddressPurpose,
                    'CommunicationPreferences': {
                      '$type': 'Asi.Soa.Membership.DataContracts.CommunicationPreferenceDataCollection, Asi.Contracts',
                      '$values': [
                        {
                          '$type': 'Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts',
                          'Reason': 'default'
                        },
                        {
                          '$type': 'Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts',
                          'Reason': 'mail'
                        },
                        {
                          '$type': 'Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts',
                          'Reason': 'bill'
                        },
                        {
                          '$type': 'Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts',
                          'Reason': 'ship'
                        }
                      ]
                    },

                    'Email': objDelivery.Email,
                    'FullAddressId': objDelivery.AddressId,
                    'Phone': objDelivery.Phone,
                    'Salutation': {
                      '$type': 'Asi.Soa.Membership.DataContracts.PartySalutationData, Asi.Contracts',
                      'SalutationMethod': {
                        '$type': 'Asi.Soa.Membership.DataContracts.PartySalutationMethodSummaryData, Asi.Contracts',
                        'PartySalutationMethodId': objDelivery.SalutationMethodId
                      },
                      'Text': objDelivery.SalutationText
                    },
                    'DisplayName': objDelivery.DisplayName,
                    'DisplayOrganizationTitle': objDelivery.DisplayOrganizationTitle,
                    'DisplayOrganizationName': objDelivery.DisplayOrganizationName
                  },
                  'AddressId': objDelivery.AddressId,
                  'CustomerParty': {
                    '$type': 'Asi.Soa.Commerce.DataContracts.CustomerPartyData, Asi.Contracts',
                    'PartyId': objDelivery.pId
                  },

                  'DeliveryId': '',
                  'DeliveryMethod': {
                    '$type': 'Asi.Soa.Commerce.DataContracts.DeliveryMethodData, Asi.Contracts',
                    'Name': 'United Parcel Service',
                    'DeliveryMethodId': 'UPS'
                  }
                }
              ]
            },

            'OrderDiscount': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },

            'LineDiscountTotal': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },


            'Lines': {
              '$type': 'Asi.Soa.Commerce.DataContracts.OrderLineDataCollection, Asi.Contracts',
              '$values':
                objMediaLines
            },

            'LineTotal': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Amount': objSelectedOrder.NetCost,
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },

            'MiscellaneousChargesTotal': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Amount': 0,
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },

            'OrderDate': this.postOrder.InvoiceDate,
            'OrderTotal': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Amount': objSelectedOrder.NetCost,
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },

            'OriginatorCustomerParty': {
              '$type': 'Asi.Soa.Commerce.DataContracts.CustomerPartyData, Asi.Contracts',
              'PartyId': objDelivery.pId
            },

            'ShippingTotal': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Amount': 0,
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },

            'SoldToCustomerParty': {
              '$type': 'Asi.Soa.Commerce.DataContracts.CustomerPartyData, Asi.Contracts',
              'PartyId': objDelivery.pId
            },

            'SourceCode': '',
            'AdditionalCharges': {
              '$type': 'Asi.Soa.Commerce.DataContracts.AdditionalChargeDataCollection, Asi.Contracts',
              '$values': [
                {
                  '$type': 'Asi.Soa.Commerce.DataContracts.AdditionalChargeData, Asi.Contracts',
                  'AdditionalChargeId': 'Freight',
                  'Description': 'Freight',
                  'TotalAmount': {
                    '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                    'Amount': 0,
                    'Currency': {
                      '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                      'CurrencyCode': 'USD',
                      'DecimalPositions': 2,
                      'ExtensionData': {
                        '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                      }
                    },
                    'IsAmountDefined': true
                  },
                  'Tax': {
                    '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxData, Asi.Contracts',
                    'Details': {
                      '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxDetailDataCollection, Asi.Contracts',
                      '$values': [
                        {
                          '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxDetailData, Asi.Contracts',
                          'TaxAmount': {
                            // tslint:disable-next-line:max-line-length
                            '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                            'Currency': {
                              '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                              'CurrencyCode': 'USD',
                              'DecimalPositions': 2,
                              'ExtensionData': {
                                // tslint:disable-next-line:max-line-length
                                '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                              }
                            },
                            'IsAmountDefined': true
                          },
                          'IsTaxInclusive': true,
                          'TaxAuthority': {
                            '$type': 'Asi.Soa.Commerce.DataContracts.TaxAuthoritySummaryData, Asi.Contracts',
                            'Name': ''
                          }
                        }
                      ]
                    },
                    'TaxTotal': {
                      // tslint:disable-next-line:max-line-length
                      '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                      'Currency': {
                        '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                        'CurrencyCode': 'USD',
                        'DecimalPositions': 2,
                        'ExtensionData': {
                          '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                        }
                      },
                      'IsAmountDefined': true
                    },
                    'InclusiveTaxTotal': {
                      // tslint:disable-next-line:max-line-length
                      '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                      'Currency': {
                        '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                        'CurrencyCode': 'USD',
                        'DecimalPositions': 2,
                        'ExtensionData': {
                          '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                        }
                      },
                      'IsAmountDefined': true
                    }
                  }
                },
                {
                  '$type': 'Asi.Soa.Commerce.DataContracts.AdditionalChargeData, Asi.Contracts',
                  'AdditionalChargeId': 'Handling',
                  'Description': 'Handling',
                  'TotalAmount': {
                    '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                    'Amount': 0,
                    'Currency': {
                      '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                      'CurrencyCode': 'USD',
                      'DecimalPositions': 2,
                      'ExtensionData': {
                        '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                      }
                    },
                    'IsAmountDefined': true
                  },
                  'Tax': {
                    '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxData, Asi.Contracts',
                    'Details': {
                      '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxDetailDataCollection, Asi.Contracts',
                      '$values': [
                        {
                          '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxDetailData, Asi.Contracts',
                          'TaxAmount': {
                            // tslint:disable-next-line:max-line-length
                            '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                            'Currency': {
                              '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                              'CurrencyCode': 'USD',
                              'DecimalPositions': 2,
                              'ExtensionData': {
                                // tslint:disable-next-line:max-line-length
                                '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                              }
                            },
                            'IsAmountDefined': true
                          },
                          'IsTaxInclusive': true,
                          'TaxAuthority': {
                            '$type': 'Asi.Soa.Commerce.DataContracts.TaxAuthoritySummaryData, Asi.Contracts',
                            'Name': ''
                          }
                        }
                      ]
                    },
                    'TaxTotal': {
                      // tslint:disable-next-line:max-line-length
                      '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                      'Currency': {
                        '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                        'CurrencyCode': 'USD',
                        'DecimalPositions': 2,
                        'ExtensionData': {
                          '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                        }
                      },
                      'IsAmountDefined': true
                    },

                    'InclusiveTaxTotal': {
                      // tslint:disable-next-line:max-line-length
                      '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                      'Currency': {
                        '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                        'CurrencyCode': 'USD',
                        'DecimalPositions': 2,
                        'ExtensionData': {
                          '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                        }
                      },
                      'IsAmountDefined': true
                    }
                  }
                },
                {
                  '$type': 'Asi.Soa.Commerce.DataContracts.AdditionalChargeData, Asi.Contracts',
                  'AdditionalChargeId': 'EventCredits',
                  'Description': 'Credits',
                  'TotalAmount': {
                    '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                    'Currency': {
                      '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                      'CurrencyCode': 'USD',
                      'DecimalPositions': 2,
                      'ExtensionData': {
                        '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                      }
                    },
                    'IsAmountDefined': true
                  },
                  'Tax': {
                    '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxData, Asi.Contracts',
                    'Details': {
                      '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxDetailDataCollection, Asi.Contracts',
                      '$values': []
                    },
                    'TaxTotal': {
                      // tslint:disable-next-line:max-line-length
                      '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                      'Currency': {
                        '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                        'CurrencyCode': 'USD',
                        'DecimalPositions': 2,
                        'ExtensionData': {
                          '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                        }
                      },
                      'IsAmountDefined': true
                    },
                    'InclusiveTaxTotal': {
                      // tslint:disable-next-line:max-line-length
                      '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                      'Currency': {
                        '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                        'CurrencyCode': 'USD',
                        'DecimalPositions': 2,
                        'ExtensionData': {
                          '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                        }
                      },
                      'IsAmountDefined': true
                    }
                  }
                }
              ]
            },

            'TaxInformation': {
              '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxInformationData, Asi.Contracts',
              'InclusiveTaxTotal': {
                '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                'Currency': {
                  '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                  'CurrencyCode': 'USD',
                  'DecimalPositions': 2,
                  'ExtensionData': {
                    '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                  }
                },
                'IsAmountDefined': true
              },
              'TaxTotal': {
                '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                'Currency': {
                  '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                  'CurrencyCode': 'USD',
                  'DecimalPositions': 2,
                  'ExtensionData': {
                    '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                  }
                },
                'IsAmountDefined': true
              },
              'OrderTaxes': {
                '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxDataCollection, Asi.Contracts',
                '$values': []
              }
            },

            'TotalBasePrice': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Amount': objSelectedOrder.NetCost,
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },

            'AdditionalAttributes': {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyDataCollection, Asi.Contracts',
              '$values': [
                {
                  '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
                  'Name': 'OrderTotalExcludingCredits',
                  'Value': {
                    '$type': 'System.Decimal',
                    '$value': objSelectedOrder.NetCost
                  }
                }
              ]
            }
          },

          'Invoices': {
            '$type': 'Asi.Soa.Commerce.DataContracts.InvoiceSummaryDataCollection, Asi.Contracts',
            '$values': []
          },

          "Payments": {
            "$type": "Asi.Soa.Commerce.DataContracts.RemittanceDataCollection, Asi.Contracts",
            "$values": [
                {
                    "$type": "Asi.Soa.Commerce.DataContracts.RemittanceData, Asi.Contracts",
                    "PaymentMethod": {
                        "$type": "Asi.Soa.Commerce.DataContracts.PaymentMethodData, Asi.Contracts",
                        "Name": "Pay Later",
                        "PaymentMethodId": "BillMe",
                        "PaymentType": "BillMe",
                        "Message": "You will be billed for the balance due"
                    },
                    "PayorParty": {
                        "$type": "Asi.Soa.Commerce.DataContracts.CustomerPartyData, Asi.Contracts",
                        "PartyId": objDelivery.pId
                    },

                    "ReferenceNumber": "MediaOrder",
                    "Message": "You will be billed for the balance due - Purchase Order Number MediaOrder"
                }
            ]
        }
        };


        const isSuccessCalled = [];
        const isErrorCalled = [];

        if (objComboOrder != null && objComboOrder !== undefined) {
          this.rcomboOrderIds = [];
          this.noOfTimesCalled = 0;

          try {
            // tslint:disable-next-line:radix
            const jsonComboOrderId = parseInt(objComboOrder.ComboOrderId);
            if (jsonComboOrderId > 0) {
              // tslint:disable-next-line:radix
              const isComboOrderIdExits = this.comboOrderIds.indexOf(jsonComboOrderId);
              if (isComboOrderIdExits >= 0) {
              } else {
                // tslint:disable-next-line:radix
                this.comboOrderIds.push(jsonComboOrderId);
              }
            }
          } catch (error) {
            console.log(error);
          }

          let sMIds = '';
          let eMIds = '';
          this.postOrdersService.saveComboOrder(this.websiteRoot, this.reqVerificationToken, objComboOrder).subscribe(result => {
            this.noOfTimesCalled = this.noOfTimesCalled + 1;
            if (result != null && result !== undefined && result !== '') {
              const cId = result.ComboOrderId;
              if (cId !== undefined && cId != null && cId !== '') {
                // tslint:disable-next-line:radix
                const nCID = parseInt(cId);
                if (nCID > 0) {
                  this.rcomboOrderIds.push(nCID);

                  if (this.comboOrderIds.length === this.noOfTimesCalled) {

                    this.comboOrderIds.forEach((data, key) => {
                      // tslint:disable-next-line:radix
                      const isOrderExits = this.rcomboOrderIds.indexOf(parseInt(data));
                      if (isOrderExits >= 0) {
                        if (key === 0) {
                          // tslint:disable-next-line:radix
                          sMIds += parseInt(data);
                        } else {
                          if (sMIds === '') {
                            // tslint:disable-next-line:radix
                            sMIds += parseInt(data);
                          } else {
                            // tslint:disable-next-line:radix
                            sMIds += ',' + parseInt(data);
                          }
                        }
                      } else {
                        if (key === 0) {
                          // tslint:disable-next-line:radix
                          eMIds += parseInt(data);
                        } else {
                          if (eMIds === '') {
                            // tslint:disable-next-line:radix
                            eMIds += parseInt(data);
                          } else {
                            // tslint:disable-next-line:radix
                            eMIds += ',' + parseInt(data);
                          }

                        }
                      }
                    });

                    this.setIsFrozenTrue(sMIds, eMIds);
                  }
                } else {
                  this.rcomboOrderIds.push(0);
                  if (this.comboOrderIds.length === this.noOfTimesCalled) {

                    this.comboOrderIds.forEach((data, key) => {
                      // tslint:disable-next-line:radix
                      const isOrderExits = this.rcomboOrderIds.indexOf(parseInt(data));
                      if (isOrderExits >= 0) {
                        if (key === 0) {
                          // tslint:disable-next-line:radix
                          sMIds += parseInt(data);
                        } else {
                          if (sMIds === '') {
                            // tslint:disable-next-line:radix
                            sMIds += parseInt(data);
                          } else {
                            // tslint:disable-next-line:radix
                            sMIds += ',' + parseInt(data);
                          }
                        }
                      } else {
                        if (key === 0) {
                          // tslint:disable-next-line:radix
                          eMIds += parseInt(data);
                        } else {
                          if (eMIds === '') {
                            // tslint:disable-next-line:radix
                            eMIds += parseInt(data);
                          } else {
                            // tslint:disable-next-line:radix
                            eMIds += ',' + parseInt(data);
                          }
                        }
                      }
                    });


                    this.setIsFrozenTrue(sMIds, eMIds);
                  }
                }
              } else {
                this.rcomboOrderIds.push(0);
                if (this.comboOrderIds.length === this.noOfTimesCalled) {

                  this.comboOrderIds.forEach((data, key) => {
                    // tslint:disable-next-line:radix
                    const isOrderExits = this.rcomboOrderIds.indexOf(parseInt(data));
                    if (isOrderExits >= 0) {
                      if (key === 0) {
                        // tslint:disable-next-line:radix
                        sMIds += parseInt(data);
                      } else {
                        if (sMIds === '') {
                          // tslint:disable-next-line:radix
                          sMIds += parseInt(data);
                        } else {
                          // tslint:disable-next-line:radix
                          sMIds += ',' + parseInt(data);
                        }
                      }
                    } else {
                      if (key === 0) {
                        // tslint:disable-next-line:radix
                        eMIds += parseInt(data);
                      } else {
                        if (eMIds === '') {
                          // tslint:disable-next-line:radix
                          eMIds += parseInt(data);
                        } else {
                          // tslint:disable-next-line:radix
                          eMIds += ',' + parseInt(data);
                        }
                      }
                    }
                  });

                  this.setIsFrozenTrue(sMIds, eMIds);
                }
              }
            } else {
              this.rcomboOrderIds.push(0);
              if (this.comboOrderIds.length === this.noOfTimesCalled) {

                this.comboOrderIds.forEach((data, key) => {
                  // tslint:disable-next-line:radix
                  const isOrderExits = this.rcomboOrderIds.indexOf(parseInt(data));
                  if (isOrderExits >= 0) {
                    if (key === 0) {
                      // tslint:disable-next-line:radix
                      sMIds += parseInt(data);
                    } else {
                      if (sMIds === '') {
                        // tslint:disable-next-line:radix
                        sMIds += parseInt(data);
                      } else {
                        // tslint:disable-next-line:radix
                        sMIds += ',' + parseInt(data);
                      }
                    }
                  } else {
                    if (key === 0) {
                      // tslint:disable-next-line:radix
                      eMIds += parseInt(data);
                    } else {
                      if (eMIds === '') {
                        // tslint:disable-next-line:radix
                        eMIds += parseInt(data);
                      } else {
                        // tslint:disable-next-line:radix
                        eMIds += ',' + parseInt(data);
                      }
                    }
                  }
                });

                this.setIsFrozenTrue(sMIds, eMIds);
              }
            }
          }, error => {
            if (error.status === 400 || error.status === '400') {
              const result = error.error;
              this.noOfTimesCalled = this.noOfTimesCalled + 1;
              if (result != null && result !== undefined && result !== '') {
                // tslint:disable-next-line:radix
                const cId = parseInt(result.ComboOrderId);
                if (cId !== undefined && cId != null) {
                  // tslint:disable-next-line:radix
                  const nCID = cId;
                  if (nCID > 0) {
                    this.rcomboOrderIds.push(nCID);
                    if (this.comboOrderIds.length === this.noOfTimesCalled) {
                      this.comboOrderIds.forEach((data, key) => {
                        // tslint:disable-next-line:radix
                        const isOrderExits = this.rcomboOrderIds.indexOf(parseInt(data));
                        if (isOrderExits >= 0) {
                          if (key === 0) {
                            // tslint:disable-next-line:radix
                            sMIds += parseInt(data);
                          } else {
                            if (sMIds === '') {
                              // tslint:disable-next-line:radix
                              sMIds += parseInt(data);
                            } else {
                              // tslint:disable-next-line:radix
                              sMIds += ',' + parseInt(data);
                            }
                          }
                        } else {
                          if (key === 0) {
                            // tslint:disable-next-line:radix
                            eMIds += parseInt(data);
                          } else {
                            if (eMIds === '') {
                              // tslint:disable-next-line:radix
                              eMIds += parseInt(data);
                            } else {
                              // tslint:disable-next-line:radix
                              eMIds += ',' + parseInt(data);
                            }
                          }
                        }
                      });

                      this.setIsFrozenTrue(sMIds, eMIds);
                    }
                  } else {
                    this.rcomboOrderIds.push(0);
                    if (this.comboOrderIds.length === this.noOfTimesCalled) {

                      this.comboOrderIds.forEach((data, key) => {
                        // tslint:disable-next-line:radix
                        const isOrderExits = this.rcomboOrderIds.indexOf(parseInt(data));
                        if (isOrderExits >= 0) {
                          if (key === 0) {
                            // tslint:disable-next-line:radix
                            sMIds += parseInt(data);
                          } else {
                            if (sMIds === '') {
                              // tslint:disable-next-line:radix
                              sMIds += parseInt(data);
                            } else {
                              // tslint:disable-next-line:radix
                              sMIds += ',' + parseInt(data);
                            }
                          }
                        } else {
                          if (key === 0) {
                            // tslint:disable-next-line:radix
                            eMIds += parseInt(data);
                          } else {
                            if (eMIds === '') {
                              // tslint:disable-next-line:radix
                              eMIds += parseInt(data);
                            } else {
                              // tslint:disable-next-line:radix
                              eMIds += ',' + parseInt(data);
                            }
                          }
                        }
                      });


                      this.setIsFrozenTrue(sMIds, eMIds);
                    }
                  }
                } else {
                  this.rcomboOrderIds.push(0);
                  if (this.comboOrderIds.length === this.noOfTimesCalled) {

                    this.comboOrderIds.forEach((data, key) => {
                      // tslint:disable-next-line:radix
                      const isOrderExits = this.rcomboOrderIds.indexOf(parseInt(data));
                      if (isOrderExits >= 0) {
                        if (key === 0) {
                          // tslint:disable-next-line:radix
                          sMIds += parseInt(data);
                        } else {
                          if (sMIds === '') {
                            // tslint:disable-next-line:radix
                            sMIds += parseInt(data);
                          } else {
                            // tslint:disable-next-line:radix
                            sMIds += ',' + parseInt(data);
                          }
                        }
                      } else {
                        if (key === 0) {
                          // tslint:disable-next-line:radix
                          eMIds += parseInt(data);
                        } else {
                          if (eMIds === '') {
                            // tslint:disable-next-line:radix
                            eMIds += parseInt(data);
                          } else {
                            // tslint:disable-next-line:radix
                            eMIds += ',' + parseInt(data);
                          }
                        }
                      }
                    });
                    this.setIsFrozenTrue(sMIds, eMIds);
                  }
                }
              } else {
                this.rcomboOrderIds.push(0);
                if (this.comboOrderIds.length === this.noOfTimesCalled) {

                  this.comboOrderIds.forEach((data, key) => {
                    // tslint:disable-next-line:radix
                    const isOrderExits = this.rcomboOrderIds.indexOf(parseInt(data));
                    if (isOrderExits >= 0) {
                      if (key === 0) {
                        // tslint:disable-next-line:radix
                        sMIds += parseInt(data);
                      } else {
                        if (sMIds === '') {
                          // tslint:disable-next-line:radix
                          sMIds += parseInt(data);
                        } else {
                          // tslint:disable-next-line:radix
                          sMIds += ',' + parseInt(data);
                        }
                      }
                    } else {
                      if (key === 0) {
                        // tslint:disable-next-line:radix
                        eMIds += parseInt(data);
                      } else {
                        if (eMIds === '') {
                          // tslint:disable-next-line:radix
                          eMIds += parseInt(data);
                        } else {
                          // tslint:disable-next-line:radix
                          eMIds += ',' + parseInt(data);
                        }
                      }
                    }
                  });
                  this.setIsFrozenTrue(sMIds, eMIds);
                }
              }
            }
            console.log(error);
          });
        }
      } else {
        console.log('saveComboOrder condition false');
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  mediaOrderLinesObject(grossmediaOrderLines, netmediaLines, objSelectedOrder) {
    try {
      const dataJson = [];

      const objJsonProduct = {
        '$type': 'Asi.Soa.Commerce.DataContracts.OrderLineData, Asi.Contracts',
        'OrderLineId': objSelectedOrder.MediaOrderId,
        'AdditionalAttributes': {
          '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyDataCollection, Asi.Contracts',
          '$values': [
            {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
              'Name': '',
              'Value': ''
            }
          ]
        },

        'ExtendedAmount': {
          '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
          'Amount': objSelectedOrder.RateCardCost,
          'Currency': {
            '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
            'CurrencyCode': 'USD',
            'DecimalPositions': 2,
            'ExtensionData': {
              '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
            }
          },
          'IsAmountDefined': true
        },

        'Item': {
          '$type': 'Asi.Soa.Commerce.DataContracts.ItemSummaryData, Asi.Contracts',
          'ItemId': objSelectedOrder.ProductCode,
        },
        // tslint:disable-next-line:max-line-length
        'Note': objSelectedOrder.AdTypeName + '|' + objSelectedOrder.AdcolorName + '|' + objSelectedOrder.AdSizeName + '|' + objSelectedOrder.FrequencyName,
        'LineNumber': 1,
        'QuantityBackordered': {
          '$type': 'System.Nullable`1[[Asi.Soa.Commerce.DataContracts.QuantityData, Asi.Contracts]], mscorlib'
        },
        'QuantityOrdered': {
          '$type': 'System.Nullable`1[[Asi.Soa.Commerce.DataContracts.QuantityData, Asi.Contracts]], mscorlib',
          'Amount': 1
        },
        'QuantityShipped': {
          '$type': 'System.Nullable`1[[Asi.Soa.Commerce.DataContracts.QuantityData, Asi.Contracts]], mscorlib',
          'Amount': 1
        },
        'Tax': {
          '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxData, Asi.Contracts',
          'Details': {
            '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxDetailDataCollection, Asi.Contracts',
            '$values': []
          },
          'TaxTotal': {
            '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
            'Currency': {
              '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
              'CurrencyCode': 'USD',
              'DecimalPositions': 2,
              'ExtensionData': {
                '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
              }
            },
            'IsAmountDefined': true
          },
          'TaxableAmountTotal': {
            '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
            'Currency': {
              '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
              'CurrencyCode': 'USD',
              'DecimalPositions': 2,
              'ExtensionData': {
                '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
              }
            },
            'IsAmountDefined': true
          },
          'InclusiveTaxTotal': {
            '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
            'Currency': {
              '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
              'CurrencyCode': 'USD',
              'DecimalPositions': 2,
              'ExtensionData': {
                '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
              }
            },
            'IsAmountDefined': true
          },
          'TaxSchedule': {
            '$type': 'Asi.Soa.Commerce.DataContracts.TaxScheduleSummaryData, Asi.Contracts',
            'Name': ''
          },
          'TaxCategory': {
            '$type': 'Asi.Soa.Commerce.DataContracts.TaxCategorySummaryData, Asi.Contracts'
          }
        },
        'UnitPrice': {
          '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
          'Amount': objSelectedOrder.RateCardCost,
          'Currency': {
            '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
            'CurrencyCode': 'USD',
            'DecimalPositions': 2,
            'ExtensionData': {
              '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
            }
          },
          'IsAmountDefined': true
        },
        'IsUnitPriceOverridden': true,
        'BaseUnitPrice': {
          '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
          'Amount': 0,
          'Currency': {
            '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
            'CurrencyCode': 'USD',
            'DecimalPositions': 2,
            'ExtensionData': {
              '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
            }
          },
          'IsAmountDefined': true
        },

        'Discount': {
          '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
          'Currency': {
            '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
            'CurrencyCode': 'USD',
            'DecimalPositions': 2,
            'ExtensionData': {
              '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
            }
          },
          'IsAmountDefined': true
        },
        'CanCombine': true,
        'SourceCode': ''
      };
      dataJson.push(objJsonProduct);

      let grossCostRunningAmount = '0';
      let netCostRunningAmount = '0';
      grossCostRunningAmount = objSelectedOrder.RateCardCost;
      netCostRunningAmount = objSelectedOrder.RateCardCost;

      if (grossmediaOrderLines.length > 0) {
        grossmediaOrderLines.forEach((data, key) => {
          const moline = data;
          let unitAmount = 0;
          let productCode = '';
          if (moline.GrossInd === true) {
            if (moline.AmountPercent === 0) {
              const tempAmount = parseFloat(grossCostRunningAmount);
              const adjustmentValue = parseFloat(moline.AdjustmentValue);
              let afterMinusAmount: any = 0;
              if (moline.SurchargeDiscount === 0) {
                afterMinusAmount = (tempAmount + adjustmentValue);
                unitAmount = adjustmentValue;
                productCode = 'SA';
              } else {
                afterMinusAmount = (tempAmount - adjustmentValue);
                unitAmount = -adjustmentValue;
                productCode = 'AD';
              }
              afterMinusAmount = afterMinusAmount.toFixed(2);
              grossCostRunningAmount = afterMinusAmount;
              netCostRunningAmount = afterMinusAmount;
            } else if (moline.AmountPercent === 1) {
              const tempAmount = parseFloat(grossCostRunningAmount);
              const adjustmentValue = parseFloat(moline.AdjustmentValue);
              const percentageValueInAmount = ((adjustmentValue * tempAmount) / 100);

              let afterMinusAmount: any = 0;
              if (moline.SurchargeDiscount === 0) {
                afterMinusAmount = (tempAmount + percentageValueInAmount);
                unitAmount = percentageValueInAmount;
                productCode = 'SA';
              } else {
                afterMinusAmount = (tempAmount - percentageValueInAmount);
                unitAmount = -percentageValueInAmount;
                productCode = 'AD';
              }
              afterMinusAmount = afterMinusAmount.toFixed(2);
              grossCostRunningAmount = afterMinusAmount;
              netCostRunningAmount = afterMinusAmount;
            }
          }

          const objLine = {
            '$type': 'Asi.Soa.Commerce.DataContracts.OrderLineData, Asi.Contracts',
            'OrderLineId': data.MediaOrderLineId,
            'AdditionalAttributes': {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyDataCollection, Asi.Contracts',
              '$values': [
                {
                  '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
                  'Name': '',
                  'Value': ''
                }
              ]
            },

            'ExtendedAmount': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Amount': unitAmount,
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },

            'Item': {
              '$type': 'Asi.Soa.Commerce.DataContracts.ItemSummaryData, Asi.Contracts',
              'ItemId': data.ProductCode,
            },
            // tslint:disable-next-line:max-line-length
            'Note': objSelectedOrder.AdTypeName + '|' + objSelectedOrder.AdcolorName + '|' + objSelectedOrder.AdSizeName + '|' + objSelectedOrder.FrequencyName,
            'LineNumber': key + 2,
            'QuantityBackordered': {
              '$type': 'System.Nullable`1[[Asi.Soa.Commerce.DataContracts.QuantityData, Asi.Contracts]], mscorlib'
            },
            'QuantityOrdered': {
              '$type': 'System.Nullable`1[[Asi.Soa.Commerce.DataContracts.QuantityData, Asi.Contracts]], mscorlib',
              'Amount': 1
            },
            'QuantityShipped': {
              '$type': 'System.Nullable`1[[Asi.Soa.Commerce.DataContracts.QuantityData, Asi.Contracts]], mscorlib',
              'Amount': 1
            },
            'Tax': {
              '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxData, Asi.Contracts',
              'Details': {
                '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxDetailDataCollection, Asi.Contracts',
                '$values': []
              },
              'TaxTotal': {
                '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                'Currency': {
                  '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                  'CurrencyCode': 'USD',
                  'DecimalPositions': 2,
                  'ExtensionData': {
                    '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                  }
                },
                'IsAmountDefined': true
              },
              'TaxableAmountTotal': {
                '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                'Currency': {
                  '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                  'CurrencyCode': 'USD',
                  'DecimalPositions': 2,
                  'ExtensionData': {
                    '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                  }
                },
                'IsAmountDefined': true
              },
              'InclusiveTaxTotal': {
                '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                'Currency': {
                  '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                  'CurrencyCode': 'USD',
                  'DecimalPositions': 2,
                  'ExtensionData': {
                    '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                  }
                },
                'IsAmountDefined': true
              },
              'TaxSchedule': {
                '$type': 'Asi.Soa.Commerce.DataContracts.TaxScheduleSummaryData, Asi.Contracts',
                'Name': ''
              },
              'TaxCategory': {
                '$type': 'Asi.Soa.Commerce.DataContracts.TaxCategorySummaryData, Asi.Contracts'
              }
            },
            'UnitPrice': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Amount': unitAmount,
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },
            'IsUnitPriceOverridden': true,
            'BaseUnitPrice': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Amount': 0,
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },

            'Discount': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },
            'CanCombine': true,
            'SourceCode': ''
          };

          dataJson.push(objLine);
          const objJsonLine = JSON.stringify(objLine);
        });
      }

      if (netmediaLines.length > 0) {
        const lineNumber = (grossmediaOrderLines.length) + 1;

        netmediaLines.forEach((data, key) => {
          const moline = data;
          let unitAmount = 0;
          let productCode = '';
          if (moline.GrossInd === true) {

          } else if (moline.GrossInd === false) {
            if (moline.AmountPercent === 0) {
              const tempAmount = parseFloat(grossCostRunningAmount);
              const adjustmentValue = parseFloat(moline.AdjustmentValue);
              let afterMinusAmount: any = 0;
              if (moline.SurchargeDiscount === 0) {
                afterMinusAmount = (tempAmount + adjustmentValue);
                unitAmount = adjustmentValue;
                productCode = 'SA';
              } else {
                afterMinusAmount = (tempAmount - adjustmentValue);
                unitAmount = -adjustmentValue;
                productCode = 'AD';
              }
              afterMinusAmount = afterMinusAmount.toFixed(2);
              netCostRunningAmount = afterMinusAmount;
            } else if (moline.AmountPercent === 1) {
              const tempAmount = parseFloat(grossCostRunningAmount);
              const adjustmentValue = parseFloat(moline.AdjustmentValue);
              const percentageValueInAmount = (adjustmentValue * tempAmount / 100);

              let afterMinusAmount: any = 0;
              if (moline.SurchargeDiscount === 0) {
                afterMinusAmount = (tempAmount + percentageValueInAmount);
                unitAmount = percentageValueInAmount;
                productCode = 'SA';
              } else {
                afterMinusAmount = (tempAmount - percentageValueInAmount);
                unitAmount = -percentageValueInAmount;
                productCode = 'AD';
              }
              afterMinusAmount = afterMinusAmount.toFixed(2);
              netCostRunningAmount = afterMinusAmount;
            }
          }


          const objJsonLine = {
            '$type': 'Asi.Soa.Commerce.DataContracts.OrderLineData, Asi.Contracts',
            'OrderLineId': data.MediaOrderLineId,
            'AdditionalAttributes': {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyDataCollection, Asi.Contracts',
              '$values': [
                {
                  '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
                  'Name': '',
                  'Value': ''
                }
              ]
            },

            'ExtendedAmount': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Amount': unitAmount,
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },

            'Item': {
              '$type': 'Asi.Soa.Commerce.DataContracts.ItemSummaryData, Asi.Contracts',
              'ItemId': data.ProductCode,
            },
            // tslint:disable-next-line:max-line-length
            'Note': objSelectedOrder.AdTypeName + '|' + objSelectedOrder.AdcolorName + '|' + objSelectedOrder.AdSizeName + '|' + objSelectedOrder.FrequencyName,
            'LineNumber': lineNumber + key + 1,
            'QuantityBackordered': {
              '$type': 'System.Nullable`1[[Asi.Soa.Commerce.DataContracts.QuantityData, Asi.Contracts]], mscorlib'
            },
            'QuantityOrdered': {
              '$type': 'System.Nullable`1[[Asi.Soa.Commerce.DataContracts.QuantityData, Asi.Contracts]], mscorlib',
              'Amount': 1
            },
            'QuantityShipped': {
              '$type': 'System.Nullable`1[[Asi.Soa.Commerce.DataContracts.QuantityData, Asi.Contracts]], mscorlib',
              'Amount': 1
            },
            'Tax': {
              '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxData, Asi.Contracts',
              'Details': {
                '$type': 'Asi.Soa.Commerce.DataContracts.OrderTaxDetailDataCollection, Asi.Contracts',
                '$values': []
              },
              'TaxTotal': {
                '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                'Currency': {
                  '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                  'CurrencyCode': 'USD',
                  'DecimalPositions': 2,
                  'ExtensionData': {
                    '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                  }
                },
                'IsAmountDefined': true
              },
              'TaxableAmountTotal': {
                '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                'Currency': {
                  '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                  'CurrencyCode': 'USD',
                  'DecimalPositions': 2,
                  'ExtensionData': {
                    '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                  }
                },
                'IsAmountDefined': true
              },
              'InclusiveTaxTotal': {
                '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                'Currency': {
                  '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                  'CurrencyCode': 'USD',
                  'DecimalPositions': 2,
                  'ExtensionData': {
                    '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                  }
                },
                'IsAmountDefined': true
              },
              'TaxSchedule': {
                '$type': 'Asi.Soa.Commerce.DataContracts.TaxScheduleSummaryData, Asi.Contracts',
                'Name': ''
              },
              'TaxCategory': {
                '$type': 'Asi.Soa.Commerce.DataContracts.TaxCategorySummaryData, Asi.Contracts'
              }
            },
            'UnitPrice': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Amount': unitAmount,
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },
            'IsUnitPriceOverridden': true,
            'BaseUnitPrice': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Amount': 0,
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },

            'Discount': {
              '$type': 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
              'Currency': {
                '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode': 'USD',
                'DecimalPositions': 2,
                'ExtensionData': {
                  '$type': 'System.Runtime.Serialization.ExtensionDataObject, System.Runtime.Serialization'
                }
              },
              'IsAmountDefined': true
            },
            'CanCombine': true,
            'SourceCode': ''
          };

          dataJson.push(objJsonLine);
          const objLine = JSON.stringify(objJsonLine);
        });
      }

      return dataJson;

    } catch (error) {
      console.log(error);
    }
  }

  generateObjectForAdjustmentSave(objAdAdjustment) {
    try {
      if (objAdAdjustment.AdjustmentValue == null
        && objAdAdjustment.AdjustmentValue === undefined
        && objAdAdjustment.AdjustmentValue === '') {
        objAdAdjustment.AdjustmentValue = parseFloat('0');
      } else {
        objAdAdjustment.AdjustmentValue = parseFloat(objAdAdjustment.AdjustmentValue);
      }

      const advertiserObject = {
        '$type': 'Asi.Soa.Commerce.DataContracts.ProductItemData, Asi.Contracts',
        'Weight': {
          '$type': 'System.Nullable`1[[Asi.Soa.Commerce.DataContracts.QuantityData, Asi.Contracts]], mscorlib'
        },
        'Image': {
          '$type': 'Asi.Soa.Core.DataContracts.VirtualFileData, Asi.Contracts',
          'VirtualPath': '',
          'ThumbnailVirtualPath': ''
        },
        'ImageUrl': '',
        'ThumbnailUrl': '',
        'HasPhysicalInventory': false,
        'PublishingInformation': {
          '$type': 'Asi.Soa.Core.DataContracts.PublishingInformationData, Asi.Contracts',
          'StartDate': null,
          'ExpirationDate': null,
          'Keywords': {
            '$type': 'System.Collections.ObjectModel.Collection`1[[System.String, mscorlib]], mscorlib',
            '$values': []
          },
          'PublishingState': 0
        },
        'TempDefaultPrice': objAdAdjustment.AdjustmentValue,
        'ItemFinancialInformation': {
          '$type': 'Asi.Soa.Commerce.DataContracts.ItemFinancialInformationData, Asi.Contracts',
          'FinancialEntity': {
            '$type': 'Asi.Soa.Commerce.DataContracts.FinancialEntityData, Asi.Contracts',
            'EntityCode': ''
          },
          'TaxCategory': {
            '$type': 'Asi.Soa.Commerce.DataContracts.TaxCategorySummaryData, Asi.Contracts',
            'Name': 'Non-Taxable',
            'TaxCategoryId': 'Non-Taxable',
            'Description': 'Non-Taxable'
          },
          'IncursShipping': false,
          'IncursHandling': false,
          'ItemFinancialAccounts': {
            '$type': 'Asi.Soa.Commerce.DataContracts.ItemFinancialAccountsData, Asi.Contracts',
            'AccountsReceivable': {
              '$type': 'Asi.Soa.Commerce.DataContracts.FinancialAccountData, Asi.Contracts'
            },
            'Income': {
              '$type': 'Asi.Soa.Commerce.DataContracts.FinancialAccountData, Asi.Contracts',
              'GLAccount': {
                '$type': 'Asi.Soa.Commerce.DataContracts.GLAccountData, Asi.Contracts',
                'GLAccountId': '',
                'GLAccountCode': '',
                'Name': ''
              }
            },
            'Inventory': {
              '$type': 'Asi.Soa.Commerce.DataContracts.FinancialAccountData, Asi.Contracts',
              'GLAccount': {
                '$type': 'Asi.Soa.Commerce.DataContracts.GLAccountData, Asi.Contracts',
                'GLAccountId': '',
                'GLAccountCode': '',
                'Name': ''
              }
            },
            'CostOfGoodsSold': {
              '$type': 'Asi.Soa.Commerce.DataContracts.FinancialAccountData, Asi.Contracts',
              'GLAccount': {
                '$type': 'Asi.Soa.Commerce.DataContracts.GLAccountData, Asi.Contracts',
                'GLAccountId': '',
                'GLAccountCode': '',
                'Name': ''
              }
            },
            'InventoryAdjustments': {
              '$type': 'Asi.Soa.Commerce.DataContracts.FinancialAccountData, Asi.Contracts',
              'GLAccount': {
                '$type': 'Asi.Soa.Commerce.DataContracts.GLAccountData, Asi.Contracts',
                'GLAccountId': '',
                'GLAccountCode': '',
                'Name': ''
              }
            },
            'DamagedGoods': {
              '$type': 'Asi.Soa.Commerce.DataContracts.FinancialAccountData, Asi.Contracts'
            },
            'DeferredIncome': {
              '$type': 'Asi.Soa.Commerce.DataContracts.FinancialAccountData, Asi.Contracts',
              'GLAccount': {
                '$type': 'Asi.Soa.Commerce.DataContracts.GLAccountData, Asi.Contracts',
                'GLAccountId': '',
                'GLAccountCode': '',
                'Name': ''
              }
            }
          }
        },
        'FairMarketValue': {
          '$type': 'Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts',
          'Currency': {
            '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
            'CurrencyCode': 'USD',
            'DecimalPositions': 2,
            'ExtensionData': null
          },
          'IsAmountDefined': true
        },

        'AdditionalAttributes': {
          '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyDataCollection, Asi.Contracts',
          '$values': [
            {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
              'Name': 'TAXABLE',
              'Value': {
                '$type': 'System.Boolean',
                '$value': false
              }
            },
            {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
              'Name': 'PST_TAXABLE',
              'Value': {
                '$type': 'System.Boolean',
                '$value': false
              }
            },
            {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
              'Name': 'TAX_AUTHORITY',
              'Value': ''
            },
            {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
              'Name': 'TAX_BY_LOCATION',
              'Value': {
                '$type': 'System.Boolean',
                '$value': false
              }
            },
            {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
              'Name': 'TAXCATEGORY_CODE',
              'Value': ''
            },
            {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
              'Name': 'WEBOPTION',
              'Value': {
                '$type': 'System.Int32',
                '$value': 2
              }
            },
            {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
              'Name': 'IsBillingBaseItem',
              'Value': {
                '$type': 'System.Boolean',
                '$value': false
              }
            },
            {
              '$type': 'Asi.Soa.Core.DataContracts.GenericPropertyData, Asi.Contracts',
              'Name': 'PayPriority',
              'Value': {
                '$type': 'System.Int32',
                '$value': 0
              }
            }
          ]
        },

        'IsPremium': false,
        'PremiumInformation': {
          '$type': 'Asi.Soa.Commerce.DataContracts.PremiumInformationData, Asi.Contracts',
          'RequiredGiftAmount': {
            '$type': 'Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts',
            'Currency': {
              '$type': 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
              'CurrencyCode': 'USD',
              'DecimalPositions': 2,
              'ExtensionData': null
            },
            'IsAmountDefined': true
          }
        },
        'AllowOrderLineNote': false,
        'Description': '',
        'ItemClass': {
          '$type': 'Asi.Soa.Commerce.DataContracts.ItemClassSummaryData, Asi.Contracts',
          'ItemClassId': 'SALES-ADJUSTMENT',
          'Name': 'Amount Ajustment'
        },
        'ItemCode': '' + objAdAdjustment.AdAdjustmentName + '',
        'ItemId': '' + objAdAdjustment.AdAdjustmentName + '',
        'Name': '' + objAdAdjustment.AdAdjustmentName + ''
      };
      return advertiserObject;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // set frozen True
  setIsFrozenTrue(sMIds, eMIds) {
    try {
      if (sMIds !== undefined && sMIds !== '' && sMIds != null) {
        const mIds = sMIds.split(',');
        if (mIds.length > 0) {
          const data = mIds;
          this.postOrdersService.setIsFrozenTrue(data, this.reqVerificationToken).subscribe(result => {

            if (result.StatusCode === 1) {
              this.displayOrderDatatableJquery(this.postOrder);
              let tsIds = [];
              let teIds = [];
              if (sMIds !== '' && sMIds != null && sMIds !== undefined) {
                tsIds = sMIds.split(',');
              }
              if (eMIds !== '' && eMIds != null && eMIds !== undefined) {
                teIds = eMIds.split(',');
              }
              const tsIdsLength = tsIds.length;
              this.successRecords = tsIdsLength;

              tsIds.forEach((data1, key) => {
                this.orderToPost.map((task, index, array) => {
                  // tslint:disable-next-line:radix
                  if (task.MediaOrderId === parseInt(data1)) {
                    this.orderToPost[index].Status = 'Success';
                    this.orderToPost[index].color = 'green';
                  }
                });
              });

              teIds.forEach((data2, key) => {
                this.orderToPost.map((task, index, array) => {
                  // tslint:disable-next-line:radix
                  if (task.MediaOrderId === parseInt(data2)) {
                    this.orderToPost[index].Status = 'Error';
                    this.orderToPost[index].color = 'red';
                  }
                });
              });

              if (sMIds !== '' && sMIds !== undefined && sMIds != null) {
                this.isStatusActive = false;
                this.isStatusHTMLActive = true;
                this.setRedText = false;
                this.setGreenText = true;
              }

              if (eMIds !== '' && eMIds !== undefined && eMIds != null) {
                this.isStatusActive = false;
                this.isStatusHTMLActive = true;
                this.setRedText = true;
                this.setGreenText = false;
              }

            } else {
              this.toastr.error(result.message, 'Error!');
            }
          }, error => {
            console.log(error);
          });
        } else {
          let tsIds = [];
          let teIds = [];
          if (sMIds !== '' && sMIds != null && sMIds !== undefined) {
            tsIds = sMIds.split(',');
          }
          if (eMIds !== '' && eMIds != null && eMIds !== undefined) {
            teIds = eMIds.split(',');
          }
          const tsIdsLength = tsIds.length;
          const teIdsLength = teIds.length;
          this.successRecords = tsIdsLength;

          tsIds.forEach((data, key) => {
            this.orderToPost.map((task, index, array) => {
              // tslint:disable-next-line:radix
              if (task.MediaOrderId === parseInt(data)) {
                this.orderToPost[index].Status = 'Success';
                this.orderToPost[index].color = 'green';
              }
            });
          });

          teIds.forEach((data, key) => {
            this.orderToPost.map((task, index, array) => {
              // tslint:disable-next-line:radix
              if (task.MediaOrderId === parseInt(data)) {
                this.orderToPost[index].Status = 'Error';
                this.orderToPost[index].color = 'red';
              }
            });
          });


          if (sMIds !== '' && sMIds !== undefined && sMIds != null) {
            this.isStatusActive = false;
            this.isStatusHTMLActive = true;
            this.setRedText = false;
            this.setGreenText = true;
          }
          if (eMIds !== '' && eMIds !== undefined && eMIds != null) {
            this.isStatusActive = false;
            this.isStatusHTMLActive = true;
            this.setRedText = true;
            this.setGreenText = false;
          }

        }
      } else {
        let tsIds = [];
        let teIds = [];
        if (sMIds !== '' && sMIds != null && sMIds !== undefined) {
          tsIds = sMIds.split(',');
        }
        if (eMIds !== '' && eMIds != null && eMIds !== undefined) {
          teIds = eMIds.split(',');
        }

        const tsIdsLength = tsIds.length;
        const teIdsLength = teIds.length;
        this.successRecords = tsIdsLength;

        tsIds.forEach((data, key) => {
          this.orderToPost.map((task, index, array) => {
            // tslint:disable-next-line:radix
            if (task.MediaOrderId === parseInt(data)) {
              this.orderToPost[index].Status = 'Success';
              this.orderToPost[index].color = 'green';
            }
          });
        });

        teIds.forEach((data, key) => {
          this.orderToPost.map((task, index, array) => {
            // tslint:disable-next-line:radix
            if (task.MediaOrderId === parseInt(data)) {
              this.orderToPost[index].Status = 'Error';
              this.orderToPost[index].color = 'red';
            } else {
              this.orderToPost[index].Status = 'Error';
              this.orderToPost[index].color = 'red';
            }
          });
        });

        if (sMIds !== '' && sMIds !== undefined && sMIds != null) {
          this.isStatusActive = false;
          this.isStatusHTMLActive = true;
          this.setRedText = false;
          this.setGreenText = true;
        }
        if (eMIds !== '' && eMIds !== undefined && eMIds != null) {
          this.isStatusActive = false;
          this.isStatusHTMLActive = true;
          this.setRedText = true;
          this.setGreenText = false;
        }

      }
    } catch (error) {
      console.log(error);
    }
  }

  // Order Export to Word
  exportDataWord() {
    try {
      let buyerId = '';
      buyerId = buyerId + 'IO';
      const HTMLtoExport = document.getElementById('exportable').innerHTML;
      const blob = new Blob([HTMLtoExport], {
        type: 'application/msword;charset=utf-8'
      });
      saveAs(blob, buyerId + '.doc');
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Order Export to Excel
  exportDataExcel() {
    try {
      let buyerId = '';
      buyerId = buyerId + 'IO';
      const HTMLtoExport = document.getElementById('exportable').innerHTML;

      const blob = new Blob([HTMLtoExport], {
        type: 'application/vnd.ms-excel;charset=utf-8'
      });
      saveAs(blob, buyerId + '.xls');
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Order Export to PDF
  exportDataPdf() {

  }



  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }
}