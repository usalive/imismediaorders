import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import 'select2';
import { AppComponent } from '../../../app.component';
import { MediaScheduleService } from '../../services/mediaSchedule.service';
import { ToastrService } from 'ngx-toastr';
import { GlobalClass } from '../../GlobalClass';
import { OrderTabService } from '../../services/orderTab.service';
import { isNullOrUndefined } from 'util';
import { OrderTabComponent } from '../orderTab/orderTab.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'asi-mediaScheduleNew',
  templateUrl: './mediaScheduleNew.component.html',
  styleUrls: ['./mediaScheduleNew.component.css']
})
export class MediaScheduleNewComponent implements OnInit {

  mediaScheduleForm: FormGroup;
  isMediaScheduleFormSubmitted = false;

  minDate = null;
  maxDate = null;

  websiteRoot = '';
  baseUrl = '';
  reqVerificationToken = '';
  imgurl = '';
  apiBaseURL = '';

  mediaSchedule: any = {};

  adOrderId = 0;

  issueDates: any = [];
  issueDatesArray = [];
  issueDate = [];
  showMsgs = false;
  rateCardDetail = '';
  rateCardMaster: any = {};
  tempArray = [];
  selected = [];

  mediaScheduleTab = [];
  flag = true;
  isRequired = false;
  mediaScheduleData: any = {};
  dvIssueDate = false;
  dvselectedIssueDate = false;
  orderGrossCost = '';
  orderNetCost = '';
  dvAdColor = true;
  dvAdSize = true;
  dvAdSizeTextbox = false;
  dvFrequency = true;
  dvColumn = false;
  dvInches = false;

  defaultAdColorId = '';
  defaultAdColorName = '';
  defaultAdSizeId = '';
  defaultAdSizeName = '';
  defaultFrequencyId = '';
  defaultFrequencyName = '';

  classifiedTextNote = '';
  totalWords = 0;
  noOfWord: any = '';
  additionalWord: any = '';
  lblAdSize = 'Ad Size';

  isDisable: any;
  isFrozen: any;

  mediaAssets: any = [];
  rateCards: any = [];
  adSizes: any = [];
  adColors: any = [];
  frequencies: any = [];

  PCIAdSizeId: any;
  PCIAdSizeName: any;

  sponsorFrequencyId: any;
  sponsorFrequencyName: any;

  checkBoxArr: any[] = [];

  dvClassifiedText: any;
  dvNoOfInsert: any;
  dvImpression: any;
  dvBoothLocation :any;
  temp: any = {};
  classifiedText = true;

  obj: any = {};

  constructor(private mediaScheduleService: MediaScheduleService,
    private toastr: ToastrService,
    private globalClass: GlobalClass,
    private orderTabService: OrderTabService,
    private formBuilder: FormBuilder,
    private orderTabComponent: OrderTabComponent,
    private appComponent: AppComponent) { }

  ngOnInit() {
    this.commonInit();
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }
    this.orderGrossCost = this.globalClass.getGrossCost();
    this.orderNetCost = this.globalClass.getNetCost();
    const ordID = this.globalClass.getOrderId();
    if (ordID !== undefined && ordID !== '' && ordID != null) {
      this.adOrderId = ordID;
    }
    console.log("getData:"+ this.globalClass.getBillingDetailsMediaOrderId());
    this.mediaScheduleFormValidation();
    this.initDateRange();
    this.getAllMediaAsset();
    this.getData();
  }
  get isValidMedia() {
    return this.mediaScheduleForm.get('Media').invalid && (this.isMediaScheduleFormSubmitted || this.mediaScheduleForm.get('Media').dirty || this.mediaScheduleForm.get('Media').touched);
  } 
  get isValidRateCard() {
    return this.mediaScheduleForm.get('RateCard').invalid && (this.isMediaScheduleFormSubmitted || this.mediaScheduleForm.get('RateCard').dirty || this.mediaScheduleForm.get('RateCard').touched);
  } 
  get isValidAdSize() {
    return this.mediaScheduleForm.get('AdSize').invalid && (this.isMediaScheduleFormSubmitted || this.mediaScheduleForm.get('AdSize').dirty || this.mediaScheduleForm.get('AdSize').touched);
  } 
  get isValidAdColor() {
    return this.mediaScheduleForm.get('AdColor').invalid && (this.isMediaScheduleFormSubmitted || this.mediaScheduleForm.get('AdColor').dirty || this.mediaScheduleForm.get('AdColor').touched);
  } 
  get isValidFrequency() {
    return this.mediaScheduleForm.get('Frequency').invalid && (this.isMediaScheduleFormSubmitted || this.mediaScheduleForm.get('Frequency').dirty || this.mediaScheduleForm.get('Frequency').touched);
  } 
  get isValidRateCardCost() {
    return this.mediaScheduleForm.get('RateCardCost').invalid && (this.isMediaScheduleFormSubmitted || this.mediaScheduleForm.get('RateCardCost').dirty || this.mediaScheduleForm.get('RateCardCost').touched);
  } 
  get isValidFromDate() {
    return this.mediaScheduleForm.get('FromDate').invalid && (this.isMediaScheduleFormSubmitted || this.mediaScheduleForm.get('FromDate').dirty || this.mediaScheduleForm.get('FromDate').touched);
  } 
  get isValidToDate() {
    return this.mediaScheduleForm.get('ToDate').invalid && (this.isMediaScheduleFormSubmitted || this.mediaScheduleForm.get('ToDate').dirty || this.mediaScheduleForm.get('ToDate').touched);
  } 
  mediaScheduleFormValidation() {
    this.mediaScheduleForm = this.formBuilder.group({
      Media: [null, Validators.required],
      RateCard: [null, Validators.required],
      AdSize: [null, Validators.required],
      AdColor: [null, Validators.required],

      Frequency: [null, Validators.required],
      NoOfInsert: [null],
      Impression: [null],
      Column: [null],
      BoothLocation :[null],
      Inches: [null],
      AdSizeTextbox: [null],
      ClassifiedText: [null],
      RateCardCost: [null],

      AdCost: [null],
      Cost: [null],
      FromDate: [null, Validators.required],
      ToDate: [null, Validators.required],
    });
  }

  isRequiredClassifiedText() {
    return true;
  }

  /// <summary>
  /// Initialize Common constiables and objects
  /// </summary>
  commonInit() {
    this.mediaSchedule = {};
    this.mediaSchedule.AdColorId = '';
    this.mediaSchedule.BaseCost = '';
    this.mediaSchedule.AdCost = '';
    this.mediaSchedule.AdSizeId = '';
    this.mediaSchedule.IssueDates = '';
    this.mediaSchedule.MediaAssetId = '';
    this.mediaSchedule.RateCardCost = '';
    this.mediaSchedule.FrequencyId = '';
    this.mediaSchedule.RateCardId = '';
    this.mediaSchedule.RateCardDetailId = '';
    this.mediaSchedule.ProductCode = '';
    this.mediaSchedule.CampaignName = '';
    this.rateCardDetail = '';
    this.dvAdColor = true;
    this.dvAdSize = true;
    this.dvAdSizeTextbox = false;
    this.dvFrequency = true;
    this.dvColumn = false;
    this.dvInches = false;
    this.mediaSchedule.billingMethodId = '';
    this.mediaSchedule.billingMethodName = '';
    this.mediaSchedule.AdSize = '';
    this.mediaSchedule.MediaAssetName = '';
    this.mediaSchedule.rateCardDetail = '';
    this.mediaSchedule.AdColorName = '';
    this.mediaSchedule.AdSizeName = '';
    this.mediaSchedule.FrequencyName = '';
    this.defaultAdColorId = '';
    this.defaultAdColorName = '';
    this.defaultAdSizeId = '';
    this.defaultAdSizeName = '';
    this.defaultFrequencyId = '';
    this.defaultFrequencyName = '';
    this.mediaSchedule.ClassifiedText = '';
    this.classifiedTextNote = '';
    this.totalWords = 0;
    this.mediaSchedule.PerWordCost = '';
    this.noOfWord = '';
    this.additionalWord = '';
    this.mediaSchedule.Column = '';
    this.mediaSchedule.Inches = '';
    this.mediaSchedule.NoOfInserts = '';
    this.mediaSchedule.Impression = '';
    this.mediaSchedule.BoothLocation ='';
    this.lblAdSize = 'Ad Size';
    this.mediaSchedule.AdColorId = this.defaultAdColorId;
  }

  onChangeFromDate(val: any) {
    try {
      this.mediaSchedule.FromDate = this.globalClass.getTwoValDate(val);
      const toDate = new Date(val);
      this.globalClass.getFromDate(toDate);
      toDate.setMonth(toDate.getMonth() + 12);
      this.mediaSchedule.ToDate = this.globalClass.getTwoValDate(toDate);
      let Id = 0;
      if (this.mediaSchedule.MediaAssetId > 0) {
        Id = this.mediaSchedule.MediaAssetId;
      } else {
        const MediaAsset = this.mediaScheduleForm.controls['Media'].value;
        if (MediaAsset === null || MediaAsset === undefined) {
          Id = null;
        } else {
          Id = MediaAsset.mediaAssetId;
        }
      }
      if ((this.mediaSchedule.FromDate !== null || this.mediaSchedule.FromDate !== undefined)
        && (this.mediaSchedule.ToDate !== null || this.mediaSchedule.ToDate !== undefined)) {
        if (!isNullOrUndefined(Id)) {
          const issueDates = {
            'MediaAssetId': Id,
            'FromDate': this.mediaSchedule.FromDate,
            'ToDate': this.mediaSchedule.ToDate
          };
          this.getIssueDates(issueDates);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  onChangeToDate(val: any) {
    try {
      this.mediaSchedule.ToDate = this.globalClass.getTwoValDate(val);
      let Id = 0;
      if (this.mediaSchedule.MediaAssetId > 0) {
        Id = this.mediaSchedule.MediaAssetId;
      } else {
        const getSelected = this.mediaScheduleForm.controls['Media'].value;
        if (getSelected === null || getSelected === undefined) {
          Id = null;
        } else {
          Id = getSelected.mediaAssetId;
        }
      }

      if ((this.mediaSchedule.FromDate !== null || this.mediaSchedule.FromDate !== undefined)
        && (this.mediaSchedule.ToDate !== null || this.mediaSchedule.ToDate !== undefined)) {
        if (!isNullOrUndefined(Id)) {
          const issueDates = {
            'MediaAssetId': Id,
            'FromDate': this.mediaSchedule.FromDate,
            'ToDate': this.mediaSchedule.ToDate
          };
          this.getIssueDates(issueDates);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  initDateRange() {
    try {
      const currDate = new Date();
      currDate.setMonth(currDate.getMonth() + 1);
      currDate.setDate(1);
      this.mediaSchedule.FromDate = this.globalClass.getTwoValDate(currDate);
      const todate = currDate;
      todate.setMonth(currDate.getMonth() + 12);
      this.mediaSchedule.ToDate = this.globalClass.getTwoValDate(todate);

      this.mediaScheduleForm.controls['FromDate'].setValue(this.mediaSchedule.FromDate);
      this.mediaScheduleForm.controls['ToDate'].setValue(this.mediaSchedule.ToDate);
    } catch (error) {
      console.log(error);
    }
  }

  /// clear all data
  clearData() {
    this.flag = false;
    this.changeMediaAsset();
    this.maintainMediaBillingMethod('Flat Rate');
    this.initDateRange();
    this.globalClass.setbillingDetailsMediaOrderid('');
    this.globalClass.removeDetailsMediaOrderid();
  }

  /// <summary>
  /// Get the data
  /// </summary>
  getData() {
    const mediaOrderId = this.globalClass.getBillingDetailsMediaOrderId();
    
    const adOrderId = this.adOrderId;
    if (adOrderId > 0 && parseInt(mediaOrderId) > 0) {
      this.fillMediaSchedule(mediaOrderId);
      this.mediaScheduleForm.controls['Media'].disable();
      this.dvIssueDate = true;
      this.dvselectedIssueDate = true;
    } else {
      const id = 's2id_ddlMediaAsset';
      const text = 'Select media asset';

      this.clearData();
      this.flag = true;
      this.selected = [];
      this.tempArray = [];
      this.issueDates = {};
      this.mediaScheduleForm.controls['Media'].enable();
      this.dvIssueDate = false;
      this.dvselectedIssueDate = false;
    }
    if (adOrderId > 0) {
      try {
        this.mediaScheduleService.getOrdersByBuyId(adOrderId, this.reqVerificationToken).subscribe(result => {
          if (result.StatusCode === 1) {
            this.isDisable = false;
          } else {
            this.isDisable = true;
          }
        });
      } catch (error) {
        console.log(error);
      }
    }
  }

  /// <summary>
  /// Fill media schedule dropdown
  /// </summary>
  fillMediaSchedule(mediaOrderId) {
    try {
      this.showLoader();
      this.mediaScheduleService.getOrdersByMediaOrderId(mediaOrderId, this.reqVerificationToken).subscribe(result => {
        this.hideLoader();
        this.mediaScheduleData = result.Data;
       console.log('mediaScheduleData1:',JSON.stringify(this.mediaScheduleData));
        if (result.Data.IsFrozen === true) {
          this.isFrozen = true;
        } else {
          this.isFrozen = false;
        }

        const CampaignName = this.globalClass.getbillingDetails().CampaignName;
        const AdvertiserId = this.globalClass.getbillingDetails().AdvertiserId;
        const AgencyId = this.globalClass.getbillingDetails().AgencyId;
        const BillTo = this.globalClass.getbillingDetails().BillTo;
        const AdCommissions = this.globalClass.getbillingDetails().AdCommissions;
        const BillToContactId = this.globalClass.getbillingDetails().BillToContactId;
        const MediaOrderId = this.globalClass.getbillingDetails().MediaOrderId;
        this.obj = {};
        if (CampaignName === '' || CampaignName == null || CampaignName === undefined) {
          this.obj.CampaignName = result.Data.CampaignName;
        } else {
          this.obj.CampaignName = CampaignName;
        }

        this.obj.AdOrderId = result.Data.BuyId;

        if (AdvertiserId === '' || AdvertiserId == null || AdvertiserId === undefined) {
          this.obj.AdvertiserId = result.Data.AdvertiserId;
        } else {
          this.obj.AdvertiserId = AdvertiserId;
        }

        if (AgencyId === '' || AgencyId == null || AgencyId === undefined) {
          this.obj.AgencyId = result.Data.AgencyId;
        } else {
          this.obj.AgencyId = AgencyId;
        }

        if (BillTo === '' || BillTo == null || BillTo === undefined) {
          this.obj.BillTo = result.Data.BillToInd;
        } else {
          this.obj.BillTo = BillTo;
        }

        if (AdCommissions === '' || AdCommissions == null || AdCommissions === undefined) {
          this.obj.AdCommissions = result.Data.MediaOrderReps;
        } else {
          this.obj.AdCommissions = AdCommissions;
        }

        if (BillToContactId === '' || BillToContactId == null || BillToContactId === undefined) {
          this.obj.BillToContactId = result.Data.ST_ID;
        } else {
          this.obj.BillToContactId = BillToContactId;
        }

        if (MediaOrderId === '' || MediaOrderId == null || MediaOrderId === undefined) {
          
          this.obj.MediaOrderId = mediaOrderId;
        } else {
          this.obj.MediaOrderId = MediaOrderId;
        }
        this.globalClass.setbillingDetails(this.obj);
        try {
          if (this.mediaScheduleData.MediaBillingMathodeName === 'Per Word') {
            this.mediaScheduleForm.controls['ClassifiedText'].setValue(this.mediaScheduleData.ClassifiedText);
            this.mediaScheduleData.RateCardCost=this.mediaScheduleData.BaseRateCardCost;
            this.changeClassifiedText();
          }
          // ----- End -----  
        } catch (error) {
          console.log(error);
        }
        if (this.mediaSchedule.MediaAssetId > 0
          && this.mediaSchedule.MediaAssetId !== undefined
          && this.mediaSchedule.MediaAssetId != null
          && this.mediaSchedule.MediaAssetId !== '') {

        } else if (this.mediaScheduleData.MediaAssetId > 0
          && this.mediaScheduleData.MediaAssetId !== undefined
          && this.mediaScheduleData.MediaAssetId != null
          && this.mediaScheduleData.MediaAssetId !== '') {
          this.mediaSchedule.MediaAssetId = this.mediaScheduleData.MediaAssetId;
          this.getAllMediaAsset();
          setTimeout(() => {
            try {
              $('#chk_' + result.Data.IssueDateId + '').prop('checked', true);
            } catch (error) {
              console.log(error);
            }
          }, 1000);
        }
        
      }, error => {
        this.hideLoader();
        console.log(error);
      });
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Get the units from database
  /// </summary>
  getUnits() {
    try {
      const MediaBillingMathodeName = this.mediaScheduleData.MediaBillingMathodeName;
      if (MediaBillingMathodeName !== undefined && MediaBillingMathodeName !== '' && MediaBillingMathodeName != null) {
        if (MediaBillingMathodeName === 'Per Word') {
          this.totalWords = this.mediaScheduleData.Units;
        } else if (MediaBillingMathodeName === 'CPM') {
          this.mediaSchedule.NoOfInserts = this.mediaScheduleData.Units;
        } else if (MediaBillingMathodeName === 'Web CPM') {
          this.mediaSchedule.Impression = this.mediaScheduleData.Units;
        }
      }

      this.mediaSchedule.Column = this.mediaScheduleData.Column;
      this.mediaSchedule.Inches = this.mediaScheduleData.Inches;
      this.mediaSchedule.AdSize = (this.mediaScheduleData.Column * this.mediaScheduleData.Inches);
      this.mediaSchedule.ClassifiedText = this.mediaScheduleData.ClassifiedText;
      this.mediaSchedule.RateCardCost = this.mediaScheduleData.BaseCalculationCost;
      this.mediaSchedule.BaseCost = this.mediaScheduleData.BaseRateCardCost;
      this.mediaSchedule.AdCost = this.mediaScheduleData.RateCardCost;
      this.mediaSchedule.FlightStartDate = this.mediaScheduleData.FlightStartDate;
      this.mediaSchedule.FlightEndDate = this.mediaScheduleData.FlightEndDate;

      this.selected = [];
      this.selected[this.mediaScheduleData.IssueDateId] = true;

    } catch (error) {
      this.hideLoader();
      console.log(error);
    }

  }

  /// <summary>
  /// Get the list of all media assets from Database
  /// </summary>
  getAllMediaAsset() {
    try {
      this.mediaScheduleService.getAll().subscribe(result => {
        this.mediaAssets = [];
        this.mediaAssets = result.Data;
       
        if (this.mediaSchedule.MediaAssetId > 0) {
          this.mediaScheduleForm.controls['Media'].setValue(this.mediaSchedule.MediaAssetId);
          const responseData = this.mediaAssets;
          const MediaAssets = alasql('SELECT MediaAssetName FROM ? AS add WHERE MediaAssetId  = ?',
            // tslint:disable-next-line:radix
            [responseData, parseInt(this.mediaSchedule.MediaAssetId)]);
          if (MediaAssets !== undefined && MediaAssets != null && MediaAssets !== '') {
          }
          if (this.mediaSchedule.RateCardId > 0
            && this.mediaSchedule.RateCardId !== undefined
            && this.mediaSchedule.RateCardId != null
            && this.mediaSchedule.RateCardId !== '') {

          } else if (this.mediaScheduleData.RateCardId > 0
            && this.mediaScheduleData.RateCardId !== undefined
            && this.mediaScheduleData.RateCardId != null
            && this.mediaScheduleData.RateCardId !== '') {
            this.mediaSchedule.RateCardId = this.mediaScheduleData.RateCardId;
            if (this.mediaScheduleData.AdSizeId > 0
              && this.mediaScheduleData.AdSizeId !== undefined
              && this.mediaScheduleData.AdSizeId != null
              && this.mediaScheduleData.AdSizeId !== '') {
              this.mediaSchedule.AdSizeId = this.mediaScheduleData.AdSizeId;
              this.changeMediaAsset();
            }
          }
        }
      });
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }

  }

  /// <summary>
  /// Get the rate card from Database by Media Asset Id
  /// </summary>
  getRateCardsbyMediaAsset(mediaAssetId) {
    try {
      if (mediaAssetId !== '' &&
        mediaAssetId !== undefined &&
        mediaAssetId != null) {
        this.mediaScheduleService.getByMediaAsset(mediaAssetId).subscribe(result => {
          this.tempArray = [];
          this.rateCardMaster = {};
          result.Data[0].RateCards.forEach((value, key) => {
            this.rateCardMaster.rateCardDetail = value.Name + ' | ' +
              (value.MediaBillingMethodName).trim() + ' | ' + value.AdTypeName;
            this.rateCardMaster.RateCardId = value.RateCardId;
            this.rateCardMaster.MediaBillingMethodId = value.MediaBillingMethodId;
            this.rateCardMaster.MediaBillingMethodName = value.MediaBillingMethodName;
            this.rateCardMaster.ProductCode = value.ProductCode;
            this.tempArray.push(this.rateCardMaster);
            this.rateCardMaster = [];
          });
          this.rateCards = [];
          this.rateCards = this.tempArray;
          if (mediaAssetId === this.mediaScheduleData.MediaAssetId
            && this.mediaScheduleData.RateCardId > 0) {
            this.mediaSchedule.RateCardId = this.mediaScheduleData.RateCardId;
            this.mediaScheduleForm.controls['RateCard'].setValue(this.mediaSchedule.RateCardId);
            const responseData = this.rateCards;
            const RateCards = alasql('SELECT rateCardDetail FROM ? AS add WHERE RateCardId  = ?',
              // tslint:disable-next-line:radix
              [responseData, parseInt(this.mediaSchedule.RateCardId)]);
            if (RateCards !== undefined
              && RateCards !== ''
              && RateCards != null
              && RateCards.length > 0) {
            }
            this.changeRateCard();
          } else {
          }

        });
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }


  }

  /// <summary>
  /// Get the list of all ad sizes from Database by Rate Card Id
  /// </summary>
  getAdSizeByRateCard(rateCardId) {
    try {
      if (rateCardId !== '' &&
        rateCardId !== undefined &&
        rateCardId != null) {
        this.mediaScheduleService.getByRateCard(rateCardId).subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.adSizes = [];
                this.adSizes = result.Data;

                result.Data.forEach((itemdatavalue, key) => {
                  if (key === 0) {
                    this.defaultAdSizeId = itemdatavalue.AdSizeId;
                    this.defaultAdSizeName = itemdatavalue.Name;
                  }
                  this.PCIAdSizeId = itemdatavalue.AdSizeId;
                  this.PCIAdSizeName = itemdatavalue.AdSizeName;
                  if (this.mediaSchedule.billingMethodName === 'PCI') {
                    this.mediaSchedule.AdSizeId = this.PCIAdSizeId;
                    this.mediaSchedule.AdSizeName = this.PCIAdSizeName;
                    this.mediaScheduleForm.controls['AdSize'].setValue(this.mediaSchedule.AdSizeId);
                    this.getAdColor(rateCardId, this.mediaSchedule.AdSizeId);
                  }
                  if (this.mediaSchedule.billingMethodName === 'Per Word' ||
                    this.mediaSchedule.billingMethodName === 'Sponsorship') {
                    this.getAdColor(rateCardId, this.defaultAdSizeId);
                  }
                });

                if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
                  && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
                  && this.mediaScheduleData.AdSizeId > 0) {
                  this.mediaSchedule.AdSizeId = this.mediaScheduleData.AdSizeId;
                  this.mediaScheduleForm.controls['AdSize'].setValue(this.mediaSchedule.AdSizeId);
                  const responseData = this.adSizes;
                  const AdSizes = alasql('SELECT Name FROM ? AS add WHERE AdSizeId  = ?',
                    // tslint:disable-next-line:radix
                    [responseData, parseInt(this.mediaSchedule.AdSizeId)]);
                  if (AdSizes !== undefined && AdSizes != null && AdSizes != null) {
                  }
                  this.changeAdSize();
                }

              }
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }

  }

  /// <summary>
  /// Get the ad color from Database by Rate Card Id and Ad Size Id
  /// </summary>
  getAdColor(rateCardId, adSizeId) {
    try {
      if (rateCardId !== '' && adSizeId !== '' &&
        rateCardId !== undefined && adSizeId !== undefined &&
        rateCardId != null && adSizeId != null) {
        this.mediaScheduleService.getbyRatecardandAdsize(rateCardId, adSizeId).subscribe(result => {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              this.adColors = [];
              this.adColors = result.Data;
              this.adColors.forEach((value, key) => {
                if (value.Name === '4 Color') {
                  this.defaultAdColorId = value.AdColorId;
                  this.defaultAdColorName = value.Name;
                } else if (key === 0) {
                  this.defaultAdColorId = value.AdColorId;
                  this.defaultAdColorName = value.Name;
                }
              });

              this.mediaSchedule.AdColorId = this.defaultAdColorId;
              this.mediaSchedule.AdColorName = this.defaultAdColorName;

              this.mediaScheduleForm.controls['AdColor'].setValue(this.mediaSchedule.AdColorId);

              this.getFrequency(rateCardId, this.defaultAdColorId, adSizeId);

              if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
                && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
                && this.mediaSchedule.AdSizeId === this.mediaScheduleData.AdSizeId
                && this.mediaScheduleData.AdColorId > 0) {
                this.mediaSchedule.AdColorId = this.mediaScheduleData.AdColorId;
                this.mediaScheduleForm.controls['AdColor'].setValue(this.mediaSchedule.AdColorId);
                const responseData = this.adColors;
                const AdColors = alasql('SELECT Name FROM ? AS add WHERE AdColorId  = ?',
                  // tslint:disable-next-line:radix
                  [responseData, parseInt(this.mediaSchedule.AdColorId)]);
                if (AdColors !== undefined && AdColors != null && AdColors !== '') {
                }
                this.changeAdColor();
              }
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Get the list of all frequencies from Database by Rate Card Id and Ad Color Id and Ad Size Id
  /// </summary>
  getFrequency(rateCardId, adColorId, adSizeId) {
    try {
      if (rateCardId !== '' && adColorId !== '' && adSizeId !== '' &&
        rateCardId !== undefined && adColorId !== undefined
        && adSizeId !== undefined &&
        rateCardId != null && adColorId != null && adSizeId != null) {
        this.mediaScheduleService.getbyRatecardColorandAdsize(rateCardId, adColorId, adSizeId).subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.frequencies = [];
                this.frequencies = result.Data;
                console.log('item',result.Data);
                result.Data.forEach((itemdatavalue, key) => {
                  if (itemdatavalue.Name === '1' || itemdatavalue.Name === '1X') {
                    
                    this.sponsorFrequencyId = itemdatavalue.FrequencyId;
                    this.sponsorFrequencyName = itemdatavalue.Name;
                    this.defaultFrequencyId = itemdatavalue.FrequencyId;
                    this.defaultFrequencyName = itemdatavalue.Name;
                  } else if (key === 0) {
                    this.defaultFrequencyId = itemdatavalue.FrequencyId;
                    this.defaultFrequencyName = itemdatavalue.Name;
                  }
                });

                if (this.mediaSchedule.billingMethodName === 'Sponsorship') {
                // console.log('getFrequency',this.mediaSchedule);
                  this.mediaSchedule.FrequencyId = this.sponsorFrequencyId;
                  this.mediaSchedule.FrequencyName = this.sponsorFrequencyName;
                  this.mediaScheduleForm.controls['Frequency'].setValue(this.mediaSchedule.FrequencyId);
                }
                if (this.mediaSchedule.billingMethodName === 'Per Word') {
                  this.mediaSchedule.AdSizeId = this.defaultAdSizeId;
                  this.mediaSchedule.AdSizeName = this.defaultAdSizeName;
                  this.mediaSchedule.FrequencyId = this.defaultFrequencyId;
                  this.mediaSchedule.FrequencyName = this.defaultFrequencyName;
                  this.mediaScheduleForm.controls['AdSize'].setValue(this.mediaSchedule.AdSizeId);
                  this.mediaScheduleForm.controls['Frequency'].setValue(this.mediaSchedule.FrequencyId);

                  this.getRateCardCostForPerWord(rateCardId, adColorId);

                }
                if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
                  && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
                  && this.mediaSchedule.AdSizeId === this.mediaScheduleData.AdSizeId
                  && this.mediaScheduleData.FrequencyId > 0) {
                  this.mediaSchedule.FrequencyId = this.mediaScheduleData.FrequencyId;
                  this.mediaScheduleForm.controls['Frequency'].setValue(this.mediaSchedule.FrequencyId);
                  const responseData = this.frequencies;
                  const Frequencies = alasql('SELECT Name FROM ? AS add WHERE FrequencyId  = ?',
                    // tslint:disable-next-line:radix
                    [responseData, parseInt(this.mediaSchedule.FrequencyId)]);
                  if (Frequencies !== undefined && Frequencies != null && Frequencies !== '') {
                  } else {
                  }
                  this.changeFrequency();
                }
              }
            }
          }
        });
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }

  }

  /// <summary>
  /// Get the Rate Card Cost per word from Database for classified text
  /// </summary>
  getRateCardCostForPerWord(rateCardId, adColorId) {
    try {
      let mediaAssetMatchingFound=false;
      if (rateCardId !== '' && adColorId !== '' &&
        rateCardId !== undefined && adColorId !== undefined &&
        rateCardId != null && adColorId != null) {
        this.mediaScheduleService.getRateCardCostForPerWord(rateCardId, adColorId).subscribe(result => {
          if (result.StatusCode === 1) {
            if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
              && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId) {
              this.getUnits();
              this.mediaSchedule.RateCardDetailId = this.mediaScheduleData.RateCardDetailId;
              mediaAssetMatchingFound=true;
            } else {
              this.mediaSchedule.RateCardCost = result.Data.RateCardCost;
              this.mediaSchedule.AdCost = result.Data.RateCardCost;
              this.mediaSchedule.BaseCost = result.Data.RateCardCost;
              this.mediaSchedule.RateCardDetailId = result.Data.RateCardDetailId;
            }
            this.mediaSchedule.PerWordCost = result.Data.PerWordCost;
            this.noOfWord = result.Data.NoOfWord;
            this.additionalWord = result.Data.AdditionalWord;
            this.classifiedTextNote = 'Fixed cost $' + (mediaAssetMatchingFound?this.mediaSchedule.BaseCost:this.mediaSchedule.RateCardCost)
              + ' for ' + this.noOfWord.toLowerCase() + ' . Additional cost $'
              + this.mediaSchedule.PerWordCost + ' per '
              + this.additionalWord + ' word(s).';
          }
        });
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Get the Rate Card Cost from Database
  /// </summary>
  getRateCardCost(rateCardId, adColorId, adSizeId, frequencyId) {
    try {
      if (this.mediaSchedule.billingMethodName === 'CPM'
        && adColorId === '') {
        adColorId = this.defaultAdColorId;
      }
      if (this.mediaSchedule.billingMethodName === 'PCI'
        && adSizeId === '') {
        adSizeId = this.PCIAdSizeId;
      }
      if (this.mediaSchedule.billingMethodName === 'Sponsorship') {
      
        adColorId = this.defaultAdColorId;
        frequencyId = this.sponsorFrequencyId;
       // console.log('sponsorship',adColorId,frequencyId)
      }

      if (rateCardId !== ''
        && adColorId !== ''
        && adSizeId !== ''
        && frequencyId !== ''
        && rateCardId !== undefined
        && adColorId !== undefined
        && adSizeId !== undefined
        && frequencyId !== undefined
        && rateCardId != null
        && adColorId != null
        && adSizeId != null
        && frequencyId != null) {
        this.mediaScheduleService.getCost(rateCardId, adColorId, adSizeId, frequencyId).subscribe(result => {
          if (result.StatusCode === 1 && result.Data != null) {
            if (this.mediaScheduleData.MediaOrderId > 0
              && this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
              && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId) {
              this.getUnits();
              this.mediaSchedule.RateCardDetailId = result.Data.RateCardDetailId;
            } else {
              if (this.mediaSchedule.billingMethodName === 'PCI') {
                this.mediaSchedule.RateCardCost = result.Data.RateCardCost;
                this.mediaSchedule.BaseCost = result.Data.RateCardCost;
                if (this.mediaSchedule.Inches > 0 && this.mediaSchedule.Column > 0) {

                  this.mediaSchedule.AdCost =
                    this.mediaSchedule.Column * this.mediaSchedule.Inches * result.Data.RateCardCost;
                }
              } else if (this.mediaSchedule.billingMethodName === 'CPM') {
                this.mediaSchedule.RateCardCost = result.Data.RateCardCost;
                this.mediaSchedule.BaseCost = result.Data.RateCardCost;
              } else {

                console.log('mediaschedule1', this.mediaSchedule)
                this.mediaSchedule.RateCardCost = result.Data.RateCardCost;
                this.mediaSchedule.AdCost = result.Data.RateCardCost;
                this.mediaSchedule.BaseCost = result.Data.RateCardCost;
              }
              this.mediaSchedule.RateCardDetailId = result.Data.RateCardDetailId;
            }
          }
        });

      } else {
        this.mediaSchedule.RateCardCost = '';
        this.mediaSchedule.AdCost = '';
        this.mediaSchedule.BaseCost = '';
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }

  }

  onDateCheckboxChange(event: any) {
    if (event.target.checked === true) {
      this.selected.push(event.target.value);
    } else if (event.target.checked === false) {
      this.selected.splice(this.selected.indexOf(event.target.value), 1);
    } else {
      this.selected = [];
    }
  }

  /// <summary>
  /// Get the list of all Issue Dates from Database by Media Asset Id and Date Range
  /// </summary>
  getIssueDates(Dates) {
    try {
      if (Dates.MediaAssetId !== null || Dates.MediaAssetId !== undefined) {
        this.mediaScheduleService.getIssueDate(Dates).subscribe(result => {
          if (result.StatusCode === 1) {
            this.checkBoxArr = [];
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.issueDates = [];
                this.issueDates = alasql('SELECT * FROM ? AS add order by CoverDate desc', [result.Data]);
                this.issueDates.forEach((itemdatavalue, key) => {
                  const objchk: any = {};
                  const date = new Date(itemdatavalue.CoverDate);
                  const formatedDate = this.globalClass.getTwoValDate(itemdatavalue.CoverDate);
                  const month = date;
                  const locale = 'en-us';
                  const smonth = date.toLocaleString(locale, { month: 'long' });
                  const fullDate = formatedDate + ', ' + smonth + ' ' + date.getFullYear();
                  // tslint:disable-next-line:max-line-length
                  if (this.mediaScheduleData !== undefined && this.mediaScheduleData.IssueDateId === itemdatavalue.IssueDateId) {
                    // tslint:disable-next-line:max-line-length
                    objchk.IssueDateId = itemdatavalue.IssueDateId;
                    objchk.fullDate = fullDate;
                    objchk.selectedIssueDate = this.dvselectedIssueDate;
                  } else {
                    // tslint:disable-next-line:max-line-length
                    objchk.IssueDateId = itemdatavalue.IssueDateId;
                    objchk.fullDate = fullDate;
                    objchk.selectedIssueDate = this.dvIssueDate;
                  }
                  this.checkBoxArr.push(objchk);
                });
              } else {
                this.checkBoxArr = [];
              }
            } else {
              this.checkBoxArr = [];
            }
          } else {
            this.checkBoxArr = [];
          }
        }, error => {
          this.checkBoxArr = [];
          console.log(error);
        });
      }
    } catch (error) {
      console.log(error);
      this.hideLoader();
    }
  }

  /// <summary>
  /// Maintain media billing method
  /// </summary>
  maintainMediaBillingMethod(billingMethod) {
    this.classifiedText = false;
    if (billingMethod === 'Flat Rate') {
      this.dvAdColor = true;
      this.dvAdSize = true;
      this.dvAdSizeTextbox = false;
      this.dvFrequency = true;
      this.dvClassifiedText = false;
      this.dvNoOfInsert = false;
      this.dvColumn = false;
      this.dvInches = false;
      this.dvImpression = false;
      this.lblAdSize = 'Ad size';
      this.isRequired = false;
      this.dvBoothLocation = false;
    } else if (billingMethod === 'Web CPM') {
      this.dvAdColor = true;
      this.dvAdSize = true;
      this.dvAdSizeTextbox = false;
      this.dvFrequency = true;
      this.dvClassifiedText = false;
      this.dvNoOfInsert = false;
      this.dvColumn = false;
      this.dvInches = false;
      this.dvImpression = true;
      this.lblAdSize = 'Ad Size';
      this.isRequired = false;
      this.dvBoothLocation = false;
    } else if (billingMethod === 'Per Word') {
      this.dvAdColor = false;
      this.dvAdSize = false;
      this.dvAdSizeTextbox = false;
      this.dvFrequency = false;
      this.dvClassifiedText = true;
      this.dvNoOfInsert = false;
      this.dvColumn = false;
      this.dvInches = false;
      this.dvImpression = false;
      this.lblAdSize = 'Ad size';
      this.dvBoothLocation = false;
      try {
        this.changeClassifiedText();
      } catch (error) {
        this.hideLoader();
        console.log(error);
      }
      this.isRequired = true;
    } else if (billingMethod === 'CPM') {
      this.dvAdColor = false;
      this.dvAdSize = true;
      this.dvAdSizeTextbox = false;
      this.dvFrequency = true;
      this.dvClassifiedText = false;
      this.dvNoOfInsert = true;
      this.dvColumn = false;
      this.dvInches = false;
      this.dvImpression = false;
      this.lblAdSize = 'Ad size';
      this.isRequired = false;
      this.dvBoothLocation = false;
    } else if (billingMethod === 'PCI') {
      this.dvAdColor = true;
      this.dvAdSize = false;
      this.dvAdSizeTextbox = true;
      this.dvFrequency = true;
      this.dvClassifiedText = false;
      this.dvNoOfInsert = false;
      this.dvColumn = true;
      this.dvInches = true;
      this.dvImpression = false;
      this.lblAdSize = 'Ad size';
      this.isRequired = false;
      this.dvBoothLocation = false;
    } else if (billingMethod === 'Sponsorship') {
      this.dvAdColor = false;
      this.dvAdSize = true;
      this.dvNoOfInsert = false;
      this.dvAdSizeTextbox = false;
      this.dvFrequency = false;
      this.dvClassifiedText = false;
      this.dvBoothLocation = true;
      this.dvColumn = false;
      this.dvInches = false;
      this.dvImpression = false;
      this.lblAdSize = 'Booth';
      this.isRequired = false;
    } else {
      this.dvAdColor = true;
      this.dvAdSize = true;
      this.dvAdSizeTextbox = false;
      this.dvFrequency = true;
      this.dvClassifiedText = false;
      this.dvNoOfInsert = false;
      this.dvColumn = false;
      this.dvInches = false;
      this.dvImpression = false;
      this.lblAdSize = 'Ad size';
      this.isRequired = false;
      this.dvBoothLocation = false;
    }
  }

  /// <summary>
  /// Insert the Media Schedule to Database
  /// </summary>
  saveMediaSchedule(mediaAsset,willNavigate) {
    try {
      let isProceed = true;
      if (this.dvClassifiedText) {
        if (this.classifiedText == true) {
          isProceed = false;
        }
      }
      if (isProceed) {
        this.isMediaScheduleFormSubmitted = true;
        const AdSize = this.mediaScheduleForm.controls['AdSize'].value;
        const AdColor = this.mediaScheduleForm.controls['AdColor'].value;
        const frequency = this.mediaScheduleForm.controls['Frequency'].value;

        if (this.mediaScheduleForm.valid && mediaAsset.MediaAssetId > 0 && mediaAsset.RateCardCost > 0) {
          if (this.selected.length > 0) {
            let temparray = [];
            const issueDates = [];
            let cnt = 0;
            let len = 0;

            this.selected.forEach((itemdatavalue, key) => {
              len++;
              if (itemdatavalue) {
                // tslint:disable-next-line:radix
                const id = key;
                if (id !== this.mediaScheduleData.IssueDateId) {
                  issueDates.push(itemdatavalue);
                }
              } else {
                cnt++;
              }
            });

            if (cnt !== len) {
              this.temp = {};
              this.temp.BuyId = this.globalClass.getOrderId();
            //  console.log('this.temp.BuyId',this.temp.BuyId);
              
              this.temp.MediaAssetId = mediaAsset.MediaAssetId;
              this.temp.MediaAssetName = mediaAsset.MediaAssetName;
              this.temp.RateCardId = mediaAsset.RateCardId;
              this.temp.RateCardName = mediaAsset.rateCardDetail;
              this.temp.RateCardDetailId = mediaAsset.RateCardDetailId;
              this.temp.ProductCode = mediaAsset.ProductCode;
              this.temp.billingMethodId = mediaAsset.billingMethodId;
              this.temp.billingMethodName = mediaAsset.billingMethodName;
              this.temp.AdColorId = mediaAsset.AdColorId;
              this.temp.AdColorName = mediaAsset.AdColorName;
              this.temp.AdSize = mediaAsset.AdSize;
              this.temp.AdSizeId = mediaAsset.AdSizeId;
              this.temp.AdSizeName = mediaAsset.AdSizeName;
              this.temp.FrequencyId = mediaAsset.FrequencyId;
              this.temp.FrequencyName = mediaAsset.FrequencyName;
              this.temp.Column = mediaAsset.Column;
              this.temp.Inches = mediaAsset.Inches;
              if (mediaAsset.NoOfInserts !== '') {
                this.temp.Units = mediaAsset.NoOfInserts;
              } else if (mediaAsset.Impression !== '') {
                this.temp.Units = mediaAsset.Impression;
              } else if (this.totalWords) {
                this.temp.Units = this.totalWords;
              } else {
                this.temp.Units = '';
              }
              if(mediaAsset.BoothLocation !==''){
                this.temp.BoothLocation=mediaAsset.BoothLocation;
              }
              this.temp.ClassifiedText = mediaAsset.ClassifiedText;
              this.temp.PerWordCost = mediaAsset.PerWordCost;
              this.temp.BaseRateCardCost = mediaAsset.BaseCost;
              this.temp.BaseCalculationCost = mediaAsset.RateCardCost;
              this.temp.RateCardCost = mediaAsset.AdCost;
              this.temp.FlightStartDate = mediaAsset.FromDate;
              this.temp.FlightEndDate = mediaAsset.ToDate;
              this.temp.MediaOrderIssueDates = issueDates;

              const CampaignName = this.globalClass.getbillingDetails().CampaignName;
              const AdvertiserId = this.globalClass.getbillingDetails().AdvertiserId;
              const AdCommissions = this.globalClass.getbillingDetails().AdCommissions;
              const AdvertiserName = this.globalClass.getbillingDetails().AdvertiserName;
              const AgencyId = this.globalClass.getbillingDetails().AgencyId;
              const AgencyName = this.globalClass.getbillingDetails().AgencyName;
              const BillTo = this.globalClass.getbillingDetails().BillTo;
              const BillToContactId = this.globalClass.getbillingDetails().BillToContactId;
              const BillToContactName = this.globalClass.getbillingDetails().BillToContactName;

              this.temp.CampaignName = CampaignName !== undefined ? CampaignName : this.mediaScheduleData.CampaignName;
              this.temp.AdvertiserId = AdvertiserId !== undefined ? AdvertiserId : this.mediaScheduleData.AdvertiserId;
              this.temp.MediaOrderReps = AdCommissions !== undefined ? AdCommissions : this.mediaScheduleData.MediaOrderReps;
              this.temp.AdvertiserName = AdvertiserName !== undefined ? AdvertiserName : '';
              this.temp.AgencyId = AgencyId !== undefined ? AgencyId : this.mediaScheduleData.AgencyId;
              this.temp.AgencyName = AgencyName !== undefined ? AgencyName : '';

              if (BillTo === 0 || this.mediaScheduleData.BT_ID) {
                this.temp.BT_ID = AdvertiserId !== undefined ? AdvertiserId : this.mediaScheduleData.BT_ID;
              } else if (BillTo === 1 || this.mediaScheduleData.BT_ID) {
                this.temp.BT_ID = AgencyId !== undefined ? AgencyId : this.mediaScheduleData.BT_ID;
              } else {
                if (BillTo === 0 || BillTo === '0') {
                  this.temp.BT_ID = AdvertiserId;
                } else if (BillTo === 1 || BillTo === '1') {
                  this.temp.BT_ID = AgencyId;
                }
              }

              if (isNullOrUndefined(this.temp.BT_ID)) {
                if (BillTo === 0 || BillTo === '0') {
                  this.temp.BT_ID = AdvertiserId;
                } else if (BillTo === 1 || BillTo === '1') {
                  this.temp.BT_ID = AgencyId;
                } else {
                  this.temp.BT_ID = '';
                }
              }

              this.temp.ST_ID = BillToContactId !== undefined ? BillToContactId : this.mediaScheduleData.ST_ID;
              this.temp.BillToInd = BillTo !== undefined ? BillTo : this.mediaScheduleData.BillToInd;

              if (this.temp.BillToInd === '0') {
                this.temp.BillToInd = false;
              } else if (this.temp.BillToInd === '1') {
                this.temp.BillToInd = true;
              }

              this.temp.BillToContactId = BillToContactId !== undefined ? BillToContactId : this.mediaScheduleData.ST_ID;
              this.temp.BillToContactName = BillToContactName !== undefined ? BillToContactName : '';

              this.temp.OrderStatus = 'Proposal';
              this.temp.Headline = '';
              temparray.push(this.temp);
              this.globalClass.setMainArray(temparray);

              if (this.mediaScheduleData !== undefined && this.mediaScheduleData.MediaOrderId > 0) {
                this.temp.MediaOrderId = this.mediaScheduleData.MediaOrderId;

                this.showLoader();
                this.mediaScheduleService.update(this.temp).subscribe(result => {
                  this.isMediaScheduleFormSubmitted = false;
                  if (result.StatusCode === 1) {
                    this.hideLoader();
                    const GCost = result.Data.GrossCost;
                    const NCost = result.Data.NetCost;
                    this.globalClass.setGrossCost(parseFloat(GCost).toFixed(2));
                    this.globalClass.setNetCost(parseFloat(NCost).toFixed(2));
                    temparray = [];
                    mediaAsset = {};
                    try {
                      this.checkBoxArr.forEach(element => {
                        $('#chk_' + element.IssueDateId + '').prop('checked', true);
                      });
                    } catch (error) {
                      console.log(error);
                    }
                    if(willNavigate===false)
                    {
                      this.mediaScheduleForm.reset();
                      this.clearData();
                      this.flag = true;
                      this.selected = [];
                      this.tempArray = [];
                      this.issueDates = {};
                      this.showMsgs = false;
                      this.toastr.success('Edit to the buy ID ' + this.adOrderId, 'Success!');
                      this.mediaScheduleForm.controls['Media'].enable();
                      this.isDisable = false;
                      this.globalClass.setNetCost(parseFloat(NCost).toFixed(2));
                      this.orderGrossCost = this.globalClass.getGrossCost();
                      this.orderNetCost = this.globalClass.getNetCost();
                      this.globalClass.setbillingDetailsMediaOrderid('');
                    }
                    else
                    {
                      this.mediaScheduleForm.controls['Media'].enable();
                        const gAmount = 0;
                        const nAmount = 0;
                        this.globalClass.removeBillingDetails();
                        this.globalClass.removeOrderId();
                        this.globalClass.setGrossCost((gAmount).toFixed(2));
                        this.globalClass.setNetCost((nAmount).toFixed(2));
                        this.globalClass.removeIsFrozen();
                        this.globalClass.removeMainPartyAndOrganizationData();
                        this.globalClass.removeMainAdvertisersData();
                        this.globalClass.removeMainAgenciesData();
                        this.globalClass.removeMainBillingToContactsData();
                        this.globalClass.setbillingDetailsMediaOrderid('');
                        this.appComponent.showMainPage = true;
                        this.appComponent.showOrderFormPage = false;
                        this.appComponent.setAdvertisersTabActive();
                    }
                  } else {
                    this.hideLoader();
                    this.toastr.error(result.Message, 'Error!');
                  }
                }, error => {
                  this.hideLoader();
                  console.log(error);
                });
              } else {
                this.showLoader();
                this.mediaScheduleService.save(this.temp).subscribe(result => {
                  this.isMediaScheduleFormSubmitted = false;
                  if (result.StatusCode === 1) {
                    this.hideLoader();
                    const GCost = result.Data.GrossCost;
                    const NCost = result.Data.NetCost;
                    this.globalClass.setGrossCost(parseFloat(GCost).toFixed(2));
                    this.globalClass.setNetCost(parseFloat(NCost).toFixed(2));
                    temparray = [];
                    try {
                      this.checkBoxArr.forEach(element => {
                        $('#chk_' + element.IssueDateId + '').prop('checked', false);
                      });
                    } catch (error) {
                      console.log(error);
                    }
                    this.mediaScheduleForm.reset();
                    this.clearData();
                    this.flag = true;
                    this.selected = [];
                    this.tempArray = [];
                    this.issueDates = {};
                    this.showMsgs = false;
                    if (result.Message != null && result.Message != "") {
                      var msg = result.Message.split('||');
                      setTimeout(() => {
                        this.toastr.info(msg[0] + '<br /><br />' + msg[1]);
                      }, 0);
                  } else {
                    if(willNavigate===false)
                    {
                      this.globalClass.setbillingDetailsMediaOrderid('');
                      this.toastr.success('Add to the buy ID ' + this.adOrderId, 'Success!');
                    }
                    else{
                      const gAmount = 0;
                      const nAmount = 0;
                      this.globalClass.removeBillingDetails();
                      this.globalClass.removeOrderId();
                      this.globalClass.setGrossCost((gAmount).toFixed(2));
                      this.globalClass.setNetCost((nAmount).toFixed(2));
                      this.globalClass.removeIsFrozen();
                      this.globalClass.removeMainPartyAndOrganizationData();
                      this.globalClass.removeMainAdvertisersData();
                      this.globalClass.removeMainAgenciesData();
                      this.globalClass.removeMainBillingToContactsData();
                      this.globalClass.setbillingDetailsMediaOrderid('');
                      this.appComponent.showMainPage = true;
                      this.appComponent.showOrderFormPage = false;
                      this.appComponent.setAdvertisersTabActive();
                    }
                  }
                    this.isDisable = false;
                    this.orderGrossCost = this.globalClass.getGrossCost();
                    this.orderNetCost = this.globalClass.getNetCost();
                  } else {
                    this.hideLoader();
                    this.toastr.error(result.Message, 'Error!');
                  }
                }, error => {
                  this.hideLoader();
                  console.log(error);
                });
              }
            } else {
              this.selected = [];
              this.toastr.error('Please select issue dates...', 'Error!');
            }
          } else {
            this.selected = [];
            this.toastr.error('Please select issue dates...', 'Error!');
          }
        } else {
          this.showMsgs = true;
        }
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

   // Add Product Data in database (done by nimesh on 15 07 2020)
   finishNExit(mediaAsset,willNavigate) {
    try {
      this.saveMediaSchedule(mediaAsset,willNavigate);
    } catch (error) {
      console.log(error);
    }
  }

  changeMediaAsset() {
    try {
      this.mediaScheduleForm.controls['RateCard'].setValue(null);
      this.mediaScheduleForm.controls['AdColor'].setValue(null);
      this.mediaScheduleForm.controls['AdSize'].setValue(null);
      this.mediaScheduleForm.controls['Frequency'].setValue(null);

      this.mediaSchedule.RateCardId = '';
      this.mediaSchedule.AdSizeId = '';
      this.mediaSchedule.FrequencyId = '';
      this.mediaSchedule.RateCardName = '';
      this.mediaSchedule.AdSizeName = '';
      this.mediaSchedule.FrequencyName = '';
      this.mediaSchedule.AdCost = 0;
      this.mediaSchedule.RateCardCost = 0;
      this.rateCards = '';
      this.adColors = '';
      this.adSizes = '';
      this.frequencies = '';
      this.mediaSchedule.ClassifiedText = '';
      this.mediaSchedule.PerWordCost = '';
      this.classifiedTextNote = '';
      this.totalWords = 0;
      this.mediaSchedule.Column = '';
      this.mediaSchedule.Inches = '';
      this.mediaSchedule.NoOfInserts = '';
      this.mediaSchedule.Impression = '';
      this.mediaSchedule.NoOfInserts = '';
      this.mediaSchedule.billingMethodName = '';

      this.mediaSchedule.MediaAssetId = this.mediaScheduleForm.controls['Media'].value;
      if (this.flag) {
        const responseData = this.mediaAssets;
        const MediaAssets = alasql('SELECT MediaAssetName FROM ? AS add WHERE MediaAssetId  = ?',
          // tslint:disable-next-line:radix
          [responseData, parseInt(this.mediaSchedule.MediaAssetId)]);
        if (MediaAssets !== undefined && MediaAssets !== '' && MediaAssets != null) {
          this.mediaSchedule.MediaAssetName = MediaAssets[0].MediaAssetName;
        }
        this.getRateCardsbyMediaAsset(this.mediaSchedule.MediaAssetId);
      } else {
        this.mediaScheduleForm.controls['Media'].setValue(null);
        this.getAllMediaAsset();
      }

      if (!isNullOrUndefined(this.mediaSchedule.FromDate)) {
        if (!isNullOrUndefined(this.mediaSchedule.ToDate)) {
          if (!isNullOrUndefined(this.mediaSchedule.MediaAssetId)) {
            const issueDates = {
              'MediaAssetId': this.mediaSchedule.MediaAssetId,
              'FromDate': this.mediaSchedule.FromDate,
              'ToDate': this.mediaSchedule.ToDate
            };
            this.getIssueDates(issueDates);
          }
        }
      }

      this.maintainMediaBillingMethod(this.mediaSchedule.billingMethodName);
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  changeRateCard() {
    try {
      this.mediaScheduleForm.controls['AdColor'].setValue(null);
      this.mediaScheduleForm.controls['AdSize'].setValue(null);
      this.mediaScheduleForm.controls['Frequency'].setValue(null);

      this.mediaSchedule.AdSizeId = '';
      this.mediaSchedule.FrequencyId = '';
      this.mediaSchedule.AdSizeName = '';
      this.mediaSchedule.FrequencyName = '';
      this.mediaSchedule.AdCost = 0;
      this.mediaSchedule.RateCardCost = 0;
      this.adColors = '';
      this.adSizes = '';
      this.frequencies = '';
      this.mediaSchedule.ClassifiedText = '';
      this.mediaSchedule.PerWordCost = '';
      this.classifiedTextNote = '';
      this.totalWords = 0;
      this.mediaSchedule.Column = '';
      this.mediaSchedule.Inches = '';
      this.mediaSchedule.NoOfInserts = '';
      this.mediaSchedule.Impression = '';
      this.mediaSchedule.NoOfInserts = '';

      let rateCardId = '';
      this.mediaSchedule.RateCardId = this.mediaScheduleForm.controls['RateCard'].value;
      if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
        && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
        && this.mediaScheduleData.RateCardId > 0
        && this.mediaScheduleData.RateCardId !== ''
        && this.mediaScheduleData.RateCardId != null
        && this.mediaScheduleData.RateCardId !== undefined) {
        rateCardId = this.mediaScheduleData.RateCardId;
      } else {
        rateCardId = this.mediaSchedule.RateCardId;
      }
      const responseData = this.tempArray;
      let billingMethod = '';

      // tslint:disable-next-line:radix
      const RateCards = alasql('SELECT * FROM ? AS add WHERE RateCardId  = ?', [responseData, parseInt(rateCardId)]);
      if (RateCards !== undefined && RateCards != null && RateCards !== '' && RateCards.length > 0) {
        this.mediaSchedule.rateCardDetail = RateCards[0].rateCardDetail;
        billingMethod = (RateCards[0].MediaBillingMethodName).trim();
      } else {
        
      }


      this.mediaSchedule.billingMethodName = billingMethod;

      this.tempArray.forEach((itemdatavalue, key) => {
        const billingMethodName = itemdatavalue.MediaBillingMethodName.trim();
        if (billingMethodName === this.mediaSchedule.billingMethodName) {
          this.mediaSchedule.billingMethodId = itemdatavalue.MediaBillingMethodId;
          this.mediaSchedule.ProductCode = itemdatavalue.ProductCode;
        }
      });

      // tslint:disable-next-line:no-shadowed-constiable
      let adColorId = '';
      if (this.mediaSchedule.billingMethodName === 'Flat Rate'
        || this.mediaSchedule.billingMethodName === 'Web CPM'
        || this.mediaSchedule.billingMethodName === 'PCI') {
        if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
          && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
          && this.mediaSchedule.AdColorId === this.mediaScheduleData.AdColorId
          && this.mediaScheduleData.AdColorId > 0
          && this.mediaScheduleData.AdColorId !== ''
          && this.mediaScheduleData.AdColorId != null
          && this.mediaScheduleData.AdColorId !== undefined) {
          adColorId = this.mediaScheduleData.AdColorId;
        } else {
          adColorId = this.mediaSchedule.AdColorId;
        }
      }
      //Sponsorship else 
      else {
        adColorId = this.defaultAdColorId;
      }

      let adSizeId = '';
      if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
        && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
        && this.mediaSchedule.AdSizeId === this.mediaScheduleData.AdSizeId
        && this.mediaScheduleData.AdSizeId > 0
        && this.mediaScheduleData.AdSizeId !== ''
        && this.mediaScheduleData.AdSizeId != null
        && this.mediaScheduleData.AdSizeId !== undefined) {
        adSizeId = this.mediaScheduleData.AdSizeId;
      } else {
        adSizeId = this.mediaSchedule.AdSizeId;
      }
      let frequencyId = this.mediaSchedule.FrequencyId;
      if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
        && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
        && this.mediaSchedule.FrequencyId === this.mediaScheduleData.FrequencyId
        && this.mediaScheduleData.FrequencyId > 0
        && this.mediaScheduleData.FrequencyId !== ''
        && this.mediaScheduleData.FrequencyId != null
        && this.mediaScheduleData.FrequencyId !== undefined) {
        frequencyId = this.mediaScheduleData.FrequencyId;
      } else {
        frequencyId = this.mediaSchedule.FrequencyId;
      }

      this.maintainMediaBillingMethod(billingMethod);

      this.getAdSizeByRateCard(rateCardId);
      
      this.getRateCardCost(rateCardId, adColorId, adSizeId, frequencyId);

    } catch (error) {
      this.hideLoader();
      console.log(error);
    }

  }

  changeAdSize() {
    try {
      this.mediaSchedule.AdCost = 0;
      this.mediaSchedule.RateCardCost = 0;
      let rateCardId = '';

      if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
        && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
        && this.mediaScheduleData.RateCardId > 0
        && this.mediaScheduleData.RateCardId !== ''
        && this.mediaScheduleData.RateCardId != null
        && this.mediaScheduleData.RateCardId !== undefined) {
        rateCardId = this.mediaScheduleData.RateCardId;
      } else {
        rateCardId = this.mediaSchedule.RateCardId;
      }
      this.mediaSchedule.NoOfInserts = '';
      this.adColors = '';
      this.frequencies = '';
      this.mediaSchedule.NoOfInserts = '';

      const adColorId = this.defaultAdColorId;

      let adSizeId = '';
      this.mediaSchedule.AdSizeId = this.mediaScheduleForm.controls['AdSize'].value;
      if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
        && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
        && this.mediaSchedule.AdSizeId === this.mediaScheduleData.AdSizeId
        && this.mediaScheduleData.AdSizeId > 0
        && this.mediaScheduleData.AdSizeId !== ''
        && this.mediaScheduleData.AdSizeId != null
        && this.mediaScheduleData.AdSizeId !== undefined) {
        adSizeId = this.mediaScheduleData.AdSizeId;
      } else {
        adSizeId = this.mediaSchedule.AdSizeId;
      }
      const responseData = this.adSizes;
      // tslint:disable-next-line:radix
      const AdSizes = alasql('SELECT AdSizeName FROM ? AS add WHERE AdSizeId  = ?', [responseData, parseInt(adSizeId)]);
      if (AdSizes !== undefined && AdSizes != null && AdSizes !== '') {
        this.mediaSchedule.AdSizeName = AdSizes[0].AdSizeName;

      }
      let frequencyId = this.mediaSchedule.FrequencyId;
      if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
        && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
        && this.mediaSchedule.FrequencyId === this.mediaScheduleData.FrequencyId
        && this.mediaScheduleData.FrequencyId > 0
        && this.mediaScheduleData.FrequencyId !== ''
        && this.mediaScheduleData.FrequencyId != null
        && this.mediaScheduleData.FrequencyId !== undefined) {
        frequencyId = this.mediaScheduleData.FrequencyId;
      } else {
        frequencyId = this.mediaSchedule.FrequencyId;
      }


      this.getRateCardCost(rateCardId, adColorId, adSizeId, frequencyId);

      if (this.mediaSchedule.billingMethodName === 'Per Word') {
        this.getAdColor(rateCardId, this.defaultAdSizeId);
      } else {
        this.getAdColor(rateCardId, adSizeId);
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  changeAdColor() {
    try {
      this.mediaScheduleForm.controls['Frequency'].setValue(null);
      this.mediaSchedule.AdCost = 0;
      this.mediaSchedule.RateCardCost = 0;
      this.frequencies = '';
      this.mediaSchedule.NoOfInserts = '';
      let rateCardId = '';
      if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
        && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
        && this.mediaScheduleData.RateCardId > 0
        && this.mediaScheduleData.RateCardId !== ''
        && this.mediaScheduleData.RateCardId != null
        && this.mediaScheduleData.RateCardId !== undefined) {
        rateCardId = this.mediaScheduleData.RateCardId;
      } else {
        rateCardId = this.mediaSchedule.RateCardId;
      }
      let adColorId = '';
      this.mediaSchedule.AdColorId = this.mediaScheduleForm.controls['AdColor'].value;
      if (this.mediaSchedule.billingMethodName === 'Flat Rate'
        || this.mediaSchedule.billingMethodName === 'Web CPM'
        || this.mediaSchedule.billingMethodName === 'PCI') {

        if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
          && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
          && this.mediaSchedule.AdColorId === this.mediaScheduleData.AdColorId
          && this.mediaScheduleData.AdColorId > 0
          && this.mediaScheduleData.AdColorId !== ''
          && this.mediaScheduleData.AdColorId != null
          && this.mediaScheduleData.AdColorId !== undefined) {
          adColorId = this.mediaScheduleData.AdColorId;
        } else {
          adColorId = this.mediaSchedule.AdColorId;
        }
      } else {
        adColorId = this.defaultAdColorId;

      }
      let adSizeId = '';
      if (this.mediaSchedule.billingMethodName === 'PCI') {
        adSizeId = this.defaultAdSizeId;
      } else {

        if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
          && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
          && this.mediaSchedule.AdSizeId === this.mediaScheduleData.AdSizeId
          && this.mediaScheduleData.AdSizeId > 0
          && this.mediaScheduleData.AdSizeId !== ''
          && this.mediaScheduleData.AdSizeId != null
          && this.mediaScheduleData.AdSizeId !== undefined) {
          adSizeId = this.mediaScheduleData.AdSizeId;
        } else {
          adSizeId = this.mediaSchedule.AdSizeId;
        }
      }

      const responseData = this.adColors;
      const AdColors = alasql('SELECT AdColorName FROM ? AS add WHERE AdColorId  = ?',
        // tslint:disable-next-line:radix
        [responseData, parseInt(this.mediaSchedule.AdColorId)]);
      if (AdColors !== undefined && AdColors != null && AdColors !== '') {
        this.mediaSchedule.AdColorName = AdColors[0].AdColorName;
      }

      let frequencyId = this.mediaSchedule.FrequencyId;
      if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
        && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
        && this.mediaSchedule.FrequencyId === this.mediaScheduleData.FrequencyId
        && this.mediaScheduleData.FrequencyId > 0
        && this.mediaScheduleData.FrequencyId !== ''
        && this.mediaScheduleData.FrequencyId != null
        && this.mediaScheduleData.FrequencyId !== undefined) {
        frequencyId = this.mediaScheduleData.FrequencyId;
      } else {
        frequencyId = this.mediaSchedule.FrequencyId;
      }

      this.getFrequency(rateCardId, adColorId, adSizeId);
      this.getRateCardCost(rateCardId, adColorId, adSizeId, frequencyId);
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  changeFrequency() {
    try {
      this.mediaSchedule.AdCost = 0;
      this.mediaSchedule.RateCardCost = 0;
      const rateCardId = this.mediaSchedule.RateCardId;
      this.mediaSchedule.NoOfInserts = '';
      let adColorId = '';
      if (this.mediaSchedule.billingMethodName === 'Flat Rate'
        || this.mediaSchedule.billingMethodName === 'Web CPM'
        || this.mediaSchedule.billingMethodName === 'PCI') {
        if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
          && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
          && this.mediaSchedule.AdColorId === this.mediaScheduleData.AdColorId
          && this.mediaScheduleData.AdColorId > 0
          && this.mediaScheduleData.AdColorId !== ''
          && this.mediaScheduleData.AdColorId != null
          && this.mediaScheduleData.AdColorId !== undefined) {
          adColorId = this.mediaScheduleData.AdColorId;
        } else {
          adColorId = this.mediaSchedule.AdColorId;
        }
      } else {
        adColorId = this.defaultAdColorId;
      }
      let adSizeId = '';
      if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
        && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
        && this.mediaSchedule.AdSizeId === this.mediaScheduleData.AdSizeId
        && this.mediaScheduleData.AdSizeId > 0
        && this.mediaScheduleData.AdSizeId !== ''
        && this.mediaScheduleData.AdSizeId != null
        && this.mediaScheduleData.AdSizeId !== undefined) {
        adSizeId = this.mediaScheduleData.AdSizeId;
      } else {
        adSizeId = this.mediaSchedule.AdSizeId;
      }

      let frequencyId = this.mediaSchedule.FrequencyId;
      this.mediaSchedule.FrequencyId = this.mediaScheduleForm.controls['Frequency'].value;
      if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
        && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
        && this.mediaSchedule.FrequencyId === this.mediaScheduleData.FrequencyId
        && this.mediaScheduleData.FrequencyId > 0
        && this.mediaScheduleData.FrequencyId !== ''
        && this.mediaScheduleData.FrequencyId != null
        && this.mediaScheduleData.FrequencyId !== undefined) {
        frequencyId = this.mediaScheduleData.FrequencyId;
      } else {
        frequencyId = this.mediaSchedule.FrequencyId;
      }
      const responseData = this.frequencies;
      const Frequencies = alasql('SELECT FrequencyName FROM ? AS add WHERE FrequencyId  = ?',
        // tslint:disable-next-line:radix
        [responseData, parseInt(this.mediaSchedule.FrequencyId)]);
      if (Frequencies !== undefined && Frequencies != null
        && Frequencies !== '' && Frequencies.length > 0) {
        // initSelect2('s2id_ddfrequency', Frequencies[0].FrequencyName);
        this.mediaSchedule.FrequencyName = Frequencies[0].FrequencyName;
      } else {
      }
      this.getRateCardCost(rateCardId, adColorId, adSizeId, frequencyId);

      if (this.mediaSchedule.MediaAssetId === this.mediaScheduleData.MediaAssetId
        && this.mediaSchedule.RateCardId === this.mediaScheduleData.RateCardId
        && this.mediaSchedule.AdSizeId === this.mediaScheduleData.AdSizeId
        && this.mediaSchedule.AdColorId === this.mediaScheduleData.AdColorId
        && this.mediaSchedule.FrequencyId === this.mediaScheduleData.FrequencyId
        && this.mediaScheduleData.MediaAssetId > 0
        && this.mediaScheduleData.MediaAssetId !== undefined
        && this.mediaScheduleData.MediaAssetId != null
        && this.mediaScheduleData.MediaAssetId !== '') {
        this.getUnits();
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  changeRateCardCost() {
    try {
      this.mediaSchedule.RateCardCost = this.mediaScheduleForm.controls['RateCardCost'].value;
      if (this.mediaSchedule.billingMethodName === 'CPM') {
        this.mediaSchedule.NoOfInserts = this.mediaScheduleForm.controls['NoOfInsert'].value;
        this.mediaSchedule.AdCost = (this.mediaSchedule.NoOfInserts * this.mediaSchedule.RateCardCost / 1000);
      } else if (this.mediaSchedule.billingMethodName === 'PCI') {
        this.mediaSchedule.Inches = this.mediaScheduleForm.controls['Inches'].value;
        this.mediaSchedule.Column = this.mediaScheduleForm.controls['Column'].value;
        if (this.mediaSchedule.Inches > 0 && this.mediaSchedule.Column > 0 && this.mediaSchedule.RateCardCost > 0) {
          this.mediaSchedule.AdCost = this.mediaSchedule.Column * this.mediaSchedule.Inches * this.mediaSchedule.RateCardCost;
        }
      } else {
        this.mediaSchedule.AdCost = this.mediaSchedule.RateCardCost;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  changeNoOfInserts() {
    try {
      this.mediaSchedule.NoOfInserts = this.mediaScheduleForm.controls['NoOfInsert'].value;
      this.mediaSchedule.RateCardCost = this.mediaScheduleForm.controls['RateCardCost'].value;
      this.mediaSchedule.AdCost = (this.mediaSchedule.NoOfInserts * this.mediaSchedule.RateCardCost / 1000);
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  changeColInch() {
    try {
      this.mediaSchedule.Column = this.mediaScheduleForm.controls['Column'].value;
      this.mediaSchedule.Inches = this.mediaScheduleForm.controls['Inches'].value;

      this.mediaSchedule.AdSize = this.mediaSchedule.Column * this.mediaSchedule.Inches;
      if (this.mediaSchedule.Inches > 0 && this.mediaSchedule.Column > 0 && this.mediaSchedule.RateCardCost > 0) {
        this.mediaSchedule.AdCost = this.mediaSchedule.Column * this.mediaSchedule.Inches * this.mediaSchedule.RateCardCost;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Calculate RateCardCost for Per word
  changeClassifiedText() {
    try {
      this.mediaSchedule.ClassifiedText = this.mediaScheduleForm.controls['ClassifiedText'].value;
      if (this.mediaSchedule.ClassifiedText !== undefined) {
        if (this.mediaSchedule.ClassifiedText == null) {
          this.totalWords = 0;
          this.classifiedText = true;
        } else if (this.mediaSchedule.ClassifiedText.trim() == '') {
          this.totalWords = 0;
          this.classifiedText = true;
        } else {
          const temp = this.mediaSchedule.ClassifiedText.trim().split(' ');
          if (temp.length > 0 && temp !== '') {
            this.noOfWord = this.noOfWord.toLowerCase().replace('words','');//.replace('word','');
            this.classifiedText = false;
            this.totalWords = temp.length;
            if (temp.length > this.noOfWord) {
              this.mediaSchedule.RateCardCost =
                ((this.mediaSchedule.BaseCost) +
                  (((temp.length - this.noOfWord) /
                    this.additionalWord) *
                    (this.mediaSchedule.PerWordCost)));
                 
              this.mediaSchedule.AdCost = this.mediaSchedule.RateCardCost;
            } else {
              this.mediaSchedule.RateCardCost = this.mediaSchedule.BaseCost;
              this.mediaSchedule.AdCost = this.mediaSchedule.RateCardCost;
            }
          } else {
            this.totalWords = 0;
            this.classifiedText = true;
          }
        }
      } else {
        this.totalWords = 0;
        this.classifiedText = true;
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  // Redirect to media order adjustment tab
  redirectToMediaOrderAdjustmentTab() {
    const buyId = this.adOrderId;
    if (buyId > 0) {
      try {
        this.mediaScheduleService.getOrdersByBuyId(buyId, this.reqVerificationToken).subscribe(result => {
          if (result.StatusCode === 1) {
            this.isDisable = false;
            this.orderTabService.send_OrderTab('AdjustmentTab');
          } else {
            const id = 's2id_ddlMediaAsset';
            const text = 'Select media asset';
            const temparray = [];
            this.clearData();
            this.flag = true;
            this.selected = [];
            const issueDates = [];
            this.tempArray = [];
            this.orderTabService.send_OrderTab('AdjustmentTab');
            this.toastr.info('Please add alteast one insertion order', 'Information!');
          }
        });
      } catch (error) {
        console.log(error);
      }
    }
  }

  // Inotialization of select2 dropdown
  initSelect2(id, text) {
    const selectedState = $('#' + id).find('span.select2-chosen');
    selectedState.text(text);
  }

  // use for go to orevious tab
  goToPreviousTab() {
    if (this.adOrderId > 0) {
      this.globalClass.setOrderId(this.adOrderId);
      //this.globalClass.setFromDashboard(true);
      this.globalClass.setFromPreviousTab(true);
      this.orderTabService.send_OrderTab('OrdersTab');
    } else {
      this.globalClass.setFromPreviousTab(true);
      this.orderTabService.send_OrderTab('OrdersTab');
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }
}