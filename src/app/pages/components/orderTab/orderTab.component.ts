import { Component, OnInit, HostListener } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrderTabService } from '../../services/orderTab.service';
import { GlobalClass } from '../../GlobalClass';

declare const $: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'asi-orderTab',
  templateUrl: './orderTab.component.html',
  styleUrls: ['./orderTab.component.css']
})
export class OrderTabComponent implements OnInit {

  subscriptionForOrderTab: Subscription;
  screenWidth: any;
  screenHeight: any;

  setCursorPointer = true;
  selectedTab = '';

  // OrderFormTab = false;
  // MediaScheduleTab = true;
  orderFormTab = true;
  mediaScheduleTab = false;
  adjustmentTab = false;
  productionTab = false;

  orderFormDisable = false;
  mediaScheduleDisable = true;
  adjustmentDisable = true;
  productionDisable = true;

  isFrozen = false;
  tabOrderId: any;
  isDisable: boolean;
  orderTab: boolean;
  orderDisable: boolean;
  issueDateTabContent: any;
  mediaAssetTabContent: any;
  adAdjustmentsTabContent: any;
  rateCardsTabContent: any;
  mediaAssetPanelDiv: any;
  issueDatePanelDiv: any;
  adAdjustmentsPanelDiv: any;
  rateCardsPanelDiv: any;
  orderId: any;

  constructor(private orderTabService: OrderTabService, private globalClass: GlobalClass) {
    this.onResize();
  }

  ngOnInit() {
    if (this.globalClass.getOrderId() != null && this.globalClass.getOrderId() !== undefined && this.globalClass.getOrderId() !== '') {
      this.tabOrderId = this.globalClass.getOrderId();
    } else {
      this.tabOrderId = '';
    }
    this.isDisable = true;

    
    const orID = this.globalClass.getOrderId();
    if (orID != null && orID !== undefined && orID !== '') {
      this.orderId = this.globalClass.getOrderId();
    } else {
      this.orderId = '';
    }

    this.subscriptionForOrderTab = this.orderTabService.get_OrderTab().subscribe(message => {
      if (message.text === 'MediaScheduleTab') {
        this.setMediaScheduleTabActive();
      } else if (message.text === 'OrdersTab') {
        this.setOrdersTabActive();
      } else if (message.text === 'AdjustmentTab') {
        this.setAdjustmentTabActive();
      } else if (message.text === 'ProductionTab') {
        this.setProductionTabActive();
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenWidth = window.innerWidth;
    this.screenHeight = window.innerHeight;

    if (this.screenWidth < 768) {
      if (this.selectedTab === 'OrdersTab') {
        this.setOrdersTabActiveOnResize();
      } else if (this.selectedTab === 'MediaScheduleTab') {
        this.setMediaScheduleTabActiveOnResize();
      } else if (this.selectedTab === 'AdjustmentTab') {
        this.setAdjustmentTabActiveOnResize();
      } else if (this.selectedTab === 'ProductionTab') {
        this.setProductionTabActiveOnResize();
      }
    } else {
      if (this.selectedTab === 'OrdersTab') {
        this.setOrdersTabActiveOnResize();
      } else if (this.selectedTab === 'MediaScheduleTab') {
        this.setMediaScheduleTabActiveOnResize();
      } else if (this.selectedTab === 'AdjustmentTab') {
        this.setAdjustmentTabActiveOnResize();
      } else if (this.selectedTab === 'ProductionTab') {
        this.setProductionTabActiveOnResize();
      }
    }
  }

  // set orders tab active
  setOrdersTabActive() {
    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.issueDateTabContent = document.querySelector('#media-schedule-tab');
        this.issueDateTabContent.classList.add('collapsed');

        this.mediaAssetTabContent = document.querySelector('#orders-tab');
        this.mediaAssetTabContent.classList.remove('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-tab');
        this.adAdjustmentsTabContent.classList.add('collapsed');

        this.rateCardsTabContent = document.querySelector('#production-tab');
        this.rateCardsTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#media-schedule-collapse');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-collapse');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#production-collapse');
        this.rateCardsTabContent.classList.remove('in');

        this.mediaAssetTabContent = document.querySelector('#orders-collapse');
        this.mediaAssetTabContent.classList.add('in');
        this.mediaAssetTabContent.css('height', 'auto');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.add('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.add('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.add('noclick');

      }, 10);

    } else {
      this.mediaAssetTabContent = document.querySelector('#orders');
      if (this.mediaAssetTabContent !== null) {
        this.mediaAssetTabContent.classList.add('active');
        this.mediaAssetTabContent.classList.add('in');
      }

      this.issueDateTabContent = document.querySelector('#media-schedule');
      if (this.issueDateTabContent !== null) {
        this.issueDateTabContent.classList.remove('active');
        this.issueDateTabContent.classList.remove('in');
      }

      this.adAdjustmentsTabContent = document.querySelector('#adjustment');
      if (this.adAdjustmentsTabContent !== null) {
        this.adAdjustmentsTabContent.classList.remove('active');
        this.adAdjustmentsTabContent.classList.remove('in');
      }

      this.rateCardsTabContent = document.querySelector('#production');
      if (this.rateCardsTabContent !== null) {
        this.rateCardsTabContent.classList.remove('active');
        this.rateCardsTabContent.classList.remove('in');
      }
    }

    const orID = this.globalClass.getOrderId();
    if (orID !== null && orID !== undefined && orID !== '') {
      this.tabOrderId = orID;
    } else {
      this.globalClass.setOrderId('');
      this.tabOrderId = '';
    }

    if (this.globalClass.getFromDashboard() !== true) {
      this.globalClass.setFromDashboard(false);
    }

    this.orderFormTab = true;
    this.mediaScheduleTab = false;
    this.adjustmentTab = false;
    this.productionTab = false;

    this.orderFormDisable = false;
    this.mediaScheduleDisable = true;
    this.adjustmentDisable = true;
    this.productionDisable = true;

    this.isFrozen = false;
    this.selectedTab = 'orders';
  }

  // set media schedule tab active
  setMediaScheduleTabActive() {
    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.mediaAssetTabContent = document.querySelector('#orders-tab');
        this.mediaAssetTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#media-schedule-tab');
        this.issueDateTabContent.classList.remove('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-tab');
        this.adAdjustmentsTabContent.classList.add('collapsed');

        this.rateCardsTabContent = document.querySelector('#production-tab');
        this.rateCardsTabContent.classList.add('collapsed');

        this.mediaAssetTabContent = document.querySelector('#orders-collapse');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#media-schedule-collapse');
        this.issueDateTabContent.classList.add('in');
        this.issueDateTabContent.css('height', 'auto');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-collapse');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#production-collapse');
        this.rateCardsTabContent.classList.remove('in');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');
        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.remove('noclick');
        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.add('noclick');
        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.add('noclick');

      }, 10);

    } else {
      this.mediaAssetTabContent = document.querySelector('#orders');
      if (this.mediaAssetTabContent !== null) {
        this.mediaAssetTabContent.classList.remove('active');
        this.mediaAssetTabContent.classList.remove('in');
      }

      this.issueDateTabContent = document.querySelector('#media-schedule');
      if (this.issueDateTabContent !== null) {
        this.issueDateTabContent.classList.add('active');
        this.issueDateTabContent.classList.add('in');
      }

      this.adAdjustmentsTabContent = document.querySelector('#adjustment');
      if (this.adAdjustmentsTabContent !== null) {
        this.adAdjustmentsTabContent.classList.remove('active');
        this.adAdjustmentsTabContent.classList.remove('in');
      }

      this.rateCardsTabContent = document.querySelector('#production');
      if (this.rateCardsTabContent !== null) {
        this.rateCardsTabContent.classList.remove('active');
        this.rateCardsTabContent.classList.remove('in');
      }
    }

    if (this.globalClass.getOrderId() !== null && this.globalClass.getOrderId() !== undefined && this.globalClass.getOrderId() !== '') {
      this.tabOrderId = this.globalClass.getOrderId();
    } else {
      this.globalClass.setOrderId('');
      this.tabOrderId = '';
    }

    if (this.globalClass.getFromDashboard() !== true) {
      this.globalClass.setFromDashboard(false);
    }

    this.orderFormTab = false;
    this.mediaScheduleTab = true;
    this.adjustmentTab = false;
    this.productionTab = false;

    if (this.isFrozen === true) {
      this.orderFormDisable = true;
    } else {
      this.orderFormDisable = false;
    }
    this.mediaScheduleDisable = false;
    if (this.isDisable === true) {
      this.adjustmentDisable = true;
      this.productionDisable = true;
    } else {
      this.adjustmentDisable = false;
      this.productionDisable = false;
    }
    this.selectedTab = 'mediaschedule';
  }

  // set adjustment tab active
  setAdjustmentTabActive() {

    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.mediaAssetTabContent = document.querySelector('#orders-tab');
        this.mediaAssetTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#media-schedule-tab');
        this.issueDateTabContent.classList.add('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-tab');
        this.adAdjustmentsTabContent.classList.remove('collapsed');

        this.rateCardsTabContent = document.querySelector('#production-tab');
        this.rateCardsTabContent.classList.add('collapsed');

        this.mediaAssetTabContent = document.querySelector('#orders-collapse');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#media-schedule-collapse');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-collapse');
        this.adAdjustmentsTabContent.classList.add('in');
        this.adAdjustmentsTabContent.css('height', 'auto');

        this.rateCardsTabContent = document.querySelector('#production-collapse');
        this.rateCardsTabContent.classList.remove('in');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.remove('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.remove('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.add('noclick');

      }, 10);
    } else {

      this.mediaAssetTabContent = document.querySelector('#orders');
      if (this.mediaAssetTabContent !== null) {
        this.mediaAssetTabContent.classList.remove('active');
        this.mediaAssetTabContent.classList.remove('in');
      }

      this.issueDateTabContent = document.querySelector('#media-schedule');
      if (this.issueDateTabContent !== null) {
        this.issueDateTabContent.classList.remove('active');
        this.issueDateTabContent.classList.remove('in');
      }

      this.adAdjustmentsTabContent = document.querySelector('#adjustment');
      if (this.adAdjustmentsTabContent !== null) {
        this.adAdjustmentsTabContent.classList.add('active');
        this.adAdjustmentsTabContent.classList.add('in');
      }

      this.rateCardsTabContent = document.querySelector('#production');
      if (this.rateCardsTabContent !== null) {
        this.rateCardsTabContent.classList.remove('active');
        this.rateCardsTabContent.classList.remove('in');
      }
    }

    if (this.globalClass.getOrderId() !== null && this.globalClass.getOrderId() !== undefined && this.globalClass.getOrderId() !== '') {
      this.tabOrderId = this.globalClass.getOrderId();
    } else {
      this.globalClass.setOrderId('');
      this.tabOrderId = '';
    }

    if (this.globalClass.getFromDashboard() !== true) {
      this.globalClass.setFromDashboard(false);
    }

    this.orderFormTab = false;
    this.mediaScheduleTab = false;
    this.adjustmentTab = true;
    this.productionTab = false;

    this.orderFormDisable = false;
    this.mediaScheduleDisable = false;
    this.adjustmentDisable = false;
    this.productionDisable = false;
    this.selectedTab = 'Adjustment';
  }

  // set production tab active
  setProductionTabActive() {
    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.mediaAssetTabContent = document.querySelector('#orders-tab');
        this.mediaAssetTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#media-schedule-tab');
        this.issueDateTabContent.classList.add('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-tab');
        this.adAdjustmentsTabContent.classList.add('collapsed');

        this.rateCardsTabContent = document.querySelector('#production-tab');
        this.rateCardsTabContent.classList.remove('collapsed');

        this.mediaAssetTabContent = document.querySelector('#orders-collapse');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#media-schedule-collapse');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-collapse');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#production-collapse');
        this.rateCardsTabContent.classList.add('in');
        this.rateCardsTabContent.css('height', 'auto');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.remove('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.remove('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.remove('noclick');
      }, 10);
    } else {

      this.mediaAssetTabContent = document.querySelector('#orders');
      if (this.mediaAssetTabContent !== null) {
        this.mediaAssetTabContent.classList.remove('active');
        this.mediaAssetTabContent.classList.remove('in');
      }

      this.issueDateTabContent = document.querySelector('#media-schedule');
      if (this.issueDateTabContent !== null) {
        this.issueDateTabContent.classList.remove('active');
        this.issueDateTabContent.classList.remove('in');
      }

      this.adAdjustmentsTabContent = document.querySelector('#adjustment');
      if (this.adAdjustmentsTabContent !== null) {
        this.adAdjustmentsTabContent.classList.remove('active');
        this.adAdjustmentsTabContent.classList.remove('in');
      }

      this.rateCardsTabContent = document.querySelector('#production');
      if (this.rateCardsTabContent !== null) {
        this.rateCardsTabContent.classList.add('active');
        this.rateCardsTabContent.classList.add('in');
      }
    }

    if (this.globalClass.getOrderId() !== null && this.globalClass.getOrderId() !== undefined && this.globalClass.getOrderId() !== '') {
      this.tabOrderId = this.globalClass.getOrderId();
    } else {
      this.globalClass.setOrderId('');
      this.tabOrderId = '';
    }

    if (this.globalClass.getFromDashboard() !== true) {
      this.globalClass.setFromDashboard(false);
    }

    this.orderFormTab = false;
    this.mediaScheduleTab = false;
    this.adjustmentTab = false;
    this.productionTab = true;

    this.orderFormDisable = false;
    this.mediaScheduleDisable = false;
    this.adjustmentDisable = false;
    this.productionDisable = false;
    this.selectedTab = 'production';
  }

  // On Resize set tab active with pre filled data
  setOrdersTabActiveOnResize() {
    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.issueDateTabContent = document.querySelector('#media-schedule-tab');
        this.issueDateTabContent.classList.add('collapsed');

        this.mediaAssetTabContent = document.querySelector('#orders-tab');
        this.mediaAssetTabContent.classList.remove('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-tab');
        this.adAdjustmentsTabContent.classList.add('collapsed');

        this.rateCardsTabContent = document.querySelector('#production-tab');
        this.rateCardsTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#media-schedule-collapse');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-collapse');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#production-collapse');
        this.rateCardsTabContent.classList.remove('in');

        this.mediaAssetTabContent = document.querySelector('#orders-collapse');
        this.mediaAssetTabContent.classList.add('in');
        this.mediaAssetTabContent.css('height', 'auto');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.add('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.add('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.add('noclick');

      }, 10);

    } else {
      this.mediaAssetTabContent = document.querySelector('#orders');
      if (this.mediaAssetTabContent !== null) {
        this.mediaAssetTabContent.classList.add('active');
        this.mediaAssetTabContent.classList.add('in');
      }

      this.issueDateTabContent = document.querySelector('#media-schedule');
      if (this.issueDateTabContent !== null) {
        this.issueDateTabContent.classList.remove('active');
        this.issueDateTabContent.classList.remove('in');
      }

      this.adAdjustmentsTabContent = document.querySelector('#adjustment');
      if (this.adAdjustmentsTabContent !== null) {
        this.adAdjustmentsTabContent.classList.remove('active');
        this.adAdjustmentsTabContent.classList.remove('in');
      }

      this.rateCardsTabContent = document.querySelector('#production');
      if (this.rateCardsTabContent !== null) {
        this.rateCardsTabContent.classList.remove('active');
        this.rateCardsTabContent.classList.remove('in');
      }
    }

    if (this.globalClass.getFromDashboard() !== true) {
      this.globalClass.setFromDashboard(false);
    }

    this.orderFormTab = true;
    this.mediaScheduleTab = false;
    this.adjustmentTab = false;
    this.productionTab = false;

    this.orderFormDisable = false;
    this.mediaScheduleDisable = true;
    this.adjustmentDisable = true;
    this.productionDisable = true;

    this.isFrozen = false;
    this.selectedTab = 'orders';
  }

  setMediaScheduleTabActiveOnResize() {

    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.mediaAssetTabContent = document.querySelector('#orders-tab');
        this.mediaAssetTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#media-schedule-tab');
        this.issueDateTabContent.classList.remove('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-tab');
        this.adAdjustmentsTabContent.classList.add('collapsed');

        this.rateCardsTabContent = document.querySelector('#production-tab');
        this.rateCardsTabContent.classList.add('collapsed');

        this.mediaAssetTabContent = document.querySelector('#orders-collapse');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#media-schedule-collapse');
        this.issueDateTabContent.classList.add('in');
        this.issueDateTabContent.css('height', 'auto');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-collapse');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#production-collapse');
        this.rateCardsTabContent.classList.remove('in');


        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.remove('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.add('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.add('noclick');

      }, 10);

    } else {
      this.mediaAssetTabContent = document.querySelector('#orders');
      if (this.mediaAssetTabContent !== null) {
        this.mediaAssetTabContent.classList.remove('active');
        this.mediaAssetTabContent.classList.remove('in');
      }

      this.issueDateTabContent = document.querySelector('#media-schedule');
      if (this.issueDateTabContent !== null) {
        this.issueDateTabContent.classList.add('active');
        this.issueDateTabContent.classList.add('in');
      }

      this.adAdjustmentsTabContent = document.querySelector('#adjustment');
      if (this.adAdjustmentsTabContent !== null) {
        this.adAdjustmentsTabContent.classList.remove('active');
        this.adAdjustmentsTabContent.classList.remove('in');
      }

      this.rateCardsTabContent = document.querySelector('#production');
      if (this.rateCardsTabContent !== null) {
        this.rateCardsTabContent.classList.remove('active');
        this.rateCardsTabContent.classList.remove('in');
      }
    }
    if (this.globalClass.getFromDashboard() !== true) {
      this.globalClass.setFromDashboard(false);
    }

    this.orderFormTab = false;
    this.mediaScheduleTab = true;
    this.adjustmentTab = false;
    this.productionTab = false;

    if (this.isFrozen === true) {
      this.orderFormDisable = true;
    } else {
      this.orderFormDisable = false;
    }

    this.mediaScheduleDisable = false;
    if (this.isDisable === true) {
      this.adjustmentDisable = true;
      this.productionDisable = true;
    } else {
      this.adjustmentDisable = false;
      this.productionDisable = false;
    }
    this.selectedTab = 'mediaschedule';
  }

  setAdjustmentTabActiveOnResize() {

    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.mediaAssetTabContent = document.querySelector('#orders-tab');
        this.mediaAssetTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#media-schedule-tab');
        this.issueDateTabContent.classList.add('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-tab');
        this.adAdjustmentsTabContent.classList.remove('collapsed');

        this.rateCardsTabContent = document.querySelector('#production-tab');
        this.rateCardsTabContent.classList.add('collapsed');

        this.mediaAssetTabContent = document.querySelector('#orders-collapse');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#media-schedule-collapse');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-collapse');
        this.adAdjustmentsTabContent.classList.add('in');
        this.adAdjustmentsTabContent.css('height', 'auto');

        this.rateCardsTabContent = document.querySelector('#production-collapse');
        this.rateCardsTabContent.classList.remove('in');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.remove('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.remove('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.add('noclick');

      }, 10);
    } else {
      this.mediaAssetTabContent = document.querySelector('#orders');
      if (this.mediaAssetTabContent !== null) {
        this.mediaAssetTabContent.classList.remove('active');
        this.mediaAssetTabContent.classList.remove('in');
      }

      this.issueDateTabContent = document.querySelector('#media-schedule');
      if (this.issueDateTabContent !== null) {
        this.issueDateTabContent.classList.remove('active');
        this.issueDateTabContent.classList.remove('in');
      }

      this.adAdjustmentsTabContent = document.querySelector('#adjustment');
      if (this.adAdjustmentsTabContent !== null) {
        this.adAdjustmentsTabContent.classList.add('active');
        this.adAdjustmentsTabContent.classList.add('in');
      }

      this.rateCardsTabContent = document.querySelector('#production');
      if (this.rateCardsTabContent !== null) {
        this.rateCardsTabContent.classList.remove('active');
        this.rateCardsTabContent.classList.remove('in');
      }
    }

    if (this.globalClass.getFromDashboard() !== true) {
      this.globalClass.setFromDashboard(false);
    }

    this.orderFormTab = false;
    this.mediaScheduleTab = false;
    this.adjustmentTab = true;
    this.productionTab = false;
    
    this.orderFormDisable = false;
    this.mediaScheduleDisable = false;
    this.adjustmentDisable = false;
    this.productionDisable = false;
    this.selectedTab = 'Adjustment';
  }

  setProductionTabActiveOnResize() {
    if (this.screenWidth < 768) {
      setTimeout(() => {
        this.mediaAssetTabContent = document.querySelector('#orders-tab');
        this.mediaAssetTabContent.classList.add('collapsed');

        this.issueDateTabContent = document.querySelector('#media-schedule-tab');
        this.issueDateTabContent.classList.add('collapsed');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-tab');
        this.adAdjustmentsTabContent.classList.add('collapsed');

        this.rateCardsTabContent = document.querySelector('#production-tab');
        this.rateCardsTabContent.classList.remove('collapsed');

        this.mediaAssetTabContent = document.querySelector('#orders-collapse');
        this.mediaAssetTabContent.classList.remove('in');

        this.issueDateTabContent = document.querySelector('#media-schedule-collapse');
        this.issueDateTabContent.classList.remove('in');

        this.adAdjustmentsTabContent = document.querySelector('#adjustment-collapse');
        this.adAdjustmentsTabContent.classList.remove('in');

        this.rateCardsTabContent = document.querySelector('#production-collapse');
        this.rateCardsTabContent.classList.add('in');
        this.rateCardsTabContent.css('height', 'auto');

        this.mediaAssetPanelDiv = this.mediaAssetTabContent.closest('div.panel.panel-default');
        this.mediaAssetPanelDiv.classList.remove('noclick');

        this.issueDatePanelDiv = this.issueDateTabContent.closest('div.panel.panel-default');
        this.issueDatePanelDiv.classList.remove('noclick');

        this.adAdjustmentsPanelDiv = this.adAdjustmentsTabContent.closest('div.panel.panel-default');
        this.adAdjustmentsPanelDiv.classList.remove('noclick');

        this.rateCardsPanelDiv = this.rateCardsTabContent.closest('div.panel.panel-default');
        this.rateCardsPanelDiv.classList.remove('noclick');

      }, 10);
    } else {
      this.mediaAssetTabContent = document.querySelector('#orders');
      if (this.mediaAssetTabContent !== null) {
        this.mediaAssetTabContent.classList.remove('active');
        this.mediaAssetTabContent.classList.remove('in');
      }

      this.issueDateTabContent = document.querySelector('#media-schedule');
      if (this.issueDateTabContent !== null) {
        this.issueDateTabContent.classList.remove('active');
        this.issueDateTabContent.classList.remove('in');
      }

      this.adAdjustmentsTabContent = document.querySelector('#adjustment');
      if (this.adAdjustmentsTabContent !== null) {
        this.adAdjustmentsTabContent.classList.remove('active');
        this.adAdjustmentsTabContent.classList.remove('in');
      }

      this.rateCardsTabContent = document.querySelector('#production');
      if (this.rateCardsTabContent !== null) {
        this.rateCardsTabContent.classList.add('active');
        this.rateCardsTabContent.classList.add('in');
      }
    }

    if (this.globalClass.getFromDashboard() !== true) {
      this.globalClass.setFromDashboard(false);
    }

    this.orderFormTab = false;
    this.mediaScheduleTab = false;
    this.adjustmentTab = false;
    this.productionTab = true;

    this.orderFormDisable = false;
    this.mediaScheduleDisable = false;
    this.adjustmentDisable = false;
    this.productionDisable = false;
    this.selectedTab = 'production';
  }

}

