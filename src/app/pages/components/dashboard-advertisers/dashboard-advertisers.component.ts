import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { DashboardAdvertiserService } from '../../services/dashboardAdvertisers.service';
import { OrderTabService } from '../../services/orderTab.service';
import { AppComponent } from '../../../app.component';
import * as alasql from 'alasql';
import { GlobalClass } from '../../GlobalClass';
import { ConstantPool } from '@angular/compiler/src/constant_pool';

@Component({
  selector: 'asi-dashboard-advertisers',
  templateUrl: './dashboard-advertisers.component.html',
  styleUrls: ['./dashboard-advertisers.component.css']
})
export class DashboardAdvertisersComponent implements OnInit {
  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  apiBaseURL = '';
  // ---------------------------------
  draw = 1;
  skip = 0;
  take = 9;
  searchText = '';
  createdOrders = [];
  organizationWiseCards: any = [];
  searchTypeActive = '';
  // ---------------------------------
  partyAndOrganizationData = [];
  advertisers = [];
  agencies = [];
  billingToContacts = [];
  organizationsIds = [];
  dashboardAdvertiserIds=[];
  // ---------------------------------
  hasNext = true;
  offset = 0;
  // ---------------------------------
  searchWords = '';
  maxSize = 5;
  bigTotalItems = 0;
  bigCurrentPage = 1;
  isPaginationActive = true;

  isShowDefaultTable = false;
  dataLoading = true;

  obj: any = {};

  constructor(
    private toastr: ToastrService,
    private dashboardAdvertiserService: DashboardAdvertiserService,
    private orderTabService: OrderTabService,
    private globalClass: GlobalClass,
    private appComponent: AppComponent
  ) { }

  ngOnInit() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }

    try {
      this.getAllPartyDataThroughASI();
    } catch (error) {
      console.log(error);
    }
  }

  // Get the list of all party data from ASI
  getAllPartyDataThroughASI() {
    try {
      if (this.globalClass.getMainPartyAndOrganizationData().length <= 0 && this.globalClass.getMainAdvertisersData().length <= 0
        && this.globalClass.getMainAgenciesData().length <= 0 && this.globalClass.getMainBillingToContactsData().length <= 0) {
        this.hasNext = true;
        this.offset = 0;

        this.resetData();
        this.getPartyThroughASI('A');
      } else {
        this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
        this.advertisers = this.globalClass.getMainAdvertisersData();
        this.agencies = this.globalClass.getMainAgenciesData();
        this.billingToContacts = this.globalClass.getMainBillingToContactsData();
        this.searchTypeActive = 'start';
        this.searchText = 'A';
        this.getPartyThroughASI(this.searchText);
        // if(this.partyAndOrganizationData.length>0)
        // {
        //   this.getAllPartyStartWithEnglishAlphabet(this.searchText);
        // }
        // else{
          
        // }
      }
    } catch (error) {
      console.log(error);
    }
  }

  getPartyThroughASI(charStart) {
    try {
      this.showLoader();
      this.resetData();
      this.dashboardAdvertiserService.getAllAdvertiser_CharacterSearch(this.websiteRoot, this.baseUrl,
        this.reqVerificationToken, this.offset,charStart).subscribe(result => {
          if (result != null && result !== undefined && result !== '') {
            const ItemData = result.Items.$values;
            if (ItemData.length > 0) {
              for (const itemdatavalue of ItemData) {
                this.partyAndOrganizationData.push(itemdatavalue);
                const type = itemdatavalue.$type;
                if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                  this.advertisers.push(itemdatavalue);
                  this.agencies.push(itemdatavalue);
                } else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                  this.billingToContacts.push(itemdatavalue);
                }
              }
              this.offset = result.Offset;
              const NextOffset = this.offset + 500;
              if (result.Count === 500) {
                this.offset = NextOffset;
                this.getPartyThroughASI(charStart);
              } else {
                this.setGlobalData();
                this.searchTypeActive = 'start';
                this.searchText = charStart;
                this.hideLoader();
                this.getAllPartyStartWithEnglishAlphabet(this.searchText);
              }
            } else {
              this.setGlobalData();
              this.searchTypeActive = 'start';
              this.searchText = charStart;
              this.hideLoader();
              this.getAllPartyStartWithEnglishAlphabet(this.searchText);
            }
          } else {
            this.hideLoader();
            this.resetData();
          }
        }, error => {
          this.hideLoader();
          console.log(error);
        });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  resetData() {
    this.partyAndOrganizationData = [];
    this.advertisers = [];
    this.agencies = [];
    this.billingToContacts = [];
  }

  setGlobalData() {
    this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
    this.globalClass.setMainAdvertisersData(this.advertisers);
    this.globalClass.setMainAgenciesData(this.agencies);
    this.globalClass.setMainBillingToContactsData(this.billingToContacts);
  }

  // Get the list of all party which is start with english alphabet from ASI
  getAllPartyStartWithEnglishAlphabet(firstWord) {
    try {
      if (firstWord != null && firstWord !== '' && firstWord !== 0 && firstWord !== undefined) {
        const filterWord = firstWord + '%';
        const allPartyData = this.partyAndOrganizationData;
        if(this.partyAndOrganizationData.length>0)
        {
              const alphabetWiseFilterdata = alasql('SELECT * FROM ? AS add WHERE OrganizationName LIKE ?'
              , [this.partyAndOrganizationData, filterWord]);

            if (alphabetWiseFilterdata.length > 0) {
              const organizationIds = alasql('SELECT Id FROM ? AS add', [alphabetWiseFilterdata]);
              let idsString = '';

              organizationIds.forEach((data, key) => {
                if (key === 0) {
                  idsString += data.Id;
                } else {
                  idsString += ',' + data.Id;
                }
              });

              if (idsString != null && idsString !== '' && idsString !== '0' && idsString !== undefined) {
                this.getOrdersByAdvertisersIds(idsString);
              }
            } else {
              this.isShowDefaultTable = true;
              this.dataLoading = false;
              this.resetData();
              this.setGlobalData();
            }
        }
        else{
          this.getPartyThroughASI('A');
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  // Get the list of orders by advertisers ids
  getOrdersByAdvertisersIds(idsString) {
    try {
      if (idsString != null && idsString !== '' && idsString !== '0' && idsString !== undefined) {
        const arrayIds = idsString.split(',');
        this.obj = {};
        this.obj.Draw = this.draw;
        this.obj.Skip = this.skip;
        this.obj.Take = this.take;
        this.obj.AdvertisorId = arrayIds;
        this.showLoader();
        this.dashboardAdvertiserService.getOrdersByAdvertiser(this.reqVerificationToken, this.obj).subscribe(result => {
          if (result.StatusCode === 1) {
            this.hideLoader();
            if (result.Data.length > 0) {
              this.createdOrders = result.Data;
              if (this.createdOrders.length > 0) {
                this.getOrganizationforCreatedOrder();
              }
            } else {
              this.createdOrders = [];
              this.isShowDefaultTable = true;
              this.dataLoading = false;
              this.resetData();
              this.setGlobalData();
            }
          } else if (result.StatusCode === 3) {
            this.hideLoader();
            this.createdOrders = [];
            this.isShowDefaultTable = true;
            this.dataLoading = false;
            this.resetData();
            this.setGlobalData();
          } else {
            this.hideLoader();
            this.createdOrders = [];
            this.isShowDefaultTable = true;
            this.resetData();
            this.setGlobalData();
            this.dataLoading = false;
            this.toastr.error(result.Message, 'Error');
          }
        }, error => {
          this.hideLoader();
          console.log(error);
        });
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }


  // Get the organisation for create order
  getOrganizationforCreatedOrder() {
    try {
      if (this.createdOrders.length > 0) {
        this.organizationWiseCards = [];
        this.dashboardAdvertiserIds=[];
        this.createdOrders.forEach((data, key) => {
          const advertiserId = data.AdvertiserId;
          let organizationData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.partyAndOrganizationData, advertiserId]);
          
          const contactId = data.ST_ID;
          //const contactData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [this.partyAndOrganizationData, contactId]);
          const grossCost = data.GrossCost;
          const netCost = data.NetCost;
          const noOfOrders = data.NoOfOrders;
          const totalCount = data.TotalCount;

          if (organizationData.length > 0) {
            this.obj = {};
            this.obj.OrganizationId = advertiserId;
            this.obj.OrganizationName = organizationData[0].OrganizationName;
            this.obj.contactId = contactId;
            this.obj.contactName = contactId; //contactData[0].Name;
            this.obj.GrossCost = grossCost;
            this.obj.NetCost = netCost;
            this.obj.NoOfOrders = noOfOrders;
            this.obj.TotalCount = totalCount;
            this.organizationWiseCards.push(this.obj);
            this.dashboardAdvertiserIds.push(advertiserId);
          }
          this.isShowDefaultTable = false;
          this.dataLoading = false;
          this.globalClass.setDashboardAdvertiserIDs(this.dashboardAdvertiserIds);
        });
        this.organizationWiseCards = alasql('SELECT * FROM ? ORDER BY OrganizationName', [this.organizationWiseCards]);
        this.createCustomPagination();
      } else {
        this.isShowDefaultTable = true;
        this.dataLoading = false;
        this.resetData();
        this.setGlobalData();
      }
      if (this.organizationWiseCards.length === 0) {
        this.isShowDefaultTable = true;
        this.dataLoading = false;
        this.resetData();
        this.setGlobalData();
      }
    } catch (error) {
      console.log(error);
    }
  }

  createCustomPagination() {
    let mainData = [];
    mainData = this.organizationWiseCards;
    let paginationLength = 1;
    let totalLength = 1;
    if (mainData.length > 0) {
      totalLength = mainData[0].TotalCount;
      paginationLength = Math.ceil(totalLength / 10);
    }
    this.maxSize = 5;
    this.bigTotalItems = totalLength;
    this.isPaginationActive = false;
  }

  pageChanged(page) {
    this.bigCurrentPage = page;
    this.draw = this.bigCurrentPage;
    const skipRecords = this.bigCurrentPage - 1;
    this.skip = this.take * skipRecords;
    this.take = 9;
    if (this.searchTypeActive === 'contains') {
      this.getAllPartyContainsEnglishAlphabet(this.searchText);
    } else if (this.searchTypeActive === 'start') {
      this.getAllPartyStartWithEnglishAlphabet(this.searchText);
    }
  }

  // Get the list of all party which contain with english alphabet from ASI
  getAllPartyContainsEnglishAlphabet(firstWord) {
    try {
      if (firstWord != null && firstWord !== '' && firstWord !== 0 && firstWord !== undefined) {

        const filterWord = '%' + firstWord + '%';

        const alphabetWiseFilterdata = alasql('SELECT * FROM ? AS add WHERE OrganizationName LIKE ?',
          [this.partyAndOrganizationData, filterWord]);
console.log(alphabetWiseFilterdata);

        if (alphabetWiseFilterdata.length > 0) {
          const organizationIds = alasql('SELECT Id FROM ? AS add',
            [alphabetWiseFilterdata]);
          let idsString = '';
          organizationIds.forEach((data, key) => {
            if (key === 0) {
              idsString += data.Id;
            } else {
              idsString += ',' + data.Id;
            }
          });
          if (idsString != null && idsString !== '' && idsString !== '0' && idsString !== undefined) {
            console.log(idsString);
            //this.getOrdersByAdvertisersIds(idsString);
          }
        } else {
          this.isShowDefaultTable = true;
          this.resetData();
          this.setGlobalData();
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  // On enter event get all party search wise
  onEnterGetAllPartySearchWise(words, keyEvent) {
    try {
      this.draw = 1;
      this.skip = 0;
      this.take = 9;

      if (keyEvent.which === 13) {
        keyEvent.preventDefault();
        if (this.partyAndOrganizationData.length !== 0) {
          if (words !== '' && words !== undefined && words != null) {
            this.searchTypeActive = 'contains';
            this.getAllPartyContainsEnglishAlphabet(words);
          } else {
            this.searchTypeActive = 'start';
            this.searchText = 'A';
            this.getAllPartyStartWithEnglishAlphabet(this.searchText);
          }
        } else {
          this.getAllPartyDataThroughASI();
        }
        return false;
      }
    } catch (error) {
      console.log(error);
    }
  }

  // On click event get all party search wise
  onClickGetAllPartySearchWise(words) {
    try {
      this.draw = 1;
      this.skip = 0;
      this.take = 9;
      if (this.partyAndOrganizationData.length !== 0) {
        if (words !== '' && words !== undefined && words != null) {
          this.searchTypeActive = 'contains';
          this.getAllPartyContainsEnglishAlphabet(words);
        } else {
          this.searchTypeActive = 'start';
          this.searchText = 'A';
          this.getAllPartyStartWithEnglishAlphabet(this.searchText);
        }
      } else {
        this.getAllPartyDataThroughASI();
      }
      return false;
    } catch (error) {
      console.log(error);
    }
  }

  filterDataAlphabetsWise(startWith) {
    try {
      this.draw = 1;
      this.skip = 0;
      this.take = 9;
      this.searchText = startWith;
      this.searchTypeActive = 'start';
      //this.getAllPartyStartWithEnglishAlphabet(startWith);
      this.getPartyThroughASI(startWith);
    } catch (error) {
      console.log(error);
    }
  }

  // Redirect to order tab by advertiser
  redirectToOrderTabByAdvertiser(objfilter) {
    const id = objfilter.OrganizationId;
    if (id != null && id !== undefined && id !== '') {
      if (id > 0) {
        this.globalClass.setAdvertiserID(id);
      }
    }
    this.appComponent.ordersTab = true;
    this.appComponent.advertisersTab = false;
    this.appComponent.postOrdersTab = false;
    this.appComponent.missingMaterialsTab = false;
  }

  // Create new order
  createNewOrder(objOrder) {
    try {
      this.globalClass.setDashboardOrganizationId(objOrder.OrganizationId);
      this.globalClass.setDashboardAgencyId(objOrder.AgencyId);
      this.globalClass.setDashboardContactId(objOrder.contactId);
      this.globalClass.setGrossCost((0).toFixed(2));
      this.globalClass.setNetCost((0).toFixed(2));
      this.globalClass.setFromDashboard(true);
      this.globalClass.setPreOrderFill(true);
      this.globalClass.setFromPreviousTab(false);
      this.globalClass.setFromProductionTab(false);
      this.globalClass.setOrderId('');
      this.appComponent.showMainPage = false;
      this.appComponent.showOrderFormPage = true;
      this.globalClass.setbillingDetailsMediaOrderid('')
      this.orderTabService.send_OrderTab('OrdersTab');
    } catch (error) {
      console.log(error);
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }

  showBillToContactName(orderData)
  {
    const billToContactId = orderData.contactId;
    const organizationId = orderData.OrganizationId;
    let ItemData=[];
    try {
      this.showLoader();
      this.dashboardAdvertiserService.getAdvertiser_BillToContactNameById(this.websiteRoot, this.baseUrl,
        this.reqVerificationToken, billToContactId).subscribe(result => {
          if (result != null && result !== undefined && result !== '') {
             ItemData.push(result.Items.$values);
             for (const itemdatavalue of ItemData)
             {
              document.getElementById("anc"+ billToContactId+""+organizationId).innerHTML= billToContactId +" (" + itemdatavalue[0].Name+")";
             }
            this.hideLoader();
          }
        }, error => {
          this.hideLoader();
          console.log(error);
        });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }
}
