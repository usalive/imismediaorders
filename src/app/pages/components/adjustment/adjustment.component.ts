import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdjustmentService } from '../../services/adjustment.service';
import { OrderTabService } from '../../services/orderTab.service';
import { saveAs } from '@progress/kendo-file-saver';
import { environment } from '../../../../environments/environment';
import { AppComponent } from '../../../app.component';
import { GlobalClass } from '../../GlobalClass';
import { isNullOrUndefined } from 'util';
import * as alasql from 'alasql';
import 'select2';
import { Console } from '@angular/core/src/console';
declare var jsPDF: any;
declare var $, jQuery: any;

@Component({
  selector: 'asi-adjustment',
  templateUrl: './adjustment.component.html',
  styleUrls: ['./adjustment.component.css']
})
export class AdjustmentComponent implements OnInit {
  addStreetProposalAdjHTML1: any;
  addStreetAdjHTML1: any;

  // ---------- form var. ------------
  grossAdAdjustmentModalForm: FormGroup;
  netAdAdjustmentModalForm: FormGroup;
  multipleGrossAdAdjustmentModalForm: FormGroup;
  multipleNetAdAdjustmentModalForm: FormGroup;
  ordersDetailModalForm: FormGroup;
  proposalModalForm: FormGroup;
  frmMediaDocumentsUpload: FormGroup;

  isGrossAdAdjustmentModalFormSubmitted = false;
  isNetAdAdjustmentModalFormSubmitted = false;
  isGmultipleGrossAdAdjustmentModalFormSubmitted = false;
  isMultipleNetAdAdjustmentModalFormSubmitted = false;
  isSignedDocumentRequiredMsg=false;

  // ---------- const var. ------------
  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  apiBaseURL = '';
  likelihood = 0;
  // ---------- array var. ------------
  tempAdjustment = [];
  mediaIssueDateWiseadAdjustment = [];
  issueDateWiseAdjustment = [];
  selectedIssueMediaOrder = [];
  selectedCheckbox = [];
  selectedIssueMediaOrderData = [];
  selectedIssueMediaOrderDataDownload = [];
  selectedMediaAssetsIds = [];
  selectedMediaOrderIds = [];
  mediaOrderReps = [];
  grossAdAdjustments: any[] = [];
  selectedGrossAdjustmentCheckbox = [];
  selectedGrossAdjustmentMediaOrderLines = [];
  netAdAdjustments = [];
  selectedNetAdjustmentCheckbox = [];
  selectedNetAdjustmentMediaOrderLines = [];
  tempMediaOrderLinesforGrossMultiple = [];
  tempMediaOrderLinesforNetMultiple = [];
  loadTimeIssueDateWiseAdjustment = [];
  personData = [];
  partyAndOrganizationData = [];
  agencies: any[] = [];
  organizationWiseParty: any[] = [];
  adAdjustments: any[] = [];
  mediaOrderSignedDocumentsList: any[] = [];


  // ---------- any var. ------------
  obj: any = {};
  objGrossAndNet: any = {};
  mediaOrderLineModel: any = {};
  ordersDetail: any = {};
  commonOrderDetail: any = {};
  adjustment: any = {};
  grossAdAdjustment: any = {};
  netAdAdjustment: any = {};
  advertisers: any;
  advertisersNew: any;
  likelihoods: any = ['10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%'];
  mediaIssueDateWiseAdjustment: any;

  // ---------- bool var. ------------
  selectedAllGrossAdjustment = false;
  isShowDefaultTable = true;
  isShowDefaultInnerTable = true;
  selectedAllIssueMediaOrders = false;
  isMultiple = false;
  isShowDefaultGrossAdjustmentTable = true;
  grossAdjustmentshowMsgs = false;
  isPercentage = false;
  selectedGrossAdjustment = false;
  isShowDefaultNetAdjustmentTable = true;
  netAdjustmentshowMsgs = false;
  netisPercentage = false;
  selectedNetAdjustment = false;

  multipleGrossAdjustmentshowMsgs = false;
  multipleNetAdjustmentshowMsgs = false;
  isEditMode = false;
  isMultipleIssueDate = false;
  isAddress = false;
  isAgency = false;
  isNetChangedByMove = false;
  isGrossChangedByMoveUp = false;
  setGrossAndNetDivNoClick = true;

  mediaAssetId = 0;
  runningCost = 0;
  selectedMediaOrderId = 0;
  mediaOrderId = 0;
  mediaOrderLineId = 0;
  firstTimeOnly = 1;

  tempGrossAmount: any = parseFloat('0').toFixed(2);
  tempNetAmount = parseFloat('0').toFixed(2);
  finalGrossAmount: any = parseFloat('0').toFixed(2);
  finalNetAmount: any = parseFloat('0').toFixed(2);
  finalTotalGrossAmount: any = parseFloat('0').toFixed(2);
  finalTotalNetAmount: any = parseFloat('0').toFixed(2);
  finalTotalAdCost: any = parseFloat('0').toFixed(2);
  tempNetAmountInit = parseFloat('0').toFixed(2);

  buyerId: any = this.globalClass.getOrderId();
  insertMode = 'insertMode';
  amountType = 'Amount';
  netamountType = 'Amount';
  orderType = 'Proposal';
  grossCost: '';
  netCost: '';
  htmlTemp = '';
  editImageIcon = '';
  sampleLogoImagePath = '';
  checkboxImagePath = '';
  commonDetaildvAdjHTML = '';
  commonDetaildvProposalAdjHTML = '';
  addStreetAdjHTML = '';
  addStreetProposalAdjHTML = '';

  mediaOrderFile='';
  mediaOrderFileExtension='';
  mediaOrderFileSrc='';

  isFrozen = false;

  sequenceNumber = 0;
  dataContext = [];
  test: string;

  constructor(private adjustmentService: AdjustmentService,
    private toastr: ToastrService,
    private orderTabService: OrderTabService,
    private globalClass: GlobalClass,
    private appComponent: AppComponent,
    private formBuilder: FormBuilder) { }



  ngOnInit() {

    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split('/');
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
        this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }

    if (this.globalClass.getIsFrozen()) {
      this.isFrozen = this.globalClass.getIsFrozen();
    }

    this.validation();
    this.mediaDocumentValidation();
    this.getMediaOrderSignedDocumentList();
    this.ordersDetailModalForm = this.formBuilder.group({
    });

    this.proposalModalForm = this.formBuilder.group({
    });


    this.adjustment.adOrderId = this.globalClass.getOrderId();
    this.isShowDefaultGrossAdjustmentTable = true;
    this.grossAdjustmentshowMsgs = false;
    this.grossAdAdjustment = {};
    this.grossAdAdjustments = [];
    this.amountType = 'Amount';
    this.isPercentage = false;
    this.selectedGrossAdjustment = false;
    this.isShowDefaultNetAdjustmentTable = true;
    this.netAdjustmentshowMsgs = false;
    this.netAdAdjustment = {};
    this.netAdAdjustments = [];
    this.netamountType = 'Amount';
    this.netisPercentage = false;
    this.selectedNetAdjustment = false;
    this.tempNetAmount = parseFloat('0').toFixed(2);
    this.tempGrossAmount = parseFloat('0').toFixed(2);
    this.finalGrossAmount = parseFloat('0').toFixed(2);
    this.finalNetAmount = parseFloat('0').toFixed(2);
    this.getNoOfIssueMediaOrder();
  }

  get isValidAdAdjustmentId() {
    return this.grossAdAdjustmentModalForm.get('AdAdjustmentId').invalid && (this.isGrossAdAdjustmentModalFormSubmitted || this.grossAdAdjustmentModalForm.get('AdAdjustmentId').dirty || this.grossAdAdjustmentModalForm.get('AdAdjustmentId').touched);
  }
  get isValidAdAdjustmentValue() {
    return this.grossAdAdjustmentModalForm.get('AdAdjustmentValue').invalid && (this.isGrossAdAdjustmentModalFormSubmitted || this.grossAdAdjustmentModalForm.get('AdAdjustmentValue').dirty || this.grossAdAdjustmentModalForm.get('AdAdjustmentValue').touched);
  }
  get isValidNetAdAdjustmentId() {
    return this.netAdAdjustmentModalForm.get('AdAdjustmentId').invalid && (this.isNetAdAdjustmentModalFormSubmitted || this.netAdAdjustmentModalForm.get('AdAdjustmentId').dirty || this.netAdAdjustmentModalForm.get('AdAdjustmentId').touched);
  }
  get isValidNetAdAdjustmentValue() {
    return this.netAdAdjustmentModalForm.get('AdAdjustmentValue').invalid && (this.isNetAdAdjustmentModalFormSubmitted || this.netAdAdjustmentModalForm.get('AdAdjustmentValue').dirty || this.netAdAdjustmentModalForm.get('AdAdjustmentValue').touched);
  }
  get isValidMultipleGrossAdAdjustmentId() {
    return this.multipleGrossAdAdjustmentModalForm.get('AdAdjustmentId').invalid && (this.isGmultipleGrossAdAdjustmentModalFormSubmitted || this.multipleGrossAdAdjustmentModalForm.get('AdAdjustmentId').dirty || this.multipleGrossAdAdjustmentModalForm.get('AdAdjustmentId').touched);
  }
  get isValidMultipleGrossAdAdjustmentValue() {
    return this.multipleGrossAdAdjustmentModalForm.get('AdAdjustmentValue').invalid && (this.isGmultipleGrossAdAdjustmentModalFormSubmitted || this.multipleGrossAdAdjustmentModalForm.get('AdAdjustmentValue').dirty || this.multipleGrossAdAdjustmentModalForm.get('AdAdjustmentValue').touched);
  }
  get isValidMultipleNetAdAdjustmentId() {
    return this.multipleNetAdAdjustmentModalForm.get('AdAdjustmentId').invalid && (this.isMultipleNetAdAdjustmentModalFormSubmitted || this.multipleNetAdAdjustmentModalForm.get('AdAdjustmentId').dirty || this.multipleNetAdAdjustmentModalForm.get('AdAdjustmentId').touched);
  }
  get isValidMultipleNetAdAdjustmentValue() {
    return this.multipleNetAdAdjustmentModalForm.get('AdAdjustmentValue').invalid && (this.isMultipleNetAdAdjustmentModalFormSubmitted || this.multipleNetAdAdjustmentModalForm.get('AdAdjustmentValue').dirty || this.multipleNetAdAdjustmentModalForm.get('AdAdjustmentValue').touched);
  }
  validation() {
    this.grossAdAdjustmentModalFormValidation();
    this.netAdAdjustmentModalFormValidation();
    this.multipleGrossAdAdjustmentModalFormValidation();
    this.multipleNetAdAdjustmentModalFormValidation();
  }

  grossAdAdjustmentModalFormValidation() {
    this.grossAdAdjustmentModalForm = this.formBuilder.group({
      'AdAdjustmentId': [null, Validators.required],
      'AdAdjustmentValue': [null, Validators.required],
    });
  }

  netAdAdjustmentModalFormValidation() {
    this.netAdAdjustmentModalForm = this.formBuilder.group({
      'AdAdjustmentId': [null, Validators.required],
      'AdAdjustmentValue': [null, Validators.required],
    });
  }

  multipleGrossAdAdjustmentModalFormValidation() {
    this.multipleGrossAdAdjustmentModalForm = this.formBuilder.group({
      'AdAdjustmentId': [null, Validators.required],
      'AdAdjustmentValue': [null, Validators.required],
    });
  }

  multipleNetAdAdjustmentModalFormValidation() {
    this.multipleNetAdAdjustmentModalForm = this.formBuilder.group({
      'AdAdjustmentId': [null, Validators.required],
      'AdAdjustmentValue': [null, Validators.required],
    });
  }

  // Initialize page controls
  initPageControls() {
    this.isShowDefaultGrossAdjustmentTable = true;
    this.grossAdjustmentshowMsgs = false;
    this.grossAdAdjustment = {};
    this.grossAdAdjustments = [];
    this.amountType = 'Amount';
    this.isPercentage = false;
    this.selectedGrossAdjustment = false;
    this.isShowDefaultNetAdjustmentTable = true;
    this.netAdjustmentshowMsgs = false;
    this.netAdAdjustment = {};
    this.netAdAdjustments = [];
    this.netamountType = 'Amount';
    this.netisPercentage = false;
    this.selectedNetAdjustment = false;
    this.tempGrossAmount = parseFloat('0').toFixed(2);
    this.tempNetAmount = parseFloat('0').toFixed(2);
    this.finalNetAmount = parseFloat('0').toFixed(2);
    this.finalGrossAmount = parseFloat('0').toFixed(2);
  }

  // Initialize gross cost and net cost
  initGrossandNetCost() {
    const totalSum = alasql('SELECT sum(GrossCost),sum(NetCost) FROM ? AS add ', [this.issueDateWiseAdjustment]);
    const grossAmount = parseFloat(totalSum[0]['SUM(GrossCost)']);
    const netAmount = parseFloat(totalSum[0]['SUM(NetCost)']);
    this.globalClass.setGrossCost((grossAmount).toFixed(2));
    this.globalClass.setNetCost((netAmount).toFixed(2));
    this.grossCost = this.globalClass.getGrossCost();
    this.netCost = this.globalClass.getNetCost();
  }

  // Get number of issue media order
  getNoOfIssueMediaOrder() {
    try {
      const buyerId = this.buyerId;
      if (!isNullOrUndefined(buyerId)) {
        this.adjustmentService.getMediaOrderbyBuyId(buyerId, this.reqVerificationToken).subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.isShowDefaultTable = false;
                this.issueDateWiseAdjustment = result.Data;
                console.log("media details:"+ JSON.stringify(this.issueDateWiseAdjustment));
                if (result.Data.length > 0) {
                  this.commonOrderDetail = result.Data[0];
                  this.mediaOrderReps = this.commonOrderDetail.MediaOrderReps;
                  this.commonDetaildvAdjHTML = '';
                  this.commonDetaildvProposalAdjHTML = '';
                  let reps = ''; let htmlTemp = '';

                  this.mediaOrderReps.forEach((data, key) => {
                    reps = data.RepsName + ', ' + data.TerritoryName + ' (' + data.Commission + '%)';
                    htmlTemp += '<div><b>Reps ' + (key + 1) + ' </b> <span>' + reps + '</span></div>';
                  });
                  this.commonDetaildvAdjHTML = htmlTemp;
                  this.commonDetaildvProposalAdjHTML = htmlTemp;
                }
                if (this.firstTimeOnly === 1) {

                  this.issueDateWiseAdjustment.forEach((data, key) => {
                    this.obj = {};
                    this.obj.MediaOrderId = data.MediaOrderId;
                    this.grossCost = data.GrossCost;
                    this.netCost = data.NetCost;
                    this.loadTimeIssueDateWiseAdjustment.push(this.obj);
                  });

                  this.firstTimeOnly++;
                }
                this.initGrossandNetCost();
              } else {
                this.isShowDefaultTable = true;
                this.issueDateWiseAdjustment = [];
                this.loadTimeIssueDateWiseAdjustment = [];
              }
            } else {
              this.isShowDefaultTable = true;
              this.issueDateWiseAdjustment = [];
              this.loadTimeIssueDateWiseAdjustment = [];
            }
          } else if (result.StatusCode === 3) {
            this.isShowDefaultTable = true;
            this.issueDateWiseAdjustment = [];
            this.loadTimeIssueDateWiseAdjustment = [];
          } else {
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          console.log(error);
        });
      } else {
        console.log('Buyer Id is null');
      }
    } catch (error) {
      console.log(error);
    }
  }
  onLikelihoodChange(value) {

    try {
      this.likelihood = parseInt(value);
    }
    catch (error) {
      console.log(error);
    }
  }
  // Get the ad adjustment from Database by Media Asset Id
  getAdAdjustmentByMediaAssetId() {
    try {
      if (this.issueDateWiseAdjustment.length > 0) {
        if (this.selectedMediaAssetsIds.length > 0) {

          let mediaAssetIds = '';
          this.selectedMediaAssetsIds.forEach((data, key) => {
            const id = data;
            if (key === 0) {
              mediaAssetIds += 'id=' + id;
            } else {
              mediaAssetIds += '&id=' + id;
            }
          });

          if (mediaAssetIds != null && mediaAssetIds !== undefined && mediaAssetIds !== '') {
            try {
              this.adjustmentService.getByMultipleMediaAsset(mediaAssetIds).subscribe(result => {
                if (result.StatusCode === 1) {
                  if (result.Data != null) {
                    if (result.Data.length > 0) {
                      this.adAdjustments = result.Data;
                    }
                  }
                } else {
                  this.toastr.error(result.Message, 'Error!');
                }
              }, error => {
                console.log(error);
              });
            } catch (error) {
              console.log(error);
            }
          } else {
            console.log('media assets Id is null');
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  // redirect to production page
  redirectToProductionPage() {
    this.globalClass.setOrderId(this.buyerId);
    this.orderTabService.send_OrderTab('ProductionTab');
  }

  setInitiallyFinalNetAdjustmentAmount(finalAmount) {
    const netAdjustmentData = this.netAdAdjustments;
    if (netAdjustmentData.length <= 0) {
      this.finalNetAmount = finalAmount;
      this.globalClass.setfinalNetAmount(this.finalNetAmount);
    }
  }

  clearGrossAmountSection() {
    try {
      this.grossAdAdjustment.AdAdjustmentName = '';
      this.grossAdAdjustment.SequenceNumber = 0;
      this.grossAdAdjustment.tempAdjustmentAmount = parseFloat('0').toFixed(2);
      this.grossAdAdjustment.tempRunningAmount = parseFloat('0').toFixed(2);
      this.grossAdAdjustment.isPercentage = false;
      this.initSelect2('ddlAdAdjustmentM', 'Select ad adjustment');
    } catch (error) {
      console.log(error);
    }
  }

  clearNetAmountSection() {
    try {
      this.netAdAdjustment.AdAdjustmentName = '';
      this.netAdAdjustment.SequenceNumber = 0;
      this.netAdAdjustment.tempAdjustmentAmount = parseFloat('0').toFixed(2);
      this.netAdAdjustment.tempRunningAmount = parseFloat('0').toFixed(2);
      this.netAdAdjustment.isPercentage = false;
      this.initSelect2('ddlNetAdAdjustmentM', 'Select ad adjustment');
    } catch (error) {
      console.log(error);
    }
  }

  // Manage checkbox on and off
  manageCheckboxOnandOff(objMediaOrderId) {
    try {
      const isTrue = this.selectedIssueMediaOrder[objMediaOrderId];
      if (isTrue === true) {
        this.selectedIssueMediaOrder[objMediaOrderId] = true;
      } else {
        this.selectedIssueMediaOrder[objMediaOrderId] = false;
      }
      let cnt = 0;
      this.selectedIssueMediaOrder.forEach((data, key) => {
        if (data) {
          cnt++;
        } else {
          cnt--;
        }
      });

      if (cnt === this.issueDateWiseAdjustment.length) {
        this.selectedAllIssueMediaOrders = true;
      } else {
        this.selectedAllIssueMediaOrders = false;
      }
    } catch (error) {
      console.log(error);
    }
  }

  showMessageIfFrozen(isFrozen)
  {
    if(isFrozen)
    {
      this.toastr.warning("This media order can't be edited as it has been posted","Warning");
    }
  }

  // checkbox change event
  chechboxChange(objAdAdjustment) {
    try {

      this.manageCheckboxOnandOff(objAdAdjustment.MediaOrderId);
      const qId = objAdAdjustment.IssueDateId;
      const mId = objAdAdjustment.MediaAssetId;
      const mOrderId = objAdAdjustment.MediaOrderId;
      this.mediaOrderId = objAdAdjustment.MediaOrderId;
      const idx = this.selectedCheckbox.indexOf(mOrderId);
      if (idx > -1) {
        this.selectedCheckbox.splice(idx, 1);
        this.selectedIssueMediaOrderData.forEach((data, key) => {
          if (data.MediaOrderId === mOrderId) {
            if (this.selectedIssueMediaOrderData.length === 1) {
              this.tempGrossAmount = parseFloat('0').toFixed(2);
            }
            this.selectedIssueMediaOrderData.splice(key, 1);
            this.selectedMediaAssetsIds.splice(key, 1);
            this.selectedMediaOrderIds.splice(key, 1);
          }
        });

        if (this.selectedCheckbox.length < 1) {
          this.tempGrossAmount = parseFloat('0').toFixed(2);

          this.finalGrossAmount = parseFloat('0').toFixed(2);
          this.tempNetAmount = parseFloat('0').toFixed(2);
          this.finalNetAmount = parseFloat('0').toFixed(2);

          this.initPageControls();
          this.isShowDefaultGrossAdjustmentTable = true;
          this.isShowDefaultNetAdjustmentTable = true;
          this.grossAdAdjustments = [];
          this.netAdAdjustments = [];
        }

        if (this.selectedCheckbox.length === 1) {
          this.getAdAdjustmentByMediaAssetId();

          this.selectedMediaOrderId = this.selectedMediaOrderIds[0];
          const objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?',
            [this.issueDateWiseAdjustment, this.selectedMediaOrderId]);

          const grossAmount = parseFloat(objSelectedAdAdjustments[0].RateCardCost);
          this.tempGrossAmount = parseFloat('0');
          this.tempGrossAmount += grossAmount;
          const initialAmount = (this.tempGrossAmount).toFixed(2);
          this.tempGrossAmount = initialAmount;

          this.finalGrossAmount = initialAmount;
          this.tempNetAmount = initialAmount;

          const objSelectedAdAdjustmentsforNet = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?',
            [this.issueDateWiseAdjustment, this.selectedMediaOrderId]);
          const netAmount = parseFloat(objSelectedAdAdjustmentsforNet[0].NetCost);
          const zeroAmount: number = 0;
          let netAmountinDecimal: any = netAmount + zeroAmount;
          netAmountinDecimal = netAmountinDecimal.toFixed(2);
          this.finalNetAmount = netAmountinDecimal;

          this.globalClass.setfinalNetAmount(this.finalNetAmount);

          this.mediaOrderId = this.selectedMediaOrderIds[0];
          this.getMediaOrderLinesRow('issuelistcheck');
          this.getNetMediaOrderLinesRow('issuelistcheck');
        }
      } else {
        this.selectedCheckbox.push(mOrderId);
        this.selectedMediaAssetsIds.push(mId);
        this.selectedMediaOrderIds.push(mOrderId);
        console.log(objAdAdjustment);
        this.sampleLogoImagePath = objAdAdjustment.MediaAssetLogo;
        this.selectedIssueMediaOrderData.push(objAdAdjustment);

        // for API called now called static
        if (this.selectedCheckbox.length > 1) {
          this.tempGrossAmount = parseFloat('0').toFixed(2);
          this.finalGrossAmount = parseFloat('0').toFixed(2);
          this.tempNetAmount = parseFloat('0').toFixed(2);
          this.finalNetAmount = parseFloat('0').toFixed(2);
          this.initPageControls();
          this.isShowDefaultGrossAdjustmentTable = true;
          this.isShowDefaultNetAdjustmentTable = true;
          this.grossAdAdjustments = [];
          this.netAdAdjustments = [];
        }

        if (this.selectedCheckbox.length === 1) {
          this.getAdAdjustmentByMediaAssetId();
          this.selectedMediaOrderId = this.selectedMediaOrderIds[0];

          const objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?',
            [this.issueDateWiseAdjustment, this.selectedMediaOrderId]);

          const grossAmount = parseFloat(objSelectedAdAdjustments[0].RateCardCost);
          this.tempGrossAmount = parseFloat('0');
          this.tempGrossAmount += grossAmount;
          const initialGrossAmount = (this.tempGrossAmount).toFixed(2);
          this.tempGrossAmount = initialGrossAmount;
          this.finalGrossAmount = initialGrossAmount;
          this.globalClass.setfinalGrossAmount(this.finalGrossAmount);
          this.tempNetAmount = initialGrossAmount;
          this.tempNetAmountInit = this.tempNetAmount;

          const objSelectedAdAdjustmentsforNet = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?',
            [this.issueDateWiseAdjustment, this.selectedMediaOrderId]);
          const netAmount = parseFloat(objSelectedAdAdjustmentsforNet[0].NetCost);
          const zeroAmount = 0;
          let netAmountinDecimal: any = netAmount + zeroAmount;
          netAmountinDecimal = netAmountinDecimal.toFixed(2);
          this.finalNetAmount = netAmountinDecimal;
          this.globalClass.setfinalNetAmount(this.finalNetAmount);
          this.mediaOrderId = this.selectedMediaOrderIds[0];

          this.getMediaOrderLinesRow('issuelistcheck');
          this.getNetMediaOrderLinesRow('issuelistcheck');
        }
      }

      if (this.selectedCheckbox.length > 1) {
        this.isMultiple = true;
        this.setGrossAndNetDivNoClick = true;
      } else if (this.selectedCheckbox.length == 0) {
        this.setGrossAndNetDivNoClick = true;
      } else {
        this.isMultiple = false;
        if (this.orderType == 'Cancel') {
          this.setGrossAndNetDivNoClick = true;
        }
        this.setGrossAndNetDivNoClick = false;
      }

      this.grossAdAdjustmentModalForm.reset();
      this.netAdAdjustmentModalForm.reset();
    } catch (error) {
      console.log(error);
    }
  }

  // Manage check all checkbox of MediaOrder
  toggleAll(val: any) {
    try {
      this.tempGrossAmount = parseFloat('0').toFixed(2);
      this.finalGrossAmount = parseFloat('0').toFixed(2);
      this.finalNetAmount = parseFloat('0').toFixed(2);
      this.tempNetAmount = parseFloat('0').toFixed(2);
      this.initPageControls();
      this.isShowDefaultGrossAdjustmentTable = true;
      this.isShowDefaultNetAdjustmentTable = true;
      this.grossAdAdjustments = [];
      this.netAdAdjustments = [];

      if (val.target.checked === false) {
        this.selectedAllIssueMediaOrders = false;
        this.isMultiple = false;
        this.setGrossAndNetDivNoClick = true;

        // tslint:disable-next-line:no-shadowed-letiable
        const allMediaOrderTableTd = $('#adjustmentTable').find('tr');

        for (let i = 0; i < allMediaOrderTableTd.length; i++) {
          if (i !== 0) {
            const inputElement = allMediaOrderTableTd[i].firstElementChild.firstElementChild;
            const inputElementId = inputElement.id;
            this.selectedIssueMediaOrder[inputElementId] = false;
          }
        }

        this.selectedIssueMediaOrderData = [];
        this.selectedCheckbox = [];
        this.selectedMediaOrderIds = [];
        this.selectedMediaAssetsIds = [];
      } else {
        this.selectedIssueMediaOrderData = [];
        this.selectedCheckbox = [];
        this.selectedMediaOrderIds = [];
        this.selectedMediaAssetsIds = [];
        this.selectedAllIssueMediaOrders = true;
        const allMediaOrderTableTd = $('#adjustmentTable').find('tr');

        for (let i = 0; i < allMediaOrderTableTd.length; i++) {
          let inputElementId = '';
          if (i !== 0) {
            try {
              const inputElement = allMediaOrderTableTd[i].firstElementChild.firstElementChild;
              inputElementId = inputElement.id;
              const objAdAdjustment = alasql('SELECT * FROM ? AS add WHERE MediaOrderId Like ?',
                [this.issueDateWiseAdjustment, inputElementId]);

              this.selectedIssueMediaOrderData.push(objAdAdjustment[0]);
              this.selectedCheckbox.push(objAdAdjustment[0].MediaOrderId);
              this.selectedMediaOrderIds.push(objAdAdjustment[0].MediaOrderId);
              this.selectedMediaAssetsIds.push(objAdAdjustment[0].MediaAssetId);
            } catch (error) {
            }
            this.selectedIssueMediaOrder[inputElementId] = true;
          }
        }

        this.selectedMediaOrderId = this.selectedMediaOrderIds[0];
        if (this.selectedCheckbox.length > 1) {
          this.isMultiple = true;
          this.setGrossAndNetDivNoClick = true;
        }
      }
    } catch (error) {
      console.log(error);
    }

  }

  // deleteIssueOrderAdjustment
  deleteMediaOrderMediaLinesbyMediaOrderId(objAdjustment) {
    try {
      if(objAdjustment.IsFrozen)
      {
        this.toastr.warning("This media order can't be deleted as it has been posted","Warning");
      }
      else{
        if (confirm('Are you sure you want to delete the selected object(s) and all of their children?')) {
          const mediaOrderId = objAdjustment.MediaOrderId;
          if (!isNullOrUndefined(mediaOrderId)) {
            try {
              this.adjustmentService.deleteMediaOrderLinesbyMediaOrder(mediaOrderId, this.reqVerificationToken).subscribe(result1 => {
                if (result1.StatusCode === 1) {
                  if (result1.Data != null) {
  
                    try {
                      const idx = this.selectedCheckbox.indexOf(mediaOrderId);
                      this.selectedCheckbox.splice(idx, 1);
                      this.selectedIssueMediaOrder[mediaOrderId] = false;
                    } catch (error) {
                      console.log(error);
                    }
  
                    const data = result1.Data;
                    const grossCost = data.GrossCost;
                    const netCost = data.NetCost;
                    this.grossCost = (grossCost).toFixed(2);
                    this.netCost = (netCost).toFixed(2);
                    this.globalClass.setGrossCost(this.grossCost);
                    this.globalClass.setNetCost(this.grossCost);
                    this.getNoOfIssueMediaOrder();
                    this.isShowDefaultGrossAdjustmentTable = true;
                    this.isShowDefaultNetAdjustmentTable = true;
                    this.grossAdAdjustments = [];
                    this.netAdAdjustments = [];
                    this.toastr.success('Deleted successfully.', 'Success!');
                  }
                } else {
                  this.toastr.error(result1.Message, 'Error!');
                }
              }, error => {
                console.log(error);
              });
            } catch (error) {
              console.log(error);
            }
          } else {
            console.log('media Order Id is null');
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  }


  /// Edit Issue Order Adjustment
  editMediaOrderMediaLinesbyMediaOrderId(MediaOrderId,isFrozen) {
    if(isFrozen)
    {
      this.toastr.warning("This media order can't be edited as it has been posted","Warning");
    }
    else{
      this.globalClass.setOrderId(this.globalClass.getOrderId());
      this.globalClass.setBillingDetailsMediaOrderId(MediaOrderId);
      this.orderTabService.send_OrderTab('MediaScheduleTab');
    }
  }


  //--------------- Start Change event ----------------
  onAdjustmentChange() {
    try {
      const id = this.grossAdAdjustmentModalForm.controls['AdAdjustmentId'].value;
      const isPopUp = false;
      if (!isNullOrUndefined(id) && id !== 0) {
        if (isPopUp === false) {
        } else {
        }
        const dataContext = this.adAdjustments;
        const objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SrNo  = ?', [dataContext, id]);
        const isAmountPercent = objSelectedAdAdjustments[0].AmountPercent;
        this.grossAdAdjustment = {};
        this.grossAdAdjustmentModalForm.controls['AdAdjustmentValue'].setValue(objSelectedAdAdjustments[0].AdjustmentValue);
        if (isAmountPercent === 0) {
          this.amountType = 'Amount';
          this.isPercentage = false;
          this.grossAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
          this.grossAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
          this.grossAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
        } else if (isAmountPercent === 1) {
          this.amountType = 'Percent';
          this.isPercentage = true;
          this.grossAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
          this.grossAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
          this.grossAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
        }
      } else {
        this.grossAdAdjustmentModalForm.controls['AdAdjustmentValue'].setValue('');
        if (isPopUp === false) {
        } else {
        }

      }
    } catch (error) {
      console.log(error);
    }
  }

  onAdjustmentChangePopUp() {
    try {
      const id = this.multipleGrossAdAdjustmentModalForm.controls['AdAdjustmentId'].value;
      const isPopUp: any = true;
      if (!isNullOrUndefined(id) && id !== 0) {
        if (isPopUp === false) {
        } else {
        }

        const dataContext = this.adAdjustments;
        const objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SrNo  = ?', [dataContext, (id)]);
        const isAmountPercent = objSelectedAdAdjustments[0].AmountPercent;

        this.grossAdAdjustment.AdjustmentValue = objSelectedAdAdjustments[0].AdjustmentValue;
        this.grossAdAdjustmentModalForm.controls['AdAdjustmentValue'].setValue(objSelectedAdAdjustments[0].AdjustmentValue);
        if (isAmountPercent === 0) {
          this.amountType = 'Amount';
          this.isPercentage = false;
          this.grossAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
          this.grossAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
          this.grossAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
        } else if (isAmountPercent === 1) {
          this.amountType = 'Percent';
          this.isPercentage = true;
          this.grossAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
          this.grossAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
          this.grossAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
        }
      } else {
        this.grossAdAdjustmentModalForm.controls['AdAdjustmentValue'].setValue('');
        if (isPopUp === false) {
        } else {
        }

      }
    } catch (error) {
      console.log(error);
    }
  }

  onNetAdjustmentChangePopUp() {
    try {
      const id = this.multipleNetAdAdjustmentModalForm.controls['AdAdjustmentId'].value;
      const isPopUp: any = true;
      if (!isNullOrUndefined(id) && id !== 0) {
        if (isPopUp === false) {
        } else {
        }

        const dataContext = this.adAdjustments;
        const objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SrNo  = ?', [dataContext, id]);
        const isAmountPercent = objSelectedAdAdjustments[0].AmountPercent;
        this.netAdAdjustment.AdjustmentValue = objSelectedAdAdjustments[0].AdjustmentValue;
        if (isAmountPercent === 0) {
          this.netamountType = 'Amount';
          this.netisPercentage = false;
          this.netAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
          this.netAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
          this.netAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
        } else if (isAmountPercent === 1) {
          this.netamountType = 'Percent';
          this.netisPercentage = true;
          this.netAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
          this.netAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
          this.netAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
        }
      } else {
        this.multipleNetAdAdjustmentModalForm.controls['AdAdjustmentValue'].setValue('');
        if (isPopUp === false) {
        } else {
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  onNetAdjustmentChange() {
    try {
      const id = this.netAdAdjustmentModalForm.controls['AdAdjustmentId'].value;
      const isPopUp: any = false;
      if (!isNullOrUndefined(id) && id !== 0) {
        if (isPopUp === false) {
        } else {
        }

        const dataContext = this.adAdjustments;
        const objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SrNo  = ?', [dataContext, (id)]);
        const isAmountPercent = objSelectedAdAdjustments[0].AmountPercent;
        this.netAdAdjustment.AdjustmentValue = objSelectedAdAdjustments[0].AdjustmentValue;
        if (isAmountPercent === 0) {
          this.netamountType = 'Amount';
          this.netisPercentage = false;
          this.netAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
          this.netAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
          this.netAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
        } else if (isAmountPercent === 1) {
          this.netamountType = 'Percent';
          this.netisPercentage = true;
          this.netAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
          this.netAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
          this.netAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
        }
      } else {
        this.netAdAdjustment.AdjustmentValue = '';
        if (isPopUp === false) {
        } else {
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  onNetChechboxChange(objNetAdjustment, val: any) {
    try {
      if (val.target.checked === true) {
        const sNo = objNetAdjustment.SequenceNumber;
        const molNo = objNetAdjustment.MediaOrderLineId;
        this.selectedNetAdjustmentCheckbox.push(sNo);
        this.selectedNetAdjustmentMediaOrderLines.push(molNo);
      } else {
        const sNo = objNetAdjustment.SequenceNumber;
        const molNo = objNetAdjustment.MediaOrderLineId;
        const idx = this.selectedNetAdjustmentCheckbox.indexOf(sNo);
        const idx1 = this.selectedNetAdjustmentMediaOrderLines.indexOf(molNo);
        this.selectedNetAdjustmentCheckbox.splice(idx, 1);
        this.selectedNetAdjustmentMediaOrderLines.splice(idx1, 1);
      }
    } catch (error) {
      console.log(error);
    }
  }

  onGrossChechboxChange(objGrossAdjustment, val: any) {
    try {
      if (val.target.checked === true) {
        const sNo = objGrossAdjustment.SequenceNumber;
        const molNo = objGrossAdjustment.MediaOrderLineId;
        this.selectedGrossAdjustmentCheckbox.push(sNo);
        this.selectedGrossAdjustmentMediaOrderLines.push(molNo);
      } else {
        const sNo = objGrossAdjustment.SequenceNumber;
        const molNo = objGrossAdjustment.MediaOrderLineId;
        const idx = this.selectedGrossAdjustmentCheckbox.indexOf(sNo);
        const idx1 = this.selectedGrossAdjustmentMediaOrderLines.indexOf(molNo);
        this.selectedGrossAdjustmentCheckbox.splice(idx, 1);
        this.selectedGrossAdjustmentMediaOrderLines.splice(idx1, 1);
      }
    } catch (error) {
      console.log(error);
    }
  }
  //---------------- End Change event -----------------



  // Gross Move and  Delete event
  createSequenceOrderWiseGrossTable() {
    try {
      const objAllDeletedAdAdjustment = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [this.grossAdAdjustments, 0]);

      if (objAllDeletedAdAdjustment.length === this.grossAdAdjustments.length) {
        this.grossAdAdjustments = [];
        this.isShowDefaultGrossAdjustmentTable = true;

        // tslint:disable-next-line:max-line-length
        const objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [this.loadTimeIssueDateWiseAdjustment, this.selectedMediaOrderId]);
        const grossAmount = parseFloat(objSelectedAdAdjustments[0].GrossCost);

        this.tempGrossAmount = parseFloat('0');
        this.tempGrossAmount += grossAmount;
        const initialGrossAmount = (this.tempGrossAmount).toFixed(2);
        this.tempGrossAmount = initialGrossAmount;
        this.tempNetAmount = initialGrossAmount;

        if (isNaN(initialGrossAmount)) {
          this.finalGrossAmount = this.globalClass.getGrossCost();
          this.finalNetAmount = this.globalClass.getNetCost();
        } else {
          this.finalGrossAmount = initialGrossAmount;
          this.finalNetAmount = initialGrossAmount;
        }


        this.globalClass.setfinalNetAmount(this.finalNetAmount);

        const netAdjustmentData = this.netAdAdjustments;

        netAdjustmentData.forEach((data, key) => {
          const objAdAdjustmentName = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [netAdjustmentData, (key + 1)]);
          if (key === 0) {
            this.isNetChangedByMove = true;
          } else {
            this.isNetChangedByMove = false;
          }

          if (objAdAdjustmentName.length > 0) {
            this.getNetMediaOrderLinesRow('issuelistcheck');
          }

          if (netAdjustmentData.length === 1) {
            this.isNetChangedByMove = false;
          }

          this.selectedNetAdjustmentCheckbox = [];
          this.selectedNetAdjustmentMediaOrderLines = [];
        });

        this.selectedGrossAdjustmentCheckbox = [];
        this.selectedGrossAdjustmentMediaOrderLines = [];
      } else {

        const grossAdjustmentData = this.grossAdAdjustments;

        grossAdjustmentData.forEach((data, key) => {
          const objAdAdjustmentName = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [grossAdjustmentData, (key + 1)]);
          if (key === 0) {
            this.isGrossChangedByMoveUp = true;
          } else {
            this.isGrossChangedByMoveUp = false;
          }
          if (objAdAdjustmentName.length > 0) {
            this.getMediaOrderLinesRow('issuelistcheck');
          }

          if (grossAdjustmentData.length === 1) {
            this.isGrossChangedByMoveUp = false;
            this.isShowDefaultGrossAdjustmentTable = true;
            this.finalGrossAmount = parseFloat('0').toFixed(2);
            this.finalNetAmount = parseFloat('0').toFixed(2);
            this.tempNetAmount = parseFloat('0').toFixed(2);
            this.grossAdAdjustments = [];
          }
          this.selectedGrossAdjustmentCheckbox = [];
          this.selectedGrossAdjustmentMediaOrderLines = [];
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  deleteSelectedRecord() {
    try {
      if (this.selectedGrossAdjustmentCheckbox.length > 0) {
        if (confirm('Are you sure you want to delete the selected object(s) and all of their children?')) {
          if (this.selectedGrossAdjustmentMediaOrderLines.length > 0) {

            // multiple
            const multipleOrderLinesIds = [];
            const extraOrderlinesToRemove = [];
            this.selectedGrossAdjustmentMediaOrderLines.forEach((data, key) => {
              multipleOrderLinesIds.push(data);
            });

            this.obj = {};
            this.obj.MediaOrderLineIds = multipleOrderLinesIds;

            // single
            try {
              this.showLoader();
              this.adjustmentService.deleteMediaOrderLinesbyMediaOrderLineId(this.obj, this.reqVerificationToken).subscribe(result => {
                if (result.StatusCode === 1) {
                  this.hideLoader();
                  if (result.Data != null) {
                    const data = result.Data;
                    const grossCost = data.GrossCost;
                    const netCost = data.NetCost;

                    this.globalClass.setGrossCost((grossCost).toFixed(2));
                    this.globalClass.setNetCost((netCost).toFixed(2));
                    this.grossCost = this.globalClass.getGrossCost();
                    this.netCost = this.globalClass.getNetCost();

                    this.obj.MediaOrderLineIds.forEach((element, key) => {
                      const sNo = this.grossAdAdjustments.indexOf(key);
                      this.selectedGrossAdjustmentCheckbox.splice(sNo, 1);
                    });

                    this.getNoOfIssueMediaOrder();

                    /* remove row from grossAdAdjustments array */
                    this.getMediaOrderLinesRow('0');
                    /* remove row from grossAdAdjustments array */

                    this.toastr.success('Deleted successfully.', 'Success!');
                  }
                } else {
                  this.showLoader();
                  this.toastr.error(result.Message, 'Error!');
                }
              }, error => {
                this.hideLoader();
                console.log(error);
              });
            } catch (error) {
              this.hideLoader();
              console.log(error);
            }
          }
        }
      } else {
        this.toastr.info('Please select one ad adjustment', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  deleteSelectedNetRecord() {
    try {
      if (this.selectedNetAdjustmentCheckbox.length > 0) {
        if (confirm('Are you sure you want to delete the selected object(s) and all of their children?')) {
          if (this.selectedNetAdjustmentMediaOrderLines.length > 0) {
            // multiple
            const multipleOrderLinesIds = [];
            const extraOrderlinesToRemove = [];

            this.selectedNetAdjustmentMediaOrderLines.forEach((data, key) => {
              multipleOrderLinesIds.push(data);
            });

            this.obj = {};
            this.obj.MediaOrderLineIds = multipleOrderLinesIds;
            try {
              this.showLoader();
              this.adjustmentService.deleteMediaOrderLinesbyMediaOrderLineId(this.obj, this.reqVerificationToken).subscribe(result1 => {
                if (result1.StatusCode === 1) {

                  this.hideLoader();
                  if (result1.Data != null) {
                    const data = result1.Data;
                    const grossCost = data.GrossCost;
                    const netCost = data.NetCost;

                    this.globalClass.setGrossCost(grossCost);
                    this.globalClass.setNetCost(netCost);
                    this.grossCost = (grossCost).toFixed(2);
                    this.netCost = (netCost).toFixed(2);

                    this.obj.MediaOrderLineIds.forEach((element, key) => {
                      const sNo = this.netAdAdjustments.indexOf(key);
                      this.selectedNetAdjustmentCheckbox.splice(sNo, 1);
                    });

                    this.getNoOfIssueMediaOrder();

                    /* remove row from grossAdAdjustments array */
                    this.getNetMediaOrderLinesRow('0');
                    /* remove row from grossAdAdjustments array */

                    this.toastr.success('Deleted successfully.', 'Success!');
                  }
                } else {
                  this.hideLoader();
                  this.toastr.error(result1.Message, 'Error!');
                }
              }, error => {
                this.hideLoader();
                console.log(error);
              });
            } catch (error) {
              this.hideLoader();
              console.log(error);
            }
          }
        }
      } else {
        this.toastr.info('Please select ad adjustment', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  moveRecordOneRowAbove() {
    try {
      if (this.selectedGrossAdjustmentCheckbox.length === 1) {
        // tslint:disable-next-line:radix
        const currentSequenceNo = parseInt(this.selectedGrossAdjustmentCheckbox[0]);
        if (this.grossAdAdjustments[0].SequenceNumber !== currentSequenceNo) {

          // tslint:disable-next-line:max-line-length
          let objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [this.grossAdAdjustments, (currentSequenceNo)]);
          if (objCurrentAdAdjustments.length > 0) {
            objCurrentAdAdjustments = objCurrentAdAdjustments[0];
            this.obj = {};
            this.obj.MediaOrderLineId = objCurrentAdAdjustments.MediaOrderLineId;
            this.obj.MediaOrderId = objCurrentAdAdjustments.MediaOrderId;
            this.obj.MovedUp = true;
            try {

              // tslint:disable-next-line:no-shadowed-constiable
              this.adjustmentService.moveUpOrDownMediaOrderLines(this.obj, this.reqVerificationToken).subscribe(result => {
                if (result.StatusCode === 1) {
                  if (result.Data != null) {
                    const data = result.Data;
                    const grossCost = data.GrossCost;
                    const netCost = data.NetCost;
                    this.globalClass.setGrossCost((grossCost).toFixed(2));
                    this.globalClass.setNetCost((netCost).toFixed(2));
                    this.grossCost = this.globalClass.getGrossCost();
                    this.netCost = this.globalClass.getNetCost();
                    this.getNoOfIssueMediaOrder();
                    this.moveOneStepAboveforGross();
                  }
                } else {
                  this.toastr.error(result.Message, 'Error!');
                }
              }, error => {
                console.log(error);
              });
            } catch (error) {
              console.log(error);
            }
          }
        }
      } else {
        this.toastr.info('Please select one ad adjustment', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  moveRecordOneRowBelow() {
    try {
      if (this.selectedGrossAdjustmentCheckbox.length === 1) {
        // tslint:disable-next-line:radix
        const currentSequenceNo = parseInt(this.selectedGrossAdjustmentCheckbox[0]);
        const grossAdAdjustmentsLength = this.grossAdAdjustments.length - 1;
        if (this.grossAdAdjustments[grossAdAdjustmentsLength].SequenceNumber !== currentSequenceNo) {
          let objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
            [this.grossAdAdjustments, currentSequenceNo]);

          if (objCurrentAdAdjustments.length > 0) {
            objCurrentAdAdjustments = objCurrentAdAdjustments[0];
            this.obj = {};
            this.obj.MediaOrderLineId = objCurrentAdAdjustments.MediaOrderLineId;
            this.obj.MediaOrderId = objCurrentAdAdjustments.MediaOrderId;
            this.obj.MovedUp = false;

            try {

              // tslint:disable-next-line:no-shadowed-constiable
              this.adjustmentService.moveUpOrDownMediaOrderLines(this.obj, this.reqVerificationToken).subscribe(result => {
                if (result.StatusCode === 1) {
                  if (result.Data != null) {
                    const data = result.Data;
                    const grossCost = data.GrossCost;
                    const netCost = data.NetCost;
                    this.globalClass.setGrossCost((grossCost).toFixed(2));
                    this.globalClass.setNetCost((netCost).toFixed(2));
                    this.grossCost = this.globalClass.getGrossCost();
                    this.netCost = this.globalClass.getNetCost();
                    this.getNoOfIssueMediaOrder();
                    this.moveOneStepBelowforGross();
                  }
                } else {
                  this.toastr.error(result.Message, 'Error!');
                }
              }, error => {
                console.log(error);
              });
            } catch (error) {
              console.log(error);
            }
          }
        }
      } else {
        this.toastr.info('Please select one ad adjustment', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  moveOneNetRecordAbove() {
    try {
      if (this.selectedNetAdjustmentCheckbox.length === 1) {
        // tslint:disable-next-line:radix
        const currentSequenceNo = parseInt(this.selectedNetAdjustmentCheckbox[0]);
        if (this.netAdAdjustments[0].SequenceNumber !== currentSequenceNo) {
          let objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
            [this.netAdAdjustments, (currentSequenceNo)]);

          if (objCurrentAdAdjustments.length > 0) {
            objCurrentAdAdjustments = objCurrentAdAdjustments[0];
            this.obj = {};
            this.obj.MediaOrderLineId = objCurrentAdAdjustments.MediaOrderLineId;
            this.obj.MediaOrderId = objCurrentAdAdjustments.MediaOrderId;
            this.obj.MovedUp = true;

            try {
              this.adjustmentService.moveUpOrDownMediaOrderLines(this.obj, this.reqVerificationToken).subscribe(result => {
                if (result.StatusCode === 1) {
                  if (result.Data != null) {
                    const data = result.Data;
                    const grossCost = data.GrossCost;
                    const netCost = data.NetCost;
                    this.globalClass.setGrossCost((grossCost).toFixed(2));
                    this.globalClass.setNetCost((netCost).toFixed(2));
                    this.grossCost = this.globalClass.getGrossCost();
                    this.netCost = this.globalClass.getNetCost();
                    this.getNoOfIssueMediaOrder();
                    this.moveOneStepAboveforNet();
                  }
                } else {
                  this.toastr.error(result.Message, 'Error!');
                }
              }, error => {
                console.log(error);
              });
            } catch (error) {
              console.log(error);
            }
          }
        }
      } else {
        this.toastr.info('Please select one ad adjustment', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  moveOneNetRecordBelow() {
    try {
      if (this.selectedNetAdjustmentCheckbox.length === 1) {
        // tslint:disable-next-line:radix
        const currentSequenceNo = parseInt(this.selectedNetAdjustmentCheckbox[0]);
        const netAdAdjustmentsLength = this.netAdAdjustments.length - 1;
        if (this.netAdAdjustments[netAdAdjustmentsLength].SequenceNumber !== currentSequenceNo) {
          let objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
            [this.netAdAdjustments, (currentSequenceNo)]);

          if (objCurrentAdAdjustments.length > 0) {
            objCurrentAdAdjustments = objCurrentAdAdjustments[0];
            this.obj = {};
            this.obj.MediaOrderLineId = objCurrentAdAdjustments.MediaOrderLineId;
            this.obj.MediaOrderId = objCurrentAdAdjustments.MediaOrderId;
            this.obj.MovedUp = false;

            try {
              this.adjustmentService.moveUpOrDownMediaOrderLines(this.obj, this.reqVerificationToken).subscribe(result => {
                if (result.StatusCode === 1) {
                  if (result.Data != null) {
                    const data = result.Data;
                    const grossCost = data.GrossCost;
                    const netCost = data.NetCost;
                    this.globalClass.setGrossCost((grossCost).toFixed(2));
                    this.globalClass.setNetCost((netCost).toFixed(2));
                    this.grossCost = this.globalClass.getGrossCost();
                    this.netCost = this.globalClass.getNetCost();
                    this.getNoOfIssueMediaOrder();
                    this.moveOneStepBelowforNet();
                  }
                } else {
                  this.toastr.error(result.Message, 'Error!');
                }
              }, error => {
                console.log(error);
              });
            } catch (error) {
              console.log(error);
            }
          }
        }
      } else {
        this.toastr.info('Please select one ad adjustment', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  moveOneStepAboveforGross() {
    try {
      const currentSequenceNo = (this.selectedGrossAdjustmentCheckbox[0]);
      if (this.grossAdAdjustments[0].SequenceNumber !== currentSequenceNo) {
        const previousSequenceNo = (currentSequenceNo - 1);

        const objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
          [this.grossAdAdjustments, (currentSequenceNo)]);
        const objPreviousAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
          [this.grossAdAdjustments, (previousSequenceNo)]);

        this.grossAdAdjustments.forEach((data, key) => {
          let onceUpdateed = false;
          if (data.SequenceNumber === currentSequenceNo) {
            this.grossAdAdjustments[key].SequenceNumber = previousSequenceNo;
            onceUpdateed = true;
          }
          if (onceUpdateed !== true) {
            if (data.SequenceNumber === previousSequenceNo) {
              this.grossAdAdjustments[key].SequenceNumber = currentSequenceNo;
            }
          }
        });

        this.createSequenceOrderWiseGrossTable();
      } else {
        this.toastr.info('You can not move this records one step above', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  moveOneStepBelowforGross() {
    try {
      const currentSequenceNo = (this.selectedGrossAdjustmentCheckbox[0]);
      const grossAdAdjustmentsLength = this.grossAdAdjustments.length - 1;
      if (this.grossAdAdjustments[grossAdAdjustmentsLength].SequenceNumber !== currentSequenceNo) {
        const nextSequenceNo = (currentSequenceNo + 1);

        const objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
          [this.grossAdAdjustments, (currentSequenceNo)]);
        const objPreviousAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
          [this.grossAdAdjustments, (nextSequenceNo)]);

        this.grossAdAdjustments.forEach((data, key) => {
          let onceUpdateed = false;
          if (data.SequenceNumber === currentSequenceNo) {
            this.grossAdAdjustments[key].SequenceNumber = nextSequenceNo;
            onceUpdateed = true;
          }
          if (onceUpdateed !== true) {
            if (data.SequenceNumber === nextSequenceNo) {
              this.grossAdAdjustments[key].SequenceNumber = currentSequenceNo;
            }
          }
        });

        this.createSequenceOrderWiseGrossTable();

      } else {
        this.toastr.info('You can not move this records one step below', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  // Net Move and Gross event
  createSequenceOrderWiseNetTable() {
    try {
      const objAllDeletedAdAdjustment = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
        [this.netAdAdjustments, (0)]);
      if (objAllDeletedAdAdjustment.length === this.netAdAdjustments.length) {
        this.netAdAdjustments = [];
        this.isShowDefaultNetAdjustmentTable = true;
        this.selectedMediaOrderId = this.selectedMediaOrderIds[0];
        const grossAdjustmentData = this.grossAdAdjustments;
        if (grossAdjustmentData.length === 0) {
          const objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?',
            [this.issueDateWiseAdjustment, (this.selectedMediaOrderId)]);
          const grossAmount = parseFloat(objSelectedAdAdjustments[0].GrossCost);
          this.tempGrossAmount = parseFloat('0');
          this.tempGrossAmount += grossAmount;
          const initialAmount = (this.tempGrossAmount).toFixed(2);
          this.tempGrossAmount = initialAmount;
          this.finalGrossAmount = initialAmount;
          this.tempNetAmount = initialAmount;
        }

        this.tempNetAmount = this.finalGrossAmount;
        this.finalNetAmount = this.tempNetAmount;

        this.globalClass.setfinalNetAmount(this.finalNetAmount);

        this.selectedNetAdjustmentCheckbox = [];
        this.selectedNetAdjustmentMediaOrderLines = [];
      } else {
        const netAdjustmentData = this.netAdAdjustments;

        netAdjustmentData.forEach((data, key) => {
          const objAdAdjustmentName = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
            [netAdjustmentData, (key + 1)]);
          if (key === 0) {
            this.isNetChangedByMove = true;
          } else {
            this.isNetChangedByMove = false;
          }
          if (objAdAdjustmentName.length > 0) {
            this.getNetMediaOrderLinesRow('issuelistcheck');
          }
          if (netAdjustmentData.length === 1) {
            this.isNetChangedByMove = false;
            this.finalNetAmount = parseFloat('0').toFixed(2);
            this.isShowDefaultNetAdjustmentTable = true;
            this.netAdAdjustments = [];
          }
          this.selectedNetAdjustmentCheckbox = [];
          this.selectedNetAdjustmentMediaOrderLines = [];
        });

      }
    } catch (error) {
      console.log(error);
    }
  }

  deleteMediaOrderLinesForNet() {
    try {
      let objCurrentAdAdjustments = [];
      let sequenceMinus = 0;

      this.netAdAdjustments.forEach((data, key) => {
        let isExits = false;
        const currentSequenceNo = key + 1;
        objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
          [this.netAdAdjustments, (currentSequenceNo)]);
        if (objCurrentAdAdjustments.length > 0) {
          const sNo = (objCurrentAdAdjustments[0].SequenceNumber);

          this.selectedNetAdjustmentCheckbox.forEach((data1, key1) => {
            const cNo = (data1);
            if (cNo === sNo) {
              sequenceMinus++;
              isExits = true;
            }
          });

          if (isExits === false) {
            this.netAdAdjustments.forEach((data1, key1) => {
              const currentNo = currentSequenceNo;
              const LessNo = (currentNo - sequenceMinus);
              if (data1.SequenceNumber === currentNo) {
                this.netAdAdjustments[key1].SequenceNumber = LessNo;
              }
            });
          } else {
            this.netAdAdjustments.forEach((data1, key1) => {
              const currentNo = currentSequenceNo;
              if (sequenceMinus > 0) {
                if (data1.SequenceNumber === currentNo) {
                  this.netAdAdjustments[key1].SequenceNumber = 0;
                }
              }
            });
          }
        }
      });
      this.createSequenceOrderWiseNetTable();
    } catch (error) {
      console.log(error);
    }
  }

  moveOneStepAboveforNet() {
    try {
      const currentSequenceNo = (this.selectedNetAdjustmentCheckbox[0]);
      if (this.netAdAdjustments[0].SequenceNumber !== currentSequenceNo) {

        const previousSequenceNo = (currentSequenceNo - 1);
        const objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
          [this.netAdAdjustments, (currentSequenceNo)]);
        const objPreviousAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
          [this.netAdAdjustments, (previousSequenceNo)]);

        this.netAdAdjustments.forEach((data, key) => {
          let onceUpdateed = false;
          if (data.SequenceNumber === currentSequenceNo) {
            this.netAdAdjustments[key].SequenceNumber = previousSequenceNo;
            onceUpdateed = true;
          }
          if (onceUpdateed !== true) {
            if (data.SequenceNumber === previousSequenceNo) {
              this.netAdAdjustments[key].SequenceNumber = currentSequenceNo;
            }
          }
        });
        this.createSequenceOrderWiseNetTable();

      } else {
        this.toastr.info('You can not move this records one step above', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  moveOneStepBelowforNet() {
    try {
      const currentSequenceNo = (this.selectedNetAdjustmentCheckbox[0]);
      const netAdAdjustmentsLength = this.netAdAdjustments.length - 1;
      if (this.netAdAdjustments[netAdAdjustmentsLength].SequenceNumber !== currentSequenceNo) {
        const nextSequenceNo = (currentSequenceNo + 1);

        const objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
          [this.netAdAdjustments, (currentSequenceNo)]);
        const objPreviousAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?',
          [this.netAdAdjustments, (nextSequenceNo)]);

        this.netAdAdjustments.forEach((data, key) => {
          let onceUpdateed = false;
          if (data.SequenceNumber === currentSequenceNo) {
            this.netAdAdjustments[key].SequenceNumber = nextSequenceNo;
            onceUpdateed = true;
          }
          if (onceUpdateed !== true) {
            if (data.SequenceNumber === nextSequenceNo) {
              this.netAdAdjustments[key].SequenceNumber = currentSequenceNo;
            }
          }
        });

        this.createSequenceOrderWiseNetTable();

      } else {
        this.toastr.info('You can not move this records one step below', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  // init Pop up
  initAdAdjustmentPopUpforMultiple() {
    try {
      setTimeout(() => {
        this.multipleNetAdAdjustmentModalForm.reset();
        this.multipleGrossAdAdjustmentModalForm.reset();
        this.clearGrossAmountSection();
        this.clearNetAmountSection();
      }, 1000);
      this.grossAdAdjustments = [];
      this.netAdAdjustments = [];
      this.getAdAdjustmentByMediaAssetId();
      this.isMultiple = true;
      this.isShowDefaultGrossAdjustmentTable = true;
      this.isShowDefaultNetAdjustmentTable = true;
      this.initSelect2('ddlAdAdjustmentM', 'Select ad adjustment');
      this.initSelect2('ddlNetAdAdjustmentM', 'Select ad adjustment');

      this.multipleGrossAdAdjustmentModalForm.reset();
      this.multipleGrossAdjustmentshowMsgs = false;

      this.multipleNetAdAdjustmentModalForm.reset();
      this.multipleNetAdjustmentshowMsgs = false;

      this.initPageControls();

      const totalSum = alasql('SELECT sum(GrossCost) FROM ? AS add ', [this.selectedIssueMediaOrderData]);
      const grossAmount = parseFloat(totalSum[0]['SUM(GrossCost)']);
      this.tempGrossAmount = parseFloat('0');
      this.tempGrossAmount += grossAmount;
      const initialAmount = (this.tempGrossAmount).toFixed(2);
      this.tempGrossAmount = initialAmount;
      this.finalGrossAmount = initialAmount;
      this.tempNetAmount = initialAmount;


      const objSelectedAdAdjustmentsforNet = alasql('SELECT sum(NetCost) FROM ? AS add ', [this.selectedIssueMediaOrderData]);
      const netAmount = parseFloat(objSelectedAdAdjustmentsforNet[0]['SUM(NetCost)']);
      const netAmountinDecimal = (netAmount).toFixed(2);
      this.finalNetAmount = netAmountinDecimal;


      this.isMultipleIssueDate = true;
    } catch (error) {
      console.log(error);
    }
  }

  cancelMultipleAdAdjustment() {
    try {
      this.isMultiple = true;
      this.tempGrossAmount = (0).toFixed(2);
      this.finalGrossAmount = parseFloat('0').toFixed(2);
      this.finalNetAmount = parseFloat('0').toFixed(2);
      this.tempNetAmount = parseFloat('0').toFixed(2);
      this.tempMediaOrderLinesforGrossMultiple = [];
      this.multipleGrossAdjustmentshowMsgs = false;
      this.multipleNetAdjustmentshowMsgs = false;
      this.isShowDefaultGrossAdjustmentTable = true;
      this.isShowDefaultNetAdjustmentTable = true;
      this.multipleGrossAdAdjustmentModalForm.reset();
      this.multipleNetAdAdjustmentModalForm.reset();
      this.grossAdAdjustments = [];
      this.netAdAdjustments = [];
    } catch (error) {
      console.log(error);
    }
  }
  changeStatus(status) {
    if (status == 'Cancel') {
      this.setGrossAndNetDivNoClick = true;

    }
    if (status == 'Run') {
      this.setGrossAndNetDivNoClick = true;
    }
    else {
      this.setGrossAndNetDivNoClick = false;
    }
    this.likelihood = 0;
    $("#ddlLikelihood").val(this.likelihood);
  }
  updateOrderType(objOrderType) {
    try {
      const issueDateWiseAdjustment = this.issueDateWiseAdjustment.length;
      if (issueDateWiseAdjustment > 0) {
        if (this.selectedCheckbox.length > 0) {
          const ids = this.selectedMediaOrderIds;
          if (ids.length > 0) {
            this.obj = {};
            this.obj.MediaOrderIds = this.selectedMediaOrderIds;
            this.obj.OrderStatus = objOrderType;
            if (objOrderType == 'Cancel') {
              this.setGrossAndNetDivNoClick = true;
              this.obj.Likelihood=0;
            }
            else if (objOrderType == 'Run') {
              this.setGrossAndNetDivNoClick = true;
              this.obj.Likelihood=100;
            }
            else {
              this.obj.Likelihood = this.likelihood;
            }

            this.showLoader();
            this.adjustmentService.updateMediaOrderStatus(this.obj, this.reqVerificationToken).subscribe((result) => {
              if (result.StatusCode === 1) {
                this.hideLoader();
                this.getNoOfIssueMediaOrder();
                this.likelihood = 0;
                this.toastr.success('Save successfully.', 'Success!');
              } else {
                this.hideLoader();
                this.toastr.error(result.Message, 'Error!');
              }
            }, error => {
              this.hideLoader();
              console.log(error);
            });
          }
        } else {
          this.toastr.info('Please select at least one media order', 'Information!');
        }
      }
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  initOrdersDetailPopup() {
    try {
      this.ordersDetail = {};
      this.ordersDetailModalForm.reset();
      let responseData = this.personData;
      const contacts = alasql('SELECT * FROM ? AS add WHERE Id  = ?',
        [responseData, this.commonOrderDetail.ST_ID.toString()]);
      this.commonOrderDetail.ContactName = contacts[0].PersonName.FullName;
      let partyAddress = contacts[0].Addresses;
      if (partyAddress !== undefined) {
        partyAddress = contacts[0].Addresses.$values[0];
        if (partyAddress !== undefined) {
          const addressLines = contacts[0].Addresses.$values[0].Address.AddressLines;
          if (addressLines !== undefined && addressLines != null && addressLines !== '') {
            this.isAddress = true;
            const streetArray = addressLines.$values;
            let street = '';
            this.addStreetAdjHTML = '';
            this.addStreetProposalAdjHTML = '';
            let street1: any = '';
            streetArray.forEach((itemdatavalue, key) => {
              if (key === 0) {
                street += '<span> ' + itemdatavalue + '</span><br />';
              } else {
                street1 += '<span> ' + itemdatavalue + '</span><br />';
              }
            });

            this.addStreetAdjHTML = street;
            this.addStreetProposalAdjHTML = street;

            if (street1) {
              this.addStreetAdjHTML1 = street1;
              this.addStreetProposalAdjHTML1 = street1;
            }



            this.commonOrderDetail.City = contacts[0].Addresses.$values[0].Address.CityName;
            this.commonOrderDetail.State = contacts[0].Addresses.$values[0].Address.CountrySubEntityCode;
            this.commonOrderDetail.PostalCode = contacts[0].Addresses.$values[0].Address.PostalCode;
            this.commonOrderDetail.Country = contacts[0].Addresses.$values[0].Address.CountryName;
          } else {
            this.isAddress = false;
          }
        }
      }

      responseData = this.advertisers;
      this.advertisersNew = alasql('SELECT Name FROM ? AS add WHERE Id  = ?',
        [responseData, this.commonOrderDetail.AdvertiserId.toString()]);
      this.commonOrderDetail.AdvertiserName = this.advertisersNew[0].Name;

      responseData = this.advertisers;
      if (this.commonOrderDetail.AgencyId > 0 && !isNullOrUndefined(this.commonOrderDetail.AgencyId)) {
        const Agencies = alasql('SELECT Name FROM ? AS add WHERE Id  = ?',
          [responseData, this.commonOrderDetail.AgencyId.toString()]);
        this.commonOrderDetail.AgencyName = Agencies[0].Name;
      }

      if (this.commonOrderDetail.BillToInd === false) {
        this.isAgency = false;
        this.commonOrderDetail.BillTo = 'Advertiser';
      } else {
        this.isAgency = true;
        this.commonOrderDetail.BillTo = 'Agency';
      }

      this.commonOrderDetail.CreatedDate = this.globalClass.getDateInFormat(this.commonOrderDetail.CreatedDate);
    } catch (error) {
      console.log(error);
    }
  }

  //Get Data for popup to display insertion order and proposal
  getAdvertiserThroughASI(event: any) {
    try {
      if (this.selectedCheckbox.length > 0) {
        if (event.target.id === 'btnIO') {
          $('#ordersDetailModal').modal('show');
        } else if (event.target.id === 'btnOP') {
          $('#orderProposalModal').modal('show');
        }
        this.selectedIssueMediaOrderDataDownload = [];
        this.selectedIssueMediaOrderData = [];

        this.selectedCheckbox.forEach((MediaOrderId, keych) => {
          const objAdAdjustment = alasql('SELECT * FROM ? AS add WHERE MediaOrderId = ?', [this.issueDateWiseAdjustment, MediaOrderId]);

          this.adjustmentService.getMediaOrderLinesbyOrderId(MediaOrderId, this.reqVerificationToken).subscribe(result => {
            if (result.StatusCode === 1) {
              if (result.Data != null) {
                if (result.Data.length > 0) {
                  const grossAdjustments = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ? ORDER BY SequenceNumber ASC',
                    [result.Data, true]);
                  const netAdjustments = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ? ORDER BY SequenceNumber ASC',
                    [result.Data, false]);

                  netAdjustments.forEach((adjustment, key) => {
                    grossAdjustments.push(adjustment);
                  });

                  this.mediaIssueDateWiseAdjustment = grossAdjustments;

                  this.mediaIssueDateWiseAdjustment.forEach((value, key) => {
                    if (value.AdAdjustmentName != null) {
                      if (key === 0) {
                        this.runningCost = objAdAdjustment[0].RateCardCost;
                      }
                      value.tempAdjustmentAmount = (value.AmountPercent ? (this.runningCost * value.AdjustmentValue) / 100 :
                        value.AdjustmentValue);
                      if (value.SurchargeDiscount === 0) {
                        value.runningCost = this.runningCost + value.tempAdjustmentAmount;
                      } else {
                        value.runningCost = this.runningCost - value.tempAdjustmentAmount;
                      }

                      this.runningCost = value.runningCost;
                    }
                  });
                  objAdAdjustment[0].mediaIssueDateWiseAdjustments = this.mediaIssueDateWiseAdjustment;
                }
              } else {
                objAdAdjustment[0].mediaIssueDateWiseAdjustments = [];
              }
            }
            this.selectedIssueMediaOrderDataDownload[keych] = (objAdAdjustment[0]);
            this.selectedIssueMediaOrderData[keych] = (objAdAdjustment[0]);

            const totalAdCost = alasql('SELECT sum(RateCardCost) FROM ? AS add ', [this.selectedIssueMediaOrderData]);
            this.finalTotalAdCost = parseFloat(totalAdCost[0]['SUM(RateCardCost)']).toFixed(2);
            const totalSum = alasql('SELECT sum(GrossCost) FROM ? AS add ', [this.selectedIssueMediaOrderData]);
            this.finalTotalGrossAmount = parseFloat(totalSum[0]['SUM(GrossCost)']).toFixed(2);
            const sumNetCost = alasql('SELECT sum(NetCost) FROM ? AS add ', [this.selectedIssueMediaOrderData]);
            const netAmount = parseFloat(sumNetCost[0]['SUM(NetCost)']);
            this.finalTotalNetAmount = (netAmount).toFixed(2);

          }, error => {
            console.log(error);
          });
        });

        let offset = 0;

        if (this.globalClass.getMainPartyAndOrganizationData().length <= 0
          && this.globalClass.getMainAdvertisersData().length <= 0
          && this.globalClass.getMainAgenciesData().length <= 0
          && this.globalClass.getMainBillingToContactsData().length <= 0) {
          const HasNext = true;
          offset = 0;
          const advertisers = [];
          const contacts = [];
          //party API Call
          this.getAdvertiserData(HasNext, offset, advertisers, contacts);

        } else {
          this.partyAndOrganizationData = this.globalClass.getMainPartyAndOrganizationData();
          this.advertisers = this.globalClass.getMainAdvertisersData();
          this.agencies = this.globalClass.getMainAgenciesData();
          this.personData = this.globalClass.getMainBillingToContactsData();

          //party API Call
          this.adjustmentService.getAllAdvertiser1(this.websiteRoot, this.baseUrl,
            this.reqVerificationToken, offset).subscribe(result => {
              if (!isNullOrUndefined(result)) {
                this.isEditMode = true;
                this.initOrdersDetailPopup();
                this.changeCalledManually();
                this.isEditMode = false;
              }
            }, error => {
              console.log(error);
            });
        }
        // this.generateDynamicImageURL();
      } else {
        if (event.target.id === 'IO') {
          $('#ordersDetailModal').modal('hide');
        } else if (event.target.id === 'Pro') {
          $('#orderProposalModal').modal('hide');
        }
        this.toastr.info('Please select one issue media order', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  // called inside getAdvertiserThroughASI method
  getAdvertiserData(HasNext: boolean, offset, advertisers: any[], contacts: any[]) {
    this.adjustmentService.getAllAdvertiser1(this.websiteRoot, this.baseUrl, this.reqVerificationToken, offset).subscribe(result => {
      if (!isNullOrUndefined(result)) {
        const ItemData = result.Items.$values;
        if (ItemData.length > 0) {

          ItemData.forEach((itemdatavalue, key) => {
            this.partyAndOrganizationData.push(itemdatavalue);
            const type = itemdatavalue.$type;
            if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
              advertisers.push(itemdatavalue);
            } else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
              contacts.push(itemdatavalue);
            }
          });

          this.advertisers = advertisers;
          this.agencies = advertisers;
          this.personData = contacts;
          const totalCount = result.TotalCount;
          const count = result.Count;
          HasNext = result.HasNext;
          offset = result.Offset;
          const limit = result.Limit;
          const nextOffset = offset + 500;
          if (count === 500) {
            offset = nextOffset;
            this.getAdvertiserData(HasNext, offset, advertisers, contacts);
          } else {
            this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
            this.globalClass.setMainAdvertisersData(this.advertisers);
            this.globalClass.setMainAgenciesData(this.agencies);
            this.globalClass.setMainBillingToContactsData(this.personData);

            this.initOrdersDetailPopup();
            this.changeCalledManually();
          }
        } else {
          this.globalClass.setMainPartyAndOrganizationData(this.partyAndOrganizationData);
          this.globalClass.setMainAdvertisersData(this.advertisers);
          this.globalClass.setMainAgenciesData(this.agencies);
          this.globalClass.setMainBillingToContactsData(this.personData);
          this.initOrdersDetailPopup();
          this.changeCalledManually();
        }
      } else {
        this.advertisers = [];
        this.agencies = [];
        this.personData = [];
        this.partyAndOrganizationData = [];
      }
    }, error => {
      console.log(error);
    });
  }

  changeCalledManually() {
    try {
      if (this.isEditMode === true) {
        const advertiserSelected = this.commonOrderDetail.BillToInd;
        if ((advertiserSelected) === 0) {


          const advertiserId = this.commonOrderDetail.AdvertiserId;
          const partyData = this.partyAndOrganizationData;
          this.organizationWiseParty = [];

          partyData.forEach((person, key) => {
            if (person.PrimaryOrganization !== undefined) {
              if (person.PrimaryOrganization.OrganizationPartyId !== undefined) {
                const organizationI = person.PrimaryOrganization.OrganizationPartyId;
                if (advertiserId === organizationI) {
                  this.organizationWiseParty.push(person);
                  this.test = 'secound';

                  if (this.organizationWiseParty.length > 0) {
                    this.personData = this.organizationWiseParty;
                  }
                }
              }
            }
          });

          if (this.test = 'first') {
            let billingDetailsData = this.globalClass.getbillingDetails();
            const IsAdvertiser = (billingDetailsData.BillTo);
            if (IsAdvertiser === 0) {
              billingDetailsData = this.globalClass.getbillingDetails();
              this.personData = (billingDetailsData.BillToContactId);
            }
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  generateDynamicImageURL() {
    this.editImageIcon = '';
    this.adjustmentService.getLogoByBuyID(this.buyerId, this.reqVerificationToken).subscribe(result => {
      if (result.StatusCode === 1) {
        this.apiBaseURL = `${environment.ApiBaseURL}`;
        this.sampleLogoImagePath = this.apiBaseURL.replace('/api', '') + '/' + result.Data;
        console.log(this.sampleLogoImagePath);
      } else {
        this.sampleLogoImagePath = this.websiteRoot + '/' + this.imgurl + '/sample-logo.png';
      }
    }, error => {

      console.log(error);
    });


    this.checkboxImagePath = this.websiteRoot + this.imgurl + '/check-box-empty.png';
  }

  //--------Add Gross & Net Cost method---------
  // Add Gross Cost Adjustment
  addMediaGrossCostAdjustment(mediatype: any) {
    try {
      if ((this.selectedCheckbox.length === 1) || (this.selectedCheckbox.length > 0 && this.isMultipleIssueDate === true)) {
        /* --------------------------------------------- */
        const obj: any = {};
        let AdAdjustmentId = '';
        let AdAdjustmentName = '';
        let AdjustmentValue = '';
        let isproceed = false;

        this.isGrossAdAdjustmentModalFormSubmitted = true;

        if (mediatype === 'single') {
          if (this.grossAdAdjustmentModalForm.valid) {
            isproceed = true;
            AdAdjustmentId = this.grossAdAdjustmentModalForm.controls['AdAdjustmentId'].value;
            AdAdjustmentName = this.adAdjustments.find(s => s.SrNo === AdAdjustmentId).AdAdjustmentName;
            AdjustmentValue = this.grossAdAdjustmentModalForm.controls['AdAdjustmentValue'].value;
          } else {
            return;
          }
        } else if (mediatype === 'multiple') {
          if (this.multipleGrossAdAdjustmentModalForm.valid) {
            isproceed = true;
            AdAdjustmentId = this.multipleGrossAdAdjustmentModalForm.controls['AdAdjustmentId'].value;
            AdAdjustmentName = this.adAdjustments.find(s => s.SrNo === AdAdjustmentId).AdAdjustmentName;
            AdjustmentValue = this.multipleGrossAdAdjustmentModalForm.controls['AdAdjustmentValue'].value;
          } else {
            return;
          }
        }

        if (isproceed === true) {
          obj.MediaOrderIds = this.selectedMediaOrderIds;
          obj.AdAdjustmentName = AdAdjustmentName;
          obj.AmountPercent = this.grossAdAdjustment.AmountPercent;
          obj.SurchargeDiscount = this.grossAdAdjustment.SurchargeDiscount;
          obj.AdjustmentValue = AdjustmentValue;
          obj.GrossInd = true;
          this.adjustmentService.saveMediaOrderLines(obj, this.reqVerificationToken).subscribe(result => {
            this.isGrossAdAdjustmentModalFormSubmitted = false;
            if (result.StatusCode === 1) {

              const responseData = result.Data.MediaOrderLines;

              let MOrderId = 0;
              if (mediatype === 'single') {
                MOrderId = this.mediaOrderId;
              } else if (mediatype === 'multiple') {
                MOrderId = this.selectedMediaOrderId;
              }
              this.mediaOrderId = MOrderId;

              let objCurrentAdAdjustments: any = null;
              objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [responseData, MOrderId]);

              setTimeout(() => {
                this.mediaOrderLineId = objCurrentAdAdjustments[0].MediaOrderLineId;
              }, 500);

              const grossCost = result.Data.Cost.GrossCost;
              const netCost = result.Data.Cost.NetCost;
              this.globalClass.setGrossCost((grossCost).toFixed(2));
              this.globalClass.setNetCost((netCost).toFixed(2));
              this.grossCost = this.globalClass.getGrossCost();
              this.netCost = this.globalClass.getNetCost();
              this.getNoOfIssueMediaOrder();

              if (mediatype === 'multiple') {
                this.getMediaOrderLinesRow('multiple');
              } else {
                this.getMediaOrderLinesRow('0');
              }

              if (mediatype === 'single') {
                this.grossAdAdjustmentModalForm.reset();
                this.multipleGrossAdAdjustmentModalForm.reset();
              } else if (mediatype === 'multiple') {
                this.multipleGrossAdAdjustmentModalForm.reset();
                this.grossAdAdjustmentModalForm.reset();
              } else {
                this.grossAdAdjustmentModalForm.reset();
                this.multipleGrossAdAdjustmentModalForm.reset();
              }

              this.toastr.success('Saved successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error!');
            }
          }, error => {
            console.log(error);
          });
        }
        /* --------------------------------------------- */
      } else {
        this.toastr.info('Please select one issue media order', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }

  // Add Net Cost Adjustment
  addMediaNetCostAdjustment(mediatype: any) {
    try {
      if ((this.selectedCheckbox.length === 1) || (this.selectedCheckbox.length > 0 && this.isMultipleIssueDate === true)) {

        this.isNetAdAdjustmentModalFormSubmitted = true;
        const obj: any = {};
        let adAdjustmentId = '';
        let adAdjustmentName = '';
        let adjustmentValue = '';
        let isProceed = false;

        if (mediatype === 'single') {
          if (this.netAdAdjustmentModalForm.valid) {
            isProceed = true;
            adAdjustmentId = this.netAdAdjustmentModalForm.controls['AdAdjustmentId'].value;
            adAdjustmentName = this.adAdjustments.find(s => s.SrNo === adAdjustmentId).AdAdjustmentName;
            adjustmentValue = this.netAdAdjustmentModalForm.controls['AdAdjustmentValue'].value;
          } else {
            return;
          }
        } else if (mediatype === 'multiple') {
          if (this.multipleNetAdAdjustmentModalForm.valid) {
            isProceed = true;
            adAdjustmentId = this.multipleNetAdAdjustmentModalForm.controls['AdAdjustmentId'].value;
            adAdjustmentName = this.adAdjustments.find(s => s.SrNo === adAdjustmentId).AdAdjustmentName;
            adjustmentValue = this.multipleNetAdAdjustmentModalForm.controls['AdAdjustmentValue'].value;
          } else {
            return;
          }
        }

        if (isProceed === true) {

          obj.MediaOrderIds = this.selectedMediaOrderIds;
          obj.AdAdjustmentName = adAdjustmentName;
          obj.AmountPercent = this.netAdAdjustment.AmountPercent;
          obj.SurchargeDiscount = this.netAdAdjustment.SurchargeDiscount;
          obj.AdjustmentValue = adjustmentValue;
          obj.GrossInd = false;

          this.adjustmentService.saveMediaOrderLines(obj, this.reqVerificationToken).subscribe(result => {


            this.isNetAdAdjustmentModalFormSubmitted = false;
            if (result.StatusCode === 1) {
              const responseData = result.Data.MediaOrderLines;
              let mOrderId = 0;
              if (mediatype === 'single') {
                mOrderId = this.mediaOrderId;
              } else if (mediatype === 'multiple') {
                mOrderId = this.selectedMediaOrderId;
              }
              this.mediaOrderId = mOrderId;

              const objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [responseData, mOrderId]);
              this.mediaOrderLineId = objCurrentAdAdjustments[0].MediaOrderLineId;

              const grossCost = result.Data.Cost.GrossCost;
              const netCost = result.Data.Cost.NetCost;
              this.globalClass.setGrossCost((grossCost).toFixed(2));
              this.globalClass.setNetCost((netCost).toFixed(2));
              this.grossCost = this.globalClass.getGrossCost();
              this.netCost = this.globalClass.getNetCost();
              this.getNoOfIssueMediaOrder();

              if (mediatype === 'multiple') {
                this.getNetMediaOrderLinesRow('multiple');
              } else {
                this.getNetMediaOrderLinesRow('0');
              }


              this.clearNetAmountSection();

              if (mediatype === 'single') {
                this.netAdAdjustmentModalForm.reset();
                this.multipleNetAdAdjustmentModalForm.reset();
              } else if (mediatype === 'multiple') {
                this.multipleNetAdAdjustmentModalForm.reset();
                this.netAdAdjustmentModalForm.reset();
              } else {
                this.multipleNetAdAdjustmentModalForm.reset();
                this.netAdAdjustmentModalForm.reset();
              }

              this.toastr.success('Save successfully.', 'Success!');

            } else {
              this.toastr.error(result.Message, 'Error!');
            }
          }, error => {
            console.log(error);
          });
        }
      } else {
        this.toastr.info('Please select one issue media order', 'Information!');
      }
    } catch (error) {
      console.log(error);
    }
  }


  //---------- Bind Gross & Net table ------------
  // bind gross cost table (left)
  getMediaOrderLinesRow(str) {
    try {
      if (this.mediaOrderId != null && this.mediaOrderId != undefined) {
        setTimeout(() => {
          this.clearGrossAmountSection();
        }, 1000);
        this.adjustmentService.getMediaOrderLinesbyOrderId(this.mediaOrderId, this.reqVerificationToken).subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              const data = result.Data;
              let arrData = [];

              try {
                if (data.length > 0) {
                  const objgrossAdAdjustments = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ?',
                    [data, true]);
                  const objnetAdAdjustments = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ?',
                    [data, false]);

                  objgrossAdAdjustments.forEach((element, key) => {
                    if (key === 0) {
                      this.isGrossChangedByMoveUp = true;
                    } else {
                      this.isGrossChangedByMoveUp = false;
                    }
                  });

                  if (objgrossAdAdjustments.length === 1) {
                    this.isGrossChangedByMoveUp = false;
                  }

                  objnetAdAdjustments.forEach((element, key) => {
                    if (key === 0) {
                      this.isNetChangedByMove = true;
                    } else {
                      this.isNetChangedByMove = false;
                    }
                  });

                  if (objnetAdAdjustments.length === 1) {
                    this.isNetChangedByMove = false;
                  }
                }
              } catch (error) {
                console.log(error);
              }

              if (this.isNetChangedByMove === true) {
                this.netAdAdjustments = [];
              }

              let i = 0;
              let foundGrossData = false;
              data.forEach((element, index) => {
                const obj: any = {};
                let tempAmount: any = 0;
                let adjustmentValue: any = 0;
                let afterMinusAmount: any = 0;

                if (element.GrossInd === true) {
                  foundGrossData = true;
                  if (i === 0) {
                    arrData = [];

                    tempAmount = parseFloat(this.tempGrossAmount);
                    adjustmentValue = parseFloat(element.AdjustmentValue);

                    obj.MediaOrderId = this.mediaOrderId;
                    obj.AdAdjustmentName = element.AdAdjustmentName;
                    obj.AdjustmentValue = element.AdjustmentValue;
                    obj.MediaOrderLineId = element.MediaOrderLineId;
                    obj.ProductCode = element.ProductCode;
                    obj.SequenceNumber = element.SequenceNumber;
                    obj.GrossInd = element.GrossInd;

                    if (element.AmountPercent === 0) {
                      obj.isPercentage = false;
                      if (element.SurchargeDiscount === 0) {
                        afterMinusAmount = tempAmount + adjustmentValue;
                      } else {
                        afterMinusAmount = tempAmount - adjustmentValue;
                      }
                      obj.tempAdjustmentAmount = adjustmentValue.toFixed(2);

                    } else if (element.AmountPercent === 1) {
                      obj.isPercentage = true;
                      const percentageValueInAmount = adjustmentValue * tempAmount / 100;
                      if (element.SurchargeDiscount === 0) {
                        afterMinusAmount = tempAmount + percentageValueInAmount;
                      } else {
                        afterMinusAmount = tempAmount - percentageValueInAmount;
                      }
                      obj.tempAdjustmentAmount = percentageValueInAmount.toFixed(2);
                    }

                    afterMinusAmount = afterMinusAmount.toFixed(2);
                    obj.tempRunningAmount = afterMinusAmount;
                    this.setInitiallyFinalNetAdjustmentAmount(obj.tempRunningAmount);
                    this.finalGrossAmount = obj.tempRunningAmount;
                    this.tempNetAmount = obj.tempRunningAmount;
                    arrData.push(obj);
                    i = i + 1;
                  } else {
                    if (arrData.length === 0) {
                      tempAmount = parseFloat(this.tempGrossAmount);
                      adjustmentValue = parseFloat(element.AdjustmentValue);
                    } else {
                      if (str === 'issuelistcheck') {
                        this.sequenceNumber = i;
                        this.dataContext = arrData;
                      } else if (str === 'edit') {
                        this.sequenceNumber = i;
                        this.dataContext = arrData;
                      } else {
                        if (str === 'multiple') {
                          this.sequenceNumber = i;
                          this.dataContext = arrData;
                        } else {
                          if (this.grossAdAdjustments.length === 0) {
                            this.sequenceNumber = index;
                            this.dataContext = arrData;
                          } else {
                            this.sequenceNumber = i;
                            this.dataContext = this.grossAdAdjustments;
                          }
                        }
                      }

                      // tslint:disable-next-line:max-line-length
                      const grossAdAdjustment = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [this.dataContext, this.sequenceNumber]);
                      try {
                        tempAmount = parseFloat(grossAdAdjustment[0].tempRunningAmount);
                        adjustmentValue = parseFloat(element.AdjustmentValue);
                      } catch (error) {
                        console.log(error);
                      }
                    }

                    obj.MediaOrderId = this.mediaOrderId;
                    obj.AdAdjustmentName = element.AdAdjustmentName;
                    obj.AdjustmentValue = element.AdjustmentValue;
                    obj.MediaOrderLineId = element.MediaOrderLineId;
                    obj.ProductCode = element.ProductCode;
                    obj.SequenceNumber = element.SequenceNumber;
                    obj.GrossInd = element.GrossInd;

                    if (element.AmountPercent === 0) {
                      obj.isPercentage = false;
                      if (element.SurchargeDiscount === 0) {
                        afterMinusAmount = tempAmount + adjustmentValue;
                      } else {
                        afterMinusAmount = tempAmount - adjustmentValue;
                      }
                      obj.tempAdjustmentAmount = adjustmentValue.toFixed(2);

                    } else if (element.AmountPercent === 1) {
                      obj.isPercentage = true;
                      const percentageValueInAmount = adjustmentValue * tempAmount / 100;
                      if (element.SurchargeDiscount === 0) {
                        afterMinusAmount = tempAmount + percentageValueInAmount;
                      } else {
                        afterMinusAmount = tempAmount - percentageValueInAmount;
                      }
                      obj.tempAdjustmentAmount = percentageValueInAmount.toFixed(2);
                    }

                    afterMinusAmount = afterMinusAmount.toFixed(2);
                    obj.tempRunningAmount = afterMinusAmount;
                    this.setInitiallyFinalNetAdjustmentAmount(obj.tempRunningAmount);
                    this.finalGrossAmount = obj.tempRunningAmount;
                    this.tempNetAmount = obj.tempRunningAmount;
                    arrData.push(obj);
                    i = i + 1;
                  }
                }

                try {
                  const ind = this.selectedGrossAdjustmentMediaOrderLines.indexOf(element.MediaOrderLineId);
                  this.selectedGrossAdjustmentMediaOrderLines.splice(ind, 1);
                } catch (error) {
                  console.log(error);
                }
              });

              if (foundGrossData == false) {
                this.finalGrossAmount = this.globalClass.getfinalGrossAmount();
              }

              this.grossAdAdjustments = arrData;
              if (arrData.length == 0) {
                this.finalGrossAmount = this.globalClass.getfinalGrossAmount();
              }

              if (this.netAdAdjustments.length > 0) {
                this.getNetMediaOrderLinesRow('edit');
              }

            } else {
              this.grossAdAdjustments = [];
            }
          } else if (result.StatusCode === 3) {
            this.grossAdAdjustments = [];
            this.finalGrossAmount = this.globalClass.getfinalGrossAmount();
            if (this.netAdAdjustments.length === 0) {
              this.finalNetAmount = this.tempNetAmountInit;
            } else {
              this.finalNetAmount = this.globalClass.getfinalNetAmount();
            }
          } else {
            this.grossAdAdjustments = [];
            this.finalGrossAmount = this.globalClass.getfinalGrossAmount();
            if (this.netAdAdjustments.length === 0) {
              this.finalNetAmount = this.tempNetAmountInit;
            } else {
              this.finalNetAmount = this.globalClass.getfinalNetAmount();
            }
          }
        }, error => {
          console.log(error);
        });
      } else {
        console.log('media Order Id is null');
      }
    } catch (error) {
      console.log(error);
    }
  }

  // bind net cost table (right)
  getNetMediaOrderLinesRow(str) {
    try {
      if (this.mediaOrderId != null && this.mediaOrderId != undefined) {
        setTimeout(() => {
          this.clearNetAmountSection();
          this.clearGrossAmountSection();
        }, 1000);
        this.adjustmentService.getMediaOrderLinesbyOrderId(this.mediaOrderId, this.reqVerificationToken).subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              const data = result.Data;

              let arrData: any = [];

              try {
                if (data.length > 0) {
                  const objgrossAdAdjustments = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ?',
                    [data, true]);
                  const objnetAdAdjustments = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ?',
                    [data, false]);

                  objgrossAdAdjustments.forEach((element, key) => {
                    if (key === 0) {
                      this.isGrossChangedByMoveUp = true;
                    } else {
                      this.isGrossChangedByMoveUp = false;
                    }
                  });

                  if (objgrossAdAdjustments.length === 1) {
                    this.isGrossChangedByMoveUp = false;
                  }

                  objnetAdAdjustments.forEach((element, key) => {
                    if (key === 0) {
                      this.isNetChangedByMove = true;
                    } else {
                      this.isNetChangedByMove = false;
                    }
                  });

                  if (objnetAdAdjustments.length === 1) {
                    this.isNetChangedByMove = false;
                  }
                }
              } catch (error) {
                console.log(error);
              }

              if (this.isNetChangedByMove === true) {
                this.netAdAdjustments = [];
              }

              let i = 0;
              let foundNetData = false;
              data.forEach((element, index) => {
                const obj: any = {};
                let tempAmount: any = 0;
                let adjustmentValue: any = 0;
                let afterMinusAmount: any = 0;

                if (element.GrossInd === false) {
                  foundNetData = true;
                  if (i === 0) {
                    arrData = [];

                    if (str === 'edit' || str === '0') {
                      if (this.grossAdAdjustments.length === 0) {
                        tempAmount = parseFloat(this.tempNetAmountInit);
                      } else {
                        tempAmount = parseFloat(this.tempNetAmount);
                      }
                    } else {
                      tempAmount = parseFloat(this.tempNetAmount);
                    }

                    adjustmentValue = parseFloat(element.AdjustmentValue);

                    obj.MediaOrderId = this.mediaOrderId;
                    obj.AdAdjustmentName = element.AdAdjustmentName;
                    obj.AdjustmentValue = element.AdjustmentValue;
                    obj.MediaOrderLineId = element.MediaOrderLineId;
                    obj.ProductCode = element.ProductCode;
                    obj.SequenceNumber = element.SequenceNumber;
                    obj.GrossInd = element.GrossInd;

                    if (element.AmountPercent === 0) {
                      obj.isPercentage = false;
                      if (element.SurchargeDiscount === 0) {
                        afterMinusAmount = tempAmount + adjustmentValue;
                      } else {
                        afterMinusAmount = tempAmount - adjustmentValue;
                      }
                      obj.tempAdjustmentAmount = adjustmentValue.toFixed(2);


                    } else if (element.AmountPercent === 1) {
                      obj.isPercentage = true;
                      const percentageValueInAmount = adjustmentValue * tempAmount / 100;
                      if (element.SurchargeDiscount === 0) {
                        afterMinusAmount = tempAmount + percentageValueInAmount;
                      } else {
                        afterMinusAmount = tempAmount - percentageValueInAmount;
                      }
                      obj.tempAdjustmentAmount = percentageValueInAmount.toFixed(2);
                    }

                    afterMinusAmount = afterMinusAmount.toFixed(2);
                    obj.tempRunningAmount = afterMinusAmount;
                    this.finalNetAmount = obj.tempRunningAmount;

                    this.globalClass.setfinalNetAmount(this.finalNetAmount);

                    arrData.push(obj);
                    i = i + 1;
                  } else {

                    if (arrData.length === 0) {
                      tempAmount = parseFloat(this.tempNetAmount);
                      adjustmentValue = parseFloat(element.AdjustmentValue);
                    } else {
                      if (str === 'issuelistcheck') {
                        this.sequenceNumber = i;
                        this.dataContext = arrData;
                      } else if (str === 'edit') {

                        this.sequenceNumber = i;
                        this.dataContext = arrData;
                      } else {

                        if (str === 'multiple') {
                          this.sequenceNumber = i;
                          this.dataContext = arrData;
                        } else {
                          if (this.netAdAdjustments.length === 0) {
                            this.sequenceNumber = index;
                            this.dataContext = arrData;
                          } else {
                            this.sequenceNumber = i;
                            this.dataContext = this.netAdAdjustments;
                          }
                        }
                      }
                      // tslint:disable-next-line:max-line-length
                      const objSequenceNoWiseGrossAdAdjustment = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [this.dataContext, this.sequenceNumber]);
                      try {
                        tempAmount = parseFloat(objSequenceNoWiseGrossAdAdjustment[0].tempRunningAmount);
                        adjustmentValue = parseFloat(element.AdjustmentValue);
                      } catch (error) {
                        console.log(error);
                      }
                    }

                    obj.MediaOrderId = this.mediaOrderId;
                    obj.AdAdjustmentName = element.AdAdjustmentName;
                    obj.AdjustmentValue = element.AdjustmentValue;
                    obj.MediaOrderLineId = element.MediaOrderLineId;
                    obj.ProductCode = element.ProductCode;
                    obj.SequenceNumber = element.SequenceNumber;
                    obj.GrossInd = element.GrossInd;

                    if (element.AmountPercent === 0) {
                      obj.isPercentage = false;
                      if (element.SurchargeDiscount === 0) {
                        afterMinusAmount = tempAmount + adjustmentValue;
                      } else {
                        afterMinusAmount = tempAmount - adjustmentValue;
                      }
                      obj.tempAdjustmentAmount = adjustmentValue.toFixed(2);

                    } else if (element.AmountPercent === 1) {
                      obj.isPercentage = true;
                      const percentageValueInAmount = adjustmentValue * tempAmount / 100;
                      if (element.SurchargeDiscount === 0) {
                        afterMinusAmount = tempAmount + percentageValueInAmount;
                      } else {
                        afterMinusAmount = tempAmount - percentageValueInAmount;
                      }
                      obj.tempAdjustmentAmount = percentageValueInAmount.toFixed(2);
                    }

                    afterMinusAmount = afterMinusAmount.toFixed(2);
                    obj.tempRunningAmount = afterMinusAmount;
                    this.finalNetAmount = obj.tempRunningAmount;
                    this.globalClass.setfinalNetAmount(this.finalNetAmount);
                    arrData.push(obj);
                    i = i + 1;
                  }
                }

                try {
                  const ind = this.selectedNetAdjustmentMediaOrderLines.indexOf(element.MediaOrderLineId);
                  this.selectedNetAdjustmentMediaOrderLines.splice(ind, 1);
                } catch (error) {
                  console.log(error);
                }
              });

              this.sequenceNumber = 0;
              this.dataContext = [];
              this.netAdAdjustments = arrData;

              if (foundNetData == false) {
                setTimeout(() => {
                  if (this.grossAdAdjustments.length > 0) {
                    this.finalNetAmount = this.tempNetAmount;
                  } else {
                    if (this.netAdAdjustments.length === 0) {
                      this.finalNetAmount = this.tempNetAmountInit;
                    } else {
                      this.finalNetAmount = this.tempNetAmount;
                    }
                  }

                  this.globalClass.setfinalNetAmount(this.finalNetAmount);
                }, 200);
              }

            } else {
              this.netAdAdjustments = [];
            }
          } else if (result.StatusCode === 3) {
            this.netAdAdjustments = [];
            this.finalGrossAmount = this.globalClass.getfinalGrossAmount();
            if (this.netAdAdjustments.length === 0) {
              this.finalNetAmount = this.tempNetAmountInit;
            } else {
              this.finalNetAmount = this.globalClass.getfinalNetAmount();
            }
          } else {
            this.netAdAdjustments = [];
            this.finalGrossAmount = this.globalClass.getfinalGrossAmount();
            if (this.netAdAdjustments.length === 0) {
              this.finalNetAmount = this.tempNetAmountInit;
            } else {
              this.finalNetAmount = this.globalClass.getfinalNetAmount();
            }
          }
        }, error => {
          console.log(error);
        });
      } else {
        console.log('media Order Id is null');
      }
    } catch (error) {
      console.log(error);
    }
  }


  //---------------- Loader ----------------
  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }
  //-------------- End Loader --------------

  // Order Export
  exportDataExcel() {
    try {
      const buyerId = this.buyerId;
      this.buyerId = buyerId + 'IO';
      const HTMLtoExport = document.getElementById('exportable').innerHTML;

      const blob = new Blob([HTMLtoExport], {
        type: 'application/vnd.ms-excel;charset=utf-8'
      });
      saveAs(blob, buyerId + '.xls');
    } catch (error) {
      console.log(error);
    }
  }

  exportDataWord() {
    try {
      let buyerId = this.buyerId;
      buyerId = buyerId + 'IO';
      const HTMLtoExport = document.getElementById('exportable').innerHTML;
      const blob = new Blob([HTMLtoExport], {
        type: 'application/msword;charset=utf-8'
      });
      saveAs(blob, buyerId + '.doc');
    } catch (error) {
      console.log(error);
    }
  }

  // Proposal Export
  exportProposalExcel() {
    try {
      let buyerId = this.buyerId;
      buyerId = buyerId + 'Proposal';
      const HTMLtoExport = document.getElementById('exporProposal').innerHTML;

      const blob = new Blob([HTMLtoExport], {
        type: 'application/vnd.ms-excel;charset=utf-8'
      });
      saveAs(blob, buyerId + '.xls');
    } catch (error) {
      console.log(error);
    }
  }

  exportProposalWord() {
    try {
      let buyerId = this.buyerId;
      buyerId = buyerId + 'Proposal';
      const HTMLtoExport = document.getElementById('exporProposal').innerHTML;

      const blob = new Blob([HTMLtoExport], {
        type: 'application/msword;charset=utf-8'
      });
      saveAs(blob, buyerId + '.doc');
    } catch (error) {
      console.log(error);
    }
  }

  goToPreviousTab() {
    if (this.buyerId > 0) {
      this.globalClass.setOrderId(this.buyerId);
      this.globalClass.setbillingDetailsMediaOrderid(0);
      this.orderTabService.send_OrderTab('MediaScheduleTab');
    } else {
      this.orderTabService.send_OrderTab('MediaScheduleTab');
    }
  }

  initSelect2(id, text) {
    $('#' + id).find('span.select2-chosen').text(text);
  }

  // Add Product Data in database (done by nimesh on 15 07 2020)
  finishNExit() {
    try {
      const gAmount = 0;
      const nAmount = 0;
      this.globalClass.removeBillingDetails();
      this.globalClass.removeOrderId();
      this.globalClass.setGrossCost((gAmount).toFixed(2));
      this.globalClass.setNetCost((nAmount).toFixed(2));
      this.globalClass.removeIsFrozen();
      this.globalClass.removeMainPartyAndOrganizationData();
      this.globalClass.removeMainAdvertisersData();
      this.globalClass.removeMainAgenciesData();
      this.globalClass.removeMainBillingToContactsData();
      
      this.appComponent.showMainPage = true;
      this.appComponent.showOrderFormPage = false;
      this.appComponent.setAdvertisersTabActive();

    } catch (error) {
      console.log(error);
    }
  }

  uploadMediaOrderSignedDocuments(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {

        const logoElement = event.target.files.item(0);
        const uploadType = logoElement.type;
        const filename = event.target.files.item(0).name;
        this.mediaOrderFile = filename;
        const ext = filename.substr(filename.lastIndexOf('.') + 1);
        this.mediaOrderFileExtension = ext;
        if (uploadType !== undefined && uploadType != null) {
          const mediaSource = URL.createObjectURL(event.target.files[0]);
          const reader = new FileReader();
          reader.onload = (e: any) => {
            const strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
            this.mediaOrderFileSrc = strImage;
          };
          this.frmMediaDocumentsUpload.controls['txtMediaOrderSignedDocuments'].setValue(filename);
          reader.readAsDataURL(event.target.files[0]);
        }
        console.log(uploadType +' : '+ this.mediaOrderFileExtension);
      }
    } catch (error) {
      console.log(error);
    }
  }

  mediaDocumentValidation() {
    this.frmMediaDocumentsUpload = this.formBuilder.group({
      'txtMediaOrderSignedDocuments': [null, Validators.required],
      'mediaOrderFile-Upload':[null]
    });
  }

  onMediaOrderSignedDocumentAdd()
  {
    let filePath =this.mediaOrderFileSrc;
    let filePathName=this.mediaOrderFile;
    let filePathExtension=this.mediaOrderFileExtension;
    this.isSignedDocumentRequiredMsg=false;
    if(this.mediaOrderFile==='')
    {
      this.isSignedDocumentRequiredMsg=true;
      this.toastr.error('Please select signed document for upload', 'Error!');
    }
     if(this.isSignedDocumentRequiredMsg===false)
     {
      const MediaOrderSignedDocumentsModel = {
        MediaOrderSignedDocumentsId:0,
        BuyId: this.buyerId,
        FilePath:filePath,
        FilePathName:filePathName,
        FilePathExtension:filePathExtension,
        CreatedBy:environment.CurrentUserName
    };

    this.adjustmentService.uploadMediaOrderSignedDocuments(MediaOrderSignedDocumentsModel,this.reqVerificationToken).subscribe(result => {
      console.log(result);
      if (result.StatusCode === 1) {
        this.hideLoader();
        this.toastr.success('Signed document successfully uploaded.', 'Success!');
        this.frmMediaDocumentsUpload.controls['txtMediaOrderSignedDocuments'].setValue("");
        this.mediaOrderFileSrc='';
        this.mediaOrderFile='';
        this.mediaOrderFileExtension='';
        this.isSignedDocumentRequiredMsg=false;
        this.getMediaOrderSignedDocumentList();
      } else {
        this.hideLoader();
        this.toastr.error(result.Message, 'Error!');
      }
  }, error => {
    this.hideLoader();
    console.log(error);
  });
     }
  }

  getMediaOrderSignedDocumentList() 
  {
    this.adjustmentService.getMediaOrderSignedDocumentList(this.buyerId,this.reqVerificationToken).subscribe(result => {
      if (result.StatusCode === 1) {
        this.mediaOrderSignedDocumentsList= result.Data;
        console.log(result.Data);
      } else {
        this.sampleLogoImagePath = this.websiteRoot + '/' + this.imgurl + '/sample-logo.png';
      }
    }, error => {
      
      console.log(error);
    });
  }

  // deleteIssueOrderAdjustment
  deleteMediaOrderSignedDocumentById(objMediaSignedDocument) {
    try {
      if (confirm('Are you sure you want to delete the selected object(s) and all of their children?')) {
        const mediaOrderSignedDocumentId = objMediaSignedDocument.MediaOrderSignedDocumentsId;
        if (!isNullOrUndefined(mediaOrderSignedDocumentId)) {
          try {
            this.adjustmentService.deleteMediaOrderSignedDocument(mediaOrderSignedDocumentId, this.reqVerificationToken).subscribe(result1 => {
              if (result1.StatusCode === 1) {
                this.toastr.success('Deleted successfully.', 'Success!');
                this.getMediaOrderSignedDocumentList();
              } else {
                this.toastr.error(result1.Message, 'Error!');
              }
            }, error => {
              console.log(error);
            });
          } catch (error) {
            console.log(error);
          }
        } else {
          console.log('Media Document Id is null');
        }
      }
    } catch (error) {
      console.log(error);
    }
  }
}
