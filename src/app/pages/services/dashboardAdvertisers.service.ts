import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class DashboardAdvertiserService {
    public headers: HttpHeaders;
    public headers_token: HttpHeaders;
    constructor(private http: HttpClient) { }

    getOrdersByAdvertiser(authToken, data): Observable<any> {
        try {
            this.headers_token = new HttpHeaders({
                'Content-Type': 'application/json',
                'RequestVerificationToken': authToken
            });
            return this.http.post(`${environment.ApiBaseURL}/MediaOrder/GetOrdersByAdvertiser`, data, { headers: this.headers_token });
        } catch (error) {
            console.log(error);
        }
    }

    getAllAdvertiser1(websiteRoot, baseUrl, authToken, Offset): Observable<any> {
        try {
            this.headers_token = new HttpHeaders({
                'Content-Type': 'application/json',
                'RequestVerificationToken': authToken
            });
            return this.http.get(websiteRoot + 'api/Party?limit=500&Offset=' + Offset, { headers: this.headers_token });
        } catch (error) {
            console.log(error);
        }
    }

    getAllAdvertiser_CharacterSearch(websiteRoot, baseUrl, authToken, Offset,startsWith): Observable<any> {
        try {
            this.headers_token = new HttpHeaders({
                'Content-Type': 'application/json',
                'RequestVerificationToken': authToken
            });
            return this.http.get(websiteRoot + 'api/Party?limit=500&OrganizationName=StartsWith:'+startsWith+'&Offset=' + Offset, { headers: this.headers_token });
        } catch (error) {
            console.log(error);
        }
    }

    getAdvertiserAgency_BillToContact(websiteRoot, baseUrl, authToken, Offset,advertiserorAgencyName,filterValue): Observable<any> {
        try {
            this.headers_token = new HttpHeaders({
                'Content-Type': 'application/json',
                'RequestVerificationToken': authToken
            });
            if(filterValue==="")
                return this.http.get(websiteRoot + 'api/Party?limit=500&OrganizationName=eq:'+advertiserorAgencyName+'&Offset=' + Offset, { headers: this.headers_token });
            else
                return this.http.get(websiteRoot + 'api/Party?limit=500&OrganizationName=eq:'+advertiserorAgencyName+'&Name=contains:'+filterValue+'&Offset=' + Offset, { headers: this.headers_token });
        } catch (error) {
            console.log(error);
        }
    }


    getAdvertiser_BillToContactNameById(websiteRoot, baseUrl, authToken, contactId): Observable<any> {
        try {
            this.headers_token = new HttpHeaders({
                'Content-Type': 'application/json',
                'RequestVerificationToken': authToken
            });
            return this.http.get(websiteRoot + 'api/Party?PartyId=eq:'+contactId, { headers: this.headers_token });
        } catch (error) {
            console.log(error);
        }
    }
}
