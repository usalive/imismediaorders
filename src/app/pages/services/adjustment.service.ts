import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdjustmentService {
  baseAPI = environment.ApiBaseURL;
  mediaOrderSignedDocumentURL=this.baseAPI+'/MediaOrderSignedDocuments';
  mediaOrderSignedDocumentByBuyIdURL=this.baseAPI+'/MediaOrderSignedDocuments/getByBuyerId';
  public headers_token: HttpHeaders;  
  public sfheaders: HttpHeaders;
  authToken = environment.token;
  constructor(private http: HttpClient) { }

  getMediaOrderbyBuyId(byID, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      this.sfheaders = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'RequestVerificationToken': this.authToken
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaOrder/GetByBuyId/` + byID, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getLogoByBuyID(byID, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaOrderLines/GetLogoByBuyId/` + byID, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }


  getByMultipleMediaAsset(mediaAssetId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': 'token'
      });
      return this.http.get(`${environment.ApiBaseURL}/AdAdjustment/GetByMediaAssetIds?` + mediaAssetId, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getMediaOrderLinesbyOrderId(orderId, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaOrderLines/GetByMediaOrder/` + orderId, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  saveMediaOrderLines(data, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      const body = JSON.stringify(data);
      return this.http.post(`${environment.ApiBaseURL}/MediaOrderLines`, body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  deleteMediaOrderLinesbyMediaOrder(morderId, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.delete(`${environment.ApiBaseURL}/MediaOrder/` + morderId, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  moveUpOrDownMediaOrderLines(mediaOrderLines, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      const body = JSON.stringify(mediaOrderLines);
      return this.http.put(`${environment.ApiBaseURL}/MediaOrderLines/UpdateMediaOrderLines/`, body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  deleteMediaOrderLinesbyMediaOrderLineId(mediaOrderLineIds, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      const body = JSON.stringify(mediaOrderLineIds);
      return this.http.post(`${environment.ApiBaseURL}/MediaOrderLines/RemoveByMediaLine`, body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  updateMediaOrderStatus(mediaOrderIds, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      const body = JSON.stringify(mediaOrderIds);
      return this.http.put(`${environment.ApiBaseURL}/MediaOrder/ChangeStatus`, body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getAllAdvertiser1(websiteRoot, baseUrl, authToken, Offset): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(websiteRoot + 'api/Party?limit=500&Offset=' + Offset, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  uploadMediaOrderSignedDocuments(documentData,authToken): Observable<any> {
    const body = JSON.stringify({
      'BuyId': documentData.BuyId,
      'MediaOrderSignedDocumentsId': documentData.MediaOrderSignedDocumentsId,
      'FilePath': documentData.FilePath,
      'FilePathName': documentData.FilePathName,
      'FilePathExtension': documentData.FilePathExtension,
      'CreatedBy':documentData.CreatedBy
    });
    console.log(body);
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': 'token'
    });

    return this.http.post(`${environment.ApiBaseURL}/MediaOrderSignedDocuments`, body, { headers: this.headers_token });
  }

  getMediaOrderSignedDocumentList(byID, authToken): Observable<any> {
    try {
      //const body = JSON.stringify(byID);
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
  
      //return this.http.get(`${environment.ApiBaseURL}/MediaOrderSignedDocuments/` + byID);
      return this.http.get(`${environment.ApiBaseURL}/MediaOrderSignedDocuments/getByBuyerId/`+ byID, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  deleteMediaOrderSignedDocument(mSignedDocumentId, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      //const body = JSON.stringify(mSignedDocumentId);
      return this.http.delete(`${environment.ApiBaseURL}/MediaOrderSignedDocuments/` +mSignedDocumentId, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }
}
