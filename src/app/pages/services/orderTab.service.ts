import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderTabService {

  private subjectForOrderTab = new Subject<any>();

  constructor() { }

  send_OrderTab(message: string) {
    this.subjectForOrderTab.next({ text: message });
  }
  get_OrderTab(): Observable<any> {
    return this.subjectForOrderTab.asObservable();
  }


}
