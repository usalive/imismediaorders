import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductionService {

  public headers: HttpHeaders;
  public headers_token: HttpHeaders;
  public sfHeaders: HttpHeaders;

  constructor(private http: HttpClient) { }

  getMediaOrderbyBuyId(authToken, byID): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaOrder/GetByBuyId/` + byID, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  // Production Status Factory JS Method (named as getAll)
  getAllProductionStatus(): Observable<any> {
    try {
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
      return this.http.get(`${environment.ApiBaseURL}/ProductionStatus`, { headers: this.headers });
    } catch (error) {
      console.log(error);
    }
  }

  // Position Factory JS Method (named as getAll)
  getAllPosition(): Observable<any> {
    try {
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
      return this.http.get(`${environment.ApiBaseURL}/Position`, { headers: this.headers });
    } catch (error) {
      console.log(error);
    }
  }

  // Seperation Factory JS Method (named as getAll)
  getAllSeperation(): Observable<any> {
    try {
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
      return this.http.get(`${environment.ApiBaseURL}/Separation`, { headers: this.headers });
    } catch (error) {
      console.log(error);
    }
  }

  getOrdersNotInBuyer(buyId, adid): Observable<any> {

    try {
      this.sfHeaders = new HttpHeaders({
        RequestVerificationToken: 'token'
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaOrder/GetOrdersNotInBuyer/` + buyId + '/' + adid, { headers: this.sfHeaders });
    } catch (error) {
      console.log(error);
    }
  }

  deleteMediaOrderLinesbyMediaOrder(authToken, morderId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.delete(`${environment.ApiBaseURL}/MediaOrder/` + morderId, { headers: this.headers_token });

    } catch (error) {
      console.log(error);
    }
  }

  getById(mOrderId): Observable<any> {
    try {
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaOrderProductionDetails/GetByMediaOrder/` + mOrderId, { headers: this.headers });
    } catch (error) {
      console.log(error);
    }
  }

  getAdvertiserAgency_BillToContact(websiteRoot, authToken, Offset,advertiserorAgencyName,filterValue): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      if(advertiserorAgencyName.includes('&'))
      {
        let arrAdvertiserorAgencyName = advertiserorAgencyName.split("&");
        if(arrAdvertiserorAgencyName.length>0)
          advertiserorAgencyName = arrAdvertiserorAgencyName[0];
      }
      console.log("advertiserorAgencyName: "+ advertiserorAgencyName);
        if(filterValue==="")
            return this.http.get(websiteRoot + 'api/Party?limit=500&OrganizationName=contains:'+advertiserorAgencyName+'&Offset=' + Offset, { headers: this.headers_token });
        else
            return this.http.get(websiteRoot + 'api/Party?limit=500&OrganizationName=contains:'+advertiserorAgencyName+'&Name=contains:'+filterValue+'&Offset=' + Offset, { headers: this.headers_token });
    } catch (error) {
        console.log(error);
    }
}

  // Media Order Production Factory JS method
  saveMediaOrderProductionDetails(authToken, mediaOrder): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });

      const body = JSON.stringify({
        AdvertiserAgencyInd: mediaOrder.AdvertiserAgencyInd,
        BuyId: mediaOrder.BuyId,
        ChangesInd: mediaOrder.ChangesInd,
        Completed: mediaOrder.Completed,
        CreateExpectedInd: mediaOrder.CreateExpectedInd,

        FinalFile: mediaOrder.FinalFile,
        FinalFileExtension: mediaOrder.FinalFileExtension,
        FinalFileName: mediaOrder.FinalFileName,

        HeadLine: mediaOrder.HeadLine,
        MaterialContactId: mediaOrder.MaterialContactId,
        MaterialExpectedDate: mediaOrder.MaterialExpectedDate,
        MediaOrderIds: mediaOrder.MediaOrderIds,
        NewPickupInd: mediaOrder.NewInd,
        OnHandInd: mediaOrder.OnHandInd,

        OrigFileName: mediaOrder.OrigFileName,
        OriginalFile: mediaOrder.OriginalFile,
        OriginalFileExtension: mediaOrder.OriginalFileExtension,

        PageNumber: mediaOrder.PageNumber,
        PickupMediaOrderId: mediaOrder.PickupMediaOrderId,
        PositionId: mediaOrder.PositionId,
        ProductionStatusId: mediaOrder.ProductionStatusId,
        ProductionComment: mediaOrder.ProductionComment,

        ProofFile: mediaOrder.ProofFile,
        ProofFileExtension: mediaOrder.ProofFileExtension,
        ProofFileName: mediaOrder.ProofFileName,

        SeparationId: mediaOrder.SeparationId,
        TearSheets: mediaOrder.TearSheets,
        TrackingNumber: mediaOrder.TrackingNumber,
        WebAdUrl: mediaOrder.WebAdUrl
      });


      console.log("MediaOrderProductionDetails:"+ JSON.stringify(body));
      return this.http.post(`${environment.ApiBaseURL}/MediaOrderProductionDetails`, body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }
}
