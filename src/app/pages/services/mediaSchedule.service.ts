import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MediaScheduleService {

  public headers: HttpHeaders;
  public headers_token: HttpHeaders;
  constructor(private http: HttpClient) { }

  getOrdersByBuyId(adOrderId, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaOrder/GetByBuyId/` + adOrderId, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getOrdersByMediaOrderId(mediaOrderId, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaOrder/` + mediaOrderId, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getAll(): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaAsset`, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getByMediaAsset(mediaAssetId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
      });
      return this.http.get(`${environment.ApiBaseURL}/RateCard/GetByMediaAsset/` + mediaAssetId, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getByRateCard(rateCardId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
      });
      return this.http.get(`${environment.ApiBaseURL}/AdSize/GetByRateCard/` + rateCardId, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getbyRatecardandAdsize(rateCardId, adSizeId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
      });
      return this.http.get(`${environment.ApiBaseURL}/AdColor/GetByRateCardAndAdSize/` + rateCardId + `/` + adSizeId,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getbyRatecardColorandAdsize(rateCardId, adColorId, adSizeId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
      });
      return this.http.get(`${environment.ApiBaseURL}/Frequency/GetByRateCardColorAndAdsize/` + rateCardId + `/` + adColorId + `/` +
        adSizeId,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getRateCardCostForPerWord(rateCardId, adColorId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': 'token'
      });
      return this.http.get(`${environment.ApiBaseURL}/RateCardDetail/GetRateCardsCostForPerWord/` + rateCardId + `/` + adColorId,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getCost(rateCardId, adColorId, adSizeId, frequencyId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
      });
      return this.http.get(`${environment.ApiBaseURL}/RateCardDetail/GetRateCardCost/` + rateCardId + `/` + adColorId +
        `/` + adSizeId + `/` + frequencyId,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getIssueDate(issueDates): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': 'token'
      });
      const body = JSON.stringify(issueDates);
      return this.http.post(`${environment.ApiBaseURL}/IssueDate/GetByMediaAssetAndDateRange/`, body,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  update(mediaAsset): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': 'token'
      });
      const body = JSON.stringify(mediaAsset);
      console.log("updateMediaAsset:"+ body);
      return this.http.put(`${environment.ApiBaseURL}/MediaOrder`, body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  save(mediaAsset): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': 'token'
      });
      const body = JSON.stringify(mediaAsset);
      console.log("saveMediaAsset:"+ body);
      // tslint:disable-next-line:max-line-length
      return this.http.post(`${environment.ApiBaseURL}/MediaOrder`, body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

}
