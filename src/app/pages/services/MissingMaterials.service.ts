import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MissingMaterialsService {

  public headers: HttpHeaders;
  public headers_token: HttpHeaders;
  constructor(private http: HttpClient) { }


  getAll(): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': 'token'
      });
      return this.http.get(`${environment.ApiBaseURL}/IssueDate`, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getAllAdvertiser1(websiteRoot, authToken, Offset): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(websiteRoot + 'api/Party?limit=500&Offset=' + Offset, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getMediaOrdersAdvertiser(authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaOrder/GetAllAdvertiserForPostOrder`, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getProductionStatus(authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(`${environment.ApiBaseURL}/ProductionStatus/Completed`, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getAdvertiserDetail_ByAdvertiserId(websiteRoot, authToken,advertiserId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(websiteRoot + 'api/Party?limit=500&Id=eq:'+advertiserId, { headers: this.headers_token });    
    } catch (error) {
        console.log(error);
    }
  }

  getDataForPostOrdersDropDown(data, authToken): Observable<any> {
    try {
      const body = JSON.stringify(data);
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.post(`${environment.ApiBaseURL}/MediaOrder/FillDropdownForPostOrder`, body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getAllOrdersProductionStatusdWise(data, dataTablesParameters, authToken): Observable<any> {
    try {
      const form_data = new FormData();

      // tslint:disable-next-line:forin
      for (const key in dataTablesParameters) {
        form_data.append(key, dataTablesParameters[key]);
      }

      if (data['productionstatusId'] === null || data['productionstatusId'] === undefined) {
        form_data.append('productionstatusId', '');
      } else {
        form_data.append('productionstatusId', data['productionstatusId']);
      }


      if (data['MediaAssetId'] === null || data['MediaAssetId'] === undefined) {
        form_data.append('MediaAssetId', '');
      } else {
        form_data.append('MediaAssetId', data['MediaAssetId']);
      }

      if (data['IssueDateId'] === null || data['IssueDateId'] === undefined) {
        form_data.append('IssueDateId', '');
      } else {
        form_data.append('IssueDateId', data['IssueDateId']);
      }


      this.headers_token = new HttpHeaders({
        // 'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.post(`${environment.ApiBaseURL}/MediaOrder/GetOrdersByProductionStatus`, form_data, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  deleteMediaOrderLinesbyMediaOrder(morderId, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.delete(`${environment.ApiBaseURL}/MediaOrder` + morderId, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

}
