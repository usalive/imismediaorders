import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class DashboardOrderService {
    public headers_token: HttpHeaders;
    public sfHeaders: HttpHeaders;
    constructor(private http: HttpClient) {
        this.sfHeaders = new HttpHeaders({
            RequestVerificationToken: 'token'
        });
    }

    // Get Media Asset List of data [PAGING WISE]
    getOrdersBuyIDWise(dataTablesParameters, filterOptionsdata): Observable<any> {
        const form_data = new FormData();

        // tslint:disable-next-line:forin
        for (const key in dataTablesParameters) {
            form_data.append(key, dataTablesParameters[key]);
        }
        console.log("getordersbuyidwise:"+ filterOptionsdata);
        // tslint:disable-next-line:forin
        for (const key in filterOptionsdata) {
            console.log("key:"+ key);
            if (key === 'AdvertiserName') {
                form_data.append('AdvertiserName[FieldName]', filterOptionsdata[key]['FieldName']);
                form_data.append('AdvertiserName[ColumnNo]', filterOptionsdata[key]['ColumnNo']);
                form_data.append('AdvertiserName[FilterOption][FilterIsArray]', filterOptionsdata[key]['FilterOption']['FilterIsArray']);

                const l = filterOptionsdata[key]['FilterOption']['Ids'];

                if (l.length > 0) {
                    let i = 0;
                    for (i = 0; i < l.length; i++) {
                        form_data.append('AdvertiserName[FilterOption][Ids][]', filterOptionsdata[key]['FilterOption']['Ids'][i]);
                    }
                }
            }

            if (key === 'AdvertiserId') {
                form_data.append('AdvertiserId[FieldName]', filterOptionsdata[key]['FieldName']);
                form_data.append('AdvertiserId[ColumnNo]', filterOptionsdata[key]['ColumnNo']);
                form_data.append('AdvertiserId[FilterOption][FilterIsArray]', filterOptionsdata[key]['FilterOption']['FilterIsArray']);


                const l = filterOptionsdata[key]['FilterOption']['Ids'];
                if (l.toString().length > 0) {
                    form_data.append('AdvertiserId[FilterOption][Ids][]', filterOptionsdata[key]['FilterOption']['Ids']);
                }
            }

            if (key === 'ST_IDName') {
                form_data.append('ST_IDName[FieldName]', filterOptionsdata[key]['FieldName']);
                form_data.append('ST_IDName[ColumnNo]', filterOptionsdata[key]['ColumnNo']);
                form_data.append('ST_IDName[FilterOption][FilterIsArray]', filterOptionsdata[key]['FilterOption']['FilterIsArray']);

                const l = filterOptionsdata[key]['FilterOption']['Ids'];

                if (l.length > 0) {
                    let i = 0;
                    for (i = 0; i < l.length; i++) {
                        form_data.append('ST_IDName[FilterOption][Ids][]', filterOptionsdata[key]['FilterOption']['Ids'][i]);
                    }
                }
            }

            if (key === 'ST_ID') {
                form_data.append('ST_ID[FieldName]', filterOptionsdata[key]['FieldName']);
                form_data.append('ST_ID[ColumnNo]', filterOptionsdata[key]['ColumnNo']);
                form_data.append('ST_ID[FilterOption][FilterIsArray]', filterOptionsdata[key]['FilterOption']['FilterIsArray']);

                const l = filterOptionsdata[key]['FilterOption']['Ids'];

                if (l.toString().length > 0) {
                    form_data.append('ST_ID[FilterOption][Ids][]', filterOptionsdata[key]['FilterOption']['Ids']);
                }
            }

            if (key === 'BuyId') {
                form_data.append('BuyId[FieldName]', filterOptionsdata[key]['FieldName']);
                form_data.append('BuyId[ColumnNo]', filterOptionsdata[key]['ColumnNo']);
                form_data.append('BuyId[FilterOption][FilterIsNumber]', filterOptionsdata[key]['FilterOption']['FilterIsNumber']);
                form_data.append('BuyId[FilterOption][Type]', filterOptionsdata[key]['FilterOption']['Type']);
                form_data.append('BuyId[FilterOption][value]', filterOptionsdata[key]['FilterOption']['value']);
            }

            if (key === 'OrderStatus') {
                form_data.append('OrderStatus[FieldName]', filterOptionsdata[key]['FieldName']);
                form_data.append('OrderStatus[ColumnNo]', filterOptionsdata[key]['ColumnNo']);
                form_data.append('OrderStatus[FilterOption][Type]', filterOptionsdata[key]['FilterOption']['Type']);
                form_data.append('OrderStatus[FilterOption][value]', filterOptionsdata[key]['FilterOption']['value']);
            }

            if (key === 'GrossCost') {
                form_data.append('GrossCost[FieldName]', filterOptionsdata[key]['FieldName']);
                form_data.append('GrossCost[ColumnNo]', filterOptionsdata[key]['ColumnNo']);
                form_data.append('GrossCost[FilterOption][FilterIsNumber]', filterOptionsdata[key]['FilterOption']['FilterIsNumber']);
                form_data.append('GrossCost[FilterOption][Type]', filterOptionsdata[key]['FilterOption']['Type']);
                form_data.append('GrossCost[FilterOption][value]', filterOptionsdata[key]['FilterOption']['value']);
            }

            if (key === 'MediaAsset') {
                form_data.append('NetCost[FieldName]', filterOptionsdata[key]['FieldName']);
                form_data.append('NetCost[ColumnNo]', filterOptionsdata[key]['ColumnNo']);
                form_data.append('NetCost[FilterOption][FilterIsNumber]', filterOptionsdata[key]['FilterOption']['FilterIsNumber']);
                form_data.append('NetCost[FilterOption][Type]', filterOptionsdata[key]['FilterOption']['Type']);
                form_data.append('NetCost[FilterOption][value]', filterOptionsdata[key]['FilterOption']['value']);
            }

            if (key === 'FlightStartDate') {
                form_data.append('FlightStartDate[FieldName]', filterOptionsdata[key]['FieldName']);
                form_data.append('FlightStartDate[ColumnNo]', filterOptionsdata[key]['ColumnNo']);
                form_data.append('FlightStartDate[FilterOption][FilterIsDate]', filterOptionsdata[key]['FilterOption']['FilterIsDate']);
                form_data.append('FlightStartDate[FilterOption][FromDate]', filterOptionsdata[key]['FilterOption']['ToDate']);
                form_data.append('FlightStartDate[FilterOption][ToDate]', filterOptionsdata[key]['FilterOption']['FromDate']);
            }

            if (key === 'FlightEndDate') {
                form_data.append('FlightEndDate[FieldName]', filterOptionsdata[key]['FieldName']);
                form_data.append('FlightEndDate[ColumnNo]', filterOptionsdata[key]['ColumnNo']);
                form_data.append('FlightEndDate[FilterOption][FilterIsDate]', filterOptionsdata[key]['FilterOption']['FilterIsDate']);
                form_data.append('FlightEndDate[FilterOption][ToDate]', filterOptionsdata[key]['FilterOption']['ToDate']);
                form_data.append('FlightEndDate[FilterOption][FromDate]', filterOptionsdata[key]['FilterOption']['FromDate']);
            }
        }
        return this.http.post(`${environment.ApiBaseURL}/MediaOrder/GetOrdersBuyIdWise`, form_data);
    }

    getOrdersbuyIdwiseforcopy(byID, authToken): Observable<any> {
        try {
            this.headers_token = new HttpHeaders({
                'Content-Type': 'application/json',
                'RequestVerificationToken': authToken
            });
            return this.http.get(`${environment.ApiBaseURL}/MediaOrder/GetOrdersBuyIdWiseForCopy/` + byID, { headers: this.headers_token });
        } catch (error) {
            console.log(error);
        }
    }

    postOrderforcopy(MediaOrderData): Observable<any> {
        try {
            return this.http.post(`${environment.ApiBaseURL}/MediaOrder/PostOrderForCopy`, MediaOrderData, { headers: this.sfHeaders });
        } catch (error) {
            console.log(error);
        }
    }
}
