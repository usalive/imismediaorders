import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostOrdersService {
  public headers_token: HttpHeaders;
  constructor(private http: HttpClient) { }

  getAllMediaAsset(): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': 'token'
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaAsset`,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getByMediaAsset(mediaAssetId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': 'token'
      });
      return this.http.get(`${environment.ApiBaseURL}/IssueDate/GetByMediaAsset` + mediaAssetId,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getMediaOrdersAdType(authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(`${environment.ApiBaseURL}/AdType/GetAllAdTypeForPostOrder`,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getAllIssuedate(): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': 'token'
      });
      return this.http.get(`${environment.ApiBaseURL}/IssueDate`,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getAllAdvertiser1(websiteRoot, authToken, Offset): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(websiteRoot + 'api/Party?limit=500&Offset=' + Offset,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getMediaOrdersAdvertiser(authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaOrder/GetAllAdvertiserForPostOrder`,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getDataForPostOrdersDropDown(data, authToken): Observable<any> {
    try {

      const body = JSON.stringify(data);

      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.post(`${environment.ApiBaseURL}/MediaOrder/FillDropdownForPostOrder`, body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getOrdersforpostorder(dataTablesParameters, filterOptionsdata): Observable<any> {
    try {
      const form_data = new FormData();

      // tslint:disable-next-line:forin
      for (const key in dataTablesParameters) {
        form_data.append(key, dataTablesParameters[key]);
      }

      // tslint:disable-next-line:forin
      for (const key in filterOptionsdata) {

        if (filterOptionsdata[key] === undefined || filterOptionsdata[key] === null || filterOptionsdata[key] === '') {
          form_data.append(key, '');
        } else {
          form_data.append(key, filterOptionsdata[key]);
        }
        console.log(key);
      }
     const body = JSON.stringify(form_data);
     console.log("formdata:"+ body);
      return this.http.post(`${environment.ApiBaseURL}/MediaOrder/GetOrdersForPostOrder`, form_data);
    } catch (error) {
      console.log(error);
    }
  }




  getPartyById(websiteRoot, authToken, partyId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(websiteRoot + 'api/Party/' + partyId, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getMediaOrderLinesbyOrderId(orderId, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(`${environment.ApiBaseURL}/MediaOrderLines/GetByMediaOrder/` + orderId,
        { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  saveComboOrder(websiteRoot, authToken, objComboOrder): Observable<any> {
    try {

      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      const body = JSON.stringify(objComboOrder);
      return this.http.post(websiteRoot + 'api/ComboOrder', body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  setIsFrozenTrue(data, authToken): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      const body = JSON.stringify(data);
      return this.http.post(`${environment.ApiBaseURL}/MediaOrder/UpdateIsFrozenTrue`, body, { headers: this.headers_token });
    } catch (error) {
      console.log(error);
    }
  }

  getAdvertiserDetail_ByAdvertiserId(websiteRoot, authToken,advertiserId): Observable<any> {
    try {
      this.headers_token = new HttpHeaders({
        'Content-Type': 'application/json',
        'RequestVerificationToken': authToken
      });
      return this.http.get(websiteRoot + 'api/Party?limit=500&Id=eq:'+advertiserId, { headers: this.headers_token });    
    } catch (error) {
        console.log(error);
    }
  }
}
