import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  apiBaseURL = environment.ApiBaseURL;
  mediaOrder_Api = this.apiBaseURL + '/MediaOrder';
  reps_API = this.apiBaseURL + '/Reps';
  repTerritories_API = this.apiBaseURL + '/RepTerritories';

  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  public getheaders: HttpHeaders;
  authToken = environment.token;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });

    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'RequestVerificationToken': this.authToken
    });

    this.getheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
  }

  generateBuyId(): Observable<any> {
    return this.http.get(this.apiBaseURL + '/MediaOrder/GenerateBuyId', { headers: this.sfheaders });
  }

  getOrdersByBuyId(buyId): Observable<any> {
    return this.http.get(this.mediaOrder_Api + '/GetByBuyId/' + buyId, { headers: this.sfheaders });
  }

  getAllAdvertiser1(websiteRoot, Offset): Observable<any> {
    return this.http.get(websiteRoot + '/api/Party?limit=500&Offset=' + Offset, { headers: this.getheaders });
  }

  getCountry(websiteRoot, Offset): Observable<any> {
    return this.http.get(websiteRoot + '/api/country?limit=500&Offset=' + Offset, { headers: this.getheaders });
  }

  getAllReps(): Observable<any> {
    return this.http.get(this.reps_API);
  }

  getRepsForOrderCreation(): Observable<any> {
    return this.http.get(this.reps_API + '/GetRepsForOrderCreation');
  }

  saveOrganization(websiteRoot, advertiserObject): Observable<any> {
    const body = JSON.stringify(advertiserObject);
    return this.http.post(websiteRoot + 'api/Party', body, { headers: this.sfheaders });
  }

  saveContact(websiteRoot, contact): Observable<any> {
    const body = JSON.stringify(contact);
    return this.http.post(websiteRoot + 'api/Party', body, { headers: this.sfheaders });
  }

  getAllTerritories(id): Observable<any> {
    return this.http.get(this.repTerritories_API + '/GetByRep/' + id);
  }

  getOrganisationById(websiteRoot, id): Observable<any> {
    return this.http.get(websiteRoot + 'api/Party/' + id, { headers: this.sfheaders });
  }

  getAdvertiserAgencyMapping_AgencyList(websiteRoot, advertiserId): Observable<any> {
    return this.http.get(this.apiBaseURL + '/advertiseragencymapping/List/' + advertiserId, { headers: this.getheaders });
  }

  getAdvertiserRepTerritoryMapping_AgencyList(websiteRoot, advertiserId): Observable<any> {
    return this.http.get(this.apiBaseURL + '/advertiserrepterritorymapping/List/' + advertiserId, { headers: this.getheaders });
  }

  getAdvertiserAgency_BillToContact(websiteRoot, Offset,advertiserorAgencyName,filterValue): Observable<any> {
    try {
      if(advertiserorAgencyName.includes('&'))
      {
        let arrAdvertiserorAgencyName = advertiserorAgencyName.split("&");
        if(arrAdvertiserorAgencyName.length>0)
          advertiserorAgencyName = arrAdvertiserorAgencyName[0];
      }
      console.log("advertiserorAgencyName: "+ advertiserorAgencyName);
        if(filterValue==="")
            return this.http.get(websiteRoot + 'api/Party?limit=500&OrganizationName=contains:'+advertiserorAgencyName+'&Offset=' + Offset, { headers: this.getheaders });
        else
            return this.http.get(websiteRoot + 'api/Party?limit=500&OrganizationName=contains:'+advertiserorAgencyName+'&Name=contains:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
    } catch (error) {
        console.log(error);
    }
  }

  getAdvertiserByFilterSearch(websiteRoot, Offset,filterOption,filterValue): Observable<any> {
    try{
      if(filterOption==="All")
      {
        return this.http.get(websiteRoot + '/api/Party?limit=500&Offset=' + Offset, { headers: this.getheaders });
      }
      else{
        if(filterOption==="Equals")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=eq:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        }
        if(filterOption==="Contains")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=contains:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        }
        if(filterOption==="StartsWith")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=startsWith:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        } 
        if(filterOption==="EndsWith")
        {
          return this.http.get(websiteRoot + '/api/Party?limit=500&OrganizationName=endsWith:'+filterValue+'&Offset=' + Offset, { headers: this.getheaders });
        }
      }
    }
    catch(error)
    {
      console.log("getAdvertiserByFilterSearch:"+error);
    }
    
  }

}
