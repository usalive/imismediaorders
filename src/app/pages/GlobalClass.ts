import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
@Injectable()

export class DataTablesResponse {
    data: any[];
    draw: number;
    recordsFiltered: number;
    recordsTotal: number;
}

export class GlobalClass {

    mainPartyAndOrganization = [];
    mainAdvertisers = [];
    mainAgencies = [];
    mainBillingToContacts = [];
    dashboardAdvertiserIds=[];
    repTerritories = [];

    dashboardOrganizationId: any;
    dashboardAgencyId: any;
    dashboardContactId: any;
    fromDashboard = false;
    preOrderFill = false;
    fromPreviousTab = false;
    fromProductionTab = false;
    orderId: any;
    grossCost = (0).toFixed(2);
    netCost = (0).toFixed(2);
    billingDetailsMediaOrderid = '';
    advertiserID: any;

    billingDetails: any = {};
    mainArray: any = [];
    detailsMediaOrder: '';
    productCode: any;
    billingMethodId: any;
    mediaAssetId: any;
    mediaAssetName: any;
    obj: any = {};
    mediaAsset: any = [];

    isFrozen: boolean = false;

    finalGrossAmount = 0;
    finalNetAmount = 0;

    constructor() { }

    getRepTerritories()
    {
        return this.repTerritories;
    }

    setRepTerritories(repTerritories)
    {
        this.repTerritories= repTerritories;
    }

    // --------------------------------------------------------

    getbillingDetailsMediaOrderid() {
        return this.billingDetailsMediaOrderid;
    }

    setbillingDetailsMediaOrderid(billingDetailsMediaOrderid) {
        this.billingDetailsMediaOrderid = billingDetailsMediaOrderid;
    }

    removeDetailsMediaOrderid() {
        this.billingDetailsMediaOrderid = null;
    }

    // --------------------------------------------------------

    getDetailsMediaOrder() {
        return this.detailsMediaOrder;
    }

    setDetailsMediaOrder(DetailsMediaOrder) {
        this.detailsMediaOrder = DetailsMediaOrder;
    }

    removeDetailsMediaOrder() {
        this.detailsMediaOrder = null;
    }

    // --------------------------------------------------------
    getDashboardOrganizationId() {
        return this.dashboardOrganizationId;
    }

    getDashboardAgencyId() {
        return this.dashboardAgencyId;
    }

    getbillingDetails(): any {
        return this.billingDetails;
    }

    getDashboardContactId() {
        return this.dashboardContactId;
    }

    getFromPreviousTab()
    {
        return this.fromPreviousTab;
    }

    getFromProductionTab()
    {
        return this.fromProductionTab;
    }

    getFromDashboard() {
        return this.fromDashboard;
    }

    getPreOrderFill() {
        return this.preOrderFill;
    }

    getOrderId() {
        return this.orderId;
    }

    getAdvertiserID() {
        return this.advertiserID;
    }

    getMainArray(): any {
        return this.mainArray;
    }


    getBillingDetailsMediaOrderId() {
        return this.billingDetailsMediaOrderid;
    }

    getProductCode() {
        return this.productCode;
    }

    getBillingMethodID() {
        return this.billingMethodId;
    }

    // --------------------------------------------------------

    setDashboardOrganizationId(dashboardOrganizationId) {
        this.dashboardOrganizationId = dashboardOrganizationId;
    }

    setDashboardAgencyId(dashboardAgencyId) {
        this.dashboardAgencyId = dashboardAgencyId;
    }

    setDashboardContactId(dashboardContactId) {
        this.dashboardContactId = dashboardContactId;
    }

    setFromPreviousTab(previousTab)
    {
        this.fromPreviousTab=previousTab;
    }

    setFromProductionTab(productionTab)
    {
        this.fromProductionTab=productionTab;
    }

    setFromDashboard(fromDashboard) {
        this.fromDashboard = fromDashboard;
    }

    setPreOrderFill(preOrderFill) {
        this.preOrderFill = preOrderFill;
    }

    setOrderId(OrderId) {
        this.orderId = OrderId;
    }

    setbillingDetails(billingDetails) {
        this.billingDetails = billingDetails;
    }

    setAdvertiserID(AdvertiserID) {
        this.advertiserID = AdvertiserID;
    }

    setMainArray(mainArray): any {
        this.mainArray = mainArray;
    }

    setBillingDetailsMediaOrderId(MediaOrderId) {
        this.billingDetailsMediaOrderid = MediaOrderId;
    }

    setProductCode(ProductCode) {
        this.productCode = ProductCode;
    }

    setBillingMethodID(billingMethodId) {
        this.billingMethodId = billingMethodId;
    }

    getIsFrozen() {
        return this.isFrozen;
    }

    setIsFrozen(IsFriz) {
        this.isFrozen = IsFriz;
    }

    removeIsFrozen() {
        this.isFrozen = false;
    }

    getfinalGrossAmount() {
        return this.finalGrossAmount;
    }

    setfinalGrossAmount(finalGrossAmount) {
        this.finalGrossAmount = finalGrossAmount;
    }

    getfinalNetAmount() {
        return this.finalNetAmount;
    }

    setfinalNetAmount(finalNetAmount) {
        this.finalNetAmount = finalNetAmount;
    }


    removeAdvertiserID() {
        this.advertiserID = null;
    }

    removeOrderId() {
        this.orderId = null;
    }

    removeBillingDetails() {
        this.billingDetails = {};
    }


    // --------------------------------------------------------
    getMainPartyAndOrganizationData(): any {
        return this.mainPartyAndOrganization;
    }

    setMainPartyAndOrganizationData(MainPartyAndOrganizationData) {
        this.mainPartyAndOrganization = MainPartyAndOrganizationData;
    }

    removeMainPartyAndOrganizationData() {
        this.mainPartyAndOrganization = [];
    }

    /*----------------------------------------------------------*/

    getMainAdvertisersData(): any {
        return this.mainAdvertisers;
    }

    setMainAdvertisersData(MainAdvertisers) {
        this.mainAdvertisers = MainAdvertisers;
    }

    removeMainAdvertisersData() {
        this.mainAdvertisers = [];
    }

    /*----------------------------------------------------------*/

    getMainAgenciesData(): any {
        return this.mainAgencies;
    }

    setMainAgenciesData(MainAgencies) {
        this.mainAgencies = MainAgencies;
    }

    removeMainAgenciesData() {
        this.mainAgencies = [];
    }

    /*----------------------------------------------------------*/

    getDashboardAdvertiserIDs():any{
        return this.dashboardAdvertiserIds;
    }

    setDashboardAdvertiserIDs(advertiserIds){
        this.dashboardAdvertiserIds = advertiserIds;
    }

    getMainBillingToContactsData(): any {
        return this.mainBillingToContacts;
    }

    setMainBillingToContactsData(MainBillingToContacts) {
        this.mainBillingToContacts = MainBillingToContacts;
    }

    removeMainBillingToContactsData() {
        this.mainBillingToContacts = [];
    }

    getDate(dateTime) {
        const coverDate_yy = new Date(dateTime).getFullYear();
        const coverDate_mm = new Date(dateTime).getMonth() + 1;
        const coverDate_dd = new Date(dateTime).getDate();
        const coverDate = coverDate_mm + '/' + coverDate_dd + '/' + coverDate_yy;
        return coverDate;
    }

    getFromDate(dateTime) {
        const coverDate_yy = new Date(dateTime).getFullYear();
        const coverDate_mm = new Date(dateTime).getMonth() + 12;
        const coverDate_dd = new Date(dateTime).getDate();
        const coverDate = coverDate_mm + '/' + coverDate_dd + '/' + coverDate_yy;
        return coverDate;
    }

    getDateInFormat(date) {

        let d, m;
        try {
            const dateObj = new Date(date);

            m = dateObj.getMonth() + 1;

            if (m.toString().length === 1) {
                m = '0' + m;
            }

            d = dateObj.getDate();

            if (d.toString().length === 1) {
                d = '0' + d;
            }

            const year = dateObj.getFullYear();
            const newdate = m + '/' + d + '/' + year;
            return newdate;
        } catch (error) {
            console.log(error);
        }
    }

    getTwoValDate(dateTime) {
        const coverDate_yy = new Date(dateTime).getFullYear();
        let coverDate_mm = (new Date(dateTime).getMonth() + 1).toString();
        let coverDate_dd = (new Date(dateTime).getDate()).toString();

        const tempMM = coverDate_mm.toString();
        if (tempMM !== null || tempMM !== undefined || tempMM !== 'undefined') {
            if (tempMM.length === 1) {
                coverDate_mm = '0' + coverDate_mm;
            }
        }

        const tempDD = coverDate_dd.toString();
        if (tempDD !== null || tempDD !== undefined || tempDD !== 'undefined') {
            if (tempDD.length === 1) {
                coverDate_dd = '0' + coverDate_dd;
            }
        }

        const coverDate = coverDate_mm + '/' + coverDate_dd + '/' + coverDate_yy;
        return coverDate;
    }

    // --------------------------------------------------------
    getGrossCost(): any {
        return this.grossCost;
    }
    setGrossCost(GrossCost) {
        this.grossCost = GrossCost;
    }

    getNetCost(): any {
        return this.netCost;
    }
    setNetCost(NetCost) {
        this.netCost = NetCost;
    }
    setMediaAssets(mediaAssetId) {
        this.mediaAssetId = mediaAssetId;
    }
    getMediaAssets() {
        return this.mediaAssetId;
    }
    // --------------------------------------------------------

    isEmpty(value) {
        if (!isNullOrUndefined(value)) {
            if (value.toLowerCase() == 'null' || value.toLowerCase() == 'undefined') {
                return '';
            } else {
                return value;
            }
        } else {
            return '';
        }
    }
}
