import { Component, OnInit, HostListener } from '@angular/core';
import { environment } from '../environments/environment';
import { GlobalClass } from '../app/pages/GlobalClass';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'asi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  orderId: number;

  t1: any = null;
  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  currentUserName = '';

  advertisersTab = false;
  ordersTab = false;
  postOrdersTab = false;
  missingMaterialsTab = false;

  advertisersDisable = false;
  ordersDisable = false;
  postOrdersDisable = false;
  missingMaterialsDisable = false;

  showMainPage = true;
  showOrderFormPage = false;

  isLoading = true;

  constructor(private globalClass: GlobalClass, private cookieService: CookieService) {
  }

  ngOnInit() {
    try {
      const url = window.location.href;
      if (url.search('Advertisers') > 1) {
        this.setAdvertisersTabActive();
      } else if (url.search('dashBoard%2FOrders') > 1) {
        if (url.lastIndexOf('dashBoard%2FOrders%2F') > 1) {
          // From dash board with id
          this.setOrdersTabActive(true);
        } else {
          // Normal conditon
          this.setOrdersTabActive();
        }

      } else if (url.search('PostOrders') > 1) {
        this.setPostOrdersTabActive();
      } else if (url.search('MissingMaterials') > 1) {
        this.setMissingMaterialsTabActive();
      } else if (url.search('ordersDetails') > 1) {
        this.redirectToMediaOrder();
      } else {
        this.setAdvertisersTabActive();
      }
    } catch (error) {
      console.log(error);
    }

    this.setTokenData();
  }

  setTokenData() {
    try {
      this.currentUserName = environment.CurrentUserName;
      this.baseUrl = environment.baseUrl;
   
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }
  }

  setAdvertisersTabActive() {
    try {
      let url = window.location.href;
      if (url.indexOf('#') > -1) {
        url = url.substring(0, url.lastIndexOf('#'));
       
      }
      location.replace(url + '#/dashBoard/Advertisers');
    } catch (error) {
      console.log(error);
    }
    this.advertisersTab = true;
    this.ordersTab = false;
    this.postOrdersTab = false;
    this.missingMaterialsTab = false;

    this.advertisersDisable = false;
    this.ordersDisable = false;
    this.postOrdersDisable = false;
    this.missingMaterialsDisable = false;
  }

  setOrdersTabActive(isFromDashboard = false) {

    if (isFromDashboard) {
      try {
        let url = window.location.href;
        const t = url.substring(url.lastIndexOf('Orders%2F'));
        this.t1 = t.substring(t.lastIndexOf('%2F') + 3);
        if (url.indexOf('#') > -1) {
          url = url.substring(0, url.lastIndexOf('#'));
        }
        location.replace(url + '#/dashBoard/' + t);
      } catch (error) {
        console.log(error);
      }
    }
    else {
      try {
        let url = window.location.href;
        if (url.indexOf('#') > -1) {
          url = url.substring(0, url.lastIndexOf('#'));
         
        }
        location.replace(url + '#/dashBoard/Orders');
      } catch (error) {
        console.log(error);
      }
    }

    this.globalClass.removeAdvertiserID();
    this.globalClass.setDashboardAdvertiserIDs(this.globalClass.dashboardAdvertiserIds);
    this.advertisersTab = false;
    this.ordersTab = true;
    this.postOrdersTab = false;
    this.missingMaterialsTab = false;

    this.advertisersDisable = false;
    this.ordersDisable = false;
    this.postOrdersDisable = false;
    this.missingMaterialsDisable = false;
  }

  setPostOrdersTabActive() {
    try {
      let url = window.location.href;
      if (url.indexOf('#') > -1) {
        url = url.substring(0, url.lastIndexOf('#'));
       
      }
      location.replace(url + '#/dashBoard/PostOrders');
    } catch (error) {
      console.log(error);
    }
    this.advertisersTab = false;
    this.ordersTab = false;
    this.postOrdersTab = true;
    this.missingMaterialsTab = false;

    this.advertisersDisable = false;
    this.ordersDisable = false;
    this.postOrdersDisable = false;
    this.missingMaterialsDisable = false;
  }

  setMissingMaterialsTabActive() {
    try {
      let url = window.location.href;
      if (url.indexOf('#') > -1) {
        url = url.substring(0, url.lastIndexOf('#'));
       
      }
      location.replace(url + '#/dashBoard/MissingMaterials');
    } catch (error) {
      console.log(error);
    }
    this.advertisersTab = false;
    this.ordersTab = false;
    this.postOrdersTab = false;
    this.missingMaterialsTab = true;

    this.advertisersDisable = false;
    this.ordersDisable = false;
    this.postOrdersDisable = false;
    this.missingMaterialsDisable = false;
  }


  // redirtect to media order Form (dashboard ipart and also for orser ipart)
  redirectToMediaOrder() {
    try {
      const orderId = sessionStorage.getItem('OrderId');
      if (orderId == '' || orderId == null || orderId == undefined) {
        sessionStorage.removeItem('OrderId');
      } else {
        this.orderId = parseInt(orderId);
        sessionStorage.removeItem('OrderId');
      }
      let url = window.location.href;
      if (url.indexOf('#') > -1) {
        url = url.substring(0, url.lastIndexOf('#'));
       
      }
      location.replace(url + '#/ordersDetails');
    } catch (error) {
      console.log(error);
    }
    this.globalClass.setFromDashboard(false);
    this.globalClass.setPreOrderFill(false);
    this.globalClass.setDashboardOrganizationId(0);
    this.globalClass.setDashboardAgencyId(0);
    this.globalClass.setOrderId('');
    this.globalClass.setbillingDetailsMediaOrderid('');
    this.globalClass.removeMainPartyAndOrganizationData();
    this.globalClass.removeMainAdvertisersData();
    this.globalClass.removeMainAgenciesData();
    this.globalClass.removeMainBillingToContactsData();
    this.showMainPage = false;
    this.showOrderFormPage = true;
  }

  redirectBackToOrders() {
    this.showMainPage = true;
    this.showOrderFormPage = false;
  }
}
